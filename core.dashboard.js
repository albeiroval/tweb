/*
*  CLASS : TTreeView para usar con core.dashboard.php
*  AUTOR : Albeiro Valencia
*  FECHA : 07/11/2021 
*/

var TTreeView = function() {

  var btnShow     = true;
  var itemID      = null;

  var Home_ID     = null;
  var Home_LEFT   = 0;

  var panel_ID    = null;
  var panel_FOCUS = false;
  var panel_LEFT  = 0;

  this.init = function() {

    $('#tree').bstreeview({ 
      data: tree,
      expandIcon: 'fa fa-angle-down fa-fw',
      collapseIcon: 'fa fa-angle-right fa-fw',
      indent: 1.25,
      parentsMarginLeft: '1.25rem',
      openNodeLinkOnNewTab: false,
      highlightSelected : true,
      onNodeSelected: function(event, data) {
        console.log("selected node", event, data);
      }  
    });

    this.initActions();
    this.colorFocus();
    this.hideAllPanels();
   
  };

  this.initActions = function() {

    for (var i = 0; i < aItems.length; i++) {
      var item    = aItems[i];
      var onclick;
      if ( item.type == "JS" ) {
        onclick = item.action + ";oMenu.hidePanel();";
      } else if ( item.type == "PHP" ) {  
        onclick = "oMenu.showPanel('" + item.action + "');oMenu.toogle();";
      } 
      document.getElementById(item.id).setAttribute("onclick", onclick );  
    }

  };

  this.colorFocus = function() {

    $('#tree').on("click", ".list-group-item", function() { 
      if ( itemID != null ) {
         $(itemID).removeClass('active');  
      }
      itemID = "#" + this.id;
      $(this).addClass('active');
    });

  };

  this.toogle = function() {

    var menu = document.getElementById("side-bar");
  	if (menu.style.display === "none") {
      menu.style.display = "block";
  	} else {
      menu.style.display = "none";
 	  }
    
    if ( btnShow ) {

      btnShow = false;
      document.getElementById("toogle-btn").style.left = "5px"; 

      if ( panel_FOCUS ) {
        let element = document.getElementById(panel_ID);
        if (element) {
          panel_LEFT = element.style.left;
          element.style.left = "45px"; 
        }  
      }

      if ( Home_ID != panel_ID ) {
        let element = document.getElementById(Home_ID);
        if (element) {
          Home_LEFT = element.style.left;
          element.style.left = "45px"; 
        }  
      }

    } else {

      btnShow = true;
      document.getElementById("toogle-btn").style.left = btnLeftPos; 

      if ( panel_FOCUS ) {
        let element = document.getElementById(panel_ID);
        if (element) {
          element.style.left = panel_LEFT; 
        }  
      }

      if ( Home_ID != panel_ID ) {
        let element = document.getElementById(Home_ID);
        if (element) {
          element.style.left = Home_LEFT; 
        }  
      }  

    }

	};

  this.hideAllPanels = function() {

    for (var i = 0; i < aItems.length; i++) {
      var item = aItems[i];
      if (item.type == "PHP") {
        if ( item.home == false ) {
          document.getElementById(item.action).style.display = "none";
        } else {
          Home_ID = item.action;
        }  
      }   
    }  

  };

  this.showPanel = function(cId) {
    
    if ( panel_ID != null ) {
      let elem = document.getElementById(panel_ID);
      if (elem) {
        elem.style.display = "none";  
      }
    }

    // asigna el id y activa el panel
    panel_ID    = cId;
    panel_FOCUS = true;
    document.getElementById(cId).style.display = "block";

    if ( Home_ID  != cId ) {
      let elem = document.getElementById(Home_ID);
      if ( elem ) {
        elem.style.display = "none";  
      }  
    }  

  };  

  this.hidePanel = function() {
    
    if ( panel_ID != null ) {
      document.getElementById(panel_ID).style.display = "none";  
    }

    panel_FOCUS = false;

  };

  this.footerCss = function ( idFooter, classFooter, idMain ) {

    // https://stackoverflow.com/questions/15615552/get-div-height-with-plain-javascript?rq=1
    let hMain   = document.getElementById(idMain).clientHeight;
    if ( document.getElementById(idFooter) ) {
      
      let hFooter = document.getElementById(idFooter).clientHeight;
      let nTop = hMain - hFooter;
      let elem = document.getElementById(idFooter);
      elem.style.top = nTop;

      let style = elem.getAttribute("style");
      style += "display : flex;justify-content: space-between;";
      elem.setAttribute("style", style);
      
    }  

    let obj = new TDisplay();
    if ( obj.getWidth() <= 768 ) {
      obj.setClass( idFooter, classFooter );
    }

  };
  
};
