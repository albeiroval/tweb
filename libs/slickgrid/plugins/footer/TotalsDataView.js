/**
 * A data provider that caclulates a total of all values for any column that has a hasTotal flag set to true.
 * @param {Slick.Data.DataView || []} data A slick grid data view or an array of data
 * @param {Object} columns The column definitions
 * @constructor
 */
function TotalsDataView(data, columns) {
  var totals = {};
  var totalsMetadata = {
    cssClasses: 'total-spacer-row',
    columns: {}
  };

  // Make the totals not editable.
  for (var i = 0; i < columns.length; i++) {
    totalsMetadata.columns[i] = {
      editor: null
    };
  }

  var self = this;

  if (data.onRowCountChanged) {
    data.onRowCountChanged.subscribe(function (e, args) {
      self.updateTotals();
    });
  }

  if (data.onRowsChanged) {
    data.onRowsChanged.subscribe(function (e, args) {
      self.updateTotals();
    });
  }

  this.getLength = function () {
    var length = data.getLength ? data.getLength() : data.length;
    return length + 1;
  };

  this.getItem = function (index) {
    return data.getItem ? data.getItem(index) : data[index];
  };

  this.getItemMetadata = function (index) {
    if (index == (this.getLength() - 1)) {
      return totalsMetadata;
    } else {
      return data.getItemMetadata ? data.getItemMetadata(index) : null;
    }
  };

  this.getTotals = function () {
    return totals;
  };

  this.updateTotals = function () {

    var columnIdx = columns.length;
    while (columnIdx--) {
	
      var column = columns[columnIdx];
	  
      if (!column.hasTotal) {
        continue;
      }

      var total = 0;
      var i = this.getLength() - 1;
      var dataItems = data.getItems ? data.getItems() : data;
	  
      while (i--) {
        //total += (parseInt(dataItems[i][column.field], 10) || 0);	//	CAF 190512
        total += (dataItems[i][column.field] || 0);
      }
	  
	  //	TWEB Fijar número de decimales
	  
			var nDecimals = 0 ;
		  
			if( column.editorFixedDecimalPlaces ) {
        nDecimals = column.editorFixedDecimalPlaces;
      }  

      // total = total.toFixed( nDecimals );
      total = this.num2Money(total, nDecimals); 
		
		//	-------------------------------------
			
      totals[column.id] = total;
    }
  };

  /*
  *  Function : num2Money()
  *  @return = number to string format 999,999,999.99
  */
  this.num2Money = function( number, ndec ) {
    if (typeof ndec == "undefined") {
      ndec = 2;
    }
	  var value = Number(parseFloat(number).toFixed(ndec)).toLocaleString('en', {
												minimumFractionDigits: 2
										 });
	  return value;
  };

  this.updateTotals();
}
