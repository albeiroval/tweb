/***
 * Contains basic SlickGrid formatters.
 * 
 * NOTE:  These are merely examples.  You will most likely need to implement something more
 *        robust/extensible/localizable/etc. for your use!
 * 
 * @module Formatters
 * @namespace Slick
 */

 
(function ($) {
  // register namespace
  $.extend(true, window, {
    "Slick": {
      "Formatters": {
        "PercentComplete": PercentCompleteFormatter,
        "PercentCompleteBar": PercentCompleteBarFormatter,
        "YesNo": YesNoFormatter,
        "Checkmark": CheckmarkFormatter,
		"Float": FloatFormatter,
		"Select": SelectFormatter,
		"Price": PriceFormatter
      }
    }
  });

  function PercentCompleteFormatter(row, cell, value, columnDef, dataContext) {
    if (value == null || value === "") {
      return "-";
    } else if (value < 50) {
      return "<span style='color:red;font-weight:bold;'>" + value + "%</span>";
    } else {
      return "<span style='color:green'>" + value + "%</span>";
    }
  }

  function PercentCompleteBarFormatter(row, cell, value, columnDef, dataContext) {
    if (value == null || value === "") {
      return "";
    }

    var color;

    if (value < 30) {
      color = "red";
    } else if (value < 70) {
      color = "silver";
    } else {
      color = "green";
    }

    return "<span class='percent-complete-bar' style='background:" + color + ";width:" + value + "%'></span>";
  }

  function YesNoFormatter(row, cell, value, columnDef, dataContext) {
    return value ? "Yes" : "No";
  }

  function CheckmarkFormatter(row, cell, value, columnDef, dataContext) {
    return value ? "<img src='" + TWEB_PATH_LIBS + '/slickgrid/images/tick.png' + "'>" : "";
  }
  
  function FloatFormatter(row, cell, value, columnDef, dataContext) {

	var nDecimals =  2;
	
	if ( typeof columnDef.editorFixedDecimalPlaces == 'number' ){
		nDecimals = columnDef.editorFixedDecimalPlaces ;	
	}
	
    return Number(value).toFixed( nDecimals );
  }  
  
	function SelectFormatter(row, cell, value, columnDef, dataContext) {  

		var cOptions 	= columnDef.editorOptions.options ;
		var cValues 	= columnDef.editorOptions.values ;
		
		var aKey 		= cOptions.split(','); 	// '|'
		var aValues 	= cValues.split(',');	// '|'
		
		var nPos = jQuery.inArray( value, aKey );

		if ( nPos >= 0 ) {
			return aValues[ nPos ];
		} else {
			return value;		
		}
	}
	
	function PriceFormatter(row, cell, value, columnDef, dataContext) {

		if (value == null) {
		   return "";
		   
		} else {
		  var nDecimals =  2;
		  
		  if ( typeof columnDef.editorFixedDecimalPlaces == 'number' ){
			 nDecimals = columnDef.editorFixedDecimalPlaces ;  
		  }
	

		  var nValue = value;
		  
		  if ( typeof value == 'string' ){
			 nValue = Number(value).toFixed( nDecimals )
		  } else {
			nValue = value.toFixed( nDecimals )
		  }
	
		  
		  return format2(nValue);
		}  
	}

	function format2(num){
	
		var n = num.toString(), p = n.indexOf('.');
		
	

		return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function($0, i){
			return p<0 || i<p ? ($0+',') : $0;
		});
	}
	
})(jQuery);
