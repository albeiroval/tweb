/*
*  Renderizado para crear el INPUT con TVALWEB
*  SOLO PARA USO DE TVALWEB (C)(R)
*  AUTOR : Albeiro Valencia
*  FECHA : 18/09/2021 
*/

const select = (selector, Boolean = false) => Boolean ? document.querySelectorAll(selector) : document.querySelector(selector);

/*
const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thirsday', 'Friday', 'Sunday'];
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 
                 'November', 'December'];
*/                 
/*
 *  FUNCIONES PARA USAR Input Datepicker de TVALWEB
 */

const weekdays = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
const months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 
                 'Noviembre', 'Diciembre'];

class TDatePicker {

    constructor(id) {
        const elem = document.getElementById(id);
        if (elem) {
            elem.value = setInputToDay();
            elem.onfocus = function(e) {
                new DatePicker(e.target);
            };
        }
        this.id = id;    
    }

    getvalue() {
      const value = document.getElementById(this.id).value;
      return value;
    }

    setvalue( value ) {
      document.getElementById(this.id).value = value;
    }

}

function daysInMonth (month, year) {
  return new Date(year, ++month, 0).getDate();
}

function getDay(year, month, day) {
  const d = new Date(year, month, day);
  return weekdays[d.getDay()];
}

function setInputToDay() {
  const now = new Date();
  const day = ("0" + now.getDate()).slice(-2);
  const month = ("0" + (now.getMonth() + 1)).slice(-2);
  const year  = now.getFullYear();
  const today = day+"/"+month+"/"+year;
  return today;
}

// FINAL 