/**
 * Funciones que reemplazan alert(), confirm(), prompt(), MsgCombo() 
 * EN BOOTSTRAP
 * Bootbox.js : http://bootboxjs.com/
 * Toastr : https://github.com/CodeSeven/toastr
 * Alertify : https://alertifyjs.com/
 * AUTOR : ALBEIRO VALENCIA
 * FECHA : 21/09/2021
 * TODOS LOS DERECHOS RESERVADOS
 */

 function BMsgYesNo( title, action ) { 
	bootbox.confirm({ 
    size: "small", // 'small', 'large', 'extra-large'
		message: '<h5>' + title + '</h5>',
		onEscape: false,
		centerVertical : true,
		backdrop: true,
		buttons: {
			confirm: {
				label: 'Si',
				className: 'btn-success'
			},
			cancel: {
				label: 'No',
				className: 'btn-danger'
			}
		},
    callback: function(result) { 
			if (result) {
				callfunc(action);	
			}
		}
	});
}	

//-------------------------------------

function ToastOk( message ) {
	__Notify( message, 'success', 1000 );
}

//-------------------------------------

function ToastError( message ) {
	__Notify( message, 'error', 1500 );
}

//-------------------------------------

function __Notify( message, type, milisecs ) {

	message 	= (message) ? message :'Informacion';
	type 			= (message) ? type : 'success';
	milisecs 	= (milisecs) ? milisecs : 1500;

	if ( type == 'success') {
		toastr.success(message, null, {timeOut: milisecs});
	} else {
		toastr.error(message, null, {timeOut: milisecs});
	}	

}

//-------------------------------------

function BMsgTimer(  message, type, milisecs ) {

	message 	= (message) ? message :'Informacion';
	type 			= (message) ? type : 'success';
	milisecs 	= (milisecs) ? milisecs : 1500;

	var icon; 
	if ( type == 'success' ) {
		icon = "fa fa-check";
	} else {
		icon = "fa fa-times";
	}

	window.setTimeout(function(){
    bootbox.hideAll();
	}, milisecs); // 10000 = 10 seconds expressed in milliseconds

	var html = ''; 
	html += '<div style="display: flex;width: 100%;">';
	html += '	<i class="' + icon + '" style="float: left;width: 20%;padding: 6px;margin: auto;"></i>';
	html += '	<p style="float : right;padding: 6px;margin: auto;">' + message + '</p>'; 
	html += '</div>'; 

	bootbox.dialog({ 
		message: html, 
		size : 'small',
		closeButton: false 
	}).find('.modal-dialog').css('margin', 'auto');

}

//-------------------------------------

function BMsgInfo( message, callback, argument ) {
	bootbox.alert("Your message <b>" + message + "</b>");
}

//-------------------------------------

function BMsgError( message, callback, argument ) {
  bootbox.alert("Your message <b>" + message + "</b>");
}

//-------------------------------------

function BMsgCombo( aDatos, message, bCallback ) {
  bootbox.alert("Your message <b>" + message + "</b>");
}	

//-------------------------------------

function BMsgPrompt( message, bCallback, value, placeholder ) {
  bootbox.alert("Your message <b>" + message + "</b>");
}

//-------------------------------------

function callfunc( bCallback, argument ) {
	if ( typeof bCallback == 'function' ) {	
		var fnparams = [ argument ];
		bCallback.apply(null, fnparams);	
	}
	else
		console.log('Error in function : ' + bCallback + '  Arguments : ' + argument ); 
}

// FINAL