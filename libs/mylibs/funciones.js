/*
* Class  TDisplay()
* @return = object
*/
class TDisplay {
	constructor() {

		this.getHeight = function () {
			return window.screen.availHeight;
		};

		this.getWidth = function () {
			return window.screen.availWidth;
		};

		this.setClass = function (cId, cClass) {
			var elem = document.getElementById(cId);
			console.log(cId, cClass);
			elem.classList.add(cClass);
		};

	}
}

/*
* Function SetButtonText()
* Cambia el texto del botton de un TButtom
*/
function SetButtonText( cId, cText ) {
	var element = document.getElementById(cId).getElementsByTagName('p')[0];
	element.innerHTML = cText;
} 

/*
* Function getToDay()
* @return = fecha actual para <input type="date">
*/

function getToDay() {
	var now = new Date();
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);
	var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
	return today;
}

/*
*  Function : getDate()
*  @return = fecha actual en formato string 'dd-mm-yyyy'
*/
function getDate() {
	var date  = new Date();
	var day   = date.getDate();
	var month = date.getMonth() + 1;
  var year  = date.getFullYear();
  return ( day.toString().padStart(2, "0") + '-' + 
           month.toString().padStart(2, "0") + '-' + 
           year.toString() );
}

/*
*  Function : getStrDate()
*  @return = fecha actual en formato string 'dd mm yyyy'
*/
function getStrDate() {
	var date  = new Date();
	var day   = date.getDate();
	var month = date.getMonth() + 1;
  var year  = date.getFullYear();
  return ( day.toString().padStart(2, "0") + ' de ' + 
           nameMonth(month) + ' de ' + 
           year.toString() );
}

//-------------------------------------

function nameMonth( nMont ) {
	var cMonth;
	switch (nMont) {
		case 1:
			cMonth = "Enero";
			break;
		case 2:
			cMonth = "Febrero";
			break;
		case 3:
			cMonth = "Marzo";
			break;
		case 4:
			cMonth = "Abril";
			break;
		case 5:
			cMonth = "Mayo";
			break;
		case 6:
			cMonth = "Junio";
			break;
		case 7:
			cMonth = "Julio";
			break;
		case 8:
			cMonth = "Agosto";
			break;
		case 9:
			cMonth = "Septiembre";
			break;
		case 10:
			cMonth = "Octubre";
			break;
		case 11:
			cMonth = "Noviembre";
			break;
		case 12:
			cMonth = "Diciembre";
			break;
		default:
			cMonth = "Error en Mes";
			break;
	}

	return cMonth;
} 

//-------------------------------------

/*
*  Function : getDateSQL()
*  @return = fecha actual en formato string 'dd-mm-yyyy'
*/
function getDateSQL() {
	var date  = new Date();
	var day   = date.getDate();
	var month = date.getMonth() + 1;
  var year  = date.getFullYear();
	return ( year.toString() + '-' + 
					 month.toString().padStart(2, "0") + '-' +
					 day.toString().padStart(2, "0") );
}

/*
*  Function : getFirstDateSQL()
*  @return = fecha actual en formato string 'dd-mm-yyyy'
*/
function getFirstDateSQL() {
	var date  = new Date();
	var day   = 1;
	var month = date.getMonth() + 1;
  var year  = date.getFullYear();
	return ( year.toString() + '-' + 
					 month.toString().padStart(2, "0") + '-' +
					 day.toString().padStart(2, "0") );
}

//-------------------------------------

/*
*  Function : getTime()
*  @return = hora actual en formato string 'hh:mm:ss'
*/
function getTime() {
  var date  = new Date();
  var hor   = date.getHours().toString().padStart(2, "0");
  var min   = date.getMinutes().toString().padStart(2, "0"); 
  var sec   = date.getSeconds().toString().padStart(2, "0");
  return ( hor + ':' + min + ':' + sec );
}  

//--------------------------------------

function validURL(url) {
	try {
		new URL(url);
	} catch (e) {
		return false;
	}
	return true;
}

//-------------------------------------

/*
*	padLeadingZeros(57, 3);// "057"
* padLeadingZeros(57, 4); //"0057"
*/
function padLeadingZeros(num, size) {
	var s = num+"";
	while (s.length < size) s = "0" + s;
	return s;
}

//-------------------------------------

/*
*  Function : num2Money()
*  @return = number to string format 999,999,999.99
*/

function num2Money(number) {
	var value = Number(parseFloat(number).toFixed(2)).toLocaleString('en', {
												minimumFractionDigits: 2
										 });
	return value;
}

//-------------------------------------

/* 
*	Function AddJs(str) 
*/
function AddJs(str) {
	var filesrc = document.createElement("script");
	filesrc.src = str;
	document.body.appendChild(filesrc);
}

//-------------------------------------

/* 
*	Function AddCss(str) 
*/
function AddCss(str) {
	var fileref = document.createElement("link");
	fileref.rel  = "stylesheet";
	fileref.type = "text/css";
	fileref.href = str;
	document.body.appendChild(fileref);
}	

//-------------------------------------

function getRootPathWeb() {
  var _location = document.location.toString();
  var applicationNameIndex = _location.indexOf('/', _location.indexOf('://') + 3);
  var applicationName = _location.substring(0, applicationNameIndex) + '/';
  return applicationName;
}

//-------------------------------------

function getRootWebSitePath() {
  var _location = document.location.toString();
  var applicationNameIndex = _location.indexOf('/', _location.indexOf('://') + 3);
  var applicationName = _location.substring(0, applicationNameIndex) + '/';
  var webFolderIndex = _location.indexOf('/', _location.indexOf(applicationName) + applicationName.length);
  var webFolderFullPath = _location.substring(0, webFolderIndex);
  return webFolderFullPath;
}

/**
 * Represents a search trough an array.
 * @function search
 * @param {Array} array - The array you wanna search trough
 * @param {string} key - The key to search for
 * @param {string} [prop] - The property name to find it in
 */
function Search_Array(array, key, prop) {
	var lSucess = false;

    // Optional, but fallback to key['name'] if not selected
    prop = (typeof prop === 'undefined') ? '' : prop;    
   
    if ( prop.trim() )
    {
    	for (var i=0; i < array.length; i++) {
        	if (array[i][prop] === key) {
            	lSucess = true;
            	break; 
        	}
    	}
    }	

    return lSucess;
}

/**
 * array.isArray( obj ) 
 */
function isArray( array ) {
	if ( typeof array === "undefined" ) 
    return false;
  else if ( toString.call(array) === "[object Array]" ) 
  	return true;
  else if ( typeof array.length === "number" ) 
  	return true;
    
  return false;
}

/**
 * Covierte String a Numero 
 */
function Str2Number( data ) {
	var nReturn = 0;
    
  if ( typeof data === 'string' ) {
			nReturn = Number( data.trim() );
    	if ( isNaN( nReturn ) )
				nReturn = 0; 
	}
	else if ( typeof data === 'number' ) 
			nReturn = data;
	
	return nReturn;   	
}

/* 
*  empty : sure that variable is a string, doesn't contains only spaces, 
*          and can contain '0' ***(string).
*/
function empty(str) {
	return !str || !/[^\s]+/.test(str);
}

/*
*   format(num) : number with thousands and decimals   
*/
function format(num) {
    var n = num.toString(), p = n.indexOf('.');
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function($0, i){
        return p<0 || i<p ? ($0+',') : $0;
    });
}

/**
 * MaskMoney : by Carlos Vargas 
 */
function GetMyMoney( nID ) {
	return $( "#" + nID ).maskMoney( "unmasked" )[0];
}

function SetMyMoney( nID, nValue ) {
	if( typeof nValue == 'string' )
		$( "#" + nID ).maskMoney( "mask", parseFloat( nValue ) );
	else
		$( "#" + nID ).maskMoney( "mask", nValue );
}

function isEmpty(str) {
    return (!str || str.length === 0 || str === undefined );
}

function DoWork() {
 	MsgInfo( "Work in progress" );
}

function GoTo( cUrl ) {
 	window.location.assign( cUrl );
}

function GoToNew( cUrl ) {
  	window.open( cUrl );
}
