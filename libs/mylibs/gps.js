/*
* https://www.w3.org/TR/geolocation/
*/
function GPS() { 
  
  var options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };

  this.init = function() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition( success, error, options );
    } else { 
      console.log( "Geolocation is not supported by this browser." );
    }
  };	

  function success( pos ) {
    var crd = pos.coords;
    let userLatitude = crd.latitude;
    let userLongitude = crd.longitude;
    localStorage.setItem("USER_LATITUDE", userLatitude);
    localStorage.setItem("USER_LONGITUDE", userLongitude);
    console.log( userLatitude, userLongitude );
  }

  function error(err) {
    console.warn('ERROR(${err.code}): ${err.message}');
    console.log('Latitude : 9');
  }

  this.coords = function() {
    var latitude = localStorage.getItem('USER_LATITUDE');
    var longitude = localStorage.getItem('USER_LONGITUDE');
    coords = { lat : latitude, long : longitude };
    console.log( "coords :", coords );
    return coords; 
  };

}

function getGPS() {
  var oGps = new GPS();
  oGps.init();
  var coords = oGps.coords();
  console.log( coords );
  return coords; 
}
