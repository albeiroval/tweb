<?php

function console_file( $message, $isnew = false ) {
   $fichero = 'log_file.txt';
   if ( isset($message) ) {
     $message .= "\n";
 
     if ( gettype($isnew) == "boolean" ) {
       $rewrite = $isnew;
     } else {
       $rewrite = false;
     }
     if ( $rewrite == true ) {
       file_put_contents($fichero, $message);
     } else { 
       // usando la bandera FILE_APPEND para añadir el contenido al final del fichero
       // y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
       file_put_contents($fichero, $message, FILE_APPEND | LOCK_EX);
     }	
     
   }
 }

//--------------------------------------

function getDateTime() {

/*
* 1ra forma :  date("d") . "/" . date("m") . date("Y"); 
* 2da forma :  $hoy = getdate();
*              return Array(
*                 [seconds] => 40
*                 [minutes] => 58
*                 [hours]   => 21
*                 [mday]    => 17
*                 [wday]    => 2
*                 [mon]     => 6
*                 [year]    => 2003
*                 [yday]    => 167
*                 [weekday] => Tuesday
*                 [month]   => June
*                 [0]       => 1055901520
*              )          
*/

  $hoy = getdate();
 
  return [ 'date'    => $hoy['mday'] . "/" . $hoy['mon'] . "/" . $hoy['year'], 
           'time'    => $hoy['hours'] . ":" . $hoy['minutes'] . ":" . $hoy['seconds'],
           'seconds' => $hoy['0'] ];

 }

//--------------------------------------

function getDateSQL() {
  $hoy  = getdate();
  $cSql = $hoy['year'] . "-" . $hoy['mon'] . "-" . $hoy['mday'];       
  return $cSql;
}

//--------------------------------------

function AddJs( $cUrl ) 
{
	$cHtml = '<script src="' . $cUrl . '"></script>' ;
	
	echo $cHtml ;
}

//--------------------------------------

function AddCss( $cUrl )
{
	$cHtml = '<link type="text/css" rel="stylesheet" href="' . $cUrl . '" media="screen">';
		
	echo $cHtml ;
}	

//--------------------------------------

function Str2Number( $value )
{
   $cType = gettype( $value );
   
   if ($cType = "string")
   {
      $value = floatval( $value );
   }  

   $cType = gettype( $value );
   
   if ( $cType = "integer" )
   {
      $value = $value * 100;
   }
   
   return $value;    
}

?>