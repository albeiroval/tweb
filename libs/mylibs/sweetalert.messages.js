/**
 * Funciones que reemplazan alert(), confirm(), prompt(), MsgCombo() 
 * Libreria : sweetAlert2 
 * https://sweetalert2.github.io/ 
 * Author : Albeiro Valencia
 * Fecha  : 07/02/2022
 * Referencias :
 * https://stackoverflow.com/questions/58764296/sweetalert2-correct-way-to-completely-disable-animation-with-version-9-0-0
 * https://github.com/sweetalert2/sweetalert2/releases/tag/v9.0.0
 */

//-------------------------------------

function JSuccess( message ) {
	__Notify( message, 'success', 1000, '#66ffb3' );
}

//-------------------------------------

function JError( message ) {
	__Notify( message, 'error', 1500, '#ff4d4d' );
}

//-------------------------------------

function __Notify( message, type, milisecs, color ) {
	if ( isEmpty( message ) ) { message = 'Informacion'; }
	if ( isEmpty( type ) ) { message = 'success'; }
	if ( isEmpty( milisecs ) ) { milisecs = 1500; }
	if ( isEmpty( color ) ) { color = '#fff'; }

	Swal.fire({
		position: 'top-end',
		title : '<b style="color : #fff">' + message + '</b>',
		background: color,
		showConfirmButton: false,
		timer: milisecs
	});
}

//-------------------------------------

function JMsgInfo( message, callback, argument) {
	if ( isEmpty( message ) ) { message = 'Informacion'; }

	Swal.fire({  
		title: '<p style="word-break: break-word;font-size: 26px;color: #101111;">'+message+'</p>',
		confirmButtonColor: "#1C4FE8",  
		icon: "info",   
		confirmButtonText: '<p style="font-size: 18px;margin: auto;color: black;">Aceptar'+
											 '<i class="fas fa-check" style="margin-left: 10px;"></i></p>'		
	}).then( function(result) {
		if (result.value && callback) {
				callfunc( callback, argument) ; 
		}	
	});
}

//-------------------------------------

function JMsgError( message, callback, argument) {
	if ( isEmpty( message ) ) { message = 'Error'; }
	
	Swal.fire({  
		title: '<p style="word-break: break-word;font-size: 26px;color: #101111;">'+message+'</p>',
		confirmButtonColor: "#1C4FE8",  
		icon: "error",   
		confirmButtonText: '<p style="font-size: 18px;margin: auto;color: black;">Aceptar'+
											 '<i class="fas fa-check" style="margin-left: 10px;"></i></p>'		
	}).then( function(result) {
		if (result.value && callback) {
				callfunc( callback, argument) ; 
		}	
  });
}

//-------------------------------------

function JMsgYesNo( message, callback, argument ) {
	if ( isEmpty( message ) ) { message = 'Seleccione'; }

	Swal.fire({  
		title: '<p style="word-break: break-word;font-size: 26px;color: #101111;">'+message+'</p>',
		icon: 'question',
		showCancelButton: true,
		confirmButtonColor: '#00e6e6',
		cancelButtonColor: '#d33',
		confirmButtonText: '<p style="font-size: 18px;margin: auto;color: black;">Si'+
											 '<i class="fas fa-check" style="margin-left: 10px;"></i></p>',
		cancelButtonText: '<p style="font-size: 18px;margin: auto;color: black;">No'+ 
										  '<i class="fas fa-times" style="margin-left: 10px;"></i>'
 }).then( function(result) {
  		if (result.value) {
				callfunc( callback, argument ) ; 
  		} else if ( result.dismiss === Swal.DismissReason.cancel ) { // Read more about handling 
			  // function cancel 
			}
  	});

}

//-------------------------------------

function JMsgCombo( aDatos, message, bCallback ) {
	if ( isEmpty( message ) ) { message = 'Buscar :'; }
	
	Swal.fire({  
		title: '<p style="word-break: break-word;font-size: 26px;color: #101111;">'+message+'</p>',
		input: 'select',
		inputOptions: aDatos,
		showCancelButton: true,
		confirmButtonText: '<p style="font-size: 18px;margin: auto;color: black;">Aceptar'+
											 '<i class="fas fa-check" style="margin-left: 10px;"></i></p>',
		cancelButtonText: '<p style="font-size: 18px;margin: auto;color: black;">Cancelar'+ 
										  '<i class="fas fa-times" style="margin-left: 10px;"></i>'
	}).then( function(result) {
		if (result.isConfirmed) {
			callfunc( bCallback, { data : aDatos, index : result.value } ) ; 
		} else if ( result.dismiss === Swal.DismissReason.cancel ) { // Read more about handling 
			// console.log('Opcion Cancelada');
		}
	});	
	
}	

//-------------------------------------
// https://stackoverflow.com/questions/44118673/sweet-alert-immidiately-closed-before-click-ok-button-or-hit-enter-when-sweet-al

function JMsgPrompt( message, bCallback, value, placeholder ) {
	if ( isEmpty( message ) ) { message = 'Lectura :'; }
	if ( isEmpty( value ) ) { value = ''; }

	Swal.fire({
		title: '<p style="word-break: break-word;font-size: 26px;color: #101111;">'+message+'</p>',
		input: 'text',
		inputValue: value,
		confirmButtonText: '<p style="font-size: 18px;margin: auto;color: black;">Aceptar'+
											 '<i class="fas fa-check" style="margin-left: 10px;"></i></p>',		
		showCancelButton: true,
		cancelButtonText: '<p style="font-size: 18px;margin: auto;color: black;">Cancelar'+ 
										  '<i class="fas fa-times" style="margin-left: 10px;"></i>',
		inputPlaceholder: placeholder,
		inputValidator: function(result) {
			if (!result) {
				return 'Debe ingresar dato !! ';
			}
		}
	}).then( function(result) {
		if (result.isConfirmed) {
			callfunc( bCallback, result.value ); 
		} else if ( result.dismiss === Swal.DismissReason.cancel ) { // Read more about handling 
			// console.log('Opcion Cancelada');
		}
	});	

}

//-------------------------------------

function callfunc( bCallback, argument) {
	if ( typeof bCallback == 'function' ) {	
		var fnparams = [ argument ];
		bCallback.apply(null, fnparams);	
	}
	else
		console.log('Error in function : ' + bCallback + '  Arguments : ' + argument); 
}

// FINAL
