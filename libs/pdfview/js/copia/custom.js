/**
 *  MODULO : Vista de Archivos PDF.
 * 	AUTOR  : ANTONIO ALBEIRO VALENCIA
 *  FECHA  : 20/01/2021
 *  DOCUMENTACION : 	https://mozilla.github.io/pdf.js/
 * 										https://unpkg.com/browse/pdfjs-dist@2.5.207/
 *                    https://stackoverflow.com/questions/27607526/zoom-on-simple-pdf-js-viewer  
 */
	
	/*
	*   Constantes Globales
	*/

	const __base64 = "base64";

	var DEFAULT_SCALE_DELTA = 1.5; 
	var MAX_SCALE = 5;
  var MIN_SCALE = DEFAULT_SCALE_DELTA;

  /*
	*   Variables Global que maneja el documento PDF
	*/
	
	var myDoc = {}; 

  myDoc.pdf   = null;
  myDoc.page  = 1;
  myDoc.scale = MIN_SCALE;
  myDoc.id    = null;
  myDoc.type  = null;
  myDoc.data  = null;
  myDoc.blob  = null;
  		
	//------------------------------------------		

	/*
	*  Inicializa la clase
	*/		
	function initializePDFJS( object ) {
		
		console.log("initializePDFJS", object);

		myDoc.id    = object.id;
		myDoc.type  = object.type;
		myDoc.scale = object.scale;
		myDoc.page  = 1;
		
		if ( myDoc.type == __base64 ) {
			myDoc.data = atob(object.data);	
			myDoc.blob = object.data;
		} else {
			myDoc.data = object.data;
		}	
		
    var pdfjsLib;

		pdfjsLib = window['pdfjs-dist/build/pdf'];
		pdfjsLib.GlobalWorkerOptions.workerSrc = './js/pdf.worker.js';

		if ( myDoc.type == __base64 ) {
			loadingTask = pdfjsLib.getDocument( { data: myDoc.data } );
		} else {
			loadingTask = pdfjsLib.getDocument( myDoc.data );
		}

		/**
		 * Asynchronously downloads PDF.
		 */
		loadingTask.promise.then(function(pdf) {	
			myDoc.pdf = pdf;
			document.getElementById('page_count').textContent = myDoc.pdf.numPages; // Muestra el numero de pagina
	    renderPage(myDoc.page); // Initial/first page rendering
		});
		
	}		
	
	//------------------------------------------

	/**
 	* Get page info from document, resize canvas accordingly, and render page.
 	* @param num Page number.
 	*/
	function renderPage(num) {

    // Using promise to fetch the page
    myDoc.pdf.getPage(num).then(function(page) {

      var canvas = document.getElementById(myDoc.id);
      var ctx    = canvas.getContext('2d');

      var viewport = page.getViewport({scale: myDoc.scale});

	    canvas.height = viewport.height;
	    canvas.width  = viewport.width;

	    // Render PDF page into canvas context
	    var renderContext = {
	      canvasContext: ctx,
	      viewport: viewport
	    };
      
      page.render(renderContext);
      
	  });

	  // Update page counters
	  document.getElementById('page_num').textContent = num;

	}
  
	//-------------------------------------------
  
	/**
	* Displays previous page.
	*/
	function onPrevPage() {
    if (myDoc.page <= 1) {
	    return;
	  }
	  myDoc.page--;
    renderPage(myDoc.page);
	}

	/**
	* Displays next page.
	*/
	function onNextPage() {
    if (myDoc.page >= myDoc.pdf.numPages) {
	    return;
	  }
	  myDoc.page++;
    renderPage(myDoc.page);
  }

	//-------------------------------------------

	/*
	* Display canvas ZoomIn
	*/
	function zoomPlus() {
    if ( myDoc.scale >= MAX_SCALE ) {
			return;
    }
    myDoc.scale += 0.25;
    renderPage(myDoc.page);
	}
	
	/*
	* Display canvas ZoomOut
	*/
	function zoomMin() {
    if ( myDoc.scale <= MIN_SCALE ) {
			return;
    }
    myDoc.scale -= 0.25;
		renderPage(myDoc.page);
	}
  
  //-------------------------------------------
  
	/*
	*  PRINT PDF
	*  https://printjs.crabbly.com/
	*/
	function printPDF() {
		if ( myDoc.type == __base64 ) {
			printJS({ 
				printable: myDoc.blob,
				type: 'pdf',
				base64: true
			});
		} else {
			printJS( window.location.pathname + "/" +  myDoc.data );
		}	
	}	

  //-------------------------------------------

	/*
	* DOWNLOAD PDF 
	* https://github.com/eligrey/FileSaver.js 
	*/
	function downloadPDF() {
		if ( myDoc.type == __base64 ) {
			base64toPDF( myDoc.blob );
		} else {	
			saveAs(myDoc.data, "document.pdf");	
		}	
	}

	/*
  *  Download formato base64 a Archivo PDF
  */
	function base64toPDF(base64) {
    var bufferArray = base64ToArrayBuffer(base64);
    var blobStore = new Blob([bufferArray], { type: "application/pdf" });
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blobStore);
        return;
    }
    var data = window.URL.createObjectURL(blobStore);
    var link = document.createElement('a');
    document.body.appendChild(link);
    link.href = data;
    link.download = "file.pdf";
    link.click();
    window.URL.revokeObjectURL(data);
    link.remove();
	}

	function base64ToArrayBuffer(data) {
    var bString = window.atob(data);
    var bLength = bString.length;
    var bytes = new Uint8Array(bLength);
    for (var i = 0; i < bLength; i++) {
        var ascii = bString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
	}
	
	/* FINAL */