var __core_msg = {
	_msg_image_error		: TWEB_PATH_IMAGES + '/alert.png',
	_msg_image_info			: TWEB_PATH_IMAGES + '/info2.png',
	_msg_image_dlg 			: TWEB_PATH_IMAGES + '/form.png',
	_msg_image_log 			: TWEB_PATH_IMAGES + '/form.png',
	_msg_image_loading	: TWEB_PATH_IMAGES + '/info.png',
	_msg_gif_loading		: TWEB_PATH_IMAGES + '/loader.gif',
	_msg_image_new			: TWEB_PATH_IMAGES + '/new.png',
	_sound_success			: TWEB_PATH_SOUND + '/msg_success.mp3',
	_sound_warn					: TWEB_PATH_SOUND + '/msg_warn.mp3',
	_sound_error				: TWEB_PATH_SOUND + '/msg_error.mp3',
	_sound_info					: TWEB_PATH_SOUND + '/msg_info.mp3',	
	_dlg_error_title 		: 'TWeb Error !',
	_dlg_show    				: { effect: "fade", duration: 300 },
	_dlg_hide    				: { effect: "fade", duration: 300 },	
	_dlg_btn_accept			: 'Aceptar',
	_dlg_btn_close			: 'Cerrar',
	_dlg_title					: 'Diálogo',
	_dlg_title_error		: 'Sistema Error',
	_dlg_title_info			: 'Información',
	_dlg_title_confirm	: 'Confirma',
	_dlg_title_get			: 'Entrada',
	_dlg_title_log			: 'Log del sistema',
	_dlg_title_loading	: 'Loading',
	_dlg_loading				: 'Cargando...',
	_dlg_connect				: 'Conectando...',
	_error_dlg_exist		: 'Error. Diálogo ya existe: ',
	_error_id_exist			: 'Error. Selector ya existe: ',
	_error_dlg_no_tdialog	: 'Error. Diàlogo no es de clase TDialog',
	_error_tab_load_php		: 'Error cargando ',
	_error_max_id				: 'Demasiados controles con el mismo id',
	_error_no_function	: 'no es una funcion',
	_error_event_no_exist	: 'Evento no existe ',
	_error_init_event		: 'Error iniciando evento',
	_error_no_file			: 'Fichero no existe',
	_error_no_module		: 'No se ha cargado el módulo ',
	logic_true					: 'Si',
	logic_false					: 'No'

};

function _( cTag ) {

	if( cTag in __core_msg ) 	
		return __core_msg[ cTag ]; 				
	else 
		alert( 'Error tag: ' + cTag );
}
