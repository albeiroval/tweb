<?php
/*
* Class  : TXFrame( <id>, <with> );
* Autor  : Antonio Albeiro Valencia
* Fecha  : 18/01/2021
*/

class TXFrame {
  
  public $textBtn = 'Close';
  public $nWidth  = 551;

  protected $cId    = 'xframe';
  protected $cTitle = 'Load PDF from base64';
  
  public function __construct( $cId = 'xframe', $cTitle = '' ) {
    if ( !empty($cId) ) { $this->cId = $cId; }
    if ( !empty($cTitle) ) { $this->cTitle = $cTitle; }
  }
  
  public function Activate() {

    $cStyle = '';

    if ( $this->nWidth > 0 ) {
      $cStyle .= 'style="width: 100%;max-width: ' . $this->nWidth .'px;"'; 
    }
    
    // CODE HTML

    // Modal 
    $cHtml  = '<div id="'.$this->cId.'" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">' . PHP_EOL;	
		$cHtml .= ' <div class="modal-dialog modal-xl"' . $cStyle .'>' . PHP_EOL;

    // Modal content 
    $cHtml .= ' <div class="modal-content">' . PHP_EOL;

    // Modal Header
    $cHtml .= ' <div class="modal-header" style="height: 50px;">' . PHP_EOL;
    $cHtml .= '   <h5 class="modal-title" style="margin: auto;">' . $this->cTitle . '</h4>' . PHP_EOL;
    $cHtml .= ' </div>' . PHP_EOL;

    // Modal Body
    $cHtml .= ' <div class="modal-body">' . PHP_EOL;
    $cHtml .= '   <iframe id="iframe"><p>It appears your web browser does support iframes.</p></iframe>' . PHP_EOL;
    $cHtml .= ' </div>' . PHP_EOL;

    // Modal footer
    $cHtml .= ' <div class="modal-footer" style="height: 50px;">' . PHP_EOL;
    $cHtml .= '   <button type="button" class="btn btn-default" data-dismiss="modal">'.$this->textBtn.'</button>' . PHP_EOL;
    $cHtml .= ' </div>' . PHP_EOL;

    // End Div's
    $cHtml .= ' </div>'  . PHP_EOL;
    $cHtml .= ' </div>'  . PHP_EOL;
    $cHtml .= '</div>'   . PHP_EOL;
    
    echo $cHtml;

    // STYLE
    $cStyle  = '<style>' . PHP_EOL;

    $cStyle .= '  #iframe {'        . PHP_EOL;
    $cStyle .= '    width: 100%;'   . PHP_EOL;
    $cStyle .= '    height: 450px;' . PHP_EOL;
    $cStyle .= '  }'                . PHP_EOL;	
    
    $cStyle .= '  @media (max-width : 360px) {' . PHP_EOL;
    $cStyle .= '      #iframe {'                . PHP_EOL;
    $cStyle .= '      width: 100%;'             . PHP_EOL;
    $cStyle .= '      height: 410px;'           . PHP_EOL;
    $cStyle .= '    }'                          . PHP_EOL;
    $cStyle .= '  }'                            . PHP_EOL;

    $cStyle .= '</style>' . PHP_EOL;
    
    echo $cStyle;
   
    // JAVASCRIPT
    $cJS  = '<script>' . PHP_EOL;
    
    $cJS .= '$(function() {'                                           . PHP_EOL;
    $cJS .= '  var oFrame = new TFrame("' . $this->cId . '");'         . PHP_EOL;
		$cJS .= '  var o = new TControl();'                                . PHP_EOL;
    $cJS .= "  o.ControlInit('" . $this->cId . "', oFrame, 'xframe');" . PHP_EOL;
    $cJS .= '});'                                                      . PHP_EOL;

    $cJS .= '</script>' . PHP_EOL;

    echo $cJS;

  }
    
}

?>

<script>

// https://stackoverflow.com/questions/23115581/can-i-explicitly-specify-a-mime-type-in-an-img-tag

var TFrame = function( cId ) {
  
  var cType = '';

  this.cId    = "#" + cId;
  this.base64 = '';
  
  //--------------------------

  this.show = function() {
    $(this.cId).modal('show');
  }

  //--------------------------

  this.setBase64 = function( base64 ) {
    $("#iframe").attr("src", base64);
  }

  //--------------------------

  this.loadImageFromAjax = function( cType, cUrl, oParam ) {
    
    var oAjax = $.ajax({
      type    : "POST",
      url     : cUrl,
      cache   : false,
      data    : oParam,
      dataType: "json",
      success : function(respond) {
        if ( respond != null ) {
          loadBase64( respond, cType );
        } else {
          JMsgError("No se encontro registro base64");
        }
      },
      error : function (jqXhr, textStatus, errorMessage) {
        if ( jqXhr.status == 200 ) {
		      Msg_AjaxError( cUrl, jqXhr, textStatus );
        } else {
          Msg_Dispatcher( jqXhr, cUrl );		
        }
      }
    });
    
  }
  
  //--------------------------
  // https://mozilla.github.io/pdf.js/examples/

  function loadBase64( respond, cType ) {
    var lOk   = true;
    var cMime = '';
    
    switch (cType) {
      
      case 'pdf' :
        cMime = "data:application/pdf;base64,";
        break;

      case 'png' :
        cMime = "data:image/png;base64,";
        break;

      case 'jpg' :
        cMime = "data:image/jpg;base64,";
        break;

      case 'jpeg' :
        cMime = "data:image/jpeg;base64,";
        break;
      
    default:
      lOk = false;
      JMsgError("Tipo de archivo debe ser [pdf/png/jpg/jpeg]");
      break;
    }

    if ( lOk ) {
      console.log(respond);
      $("#iframe").attr("src", cMime + respond);
    }  

  }  

  //--------------------------
  
  function Msg_Dispatcher( jqXhr, cUrl ) {

    cUrl = ( typeof( cUrl ) === 'string') ? cUrl : '';

    var lError 			= true;
    var cErrorTxt  	= "";

    //	Siempre que se ponga un codigo error ira acompañado de un texto:
    //	header("HTTP/1.0 902 Error );

    switch ( jqXhr.status  ) {

      case 900:

        cErrorTxt =  jqXhr.statusText;	

        if ( cErrorTxt == '' ) {
          cErrorTxt =  jqXhr.status + ' '  + jqXhr.statusText ;				
        }	

        Msg_Swal( cErrorTxt );

        break;

      case 901:

        var cUrl =  jqXhr.statusText;			

        window.location.href = cUrl;												

        break;	

      case 902:	

        //	statusText ->  mifuncion var1 var2
        //		cFunction será mifuncion
        //		aParam será un array con [ var1, var2 ] 

        var cParam			= jqXhr.statusText;				
        var aParam  		= cParam.split(' ' ) ;			
        var cFunction 	= aParam[0];

        aParam = aParam.splice(1) ;

        if ( cFunction !== ''  ) {				
        
          var fn = window[ cFunction ];

          // is object a function?
          if (typeof fn === "function") {				
          
            //var fnparams = [ cErrorTxt, jqXhr, textStatus ];
            //fn.apply(null, fnparams );	
            fn.apply( null, aParam );	
          
          } else {
          
            //if ( oThis.lLog ) console.error( cFunction + ', no es una funcion valida' );				
            console.error( 'TWeb', 'Error asignando ' + cFunction + ' a code 902' );				
          }				
        }															

        break;					

      case 200:
      
        lError = false;

        break;

      case 404:	// No file

        cUrl = cUrl.substring(0, cUrl.indexOf("?"));
        Msg_Swal( 'Archivo no existe : ' + cUrl );

        break;

      case 500:
      
        cErrorTxt = jqXhr.status + ' '  + jqXhr.statusText ;		
        Msg_Swal( cErrorTxt );

        break;			

      default: 

        cErrorTxt = jqXhr.status + ' '  + jqXhr.statusText ;		
        Msg_Swal( cErrorTxt );

        break;							
    }

    return lError;

  }	

  //--------------------------
  
  function Msg_AjaxError( cUrl, jqXhr, textStatus ) {
    cUrl = cUrl.substring(0, cUrl.indexOf("?"));
  
    console.log("error ajax", cUrl, jqXhr, textStatus);
  
    var newLine = "<br>"; 
    msg  = "url : " + cUrl + newLine;
    msg += "status : " + jqXhr.status + newLine; 
    msg += "type : " + textStatus + newLine;
    msg += "description  : " + jqXhr.responseText + newLine; 
  
    Msg_Swal( msg );
  }

  //--------------------------
  
  function Msg_Swal( msg ) {
	  Swal.fire({
		  title: 'Error ajax',
		  html: msg
		});
  }

}

</script>