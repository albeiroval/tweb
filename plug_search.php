<?php
include_once ( 'core.php' );
include_once ( 'core.grid.php' );

	//	Paràmetres...

		$cId_Dialog	= TPost( '_id' );
		$cTitle		= TPost( 'cTitle' );
		$cField		= TPost( 'cField' );
		$cId_Grid	= TPost( 'cId_Grid' );
		
	
	$oRc 	= new TRc( 'core.rc', 'search' );
	
	$oWnd = $oRc->TDialog( '_tweb_search' );	
	
		$oRadio = $oRc->TRadio( $oWnd, 10020, array( 10021, 10022 ), null, 1 );
		
		$oGet = $oRc->TGet( $oWnd, 10010, '' );
			$oGet->SetKey( VK_RETURN , "_tweb_grid_search_accept()" );
		
		$oRc->TButton( $oWnd, 10100, null, '_tweb_grid_exit()' );
		$oRc->TButton( $oWnd, 10110, null, '_tweb_grid_search_accept()' );
			
	$oWnd->Activate();			
?>
<script>

	function _tweb_grid_search_accept() {
	
		var cTitle	= "<?php echo $cTitle; ?>";
		var cField	= "<?php echo $cField; ?>";
		var cId 	= "<?php echo $cId_Grid; ?>";		
		
		var o		= new TControl()
		var cSearch = o.Get( 10010 );		
		var nOption = o.Get( 10020 );		
		var oGrid	= o.GetControl( cId )

		switch ( nOption ) {
		
			case '1':
		
				if ( cSearch !== '' ) {					
					oGrid.Search( cField, cSearch, true, false )							
				}	
				
				break;

			case '2':

				var oDataset = oGrid.GetDataView();				

				//var searchTerm = new Object();     
				//	searchTerm[ cField ] = cSearch;		

				myfilter = function( item, args ) {
					lFound = item[ cField ].indexOf( args ) >= 0;
					return lFound; 
				}
			
				
				oDataset.beginUpdate()				
				
				if ( cSearch !== '' ) {
					oDataset.setFilter( myfilter );
					oDataset.setFilterArgs( cSearch );		
				} else {
					oDataset.setFilter( null );
					oDataset.setFilterArgs();									
				}
				
				oDataset.endUpdate();														
			
				break;								
		}

		o.Focus( 10010 )
	}
	
		
	
	function _tweb_grid_exit() {
	
		var cId_Dialog	= "<?php echo $cId_Dialog;  ?>" ;
		
		$('#' + cId_Dialog ).dialog( 'close' );						
	}
	
</script>