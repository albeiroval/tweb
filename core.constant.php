<?php 
define( 'TWEB_VERSION'		, '1.5');
define( 'TWEB_VERSION_DATE'	, '18/05/2019'	);

//	Definició de Paths...		
define( 'TWEB_PATH_IMAGES', TWEB_PATH . '/images' );
define( 'TWEB_PATH_SOUND'	, TWEB_PATH . '/sound' );
define( 'TWEB_PATH_LIBS'	, TWEB_PATH . '/libs' );		
define( 'TWEB_PATH_LOG'		,  dirname( __FILE__ ) . '/log' );	

//	Keys...
define( 'VK_F3'				,  114 );
define( 'VK_RETURN'         ,  13 );
define( 'VK_INSERT'         ,  45 );
define( 'VK_DELETE'         ,  46 );

//	Colors...
define( 'CLR_BLACK'			, '#000000' );
define( 'CLR_WHITE'			, '#FFFFFF' );
define( 'CLR_RED'     	  	, '#FF0000' );
define( 'CLR_GREEN'  	  	, '#32CD32' );
define( 'CLR_BLUE'    	  	, '#4876FF' );
define( 'CLR_YELLOW'  	  	, '#FFFF00' );
define( 'CLR_GRAY'    	  	, '#BEBEBE' );
define( 'CLR_HGRAY'    	  	, '#F2F2F2' );
define( 'CLR_HGREEN'  	  	, '#9AFF9A' );
define( 'CLR_HRED'    	  	, '#F5A9A9' );
define( 'CLR_HBLUE'    	  	, '#A9E2F3' );
		
//	Definición Controles...
define( 'TWEB_CONTROL_WIDTH_DEFAULT'	, 150 );
define( 'TWEB_CONTROL_HEIGHT_DEFAULT'	,  22 );
define( 'TWEB_BAR_HEIGHT_DEFAULT'		,  50 );
define( 'TWEB_BUTTON_HEIGHT_DEFAULT'	,  32 );
define( 'TWEB_IMAGE_WIDTH_DEFAULT'		, 100 );
define( 'TWEB_IMAGE_HEIGHT_DEFAULT'		, 100 );
define( 'TWEB_GRID_WIDTH_DEFAULT'		, 500 );
define( 'TWEB_GRID_HEIGHT_DEFAULT'		, 400 );
define( 'TWEB_FOLDER_WIDTH_DEFAULT'		, 600 );
define( 'TWEB_FOLDER_HEIGHT_DEFAULT'	, 500 );
define( 'TWEB_GROUP_WIDTH_DEFAULT'		, 600 );
define( 'TWEB_GROUP_HEIGHT_DEFAULT'		, 500 );
define( 'TWEB_PANEL_WIDTH_DEFAULT'		, 300 );
define( 'TWEB_PANEL_HEIGHT_DEFAULT'		, 200 );
define( 'TWEB_BOX_WIDTH_DEFAULT'		, 300 );
define( 'TWEB_BOX_HEIGHT_DEFAULT'		, 200 );
define( 'TWEB_MAPS_WIDTH_DEFAULT'		, 300 );
define( 'TWEB_MAPS_HEIGHT_DEFAULT'		, 200 );
define( 'TWEB_YOUTUBE_WIDTH_DEFAULT'	, 300 );
define( 'TWEB_YOUTUBE_HEIGHT_DEFAULT'	, 200 );		
define( 'TWEB_TREE_WIDTH_DEFAULT'		, 300 );
define( 'TWEB_TREE_HEIGHT_DEFAULT'		, 400 );		
define( 'TWEB_FRAME_WIDTH_DEFAULT'		, 300 );		
define( 'TWEB_FRAME_HEIGHT_DEFAULT'		, 400 );		
define( 'TWEB_OBJECT_WIDTH_DEFAULT'		, 300 );		
define( 'TWEB_OBJECT_HEIGHT_DEFAULT'	, 400 );		
define( 'TWEB_ACCORDION_WIDTH_DEFAULT'	, 300 );		
define( 'TWEB_ACCORDION_HEIGHT_DEFAULT'	, 400 );			
define( 'TWEB_MSGITEM_WIDTH_DEFAULT'	, 250 );			
		
//	WEB ...
define( 'TWEB_HTML_LANG', 'es-ES' );
		
//	Definición literales...
		
//	Error...
define( 'TWEB_ERROR_SESSION'	, 'No Autorizado' );			// Defecto
define( 'TWEB_ERROR_SESSION_101', 'No esta autenticado !' );	// NO existe la session
define( 'TWEB_ERROR_SESSION_102', 'Sesion ha expirado !' );		// Timeout !
define( 'TWEB_ERROR_SESSION_103', 'Sesión errónea !' );			// Session erronea / distinta
define( 'TWEB_ERROR_CREATE_TMPDIR', 'Error creant directori temporal' );
		
//	RC ...
define( 'TWEB_RC_DIALOG'		, 'Diálogo' );
define( 'TWEB_RC_NO_FILE'		, 'Fichero RC no existe' );
define( 'TWEB_RC_NO_DIALOG'		, 'No se ha especificado nombre de diálogo del RC' );
define( 'TWEB_RC_NO_OPEN'		, 'Unable to open file!' );
define( 'TWEB_RC_NO_RESOURCE'	, 'No existe el diálog' );
define( 'TWEB_RC_NO_CONTROL'	, 'No existe el control' );
		
//	Maps ...

// Key for TWEB. Cada usuario se tendria de obtener la suya para uso personal...
define( 'TWEB_KEY_MAP'			, 'AIzaSyBmIUVLp_AXKaCF-o7fVabKKVTCBRwBVe4' );	
		
//	Files
define( 'TWEB_IMAGE_SIGNAL'		, TWEB_PATH_IMAGES . '/signal.gif' );		
		

//	TDataset
define( 'TWEB_DS_TABLE'			, 'Tabla' );
define( 'TWEB_DS_ERROR'			, 'Error' );
define( 'TWEB_DS_TRACE'			, 'Traza' );
define( 'TWEB_DS_CALL'			, 'Llamado desde: ' );
define( 'TWEB_DS_FIELD_WRONG'	, 'Campo no existe: ' );
	
?>