/*
*  Author and copyright: Albeiro Valencia (http://avcsistemas.com/foro)
*  CORE para manejo de la case TModal.
*  Fecha : 11/09/2020 Derechos Reservados
*  https://github.com/shaack/bootstrap-show-modal
*  https://www.jqueryscript.net/demo/Dynamic-Bootstrap-4-Modals/
*  https://www.codehim.com/download/dynamic-bootstrap-modals.zip
*/

/* jshint esversion: 8 */

var indexModal = 0;

class TModal {

  constructor( _idModal = "" ) {

    var oModal    = null;
    var header    = { color: '', backGround: '' };
    var oMap      = { idmap: null, latitud: null, longitud: null };
    var image     = { witdh: '50%', height: '50% ' };

    var _isValidFile  = false;
    var _uploadBase64 = null;
    var _uploadFile   = null;
    var _uploadType   = null;

    // Array de datos 
    var aDataTable = [];

    // Controla los eventos del modal
    var aModal  = {};
    aModal.idfocus     = ''; 
    aModal.setvalue    = [];
    aModal.imgBase64   = [];
    aModal.image       = [];
    aModal.disabled    = [];
    aModal.enabled     = [];
    aModal.btnfunction = [];
    aModal.onvalid     = [];
    aModal.onchange    = [];
    aModal.footerClr   = '';

    this.idModal  = "";
    this.props    = { idupload: null };
    this.title    = '';
    this.listbox  = [];
    this.checked  = [];
    
    if (typeof TModal.aModal === "undefined") {
      TModal.aModal = [];
    }

    //-----------------------------------------------------
    this.setmodal = function (props) {
      TModal.aModal.push(props);
    };

    //-----------------------------------------------------
    this.getmodal = function () {
      return this.props;
    };

    //-----------------------------------------------------
    this.getprops = function () {

      var props = {};

      if ( _idModal ) {

        var length = TModal.aModal.length;

        for (let index = 0; index < length; ++index) {

          if (TModal.aModal[index].id == _idModal) {

            header.color      = TModal.aModal[index].titleclr.clrtext;
            header.backGround = TModal.aModal[index].titleclr.bgtext;

            props.id        = TModal.aModal[index].id;
            props.title     = TModal.aModal[index].title;
            props.body      = TModal.aModal[index].body;
            props.footer    = TModal.aModal[index].footer;
            props.options   = TModal.aModal[index].options;
            props.data      = TModal.aModal[index].data;
            props.istable   = TModal.aModal[index].istable;
            props.idupload  = TModal.aModal[index].idupload;
            props.width     = TModal.aModal[index].width;
            props.omodal    = TModal.aModal[index].modal;

            break;

          }

        }

      }

      return props;

    };

    //-----------------------------------------------------
    this.show = function () {

      // create props
      var props = {};

      props.body    = this.props.body;
      props.footer  = this.props.footer;
      props.options = this.props.options;
      props.istable = this.props.istable;
      props.data    = this.props.data;
      props.modalDialogClass = this.props.width + " modal-shadow";

      if (this.title) {
        props.title = this.title;
      }

      props.onShown = function (modal) {

        // set focus
        if (aModal.idfocus) {
          $(aModal.idfocus).focus();
        }

        // set's values
        if (aModal.setvalue) {
          $.each(aModal.setvalue, function (index, obj) {
            $(obj.id).val(obj.value);
          });
        }

        // Set images base64
        if (aModal.imgBase64) {
          $.each(aModal.imgBase64, function (index, obj) {
            showImage(obj);
          });
        }

        // Disabled id's
        if (aModal.disabled) {
          $.each(aModal.disabled, function (index, id) {
            $(id).prop('disabled', true);
          });
        }

        // Enabled id's
        if (aModal.enabled) {
          $.each(aModal.enabled, function (index, id) {
            $(id).prop('disabled', false);
          });
        }

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
          $('.selectpicker').selectpicker('mobile');
        } else {
          $('.selectpicker').selectpicker({ size: 10 });
        }

      };

      props.onCreate = function (modal) {

        // button's onClick function
        if (aModal.btnfunction) {
          var aItems = aModal.btnfunction;
          for (let index = 0; index < aItems.length; index++) {
            const item = aItems[index];
            let elem = document.getElementById(item.id);
            if ( elem ) {
              elem.removeAttribute("onclick");
              elem.onclick = function(event) { 
                event.preventDefault();
                executeFunction(item.action);
              };  
            }
          }
        }

        // onvalid input's function
        // https://keycode.info/
        if (aModal.onvalid) {
          var aItems = aModal.onvalid;
          for (let index = 0; index < aItems.length; index++) {
            const item = aItems[index];
            let elem = document.getElementById(item.id);
            if ( elem ) {
              elem.addEventListener("keydown", event => {
                event.preventDefault();
                if (event.code === "Tab") {
                  executeFunction(item.action);
                } else if (event.code === "Enter") {
                  executeFunction(item.action);
                }
              });
            }
          }
        }

        // onchange function
        if (aModal.onchange) {
          var aItems = aModal.onchange;
          for (let index = 0; index < aItems.length; index++) {
            const item = aItems[index];
            console.log(item);
            $("select").on('change', function(){
              executeFunction(item.action, this.value);
            });
          }  
        }

      };  

      if (props) {
        oModal = new Modal(props);
      }

      // Set css header and footer
      setHeaderCss();
      setFooterCss();

      // Show Table 
      if (props.istable) {
        var $table = $('#table');
        $table.bootstrapTable('destroy');
        $table.bootstrapTable({ data: aDataTable, locale: 'es-ES' });
      }

      // Show Map
      if (oMap.idmap) {
        this.createMap();
      }

      // Show Image's
      this.createimages();

      // Show listbox
      for (let index = 0; index < this.listbox.length; index++) {
        let item = this.listbox[index];
        createlistbox(item.id, item.data);
      }

      //Show checked
      for (let index = 0; index < this.checked.length; index++) {
        let item = this.checked[index];
        createcheked( item.id, item.value );
      }
      
    };

    //-----------------------------------------------------
    function showImage(obj) {

      // 'data:image/png;base64,'
      const dataType = "data:" + _uploadType + ";base64,";

      $(obj.id).attr('src', dataType + obj.src);

      const elem = document.getElementById(obj.id.substring(1));
      if (elem) {
        const css = "width : " + image.witdh + "; height: " + image.height + "; margin : auto";
        elem.style.cssText = css;
      }

    }

    //-----------------------------------------------------
    function setHeaderCss() {
      var elem = document.getElementById('modal-header');
      if (elem) {
        var css = "color: " + header.color + "; background-color: " + header.backGround;
        elem.style.cssText = css;
      }
    }

    //-----------------------------------------------------
    this.SetFooterColor = function (cColor) {

      if (typeof aModal.footerClr === "undefined") {
        aModal.footerClr = '';
      }

      aModal.footerClr = cColor;

    };

    //-----------------------------------------------------
    function setFooterCss() {
      var elem = document.getElementById('modal-footer');
      if (elem && aModal.footerClr) {
        elem.style.backgroundColor = aModal.footerClr;
      }
    }

    //-----------------------------------------------------
    // https://stackoverflow.com/questions/359788/how-to-execute-a-javascript-function-when-i-have-its-name-as-a-string
    function executeFunction(functionName, params) {
      if (typeof functionName == 'function') {
        if ( params) {
          functionName.apply(null, [params]);
        } else {
          functionName.apply(null, null);
        }  
      } else {
        alert('Error function No Existe: ' + functionName);
      }  
    }

    //-----------------------------------------------------
    this.ontitle = function (title, clrTxt, clrBack) {

      if (title == null || title == '') { title = 'TEST TMODAL'; }
      if (clrTxt == null || clrTxt == '') { clrTxt = header.color; }
      if (clrBack == null || clrBack == '') { clrBack = header.backGround; }

      this.title = '<h5 class="modal-title">' + title + '</h5>';
      header.color = clrTxt;
      header.backGround = clrBack;

    };

    //-----------------------------------------------------
    this.hide = function () {
      if (oModal) {
        oMap = { idmap: null, latitud: null, longitud: null };
        this.clearUpload();
        oModal.hide();
      }
    };

    //-----------------------------------------------------
    this.get = function (cId) {
      var value = "";
      let elem = document.getElementById(cId)
      if ( elem ) {
        value = elem.value;
      }
      return value;
    };

    //-----------------------------------------------------
    this.iniset = function (cId, value) {

      if (typeof aModal.setvalue === "undefined") {
        aModal.setvalue = [];
      }

      if (value) {
        var idCtrl = "#" + cId;
        var index = aModal.setvalue.findIndex(p => p.id === idCtrl);

        if (index === -1) {
          aModal.setvalue.push({ id: idCtrl, value: value });
        } else {
          aModal.setvalue[index].value = value;
        }
      }

    };

    //-----------------------------------------------------
    this.set = function (cId, value) {
      this.iniset(cId, value);
    };

    //-----------------------------------------------------
    this.setlistbox = function (cId, aData, cDefault) {
      let elem = document.getElementById(cId);
      if ( elem ) {
        setTimeout(function() { 
            createlistbox(cId, aData);  
            if ( cDefault ) {
              $("#"+cId).selectpicker('val', cDefault); 
            }  
          }, 300 );  
      } else {
        this.listbox.push({ id: cId, data: aData });  
      }  
    };

    //-----------------------------------------------------
    function createlistbox(cId, aData) {
      if ( Array.isArray(aData) ) {
        var aItems;
        var options = [];
        for (let i = 0; i < aData.length; i++) {
          var option = '<option value="'+aData[i]+'" data-token="'+aData[i]+'">'+aData[i]+'</option>';
          options.push(option);
        }
        aItems = options.join('');
        $("#"+cId).html(aItems).selectpicker('refresh');
      } else {
        JMsgError("items debe ser un array");
      }
    }

    //-----------------------------------------------------
    this.setvalue_listbox = function(cId, value ) {
      setTimeout( function() { $("#"+cId).selectpicker('val', value); }, 300 );
    };

    //-----------------------------------------------------
    this.enabled = function (cId) {

      if (typeof aModal.enabled === "undefined") {
        aModal.enabled = [];
      }

      var idCtrl = "#" + cId;
      var index = aModal.enabled.indexOf(idCtrl);

      if (index === -1) {
        aModal.enabled.push(idCtrl);
      } else {
        aModal.enabled[index] = idCtrl;
      }

    };

    //-----------------------------------------------------
    this.disabled = function (cId) {

      if (typeof aModal.disabled === "undefined") {
        aModal.disabled = [];
      }

      var idCtrl = "#" + cId;
      var index = aModal.disabled.indexOf(idCtrl);

      if (index === -1) {
        aModal.disabled.push(idCtrl);
      } else {
        aModal.disabled[index] = idCtrl;
      }

    };

    //-----------------------------------------------------
    this.inifocus = function (cId) {
      aModal.idfocus = "#" + cId;
    };

    //-----------------------------------------------------
    this.focus = function (cId) {
      this.inifocus(cId);
    };

    //-----------------------------------------------------
    this.onvalid = function (cId, bfunc) {
      if (bfunc) {
        var index = aModal.onvalid.findIndex(p => p.id === cId);
        if (index === -1) {
          aModal.onvalid.push({ id: cId, action: bfunc });
        } else {
          aModal.onvalid[index].action = bfunc;
        }
      }
    };

    //-----------------------------------------------------
    this.onchange = function (cId, bfunc) {
      if (bfunc) {
        var index = aModal.onchange.findIndex(p => p.id === cId);
        if (index === -1) {
          aModal.onchange.push({ id: cId, action: bfunc });
        } else {
          aModal.onchange[index].action = bfunc;
        }
      }
    };

    //-----------------------------------------------------
    this.btnfunction = function (cId, bfunc) {
      if (bfunc) {
        var index = aModal.btnfunction.findIndex(p => p.id === cId);
        if (index === -1) {
          aModal.btnfunction.push({ id: cId, action: bfunc });
        } else {
          aModal.btnfunction[index].action = bfunc;
          let elem = document.getElementById(cId);
          if (elem) {
            elem.removeEventListener("click", bfunc); 
          }  
        }
      }
    };

    //-----------------------------------------------------
    this.setTable = function( aData ) {
      aDataTable = aData;
    };

    //-----------------------------------------------------
    this.getTable = function () {
      var $table = $('#table');
      return $table.bootstrapTable('getData');
    };

    //-----------------------------------------------------
    this.getTableSelec = function () {
      var $table = $('#table');
      return $table.bootstrapTable('getSelections');
    };

    //------------------------------------------------------
    this.addTableRow = function( aRow ) {
      var $table = $('#table');
      $table.bootstrapTable('append', aRow );
    };

    //------------------------------------------------------
    this.delTableRow = function() {
      var $table = $('#table');
      var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
        return row.id
      })
      $table.bootstrapTable('remove', {
        field: 'id',
        values: ids
      })
    };

    //-----------------------------------------------------
    this.tablerowselect = function() {
      var $table = $('#table');
      var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
        return row;
      });
      if ( ids.length > 0 ) {
         return true; 
      } else {
        return false;
      }
    }; 

    //-----------------------------------------------------
    this.getvalueradio = function (cId) {
      var value = '';
      var radios = document.getElementsByName('radio-' + cId);
      for (let i = 0; i < radios.length; i++) {
        if (radios[i].checked) {
          value = radios[i].value;
          break;
        }
      }
      return value;
    };

    //-----------------------------------------------------
    this.getchecked = function(cId) {
      var value = 0;
      var elem = document.getElementById(cId);
      if (elem) {
        if (elem.checked) {
          value = 1;
        }
      }
      return value;
    };

    //-----------------------------------------------------
    this.setchecked = function(cId, lCheck = true) {
      var data = (typeof lCheck === "boolean") ? lCheck : true;
      this.checked.push({ id : cId, value : data }); 
    };  

    //-----------------------------------------------------
    function createcheked(cId, value) {
      let elem = document.getElementById(cId);
      if (elem) {
        elem.checked = value;
      }
    }

    //-----------------------------------------------------
    this.setimage = function (cId, image) {
      aModal.image.push({ id : cId, src : image});
    };

    //-----------------------------------------------------
    this.createimages = function() {
      for (let index = 0; index < aModal.image.length; index++) {
        var item = aModal.image[index];
        var elem = document.getElementById(item.id);
        if (elem) {
          if (this.checkimage(item.src)) { 
            elem.setAttribute('src', item.src);
          } else {
            elem.setAttribute('src', TWEB_PATH_IMAGES + '/no-image.jpg');
          }
        }
      }  
    } 

    //-----------------------------------------------------
    this.checkimage = function (urlFile) {
      var xhr = new XMLHttpRequest();
      xhr.open('HEAD', urlFile, false);
      xhr.send();
     
      if (xhr.status == "404") {
        return false;
      } else {
        return true;
      }
    };

    //-----------------------------------------------------
    this.setImageBase64 = function (cId, base64, w, h) {

      if (typeof aModal.imgBase64 === "undefined") {
        aModal.imgBase64 = [];
      }

      if (base64) {
        var idCtrl = "#" + cId;
        var index = aModal.imgBase64.findIndex(p => p.id === idCtrl);

        if (index === -1) {
          aModal.imgBase64.push({ id: idCtrl, src: base64 });
        } else {
          aModal.imgBase64[index].src = base64;
        }

        if (w) {
          if (h == null)
            h = w;
        } else {
          w = '50%';
          h = '50%';
        }

        image = { witdh: w, height: h };

      }

    };

    //-----------------------------------------------------
    this.setMapWeb = function (latitud, longitud) {
      oMap.idmap = 'map_canvas';
      oMap.latitud = latitud;
      oMap.longitud = longitud;
    };

    //-----------------------------------------------------
    this.createMap = function () {
      var map = L.map(oMap.idmap).setView([oMap.latitud, oMap.longitud], 15);
      L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
        maxZoom: 18
      }).addTo(map);
      L.control.scale().addTo(map);
      L.marker([oMap.latitud, oMap.longitud], { draggable: true }).addTo(map);
    };

    //-----------------------------------------------------
    this.clearUpload = function () {
      if (this.props.idupload) {
        var elem = document.getElementById(this.props.idupload);
        if (elem) {
          elem.value = "";
        }
        _isValidFile = false;
      }
    };

    //-----------------------------------------------------
    this.uploadFile = function (event) {
      __getFileBase64("#" + this.props.idupload);
    };

    //-----------------------------------------------------
    this.getUploadFile = function () {
      return _uploadFile;
    };

    //-----------------------------------------------------
    this.setUploadFile = function (cfiletype) {
      _uploadFile = cfiletype;
    };

    //-----------------------------------------------------
    async function __getFileBase64(cId) {

      var file = document.querySelector(cId).files[0];
      var namefile = " ";

      _uploadFile = file;

      if (typeimage(file.type)) {
        _uploadBase64 = await toBase64(file);
        _isValidFile = true;
        _uploadType = file.type;
        namefile = file.name;
      } else {
        _isValidFile = false;
        _uploadType = null;
        namefile = "error de tipo de archivo ";
        MsgNotify("Tipo de Archivo debe ser BMP/JPG/JPEG/PNG", "error");
      }

      var elem = document.getElementsByClassName('custom-file-label')[0];
      elem.innerText = namefile;

    }

    //-----------------------------------------------------
    function typeimage(type) {
      if (type == "image/bmp" || type == "image/png" || type == "image/jpg" ||
        type == "image/jpeg" || type == "application/pdf") {
        return true;
      }
      return false;
    }

    //-----------------------------------------------------
    const toBase64 = file => new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });

    //-----------------------------------------------------
    this.getUploadBase64 = function () {
      return onlyBase64(_uploadBase64);
    };

    function onlyBase64(str) {
      var newstr = str.replace('data:application/pdf;base64,', '');
      newstr = newstr.replace('data:image/png;base64,', '');
      newstr = newstr.replace('data:image/jpg;base64,', '');
      newstr = newstr.replace('data:image/jpeg;base64,', '');
      newstr = newstr.replace('data:image/bmp;base64,', '');
      newstr = newstr.replace(' ', '+');
      return newstr;
    }

    //-----------------------------------------------------
    this.isUploadBase64 = function () {
      return _isValidFile;
    };

    //-----------------------------------------------------
    this.getUploadType = function () {
      return _uploadType;
    };

    //-----------------------------------------------------
    this.sendFileServer = function (cPhp, bCallback, params, uCargo) {

      var cUrl = cPhp + '?' + Math.random();

      try {
        $.ajax({
          url: cUrl,
          dataType: 'text',
          cache: false,
          contentType: false,
          processData: false,
          data: params,
          type: 'post',
          beforeSend: function () {
            loaddingOn();
          },
          complete: function () {
            loaddingOff();
          }
        })
          .done(function (data, textStatus, jqXHR) {

            var respond = JSON.parse(data);

            loaddingOff();

            if (typeof bCallback == 'function') {
              var fnparams = [respond, uCargo, textStatus, jqXHR];
              bCallback.apply(null, fnparams);
            }

          })
          .fail(function (jqXhr, textStatus, errorThrown) {
            //	Si ha saltado un error pero hay un status 200 mostraremos el mensaje 
            if (jqXhr.status == 200) {
              Msg_AjaxError(cPhp, jqXhr, textStatus);
            } else {
              TWeb_Error_Dispatcher(jqXhr, cPhp);
            }
          });
      } catch (ex) {

        MsgError(ex, 'Error Ajax');

      }

    };

    //-----------------------------------------------------
    this.Self = function() {
      return null;
    }
    
    //-----------------------------------------------------
    this.init = function( cId ) {
      
      if ( cId ) {

        this.props    = this.getprops();
        this.idModal  = this.props.id; 
        this.title    = this.props.title;
        
        aModal.image        = [];
        aModal.btnfunction  = [];
        aModal.onvalid      = [];

      } 

    };

    //-----------------------------------------------------

    // Inicializa la CLASS
    this.init( _idModal );
    
  }
}

//----------------------------------------------------------------

class Modal {

  constructor(props) {

    this.props = {
      title: "",
      body: "",
      footer: "",
      modalClass: "fade",
      modalDialogClass: "",
      options: null,

      // Events:
      onCreate: null,
      onShown: null,
      onDispose: null,
      onSubmit: null // Callback of $.showConfirm(), called after yes or no was pressed
    };
    for (var prop in props) {
      // noinspection JSUnfilteredForInLoop
      this.props[prop] = props[prop];
    }
    this.id = "bootstrap-show-modal-" + indexModal;
    indexModal++;
    this.show();
    if (this.props.onCreate) {
      this.props.onCreate(this);
    }
  }

  createContainerElement() {
    var self = this;
    this.element = document.createElement("div");
    this.element.id = this.id;
    this.element.setAttribute("class", "modal " + this.props.modalClass);
    this.element.setAttribute("style", "overflow-y: auto");
    this.element.setAttribute("tabindex", "-1");
    this.element.setAttribute("data-backdrop", "static");
    this.element.setAttribute("role", "dialog");
    this.element.setAttribute("aria-labelledby", this.id);
    this.element.innerHTML = '<div class="modal-dialog ' + this.props.modalDialogClass + '" role="document">' +
      '<div class="modal-content">' +
      '<div class="modal-header" id="modal-header">' +
      '<h5 class="modal-title"></h5>' +
      '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
      // '<span aria-hidden="true">&times;</span>' +
      '<i class="far fa-times-circle"></i>' +
      '</button>' +
      '</div>' +
      '<div class="modal-body"></div>' +
      '<div class="modal-footer" id="modal-footer" style="background-color: white;"></div>' +
      '</div>' +
      '</div>';
    document.body.appendChild(this.element);
    this.titleElement = this.element.querySelector(".modal-title");
    this.bodyElement = this.element.querySelector(".modal-body");
    this.footerElement = this.element.querySelector(".modal-footer");
    $(this.element).on('hidden.bs.modal', function () {
      self.dispose();
    });
    $(this.element).on('shown.bs.modal', function () {
      if (self.props.onShown) {
        self.props.onShown(self);
      }
    });
  }

  show() {
    if (!this.element) {
      this.createContainerElement();
      if (this.props.options) {
        $(this.element).modal(this.props.options);
      } else {
        $(this.element).modal();
      }
    } else {
      $(this.element).modal('show');
    }
    if (this.props.title) {
      $(this.titleElement).show();
      this.titleElement.innerHTML = this.props.title;
    } else {
      $(this.titleElement).hide();
    }
    if (this.props.body) {
      $(this.bodyElement).show();
      this.bodyElement.innerHTML = this.props.body;
    } else {
      $(this.bodyElement).hide();
    }
    if (this.props.footer) {
      $(this.footerElement).show();
      this.footerElement.innerHTML = this.props.footer;
    } else {
      $(this.footerElement).hide();
    }
  }

  hide() {
    $(this.element).modal('hide');
  }
  dispose() {
    $(this.element).modal('dispose');
    document.body.removeChild(this.element);
    if (this.props.onDispose) {
      this.props.onDispose(this);
    }
  }
}

//------------------- FINAL -----------------------------