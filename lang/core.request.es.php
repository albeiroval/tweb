<?php 

	return array( 
		'required' 	=> 'Parámetro {parameter} no existe',
		'is_numeric'=> 'Parámetro {parameter} no es numérico',		
		'max_len' 	=> 'Parámetro {parameter} ha de tener una longitud máxima de {len}',
		'min_len' 	=> 'Parámetro {parameter} ha de tener una longitud minima de {len}',
		'min' 		=> 'Parámetro {parameter} tiene un valor inferior de {min}',
		'contain' 	=> 'Parámetro {parameter} no contiene valores permitidos',
		'url' 		=> 'Parámetro {parameter} no tiene formato de Url',
		'mail' 		=> 'Parámetro {parameter} no tiene formato de Mail',
		'ip' 		=> 'Parámetro {parameter} no tiene formato de IP'
	);	

?>

