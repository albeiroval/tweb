<?php 

	return array( 
		'required' 	=> 'Paràmetre {parameter} no existeix',
		'is_numeric'=> 'Paràmetre {parameter} no és numèric',
		'max_len' 	=> 'Paràmetre {parameter} ha de tenir una longitud màxima de {len}',
		'min_len' 	=> 'Paràmetre {parameter} ha de tenir una longitud mínima de {len}',
		'min' 		=> 'Paràmetre {parameter} te un valor inferior de {min}',
		'contain' 	=> 'Paràmetre {parameter} no conté valors permesos',
		'url' 		=> 'Paràmetre {parameter} no te format d\'Url',
		'mail' 		=> 'Paràmetre {parameter} no te format de mail',
		'ip' 		=> 'Paràmetre {parameter} no te format d\'IP'
	);	

?>

