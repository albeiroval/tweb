/*
*	Class TDatatable
* Fecha  : 09/02/2022
* Autor  : Antonio Albeiro Valencia
* (C)(R) : Derechos Reservados, se prohibe el uso de esta libreria para otros fines distintos
*					al uso del framework TVALWEB
* Doucmrntation :
* https://datatables.net/examples/styling/semanticui.html
* https://phppot.com/jquery/calculate-sum-total-of-datatables-column-using-footer-callback/
*/

class TDataTable2 {

	constructor(cId, aOptions, cTwebPath, aData) {

		// variables globales
		var oGrid; // objeto datatable  
		var idTable 		= "#" + cId;
		var _Options 		= aOptions;
		var aColsTotal 	= aOptions.columnsTotal;
		var aColsFunc 	= aOptions.colsFunction;
		var nRowIndex 	= null;
		var bRowFocus 	= false;
		var vScrollX 		= aOptions.scrollX;
		var vScrollY 		= aOptions.scrollY;
		var cPathTWeb 	= "";
		
		// Tweb Path Lib Datatables
		if (cTwebPath != null) {
			cPathTWeb = cTwebPath + '/dataTables/';
		} else {
			cPathTWeb = '../../tweb/libs/dataTables/';
		}


		//---------------------------------------
		this.Init = function () {
			// key 13  ENTER
			// key 37  LEFT     
			// key 39  RIGHT
			// key 40  DOWN
			// key 38  UP     
			// key 37  LEFT  
			// var nKeys = [ 13, 37, 39 ];

			// Create DOM
			var cDocument = this.createDOM(_Options);
			var oParams = {};

			if (_Options.ajax) {
				oParams.ajax = _Options.ajax;
			}

			// set column checkbox
			if ( _Options.checkbox ) { 
				_Options.columns.splice(0, 0, { orderable: false,
																				className: 'select-checkbox',
																				targets:   0,
																				defaultContent: '',
                   											data: null
																				/* 
																				render: function(data, type) {
																					return type === 'display' ? '<input class="chk-select" type="checkbox">' : '';
																				}
																				*/
																			} );
			}												
			
			oParams.dom 						= cDocument;
			oParams.buttons 				= this.makeButtons(_Options);
			oParams.columns 				= _Options.columns;
			oParams.columnDefs 			= _Options.columnDefs;
			oParams.select          = { style: 'os', selector: 'td:first-child'};
			oParams.info 						= _Options.info;
			oParams.paging 					= _Options.paging;
			oParams.ordering 				= _Options.ordering;
			oParams.searching 			= _Options.searching;
			oParams.scrollCollapse 	= _Options.scrollCollapse;
			oParams.pageLength 			= _Options.pageLength;
			oParams.autoHeight 			= false,
			oParams.processing 			= true;
			oParams.keys 						= true;
			oParams.select 					= true;
			oParams.scrollY 				= vScrollY;
			oParams.deferRender 		= true;

			// oParams.scroller			= true;
			// oParams.fixedHeader	= true;

			if (vScrollX) {
				oParams.scrollX = true;
			} else {
				oParams.responsive = {
					details: {
						renderer: function (api, rowIdx, columns) {
							var data = $.map(columns, function (col, i) {
								return col.hidden ?
									'<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
									'<td>' + col.title + ':' + '</td> ' +
									'<td>' + col.data + '</td>' +
									'</tr>' :
									'';
							}).join('');
							return data ?
								$('<table/>').append(data) :
								false;
						}
					}
				};
			}

			oParams.language = {
				url: cPathTWeb + "spanish.json",
				search: '<i class="fa fa-search"></i>',
				searchPlaceholder: 'filtrar por ...',
				select: { "rows": "" }
			};

			// Function para personalizar columnas
			if (_Options.createdRow) {
				oParams.createdRow = function (row, data, index) {
					eval(_Options.createdRow);
				};
			}

			// Crea columnas Totales	
			oParams.fnFooterCallback = function (row, data, start, end, display) {
				var api = this.api();
				// converting to interger to find total
				var intVal = function (i) {
					return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
				};
				// computing column Total of the complete result 
				var aCols = aColsTotal;
				var aSuma = [];
				var index;

				if (aCols.length > 0) {
					var total = 0;
					for (index = 0; index < aCols.length; index++) {
						/* jshint -W083 */
						total = api.column(aCols[index].column)
							.data()
							.reduce(function (a, b) { return intVal(a) + intVal(b); }, 0);
						aSuma.push(total.toFixed(2));
					}
				}

				// Update footer by showing the total with the reference of the column index
				if (aCols.length > 0) {
					$(api.column(0).footer()).html('Total');
					for (index = 0; index < aCols.length; index++) {
						$(api.column(aCols[index].column).footer()).html(aCols[index].format + aSuma[index]);
					}
				}
			};

			// Crea la datatable con los parametros
			oGrid = $(idTable).DataTable(oParams);

			if (aData) {
				this.SetData(aData);
			}

		};

		//---------------------------------------
		// https://datatables.net/reference/option/dom
		this.createDOM = function (oOptions) {
			/*
			DOM :
				[B] = Buttons
					[l] = length changing input control
					[f] = filtering input
					[t] = The table!
					[i] = Table information summary
					[p] = pagination control
					[r] = processing display element
  
			Results in:
			<div class="top">
			{information}
			</div>
			{processing}
			{table}
			<div class="bottom">
			{filter}
			{length}
			{pagination}
			</div>
			<div class="clear"></div>
		  
			Example :
					$('#example').dataTable( {
					"dom": '<"top"i>rt<"bottom"flp><"clear">'
					} );

			*/

			var cDOM = '<"top"Bf>rt<"bottom"<"info"i>p><"clear">';

			if ( !oOptions.info ) {
				cDOM = cDOM.replace("i", "");
			}

			if (oOptions.hideButtons) {
				cDOM = cDOM.replace("B", "");
			}

			if (oOptions.hideFilter) {
				cDOM = cDOM.replace("f", "");
			}

			return cDOM;

		};

		//---------------------------------------
		// https://datatables.net/extensions/buttons/
		// https://datatables.net/extensions/buttons/examples/styling/icons.html
		// https://jsfiddle.net/6639xcj4/
		this.makeButtons = function( oOptions ) {

			var buttons = oOptions.exportButtons;
			var aButtons = [];

			if (typeof buttons === "string") {
				aButtons.push( this.button_copy() );
				aButtons.push( this.button_csv() );
				aButtons.push( this.button_excel() );
				aButtons.push( this.button_pdf() );
				aButtons.push( this.button_print() );
			} else if (typeof oOptions.exportButtons === "object") {
        for (let index = 0; index < buttons.length; index++) {
					if (buttons[index] == "copy") { aButtons.push( this.button_copy() ); }
					if (buttons[index] == "csv") { aButtons.push( this.button_csv() ); }
					if (buttons[index] == "excel") { aButtons.push( this.button_excel() ); }
					if (buttons[index] == "pdf") { aButtons.push( this.button_pdf() ); }
					if (buttons[index] == "print") { aButtons.push( this.button_print() ); }
				}
			}	

			/*
				Button Custom : ['copy', 'csv', 'excel', 'pdf', 'print',
					{
							text: "My button",
							action: function ( e, dt, node, config ) {
							alert( "Button activated" );
					}
			*/
			return aButtons;

		};

		//---------------------------------------
		this.button_copy = function() {
			return { 
					extend: 'copyHtml5',
					text: '<div><i class="fa fa-copy" style="margin-right:4px;"></i>Copiar<div>',
					titleAttr: 'Copiar'
				};
		}

		this.button_excel = function() {
			return {
				extend: 'excelHtml5',
				text: '<div><i class="fa fa-file-excel" style="margin-right:4px;"></i>Excel<div>',
				titleAttr: 'Excel'
			};
		}	

		this.button_csv = function() {
			return {
				extend: 'csvHtml5',
				text: '<div><i class="fa fa-file-csv" style="margin-right:4px;"></i>Csv</div>',
				titleAttr: 'CSV'
			};	
		}

		this.button_pdf = function() {
			return {
				extend: 'pdfHtml5',
				text: '<div><i class="fa fa-file-pdf" style="margin-right:4px;"></i>Pdf<div>',
				titleAttr: 'PDF'
			};	
		}

		this.button_print = function() {
			return {
				extend: 'print',
				text: '<div><i class="fa fa-print" style="margin-right:4px;"></i>Imprimir<div>',
				titleAttr: 'Imprimir'
			};	
		}

		//---------------------------------------
		this.Native = function () {
			return oGrid;
		};

		//---------------------------------------
		this.Len = function () {
			// var table = $(idTable ).DataTable();
			return oGrid.data().length;
		};

		//---------------------------------------
		this.GetData = function () {
			return oGrid.data().toArray();
		};

		//--------------------------------------- 
		this.SetData = function (data) {
			oGrid.clear().rows.add(data).draw();
    };

		//---------------------------------------
		// https://datatables.net/reference/api/ajax.url().load()
		this.setJsonfile = function (file) {
			// var table = $(idTable).DataTable();
			oGrid.clear().draw();
			oGrid.ajax.url(file).load();
		};

		//---------------------------------------
		this.JsonToArray = function (obj) {
			var arr = [];

			if (typeof obj === 'object') {
				for (var i = 0; i < obj.length; i++) {
					arr.push(Object.values(obj[i]));
				}
			}

			return arr;
		};

		//---------------------------------------
		this.InsertRow = function (data) {
			oGrid.row.add(data).draw();
		};

		//---------------------------------------
		this.UpdateRow = function (data, nRow) {
			var index = this.currentRow(nRow);

			if (index != null) {
				oGrid.row(index).data(data).draw();
			}

		};

		//---------------------------------------
		this.DeleteRow = function (nRow) {
			var index = this.currentRow(nRow);

			if (index != null) {
				oGrid.row(index).remove().draw(false);
			}

		};

		//---------------------------------------
		this.GetRow = function (nRow) {
			var index = this.currentRow(nRow);

			if (index != null) {
				return oGrid.row(index).data();
			} else {
				return null;
			}

		};

		//---------------------------------------
		this.GetColValue = function (uCol, nRow) {
			var index = this.currentRow(nRow);

			if (index != null) {
				var oRow = this.GetRow(index);
				if (typeof uCol === 'number') {
					if (uCol >= 0 && uCol < Object.keys(oRow).length) {
						return Object.values(oRow)[uCol];
					} else {
						return null;
					}
				} else if (typeof uCol === 'string') {
					return oRow[uCol];
				}
				return null;
			} else {
				return null;
			}

		};

		//-----------------------------------------
		this.currentRow = function (nRow) {
			var uRet;

			if (nRow != null) {
				uRet = nRow;
			} else if (bRowFocus) {
				uRet = nRowIndex;
			} else {
				uRet = null;
			}

			return uRet;

		};

		//---------------------------------------
		/*
		* Para reemplazar la busqueda con input
		* var str =  oGrid.columns( column ).search( value ).draw();
		*/
		this.Search = function (column, search) {
			var lString = (typeof search === 'string') ? true : false;
			var seek = false;

			if (lString) {
				oGrid.column(column).data().filter(function (value, index) {
					if (value.trim() == search.trim()) {
						seek = true;
						return;
					} else {
						return;
					}
				});
			} else {
				oGrid.column(column).data().filter(function (value, index) {
					if (value == search) {
						seek = true;
						return;
					} else {
						return;
					}
				});
			}

			return seek;
		};

		//---------------------------------------
		this.IndexRow = function () {
			return nRowIndex;
		};

		//---------------------------------------
		this.hide = function (aCols) {
			console.log("columns", aCols);
			if (Array.isArray(aCols)) {
				oGrid.columns(aCols).visible(false);
			}
		};

		//---------------------------------------
		this.GetHeaders = function () {
			return oGrid.columns().header().toArray().map(function (x) { return x.innerText; });
		};

		//---------------------------------------
		this.Version = function () {
			return 'Tweb (+), TDatatable, version 1.1a';
		};

		//---------------------------------------
		this.NumColumns = function () {
			return oGrid.columns().nodes().length;
		};

		//---------------------------------------
		/*
		*	this.EditCells
		* 1 = https://stackoverflow.com/questions/55386847/make-all-cells-editable-in-datatables
		* 2 = https://cmsdk.com/javascript/how-to-fix-39cannot-read-property-39makecellseditable39-of-undefined39-error.html
		* 3 = https://jsfiddle.net/kesv478v/
*     https://javascriptio.com/view/1295813/how-can-i-kill-all-my-functions-after-execution
		*
		*/
		// Edit the cell clicked upon using inline editing: 
		this.MakeCellsEditable = function (aColumns) {

			if (!aColumns) {
				var nLen = this.NumColumns();
				aColumns = [];
				var i;
				for (i = 0; i < nLen; i++) {
					aColumns.push(i);
				}
			}

			oGrid.MakeCellsEditable({
				"onUpdate": this.onUpdate,
				"inputCss": 'my-input-class',
				"columns": aColumns,
				"confirmationButton": {
					"confirmCss": 'my-confirm-class',
					"cancelCss": 'my-cancel-class'
				}
			});

		};

		//---------------------------------------
		this.bPostedit = function (myCallbackFunction) {
			this.onUpdate = myCallbackFunction;
		};

		//-----------------------------------------
		this.TotalColumn = function (nCol) {
			var table = $(idTable).DataTable();
			var array = table.column(nCol).data().toArray();
			var total = 0;
			for (var index = 0; index < array.length; index++) {
				total += array[index];
			}
			return total;
		};

		//---------------------------------------
		// https://www.gyrocode.com/articles/jquery-datatables-how-to-navigate-rows-with-keytable-plugin/
		// http://live.datatables.net/horageru/1/edit
		this.onClick = function () {

			var table = $(idTable).DataTable();

			var i;
			var colIndex 	= null;
			var oCell 		= null;
			var cFunction = null;

			// Event table doubleclick
			table.on('dblclick', 'tbody td', function() {
				if ( !empty(_Options.doubleclick) ) {
					nRowIndex = table.row(this).index();
					bRowFocus = true;
					/* jshint -W061 */
					eval(_Options.doubleclick);
				}
			});

			// Event table click
			table.on('click', 'tbody td', function () {

				if ($(this).hasClass('selected')) {
					$(this).removeClass('selected');
					nRowIndex = null;
					bRowFocus = false;
				}
				else {
					table.$('td.selected').removeClass('selected');
					$(this).addClass('selected');
					nRowIndex = table.row(this).index();
					bRowFocus = true;
				}

				/*
				// valida si hay una funcion en la columna
				var oRow = table.cell(this).index();
				var colNum = oRow.column;
				for (i = 0; i < aColsFunc.length; i++) {
					if (colNum == aColsFunc[i][0]) {
						if (aColsFunc[i][2] == true) { // true = is button or Icon
							cFunction = aColsFunc[i][1];
							/-- jshint -W061 --/
							eval(cFunction);
						}
						break;
					}
				}
				*/

			});
			
			// Handle event when key enter
			table.on('key', function (e, datatable, key, cell, originalEvent) {
				if (key === 13) {
					oCell = cell;
					colIndex = cell.index().column;
					cFunction = null;
					nRowIndex = table.row(cell.index().row).index();

					var data = table.row(cell.index().row).data();
					var names = table.columns().header().toArray().map(function (x) { return x.innerText; });
					var header = names[colIndex];
					for (i = 0; i < aColsFunc.length; i++) {
						if (colIndex == aColsFunc[i][0]) {
							if (aColsFunc[i][2] == false) {
								cFunction = aColsFunc[i][1];
							}
							break;
						}
					}
					if (cFunction != null) {
						setTimeout(function () { JMsgPrompt(header, UpdateCell, cell.data()); }, 10);
					}
				}
			});

			// call function user in Cell
			function UpdateCell(data) {
				oCell.data(data).draw();
				if (cFunction != null) {
					/* jshint -W061 */
					eval(cFunction);
				}
			}

		};

		//-----------------------------------------
		// Inicia la datatable y asigna los eventos 
		this.Init();
		this.onClick();

	}

}

// FINAL