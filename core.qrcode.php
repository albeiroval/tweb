<?php

class TQrcode extends TControl {

  public $cId	      = '';
  public $nTop      = 0;
  public $nLeft     = 0;
  public $nWidth    = null;

  // text code
  private $textcode = 'test';

  // render method: 'canvas', 'image' or 'div'
  private $render   = 'div';

  // version range somewhere in 1 .. 40
  private $minversion = '3';

  // error correction level: 'L', 'M', 'Q' or 'H'
  private $ecLevel = 'H';   
  
  // code color or image element
  private $fillcolor = '#000'; 

  // background color or image element, null for transparent background
  private $backcolor = '';

  // corner radius relative to module width: 0.0 .. 0.5
  private $radius = '0.5';

  // quiet zone in modules
  private $quiet = '0';

  // insert label in qr code 
  private $lbltext  = '';
  private $lblcolor = '';
  
  // insert image in qr code 
  private $image    = '';

  // modes
  // 0: normal
  // 1: label strip
  // 2: label box
  // 3: image strip
  // 4: image box
  private $mode     = '2';
  
  //----------------
	
  public function __construct( $oWnd, $cId = '', $nTop = 0, $nLeft = 0, $text = 'test',  $nWidth = null ) {

    $cId      = TDefault( $cId,     'qrcode' );
    $nWidth   = TDefault( $nWidth,  300 );
    $nHeigth  = $nWidth;
        
    parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeigth );  

    $this->cId      = $cId;
    $this->cControl = 'tqrcode';
    $this->nTop     = $nTop;
    $this->nLeft    = $nLeft;
    $this->textcode = $text;

  }  

  //----------------

  // render method: 'canvas', 'image' or 'div'
  public function type_div() { $this->render = 'div'; }
  public function type_canvas() { $this->render = 'canvas'; }
  public function type_image() { $this->render = 'image'; }

  // version range somewhere in 1 .. 40
  public function minversion( $minversion = '3' ) { $this->minversion = $minversion; }

  // error correction level: 'L', 'M', 'Q' or 'H'
  public function eclevel( $ecLevel = 'H' ) { $this->ecLevel = $ecLevel; }   
  
  // code color or image element
  public function fillcolor( $color = '#000' ) { $this->fillcolor = $color; }  

  // background color or image element, null for transparent background
  public function backcolor( $color = 'null' ) { $this->fillcolor = $color; }  

  // corner radius relative to module width: 0.0 .. 0.5
  public function radius( $radius = '0.5' ) { $this->radius = $radius; }

  // quiet zone in modules
  public function quiet( $quiet = '0' ) { $this->quiet = $quiet; }

  // modes
  // 0: normal
  // 1: label strip
  // 2: label box
  // 3: image strip
  // 4: image box
  public function insert_label( $label = '', $color = '', $mode = '2' ) { 
    $this->lbltext  = $label;
    $this->lblcolor = $color;
    $this->mode     = $mode;
  }  

  public function insert_image( $image  = 'null',  $mode = '2' ) { 
    $this->image = $image;
    $this->mode  = $mode;
  }

  //----------------

	public function Activate() {

    // Java Script library jquery.qrcode
    // https://larsjung.de/jquery-qrcode/
    $cJS  = '<script src="' . TWEB_PATH_LIBS . '/qrcode/jquery-qrcode.min.js"></script>';
    echo $cJS; 

    // div Contenedor
    $cHtml  = '<div id="' . $this->cId  . '" ';
    $cHtml .= ' style="position: absolute; ';
    $cHtml .= ' top: ' . $this->nTop . 'px; left: ' . $this->nLeft . 'px;'; 
    $cHtml .= ' width: ' . $this->nWidth . 'px; height:' . $this->nHeight . 'px;">';
    $cHtml .= '</div>';

    echo $cHtml;

    // Java Script qrcode
    $cJS  = "<script>";

    // var options
    $cJS .= " var options = { "                             . PHP_EOL;
    $cJS .= "     render: '" . $this->render . "',"         . PHP_EOL;
    $cJS .= "     minVersion: " . $this->minversion . ","   . PHP_EOL;
    $cJS .= "     maxVersion: 40,"                          . PHP_EOL;
    $cJS .= "     ecLevel: '" . $this->ecLevel  . "',"      . PHP_EOL;
    $cJS .= "     left: 0,"                                 . PHP_EOL;
    $cJS .= "     top: 0,"                                  . PHP_EOL;
    $cJS .= "     size: " . $this->nWidth  . ","            . PHP_EOL;
    $cJS .= "     fill: '" . $this->fillcolor  . "',"       . PHP_EOL;
    $cJS .= "     background: '" . $this->backcolor  . "'," . PHP_EOL;
    $cJS .= "     text: '" . $this->textcode  . "',"        . PHP_EOL;
    $cJS .= "     radius: " . $this->radius  . ","          . PHP_EOL;
    $cJS .= "     quiet: " . $this->quiet  . ","            . PHP_EOL;
    $cJS .= "     mode: " . $this->mode  . ","              . PHP_EOL;
    $cJS .= "     mSize: 0.1,"                              . PHP_EOL;
    $cJS .= "     mPosX: 0.5,"                              . PHP_EOL;
    $cJS .= "     mPosY: 0.5,"                              . PHP_EOL;
    $cJS .= "     label: '" . $this->lbltext  . "',"        . PHP_EOL;
    $cJS .= "     fontname: 'sans',"                        . PHP_EOL;
    $cJS .= "     fontcolor: '" . $this->lblcolor  . "',"   . PHP_EOL;
    $cJS .= "     image: '" . $this->image . "',"           . PHP_EOL; 
    $cJS .= "   }"                                          . PHP_EOL;

    // function create qr code
    $cJS .= " $(function() {"                                      . PHP_EOL;
    $cJS .= '   $("#' . $this->cId . '").empty().qrcode(options);' . PHP_EOL;
    $cJS .= " });"                                                 . PHP_EOL;

    $cJS .= "</script>";

    echo $cJS;
    
  }  

}  

?>