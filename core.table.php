<?php
/*
*	 Class TABLE
*  Fecha  : 10-07-2021
*  Autor  : Antonio Albeiro Valencia
*  (C)(R) : Derechos Reservados, se prohibe el uso de esta libreria para otros fines distintos
*						al uso del framework VALWEB
*/

class BTable extends TControl {

  private $aColumns      = [];
  private $dataTable     = [];
  private $aOptionsTable = [];
  private $aButtons      = [];
  private $aTitle        = null;

  function __construct( $oWnd, $cId = 'table', $nTop = 0, $nLeft = 0, $nWidth = '80%', $nHeight = '80%', $cClass = '', $aData = [] ) {
    
    $this->cClass = $cClass;

    parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight );

    $this->aOptionsTable= [ "filter"      => true,
                            "pagin"       => true,
                            "cardview"    => true,
                            "height"      => 540,
                            "header"      => "",
                            "footer"      => "",
                            "btntool"     => false,
                            "clickrow"    => "",
                            "dblclickrow" => "" ];

    $this->dataTable = $aData;                            

  }

  //----------------------------------------------

  public function AddCol( $cId, $cTitle, $cPicture = '', $cAlign = AL_LEFT, $isSort = false, 
                          $cTotal = false, $isTotal = false, $cClass = '' ) {

    $cFooter = (gettype($cTotal) === "boolean") ? $cTotal : false;
    $isTotal = (gettype($isTotal) === "boolean") ? $isTotal : false;
    $isSort  = (gettype($isSort) === "boolean") ? $isSort : false;

    $cFunction = '';
    if ( $cFooter ) {
      $cFunction = 'totalTextFormatter';
    } elseif( $isTotal ) {
      $cFunction = 'totalPriceFormatter';
    }

    $this->isTable = true;
    $this->aColumns[] = [ "field"     => $cId,
                          "title"     => $cTitle,
                          "format"    => $cPicture,
                          "align"     => $cAlign,
                          "sortable"  => $isSort,
                          "footer"    => $cFunction,
                          "class"     => $cClass,
                          "icon"      => false ];

  }

  //----------------------------------------------

  public function AddColIcon( $cId, $cTitle, $cPicture = '' ) {

    $this->isTable = true;
    $this->aColumns[] = [ "field"     => $cId,
                          "title"     => $cTitle,
                          "format"    => $cPicture,
                          "icon"      => true ];

  }

  //---------------------------------------------

	public function Title( $cLabel = "DATATABLE", $cClrText = "black", $cClrBack = "aqua", $cClass = "" ) {
		$this->aTitle = [ "label" => $cLabel,
											"color" => $cClrText,
											"fondo" => $cClrBack,
										  "class" => $cClass ];
	}

  //----------------------------------------------

  public function Paging( $option = true) {
    $this->aOptionsTable["pagin"] = (gettype($option) == "boolean") ? $option : true;
  }

  //----------------------------------------------

  public function HeaderStyle( $cAction = "" ) {
    $this->aOptionsTable["header"] = TDefault($cAction, "");
  }

  //----------------------------------------------

  public function FooterStyle( $cAction = "" ) {
    $this->aOptionsTable["footer"] = TDefault($cAction, "");
  }

  //----------------------------------------------

  public function CardView( $option = true) {
    $this->aOptionsTable["cardview"] = (gettype($option) == "boolean") ? $option : true;
  }

  //----------------------------------------------

  public function Filter( $option = true ) {
    $this->aOptionsTable["filter"] = (gettype($option) == "boolean") ? $option : true;
  }

  //----------------------------------------------

  public function Height( $nHeight = 540 ) {
    $this->aOptionsTable["height"] = (gettype($nHeight) == "integer") ? $nHeight : 540;
  }

  //----------------------------------------------

  public function Export( $option = true) {
    $this->aOptionsTable["btntool"] = (gettype($option) == "boolean") ? $option : true;
  }

  //----------------------------------------------

  public function ClickRow( $cFunction = "" ) {
    $this->aOptionsTable["clickrow"] = $cFunction;
  }

  //----------------------------------------------

  public function DblClickRow( $cFunction = "" ) {
    $this->aOptionsTable["dblclickrow"] = $cFunction;
  }

  //----------------------------------------------

  public function AddButton( $cId = '', $cText = '', $cAction = '', $cIcon = '' ) {
    $this->aButtons[] = [ "id" => $cId, "label" => $cText, "action" => $cAction, "icon" => $cIcon ];
  }

  //----------------------------------------------

  public function Activate() {

    $this->CreateHtml();

  }
  
  //----------------------------------------------
  /*
     https://examples.bootstrap-table.com/#options/trim-on-search.html#view-source
     https://examples.bootstrap-table.com/#column-options/detail-formatter.html#view-source
     https://bootstrap-table.com/docs/api/table-options/
     https://examples.bootstrap-table.com/#column-options/events.html#view-source
  */
      
  private  function CreateHtml() {

    /* Library JAVASCRIPT */
		$cScript = '<script src="' . TWEB_PATH . 'core.table.js"></script>';
		echo $cScript;

    $count = count($this->aColumns);
    if ( $count > 0 ) {

      // Create div
      $cHtml  = '<div class="ctrl-tab ' . $this->cClass . '" style="';
      $cHtml .= ' position: absolute;';
      $cHtml .= ' top : ' . $this->nTop . ';';
      $cHtml .= ' left  : ' . $this->nLeft . ';';
      $cHtml .= ' width : ' . $this->nWidth . ';';
      $cHtml .= ' max-width : ' . $this->nWidth . ';';
      $cHtml .= ' height: ' . $this->nHeight . '">';

      // Create Title
      if ($this->aTitle) {
        $cHtml .= '<div style="color:'.$this->aTitle["color"];
        $cHtml .= ' ;background-color:'.$this->aTitle["fondo"];
        $cHtml .= ' ;width: 100%;text-align:center">';
        $cHtml .= '<h3 class="'.$this->aTitle["class"].'">'.$this->aTitle["label"].'</h3>';
        $cHtml .= '</div>';	
      }

      $buttons = count($this->aButtons);
      if ( $buttons > 0 ) {
        $cHtml .= '<div class="btn-group btn-custom ctrl-tab" role="group">';
        for ($i=0; $i < $buttons; $i++) { 
          $item = $this->aButtons[$i];
          $cHtml .= '<button type="button" class="btn btn-primary"';
          $cHtml .= ' id="'.$item["id"].'" onclick="'.$item["action"].'">';
          if ( !empty($item["icon"]) ) {
            $cHtml .= ' <i class="fa '.$item["icon"].'"></i> ';
          }
          $cHtml .= $item["label"];
          $cHtml .= '</button>';  
        }
        $cHtml .= '</div>';
      }  

      $cHtml .= '<table ';
      $cHtml .= ' id="' .$this->cId. '"';
      $cHtml .= ' data-toggle="table"';
      $cHtml .= ' data-cache="true"';
      $cHtml .= ' data-click-to-select="true"';
      $cHtml .= ' data-id-field="id"';
      $cHtml .= ' data-height="' .$this->aOptionsTable["height"]. '"';
      $cHtml .= ' data-header-style="' .$this->aOptionsTable["header"].'"';
      $cHtml .= ' data-footer-style="' .$this->aOptionsTable["footer"].'"';
      $cHtml .= ' data-checkbox-header="false"';
      $cHtml .= ' data-show-footer="true"';
      $cHtml .= ' data-key-events="true"';
      
      // Begin attribs Table
      if ( $this->aOptionsTable["filter"] == true ) {
        $cHtml .= ' data-search="true"';
        $cHtml .= ' data-trim-on-search="false"';   // Evita eliminar el key space
        $cHtml .= ' data-search-highlight="true"';
        // $cHtml .= ' data-search-on-enter-key="true"';
      }

      if ( $this->aOptionsTable["pagin"] == true ) {
        $cHtml .= ' data-page-list="[10, 25, 50, 100, all]"';
        $cHtml .= ' data-pagination="true"';
        $cHtml .= ' data-show-pagination-switch="true"';
      }  
      
      if ( $this->aOptionsTable["cardview"] == true ) {
        $cHtml .= ' data-mobile-responsive="true"';
        $cHtml .= ' data-card-view="true"';
      } else {
        $cHtml .= ' data-card-view="false"';
      }

      if ( $this->aOptionsTable["btntool"] == true ) {
        $cHtml .= ' data-show-print="true"';
        $cHtml .= ' data-show-export="true"';
        $cHtml .= ' data-buttons-class="primary"';
      }

      // End Attribs Table
      $cHtml .= '>';

      $cHtml .= '<thead>';
      $cHtml .= ' <tr>';
      $cHtml .= '   <th data-checkbox="true"></th>';
      
      for ($i=0; $i < $count; $i++) { 
        
        $row = $this->aColumns[ $i ];  

        $cHtml .= '<th data-field="'.$row["field"].'"';

        if ( !empty($row["format"]) ) {
          $cHtml .= ' data-formatter="'.$row["format"].'"';
        }

        if ( !$row["icon"] ) {

          // $cHtml .= ' data-editable="true"';

          if ( !empty($row["align"]) ) {
            $cHtml .= ' data-align="'.$row["align"].'"';
          }

          if ( $row["sortable"] ) {
            $cHtml .= ' data-sortable="true"';
          }

          if ( !empty($row["footer"]) ) {
            $cHtml .= ' data-footer-formatter="'.$row["footer"].'"';
          }

          if ( !empty($row["class"]) ) {
            $cHtml .= ' class="'.$row["class"].'"';
          }

        }  
                          
        $cHtml .= '>'.$row["title"].'</th>';
      }

      $cHtml .= '  </tr>';  
      $cHtml .= ' </thead>';
      $cHtml .= '</table>';

      // End Table
      $cHtml .= '</div>';
      
    } else {
      $cHtml = "";
    }  

    if ( !empty($cHtml) ) {

      // console_php($cHtml);  
      echo $cHtml;

      $cJS  = '<script>';
      $cJS .= '$(function() {'        . PHP_EOL;
      $cJS .= '  var options = {};'   . PHP_EOL;
      $cJS .= '  options.clickrow    = "'.$this->aOptionsTable["clickrow"].'";'     . PHP_EOL;
      $cJS .= '  options.dblclickrow = "'.$this->aOptionsTable["dblclickrow"].'";'  . PHP_EOL;
      $cJS .= '  var $oGrid = new BTable("'.$this->cId.'", options);'               . PHP_EOL;
      $cJS .= '  $oGrid.create('.json_encode($this->dataTable).', true);'           . PHP_EOL;
      $cJS .= '  var oCtrl = new TControl();'                                       . PHP_EOL;
		  $cJS .= '  oCtrl.ControlInit("'.$this->cId.'", $oGrid, "BTable" );'           . PHP_EOL;
      $cJS .= '});' . PHP_EOL;
      $cJS .= '</script>';
      echo $cJS;

    }
    
  }
  
}

?>