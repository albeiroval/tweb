/*
*   https://github.com/paynen/slickgrid-totals-plugin
*/

var TGrid = function( cId, columns, options, lPagination, lGroup, lHasTotals ) {

  this.oGrid 			  = null;
  this.dataView		  = null;
  this.cId   			  = cId ;
  this.columns		  = columns ;
  this.options		  = options ;
  this.lPagination	= lPagination ;
  this.nNewItem  		= 0 ;
  this.cFuncError 	= 'MsgInfo';
  this.bDblClick 		= '';
  this.bSelect 		  = '';		// The same bDblClick
  this.bChange 		  = '';	
  this.bBeforeEdit	= '';
  this.bPostEdit 		= '';	
  this.cFuncFilter	= '';
  this.lRowOverShow	= false;
  this.lGroup			  = lGroup;	
  this.lHasTotals 	= lHasTotals;	

  var aKeys		  = {};	
  var oUpdate		= {};
  var oCss		  = {};
  var oGrid 		= null;
  var dataView 	= null;

  var oObj		= this;

  this.Init	= function() {	

  // var data 	= [];		
  // var cKey	= this.cKey ;	

  var groupItemMetadataProvider = null;

  //		this.oGrid 	= new Slick.Grid( "#" + this.cId, data, this.columns, this.options );			

  if ( this.lGroup == false  ) {
    dataView 	= new Slick.Data.DataView();			
  } else {			

    groupItemMetadataProvider = new Slick.Data.GroupItemMetadataProvider();

    dataView = new Slick.Data.DataView({
      groupItemMetadataProvider: groupItemMetadataProvider,
      inlineFilters: false		// ???
    });								
  }	

  if ( this.lHasTotals ) {
    var dataProvider = new TotalsDataView(dataView, columns);
    oGrid = new Slick.Grid( "#" + this.cId, dataProvider, this.columns, this.options );
    oObj.dataProvider = dataProvider;
  } else {
    oGrid = new Slick.Grid( "#" + this.cId, dataView, this.columns, this.options );
  }
  
  if ( this.lGroup  ) {		
    oGrid.registerPlugin(groupItemMetadataProvider);
  }	
  
  // FOOTER
  
  if ( this.lHasTotals ) {
    grid = oGrid;
    //var totalsPlugin = new TotalsPlugin( 16 );
    // var totalsPlugin = new TotalsPlugin($.getScrollbarWidth());
    var totalsPlugin = new TotalsPlugin();
    oGrid.registerPlugin(totalsPlugin);			
  }
  
  //	----------------
  
  if ( this.lPagination ) {
    var cId_Pager  = this.cId + '_pager'; // OJO. Igual que en tgrid.php
  
    var pager = new Slick.Controls.Pager( dataView, oGrid, $("#" + cId_Pager ));	
  }

  oGrid.setSelectionModel(new Slick.RowSelectionModel( {selectActiveRow: true } ));		// Barra de Registre 		

  dataView.onRowCountChanged.subscribe(function (e, args ) {
    oGrid.updateRowCount();
    oGrid.render();
    
    if ( oObj.lHasTotals ) {
      dataProvider.updateTotals();
      totalsPlugin.render();			
    }			
  });

  dataView.onRowsChanged.subscribe(function (e, args) {
    oGrid.invalidateRows(args.rows);
    oGrid.render();
    
    if ( oObj.lHasTotals ) {
      dataProvider.updateTotals();
      totalsPlugin.render();			
    }
  });		
  
  /*		PENDENT. Falta veure com desactivar quan estic editant....		
    oGrid.onKeyDown.subscribe( function (e, args) {

    console.log( e );
    console.log( args );
    
    if (e.keyCode == 13 ){
      alert('p');
    }			
  
    });
  */		

  oGrid.onCellChange.subscribe(function (e,args) { 		
      
    //	'_id' será un identificador único interno. Cuando cargamos un set de datos, cada
    //	row tendra un _id que será un identificador de la fila. NO ES el posible ID que 
    //	identifique el registro de la base de datos.
       
      var cKey =  args.item._id;
     
    //	Si modificamos una celda es que ya existe y en este caso sera un [U]pdate, en
    //	caso contrario puede ser que la celda sea un U/I, es por eso que solo actualizamos
    //	el item...
       
      if( oUpdate[ cKey ] === undefined ) {				 
        oUpdate[ cKey ] = [ 'U' , args.item ];	// [U]pdate
      } else {					
        oUpdate[ cKey ][ 1 ] = args.item;				
      }
      
      if ( oObj.lHasTotals ) {
        dataProvider.updateTotals();
        totalsPlugin.render();			
      }

      if ( oObj.lGroup ) {
        dataView.refresh();
      }
      
      if ( oObj.bPostEdit == '' ) {
        return false;
      } else {

        var fn = window[oObj.bPostEdit];

        if (typeof fn === "function") {				
          var fnparams = [ args, e ];
          fn.apply(null, fnparams );								
        }								
      }
      
      
  });

  oGrid.onActiveCellChanged.subscribe( function (e, args ){

    if ( oObj.bChange == '' )
      return false;

    var fn = window[oObj.bChange];
              
    if (typeof fn === "function") {				
      var fnparams = [ args, e ];
      fn.apply(null, fnparams );								
    }							
    
  });

  dataView.onRowCountChanged.subscribe(function (e, args) {

    oGrid.updateRowCount();
    oGrid.render();
    
    if ( oObj.cFuncLenChanged == '' )
      return false;

    var fn = window[oObj.cFuncLenChanged];

    if (typeof fn === "function") {				
      var fnparams = [ args ] ;
      fn.apply(null, fnparams );								
    }			
  });	

  oGrid.onBeforeEditCell.subscribe( function (e, args ){

    if ( oObj.bBeforeEdit == '' ) {
      return true;
    } 

    var fn = window[oObj.bBeforeEdit];

    if (typeof fn === "function") {				
      var fnparams = [ args, e ];
      return fn.apply( null, fnparams );								
    }							
    
  });			
  
  oGrid.onDblClick.subscribe(function (e, args){
  
    if ( oObj.bDblClick == '' )
    
      if ( oObj.bSelect !== '' ) {
        oObj.bDblClick = oObj.bSelect;
      } else {
        return false;		
      }
      
    /*
    var cell = oGrid.getCellFromEvent(e)
    var row = cell.row;
    var col = cell.cell;
    */

    var fn = window[oObj.bDblClick];

    if (typeof fn === "function") {		

      var fnparams = [ args, e ];
      
      lEdit = fn.apply(null, fnparams );								
      
      /* PENDENT de trobar solucio. La idea es q si retorna false no editem	
        if ( !lEdit ) {
          args.grid.cancelCurrentEdit();
          e.stopPropagation();
          e.preventDefault();
        }
      */				
    }	
  });

  
  oGrid.onKeyDown.subscribe(function(e, args) {
      
    var fn = null;
    var fnparams = null;

    if ( e.which in aKeys ){
    
      var cFunc = aKeys[ e.which ];

      fn = window[ cFunc ];

      if (typeof fn === "function") {	
        fnparams = [ e, args ];
        fn.apply(null, fnparams );								
      }																	
    } else {			
  
      //if ( e.which == 13 && oObj.bDblClick !== '' ) {
      if ( e.which == 13 && oObj.bSelect !== '' ) {

        fn = window[ oObj.bSelect ];

        if (typeof fn === "function") {	
          fnparams = [ e, args ];
          fn.apply(null, fnparams );								
        }												
      }						
    }
    
  });

  
  oGrid.onHeaderClick.subscribe(function(e, args) {		
    
    console.log('Width', args.column.width );	// Para efectos de diseño...
    
    if ( e.ctrlKey ) {			
    
      /*
      var cTitle 	= args.column.name
      var cField 	= args.column.field

      var cSearch = MsgGet( '', cTitle )
      
      if ( cSearch !== '' ) {					

        oObj.Search( cField, cSearch, true, false )							
      }				
      */
      
      var oOptions	= {};
      oOptions.title        = 'Search';
      oOptions.centered     = true;
      oOptions.modal        = true;
      oOptions.resizable    = false;
      oOptions.dialogextend = false;						
      //oOptions[ 'maximizable'	] = true;						
      //oOptions[ 'minimizable'	] = false;						
      //oOptions[ 'collapsable' ] = false;					
      var oPar = {};
      oPar.cTitle   = args.column.name;
      oPar.cField   = args.column.field;										
      oPar.cId_Grid = oObj.cId; 										
        
      TLoadDialog( 'login', TWEB_PATH + '/plug_search.php', oPar, oOptions );
      
    }			
  });

  oGrid.onMouseEnter.subscribe(function(e, args){	

    if ( oObj.lRowOverShow == false )
      return null;		
  
    var cell = oGrid.getCellFromEvent(e),
      param = {},
      columnCss = {};
      
      for(var index in columns){
        var id = columns[index].id;
        columnCss[id] = 'fw_highlighter_style';
      }
      param[cell.row] = columnCss;
      args.grid.setCellCssStyles("row_highlighter", param);
    });
    
/*		  
      oGrid.onMouseLeave.subscribe(function(){
    console.log("Still firing");
  })		  
*/		
  
  
  // Subscribe to the grid's onSort event.
  // It only gets fired for sortable columns, so make sure your column definition has `sortable = true`.
  oGrid.onSort.subscribe(function(e, args) {
  
    if ( e.ctrlKey ) {				
      
    } else {				
    
      // args.multiColumnSort indicates whether or not this is a multi-column sort.
      // If it is, args.sortCols will have an array of {sortCol:..., sortAsc:...} objects.
      // If not, the sort column and direction will be in args.sortCol & args.sortAsc.

      // We'll use a simple comparer function here.
      var comparer = function(a, b) {
      
        var lReturn = null;
        var x; 
        var y;

        switch ( args.sortCol.sort ) {
        
        case 'date': 
        
          x = a[args.sortCol.field]; 
          y = b[args.sortCol.field];
          var ax = x.split('/');
          var ay = y.split('/');
          x = padLeft( ax[2], 4 ) + padLeft( ax[1], 2 ) + padLeft( ax[0], 2 );
          y = padLeft( ay[2], 4 ) + padLeft( ay[1], 2 ) + padLeft( ay[0], 2 );
          
          lReturn =  ( x == y ? 0 : (x > y ? 1 : -1));	
          
          break;
          
        case 'combo': 
          
          //	Preparamos tabla de valores
          
            aKey 	= args.sortCol.editorOptions.options.split( ',' ) ;
            aValues = args.sortCol.editorOptions.values.split( ',' ) ;
          
          //	Value A
          
            key_A 	= a[args.sortCol.field];
            nPos_A 	= aKey.indexOf( key_A );
            cValue_A = aValues[ nPos_A ] ;
            
          //	Value B
          
            key_B 	= b[args.sortCol.field];
            nPos_B 	= aKey.indexOf( key_B );
            
            cValue_B = aValues[ nPos_B ] ;
        
          //	Comparación

          lReturn = ( cValue_A == cValue_B ? 0 : ( cValue_A > cValue_B ? 1 : -1));
            
          break;						
          
        default:
        
          x = a[args.sortCol.field]; 
          y = b[args.sortCol.field];
          lReturn =  (x == y ? 0 : (x > y ? 1 : -1));	
          
          break;

        }	

        return lReturn;
      };

      // Delegate the sorting to DataView.
      // This will fire the change events and update the grid.
      dataView.sort(comparer, args.sortAsc);
    
    }	
    
  });		
  
  //		dataView.getItemMetadata = my_metadata(dataView.getItemMetadata);		
  
  /*		
    var c = this.cFuncError + '( args.validationResults.msg )' ;
  
    this.oGrid.onValidationError.subscribe(function (e, args) {					
      eval( c );
    });		
  */		

  oGrid.registerPlugin( new Slick.AutoTooltips({ enableForHeaderCells: true }) );
  oGrid.registerPlugin( new Slick.CellExternalCopyManager());	
  oGrid.render();

  this.SetCss();		

};	

this.Reset = function() {

  //	Clean oUpdate...

  oUpdate		= {};		

  //	Clean oCss...
  
  for (var key in oCss ) {
    oGrid.removeCellCssStyles( key );		
  }
  
  oCss = {};
};

this.SetTitle = function( cTitle ) {
  var cId 	= this.cId + '_title';	
  var o 		= $( '#' + cId );		
  var cLabel 	= '<label>' + cTitle + '</label>';
  
  o.html( cLabel );
};

this.SetKey  = function( nKey, cFunction ) {		
  aKeys[ nKey ] = cFunction;
};

this.SetStruct = function( aStr ) {		
  
  oGrid.setColumns( aStr ) ;
};

this.SetGroup = function( cGetter, func_formatter, aAggregators ) {

  if ( this.lGroup == false ) {		
    alert( 'TGrid Error. No existe ninguna columna con grupo' );
    return false;		
  }
  
  if (typeof( cGetter ) === 'undefined') {
    dataView.setGrouping([]);
    return null;
  } 

  /* Original crida ...
  
    dataView.setGrouping({
      getter: "clase",
      formatter: Grup_Title,
      //formatter: function (g) {
      //  return "Clase:  " + g.value + "  <span style='color:green'>(" + g.count + " items)</span>";
      //},
      aggregators: [
        new Slick.Data.Aggregators.Avg("edad"),
        new Slick.Data.Aggregators.Sum("total")
      ],
      aggregateCollapsed: false,		// Si true, apareix subtotals quan està plegat
      lazyTotalsCalculation: true		// ???
    });		
  
  */

  dataView.setGrouping({
    getter: cGetter,
    formatter: func_formatter,
    aggregators: aAggregators,
    aggregateCollapsed: false,		// Si true, apareix subtotals quan està plegat
    lazyTotalsCalculation: true		// ???
  });						
};

this.GroupCollapse 	= function() { 
  dataView.collapseAllGroups(); 
};

this.GroupExpand 	= function() { 
  dataView.expandAllGroups(); 
};

this.SetData 		= function( aData ) {	

  //	Reseteamos GRID. Asi forxamos eventos onChangeRow,count,...

    this.Reset();
    
    dataView.beginUpdate();
    dataView.setItems( [], 'id' );		
    dataView.endUpdate();
    oGrid.updateRowCount();
  
  //	-----------------------		
  
  if (typeof( aData ) === 'undefined') aData = [];		
  
  var  nTotal = aData.length ;
  
  for (var i = 0; i < nTotal; i++) {
  
    aData[ i ].id = '_' + i;
  
  }				
  
  // https://github.com/mleibman/SlickGrid/issues/313		(resizeCanvas(), updateRowCount())

  dataView.beginUpdate();

  dataView.setItems( aData , 'id' );		
  
  //	Filter...
  
    if ( this.cFuncFilter !== '' ) {
    
      // find object
      var fn = window[this.cFuncFilter];
                
      // is object a function?
      if (typeof fn === "function") {				

        dataView.setFilterArgs({			
          //searchString: searchString
          searchString: ''
        });
        
        dataView.setFilter( fn );													
      } else {
        MsgAlert( 'Error en definicion filtro de grid.\n\nNo existe la funcion ' + this.cFuncFilter );
      }
    }								

  //	-----------------------------------------------------

  dataView.endUpdate();

  oGrid.updateRowCount();		
  
  oGrid.invalidate();

  oGrid.render();			

  oGrid.resizeCanvas();

  oGrid.gotoCell( 0, 0 );	
  
};

  this.GetData = function() {	
    return dataView.getItems() ;	
  };

  this.GetItem = function( nId ){

    var nTotal = dataView.getLength();
  
    if ( nTotal == 0 ) {
      return null;
    }
    
    var oRow = oGrid.getActiveCell() ;	

    if ( oRow == null ) {
      return null;
    }		

    var oItem = dataView.getItem( oRow.row );
  
    return oItem;
  };

this.Search = function( cField, uVal, lGo, lExactly, lFromHere ){

  lGo 	  	= (typeof lGo 		  == 'boolean' ) ? lGo : true ;	
  lExactly	= (typeof lExactly	== 'boolean' ) ? lExactly : true ;	
  lFromHere	= (typeof lFromHere	== 'boolean' ) ? lFromHere : true ;	
  
  var oItem 	= null;
  var aData 	= dataView.getItems() ;		
  var nTotal  = aData.length ;				
  
  if ( nTotal == 0 ){
    return null;
  }		

  var nStart = ( lFromHere ) ? ( this.GetRow() + 1 ) : 0;
  var cCad1, cCad2;

  for (var i = nStart; i < nTotal; i++) {
  
    if ( lExactly ) {
    
      lFound = ( aData[i][ cField ] == uVal );			
    
    } else {
    
      cCad1 = aData[i][ cField ].toLowerCase();
      cCad2 = uVal.toLowerCase();
      
      lFound = ( cCad1.indexOf( cCad2 ) >= 0 );

    }						

    if ( lFound ){
    
      oItem = aData[i];
      
      if ( lGo ) { 				
        
        //	Buscamos la columna del campo que hemos buscado
      
          var oCols 	= oGrid.getColumns();
          var nCols   = oCols.length ;	
          var nCol 	= -1;
          
          for (var j = 0; j < nCols; j++) {							
            
            if ( oCols[j].field == cField ) {
              nCol = j;
              break;
            }	

          }			
        
          if ( nCol >= 0 ) {	
            this.SetPos( i+1, nCol+1, false );
          } else
            this.SetPos( i+1, null, false );
        
      }
        
      break;
    }								
  }						
  
  return oItem;		
};

//	Chequear si existe un valor dentro del grid...

this.ExistValue = function( cField, uVal ) {		

  var aData 	= dataView.getItems();		
  var nTotal  = aData.length ;
  
  if ( nTotal == 0 ){
    return false;
  }			
    
  var nRow 	= this.GetRow();		
  var lFound 	= false;				

  for (var i = 0; i < nTotal; i++) {
  
    if ( i !== nRow ) {

      if ( aData[i][ cField ] == uVal ) {
        lFound = true;
        break;
      }
    
    } 								
  }		

  return lFound;		
}	;

this.GetDataJSON = function() {	

  if ( dataView.IsFilter() )
    return JSON.stringify( dataView.getFilteredItems() );	
  else
    return JSON.stringify( dataView.getItems() );	
};

this.GetUpdate = function() {
  return oUpdate;
};

this.GetChanges = function() {
   return JSON.stringify( oUpdate );		
};

this.Len = function() {
  return dataView.getLength();		
};

this.IsEdited = function() {
  
  var nTotal 		= TObjectLen( oUpdate ) ;
  var lEdited 	= nTotal > 0 ? true : false ;

  return lEdited;
}	;

/*	oReg es opcional y es una array con autocontenido de columna:
  oReg[ 'fecha' ] = '01/07/2015'
  oReg[ 'nota'  ] = 'Mi nota....'

nColPos es opcional y es la columna en la que se posicionara una vez insertado el elemento
lEdit es opcional y por defecto es false. Activa la celda para edicion, una vez insertado 
*/

this.Insert = function( oReg, nColPos, lEdit ) {	

  lEdit =  ( typeof( lEdit ) == 'boolean' ) ? lEdit : false;			

  if ( typeof( nColPos ) == 'number' ){
  
    if ( nColPos > 0 ) {
      nColPos--;
    }  
  
  } else {
  
    nColPos = 0;
  }

  
  var nCols = this.columns.length;
  
  var oCols 	= oGrid.getColumns();
  var  nTotal = oCols.length ;

  
  var oItem   = {};
  var cId 	  = '';
  
  for (var i = 0; i < nTotal; i++) {
    cId = oCols[i].id;
    oItem[ cId ] = null;		
  }				
  
  var cKey = '_NEW_' + this.nNewItem++;

  oItem.id = cKey ;												

  //		Si pasamos un objeto al insert, añadiremos al oItem sus valores		
  if ( typeof( oReg ) == 'object' ) {
  
    for (var xId in oReg ) {			
      oItem[ xId ] = oReg[ xId ];			
    }										
  }


  //	 en la tabla oUpdate un [I]nsert del Item
  oUpdate[ cKey ] = [ 'I' , oItem ];	// [I]nsert			

  //	 element al dataView
  nTotal = parseInt( dataView.getLength() );		

  // Inserto en la ultima posicion 
  dataView.insertItem( nTotal, oItem );		
  
  oGrid.focus();	
  oGrid.setActiveCell( nTotal );				
  oGrid.gotoCell( nTotal,  nColPos, lEdit );			// GoTo ultimna posicion
  
  return true;						
};

this.Edit = function ( nCol ) {

  nCol 		= (typeof nCol == 'number' ) ? ( nCol - 1 ) : 0 ;	

  var aData 	= dataView.getItems() ;		
  var nTotal  = aData.length ;

  if ( nTotal == 0 )
    return null;
  
  var oRow = oGrid.getActiveCell() ;	

  var nRow;

  if ( oRow == null ) {
    return null;
  } else {
    nRow = oRow.row;
  }							

  oGrid.gotoCell( nRow,  nCol, true );			
};

this.SetItem = function ( oItem ) {

  this.UpdateItem( null, oItem );
  
  var args = {};
  args.item = oItem;
  
  oGrid.onCellChange.notify( args );		
};

//	Index en TWeb comença en 1, i en SlickGrid en 0
this.UpdateItem = function ( nIndex, oItem ) {

  var cIndex;

  if ( nIndex === null ) {	

    var oRow = oGrid.getActiveCell() ;	

    if ( oRow == null ) {
      return null;
    } else {
      nIndex = oRow.row;
  
      // Index es camp _id .   i ser = "_" + index 				
      cIndex = '_' + nIndex ;
      // En principi oItem hauria de tenir un camp intern "_id"... . Pendent de testejar
    }		
    
  } else {
    cIndex = nIndex;
  }
      
  dataView.updateItem( cIndex, oItem );
};

this.InsertItem = function( oReg, nRow, lGo, lInsert ) {	

  if ( typeof( oReg ) !== 'object' ) {	
    return false;
  }				
  
  var nTotalRow = parseInt( dataView.getLength() );			
  
  nRow 	= (typeof nRow 		== 'number' ) ? ( nRow - 1 ) : nTotalRow ;		
  lGo 	= (typeof lGo 		== 'boolean' ) ? lGo : true ;				
  lInsert	= (typeof lInsert 	== 'boolean' ) ? lInsert : true ;				
    
  
  //	IMPORTANT !!! Si el grid NO ha estat inicialitzat, ho fem fent un SetData() buid
  
  if ( nTotalRow == 0 ){
    this.SetData();
  }			

  if ( ( nRow <= 0 ) || ( nRow > nTotalRow ) ){
    nRow = nTotalRow;
  }								

//		Creamos un Item vacio con la definicion de las columnas que definimos del grid		
  
  var oItem   = {};		

  var nCols 	= this.columns.length;		
  var oCols 	= oGrid.getColumns();
  var nTotal 	= oCols.length ;
  var cId 	= '';
  
  for (var i = 0; i < nTotal; i++) {
    
    cId = oCols[i].id;
    oItem[ cId ] = null;		
  }				
  
  //	Si INSERIM un registre que te un id, es que el ID existeix a la BD !. Sino es que és nou -> _NEW_
  
  var cKey = "";

  if ( lInsert ) {
  
    //	Si INSERIM un registre que te un id, es que el ID existeix a la BD !. Sino es que és nou -> _NEW_
    
    if ( oReg.id  === undefined )
      cKey = '_NEW_' + this.nNewItem++;
    else 
      cKey = '_UPD_' + oReg.id ;

    
    oItem._id = cKey ;												
  
  } else {
  
    oItem._id = '_' + nTotalRow;
    
  }											
  
//		Añadimos las columnas que hay en el objeto oReg y reescribe los valores						
  
  for (var xId in oReg ){			
    oItem[ xId ] = oReg[ xId ];			
  }										


//		Añadimos en la tabla oUpdate un [I]nsert del Item, en el caso q queramos dejarolo marcado
//		para añadirlo a la BD. Por defecto se insertara

  if ( lInsert )
    oUpdate[ cKey ] = [ 'I' , oItem ];	// [I]nsert			

//		Inserim element al dataView	

  dataView.insertItem( nRow, oItem );		// Inserto en la ultima posicion 

  // Pendent de xequejar, pel tema de xequejar comptador de rows	
  //Grid.updateRowCount();
  //Grid.render();		
  //
  
  // Como le hemos restado una posicion para jugar con index 0, ahora le devolvemos el inicial
  nRow++ 	;
  
  if ( lGo ) {
    this.SetPos( nRow, null, false );		
  }
  
  return true;						
}	;

this.SetPos = function ( nRow, nCol, lEdit ){

  var oRow = oGrid.getActiveCell() ;	

  if ( oRow == null ) 
    nRowNow = 0 ;
  else 
    nRowNow = oRow.row ;
  

  nRow 	= (typeof nRow == 'number' ) ? ( nRow - 1 ) : nRowNow ;
  nCol 	= (typeof nCol == 'number' ) ? ( nCol - 1 ) : 0 ;
  lEdit	= (typeof lEdit == 'boolean' ) ? lEdit : true ;		
  
  var nTotal = parseInt( dataView.getLength() );	
  
  if ( nRow  > nTotal ){
    nRow = nTotal;
  }				
  
  oGrid.focus();	
//		oGrid.setActiveCell( nTotal );	

    oGrid.gotoCell( nRow,  nCol, lEdit );			// GoTo ultimna posicion		
};

/*	-------------------------------------------------------------------------------
oGrid RDD !!!
ATENCIO Index comença per 0. FWEB comença per 1 


------------------------------------------------------------------------------- */

this.Recno = function(){

  var nTotal = dataView.getLength();
  
  if ( nTotal == 0 ) {
    return 0;
  }		
  
  var oRow = oGrid.getActiveCell() ;	

  if ( oRow == null ) {
    return 1;
  } else {				
    return oRow.row + 1;					
  }
};

//	Igual q Recno però torne el numero de Row començant per 0 
this.GetRow = function() {

  var nTotal = dataView.getLength();
  
  if ( nTotal == 0 ) {
    return 0;
  }		
  
  var oRow = oGrid.getActiveCell() ;	

  if ( oRow == null ) {
    return 0;
  } else {				
    return oRow.row;					
  }				
}	;


//	------------------------------------------------------------------------------ //	
this.HasFocus = function() {

  var nTotal = dataView.getLength();
  
  if ( nTotal == 0 ) {
    return false;
  }
    
  var oRow = oGrid.getActiveCell() ;

  if ( oRow == null ) {
    return false;
  } else {
    return true;
  }		
};

this.Delete = function( lComplete, lGroup ) {

  lComplete	= (typeof lComplete	== 'boolean' ) ? lComplete : false ;
  lGroup		= (typeof lGroup	== 'boolean' ) ? lGroup : false ;
  

//		var nTotal = oGrid.getDataLength();
  var nTotal = dataView.getLength();
  
  if ( nTotal == 0 ) {
    return false;
  }
    
  var oRow = oGrid.getActiveCell() ;		

  if ( oRow == null ) {
    return false;
  }				
    
  var oItem   = null;
  var cKey 	  = '';
  var aData 	= dataView.getItems();					

  if ( lGroup ) {
  
    oItem = this.GetItem() ;
    cKey 	= oItem._id;
    
    if ( typeof cKey == 'undefined' ) {
      return false;
    }
    
  } else {
  
    oItem 	= aData[ oRow.row ];
    cKey 	= aData[ oRow.row ]._id ;
  } 

  
//		Si la clave es una que hemos insertado en el Grid -> _NEW_xxx , eliminamos el registros
//		de la tabla		

  var cPrefix = cKey.substring( 0, 5 );   // Check si _NEW_
  
  if ( cPrefix == '_NEW_' ) { 
  
    delete oUpdate[ cKey ]; 
    
  } else { 
  
    oUpdate[ cKey ] =  [ 'D' , oItem ];		//[D]elete						
  }

  dataView.deleteItem( cKey );
  
  oRow = oGrid.getActiveCell() ;	
  
  if ( oRow == null ) {
  
    nTotal = dataView.getLength();								
    
    this.SetPos( nTotal , null, false );		
  }
  
  oGrid.focus();	
  
  //	PART NOVA. S'HA DE XEQUEJAR MOLT BE !!!!			
    
  if ( lComplete ) {
    
    if( oUpdate[ cKey ] === undefined ) {
    } else {	
      delete oUpdate[ cKey ];		
    }		
  }								
};

this.SetOptions = function( cProp, uValue ) {	

  var o = {};
  o[ cProp ] = uValue ; 
  
  oGrid.setOptions( o ) ;	
};

this.GetOptions = function( cProp ) {		
  var aOptions = oGrid.getOptions();	
  return aOptions[ cProp ];	
};


this.Refresh = function() {
  
  var nTotal = dataView.getLength();
  
  if ( nTotal == 0 ) {
    return false;
  }	
    
  var oRow = oGrid.getActiveCell() ;	
  
  if ( oRow == null )
    return false;		
    
  oGrid.invalidateRow( oRow.row );
  oGrid.render();		

};

this.GetColValue = function( cId ) {
  
  //	console.log( this.GetItem())	// Esto funciona OK !!!
  var nTotal = dataView.getLength();
  
  if ( nTotal == 0 ) {
    return false;
  }	
    
  var oRow = oGrid.getActiveCell() ;

  if ( oRow == null ) {
    return false;
  }					

  var aData = null;

  if ( dataView.IsFilter() ) 		
    aData 	= dataView.getFilteredItems();	
  else
    aData 	= dataView.getItems();		
  
  var uValue 	=  aData[ oRow.row ][ cId ] ;
  
  return uValue; 				
  
};

this.SetColValue = function( cId, uValue, lRefreshUpdate ) {

  if (typeof( lRefreshUpdate ) === 'undefined') lRefreshUpdate = true ;	

  var nTotal = dataView.getLength();
  
  if ( nTotal == 0 ) {
    return false;
  }	
    
  var oRow = oGrid.getActiveCell() ;

  if ( oRow == null ) {
    return false;
  }		

/*
  PENDENT DE PROBAR
  
  Podem crear un 4º parametre q indiqui nRow, de tal manera q no es la q estem posicionats
  per defecte. Sols hauriem de fer +-
  
  if nParRow es numeric i nParRow >= 0 i nParRow < Len(data)
  
    var _cId 	= aData[ nParRow ][ '_id' ]; 	
  sino 
    var _cId 	= aData[ oRow.row ][ '_id' ]; 				
              
*/		
  var oIt 	= this.GetItem();				

//	Versio vella, abans de grups...
  //var aData 	= dataView.getItems();		
  //var _cId 	= aData[ oRow.row ][ '_id' ]; 	
  
  var _cId 	= oIt.id;
  
//	-------------------------------------------

  var item 	= dataView.getItemById( _cId );
  
  item[ cId ] = uValue;
  
  var cKey = item._id ;		

  dataView.updateItem( _cId, item);	

  if ( lRefreshUpdate ) {
  
    if( oUpdate[ cKey ] === undefined ) {				 
      oUpdate[ cKey ] = [ 'U' , item ];	// [U]pdate
    } else {					
      oUpdate[ cKey ][ 1 ] = item;				
    }						
  
  }
  
  return null; 					
  
}	;

this.GetDataView = function() {
  return dataView;
}	;

this.Native = function() {
  return oGrid;
}	;

this.Resize = function() {
  oGrid.resizeCanvas();
};

this.SetFocus = function() {

  var oRow = oGrid.getActiveCell() ;

  if ( oRow == null ) {
    return null;
  } 
  oGrid.focus();
  oGrid.gotoCell( oRow.nRow );
};

this.SetCss = function( aCss, cKey ) {

  if( typeof aCss ==='undefined' ){
    aCss = {};
  }

  if( typeof cKey ==='undefined' ){
    cKey = '';
  }		
  
  oGrid.setCellCssStyles( cKey, aCss );	
  
  this.Refresh();
  
  oCss[ cKey ] = aCss;

};

this.DelCss = function( cKey ) {

  if( typeof cKey ==='undefined' ){
    cKey = '';
  }		
  
  oGrid.removeCellCssStyles( cKey );

  if ( cKey in oCss )
    delete oCss[ cKey ];
  
  this.Refresh();
};

this.SetFlash = function( nRow, nCol, nTimes ) {

  nCol 	= (typeof nCol == 'number' ) ? ( nCol - 1 ) : 0 ;		

  var aData 	= dataView.getItems() ;		
  var nTotal  = aData.length ;
  
  if ( nTotal == 0 )
    return null;	

  if ( typeof nRow == 'number' ) {
  
    nRow--;
    
  } else {	
  
    var oRow = oGrid.getActiveCell() ;	

    if ( oRow == null ) {
      return null;
    } else {
      nRow = oRow.row;
    }			
  }	

  oGrid.flashCell( nRow, nCol, 100 , nTimes );	

};

this.GetSelected = function() { 
  return oGrid.getSelectedRows(); 
};

this.SetSelected = function( aData ) { 
  oGrid.setSelectedRows( aData ); 
};

/*
*  Adiciones Albeiro Valencia
*/

//------------------------------------

this.GetActiveCell = function() {
  return oGrid.getActiveCell();
};

//------------------------------------

this.GetRowColValue = function( index, cId ) {
  var item = oGrid.getDataItem(index);
  console.log(item, cId);
  return item[ cId ];
};

//------------------------------------

this.SetRowColValue = function( index, cId, value ) {
  var item = oGrid.getDataItem(index);

  // Actualiza el item
  item[ cId ] = value;
  oGrid.invalidate();
  oGrid.render();

  // Actualiza los totales
  dataView.updateItem(item.id, item);
};

//------------------------------------

this.UpdateAllTotals = function() {
  
  console.log(dataView);

  dataView.refresh();

  /*
  var data = this.GetData();
  for ( var row = 0; row < data.length; row++ ) {
    var item = oGrid.getDataItem(row);
    console.log(row, item);
  }
  */

};

//------------------------------------

this.UpdateTotal = function(cell, grid) {
  var columnId = grid.getColumns()[cell].id;
  var data = this.GetData();
  
  console.log(cell, grid, dataView);
  var total = 0;
  var i = data.length;
  while (i--) {
    total += (parseInt(data[i][columnId], 10) || 0);
  }
  var columnElement = dataView.getFooterRowColumn(columnId);
  $(columnElement).html("Sum:  " + total);
};

//------------------------------------

this.TotalColumn = function( cId ) {
  var column = oGrid.getColumnIndex( cId );
  var nLen   = this.Len();
  var aData  = dataView.getItems();		
  
  var total = 0;
  var i = nLen;
  while (i--) {
    total += (parseInt(aData[i][cId], 10) || 0);
  }
  return total;
};

this.Init();

};