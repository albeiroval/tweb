<?php
/*
*	 Class TDatatable
*  Fecha  : 10-07-2021
*  Autor  : Antonio Albeiro Valencia
*  (C)(R) : Derechos Reservados, se prohibe el uso de esta libreria para otros fines distintos
*						al uso del framework VALWEB
*/

if ( !defined( 'AL_LEFT' ) ) {
	define( 'AL_LEFT', 'LEFT' );
}	

if ( !defined( 'AL_RIGHT' ) ) {
	define( 'AL_RIGHT', 'RIGHT' );
}

if ( !defined( 'AL_CENTER' ) ) {
	define( 'AL_CENTER', 'CENTER' );
}	

define( "IS_BUTTON", "BUTTON" ); 

//-------------------------------

class TDataTable extends TControl {

	public $aData			 = null;
	public $aOptions 	 = array();
	public $isFileJson = false; 
	
	private $cClassCss  = null ;
	private $cClrHeadBg  = 'aquamarine';
	private $cClrFootBg  = 'aquamarine';
	private $cClrHeadTx  = 'black';
	private $cClrFootTx  = 'black';
	private $aCols    	 = [];
	private $nCol     	 = 0;
	private $lCheckbox   = true;
	private $aColsFunc   = [];
	private $oAjax       = null;
	private $nameJson    = "./data/data.json";
	private $aBtnExport  = [];
	private $inputFilter = true;
	private $aColsFooter = ''; 
	private $FontSize    = 14;
	private $aTitle      = null;
	private $aButtons    = [];
	private $buttonsCss  = "btn-primary";
	private $aSizeRow 	 = [];
	private $doubleclick = "";
	
	//---------------------------------------------

	public function __construct( $oWnd, $cId = '', $nTop = null, $nLeft = null, 
	                             $nWidth = null, $nHeight = null, $aData = array() ) {
		$nTop			= TDefault( $nTop, '13.5%' );
		$nLeft   	= TDefault( $nLeft, '2%' );
		$nWidth 	= TDefault( $nWidth, '96%' );
		$nHeight 	= TDefault( $nHeight, 'auto' );

		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight );

		$this->cClass   = 'tweb_datatable';
		$this->cControl = 'tdatatable';
		$this->aData    = $aData;

		$this->aOptions = [	'info' 						=> true,
								 				'pagin' 			  	=> true,
								 				'ordering' 				=> true,
								 				'searching' 			=> true,
								 				'scrollCollapse' 	=> false,
								 				'scrollX' 				=> false,
								 				'scrollY' 				=> true,
								 				'pageLength' 			=> 25,
								 				'dummy' 					=> null,
								  			'createdRow'      => null ];

		$this->aSizeRow = [ "header" => 0, "cell" => 0 ];		
		
		$this->aBtnExport = [ "show" => true, "buttons" => "all" ];

	}

	//---------------------------------------------

	public function Info( $lOnOff = true ) { $this->aOptions[ 'info' ] = $lOnOff; }
	public function Paging( $lOnOff = true ) { $this->aOptions[ 'pagin' ] = $lOnOff; }
	public function Ordering( $lOnOff = true ) { $this->aOptions[ 'ordering' ] = $lOnOff; }
	public function Searching( $lOnOff = true ) { $this->aOptions[ 'searching' ] = $lOnOff; }
	public function ScrollCollapse( $lOnOff = true ) { $this->aOptions[ 'scrollCollapse' ] = $lOnOff; }
	public function ScrollX( $lValue = true ) { $this->aOptions[ 'scrollX' ] = $lValue; }
	public function ScrollY( $uValue = true ) { $this->aOptions[ 'scrollY' ] = $uValue; }
	public function PageLength( $nValue = 25 ) { $this->aOptions[ 'pageLength' ] = $nValue; }
	public function SetFormater( $createdRow = null ) { $this->aOptions['createdRow'] = $createdRow; }
	public function Filter( $show = true ) { $this->inputFilter = (gettype($show) == "boolean") ? $show : true; }
	public function FontSize( $fontsize ) { $this->FontSize = $fontsize; }

	//---------------------------------------------

	public function CheckBox( $lCheckbox = true ) { 
		$this->lCheckbox = (gettype($lCheckbox) === "boolean") ? $lCheckbox : false; 
	}
	
	//---------------------------------------------

	public function Export( $show = true, $buttons = '' ) { 
		$this->aBtnExport["show"] = (gettype($show) == "boolean") ? $show : true; 
		if ( empty($buttons) ) {
			$buttons = "all";
		}
		$this->aBtnExport["buttons"] = $buttons;
	}

	//---------------------------------------------

	public function HeaderRowSize( $Size = 0 ) {
		$Size = (gettype($Size) === "integer") ? $Size : 0;
		if ($Size > 0) {
			$this->aSizeRow["header"] = $Size;
		}	
	}

	//---------------------------------------------
	public function ButtonsCSS( $cClass = "btn-primary" ) {
		$this->buttonsCss = $cClass;
	}

	//---------------------------------------------

	public function CellRowSize( $Size = 0 ) {
		$Size = (gettype($Size) === "integer") ? $Size : 0;
		if ($Size > 0) {
			$this->aSizeRow["cell"] = $Size;
		}	
	}
	
	//---------------------------------------------

	public function SetCssClass( $cClass = null ) {
		$this->cClassCss = $cClass;
	}

	//---------------------------------------------

	public function DoubleClick( $action = '' ) { 
		$this->doubleclick = (gettype($action) == "string") ? $action : ''; 
	}

	//---------------------------------------------

	public function Title( $cLabel = "DATATABLE", $cClrText = "black", $cClrBack = "aqua", $cClass = "" ) {
		$this->aTitle = [ "label" => $cLabel,
											"color" => $cClrText,
											"fondo" => $cClrBack,
										  "class" => $cClass ];
	}

	//---------------------------------------------

	public function AddCol( $cId, $cHeader, $cFormat = null, $cAlign = null, 
	                        $cFunction = '', $lTotal = false, $cClass = null, $nWidth = null) 
	{
		$cAlign = ($cAlign == null) ? AL_LEFT : $cAlign;
		$lTotal = ($lTotal) ? $lTotal : false;			

		$oCol = new TDataTableCol( $this->nCol, $cId, $cHeader, $nWidth, $cFormat, $cAlign, null, $lTotal, $cClass );
		$this->aCols[] = $oCol;

		if ( !empty($cFunction) ) {
			$this->aColsFunc[] = [ $this->nCol, $cFunction, false ];
	   }

		$this->nCol++;

		return $oCol;
	}
	
	//---------------------------------------------

	public function AddColIcon( $cHeader, $cIcon = '', $cColor = '', $cFunction = '', $cTitle = '', $nWidth = null ) {

		if ( empty( $cIcon ) ) { $cIcon = 'fa-edit'; }
		if ( empty( $cColor ) ) { $cColor = '#F44336'; }
		if ( empty($cFunction) ) { $cFunction = 'void(0)'; }

		$cStyle = 'style=color:'.$cColor.';"';
		
		$cHtml  = '<a href="javascript:'.$cFunction.'" '.$cStyle.' title="'.$cTitle.'">';
		$cHtml .= '<i class="'.$cIcon.'" aria-hidden="true"></i>';
		$cHtml .= '</a>';

		$oCol = new TDataTableCol( $this->nCol, null, $cHeader, $nWidth, IS_BUTTON, null, $cHtml, false, null );
		$this->aCols[] = $oCol;

		/*
		if ( !empty($cFunction) ) {
			 $this->aColsFunc[] = [ $this->nCol, $cFunction, true ];
		}
		*/

		$this->nCol++;

		return $oCol;

	}

	//---------------------------------------------

	public function AddColHtml( $cHeader, $cHtml = "" ) {

		$oCol = new TDataTableCol( $this->nCol, null, $cHeader, null, IS_BUTTON, null, $cHtml, false, null );
		$this->aCols[] = $oCol;

		$this->nCol++;

		return $oCol;

	}

	//---------------------------------------------

	public function AddColButton( $cHeader, $cText = '', $cClass = '', $cFunction = '', $cTitle = '' ) {

		if ( empty( $cText ) ) { $cText = 'Editar'; }
		if ( empty( $cColor ) ) { $cColor = '#007bff'; }
		if ( empty($cClass) ) { $cClass = "btn btn-primary"; }

		$cHtml  = '<button type="button" class="'.$cClass.'" onclick="'.$cFunction.'" title="'.$cTitle.'">';
		$cHtml .= $cText;
		$cHtml .= '</button>';

		$oCol = new TDataTableCol( $this->nCol, null, $cHeader, null, IS_BUTTON, null, $cHtml, false, null );
		$this->aCols[] = $oCol;

		/*
		if ( !empty($cFunction) ) {
			 $this->aColsFunc[] = [ $this->nCol, $cFunction, true ];
		}
		*/

		$this->nCol++;

		return $oCol;
	
	}	

	//----------------------------------------------

  public function AddButton( $cId = '', $cText = '', $cAction = '', $cIcon = '' ) {
    $this->aButtons[] = [ "id" => $cId, "label" => $cText, "action" => $cAction, "icon" => $cIcon ];
  }

	//---------------------------------------------

	private function CreateColumns() {

		// Columnas Data
		$aColsData = '[';

		$string  = '';
		
		$nCols = count( $this->aCols );
		for ( $i = 0; $i < $nCols; $i++ ) {

			$cFormat = $this->aCols[ $i ]->cFormat;
			$cPrefix = $this->aCols[ $i ]->cPrefix;

			$string .= '{' ;
			$string .= ' title : "' . $this->aCols[ $i ]->cHeader . '",'  ;

			if ( $this->aCols[ $i ]->cType === IS_BUTTON ) {
				$string .= ' data : null' ; 
				$string .= ', className: "dt-center"';	
				$string .= ', render: function ( data, type, row ) {';
				if ( !empty( $this->aCols[ $i ]->cHtml ) ) {
					$string .= "   return '" . $this->aCols[ $i ]->cHtml . "';";
				}	else {
					$string .= " return ' ';";
				}
				$string .= ' }';
			} else {
				$string .= ' data : "' . $this->aCols[ $i ]->cId . '"' ; 
				$string .= ', className: "';

				$cAlign = $this->aCols[ $i ]->cAlign; 
				if ( $this->isAlign( $cAlign ) ) { 
					if ( $cAlign === AL_LEFT ) {
						$string .= 'dt-left';
					}	elseif ( $cAlign === AL_RIGHT ) {
						$string .= 'dt-right';
					} elseif ( $cAlign === AL_CENTER ) {
						$string .= 'dt-center';	
					}
				}	

				$cClass = $this->aCols[ $i ]->cClass;
				if ( $this->cClass ) {
					$string .= ' ' . $cClass . '"';	
				} else {
					$string .= '"';	
				}
			}

			if ( $cFormat === 'N' ) {
				if ( !empty($cPrefix) ) {
					$string .= ', render: $.fn.dataTable.render.number(",", ".", 2, ' . "'" . $cPrefix . "') ";
				} else  { 
					$string .= ', render: $.fn.dataTable.render.number(",", ".", 2) ';
				}	
			} elseif ( $cFormat === 'D' ) {
				$string .= ',  render: $.fn.dataTable.render.moment("DD/MM/YYYY") ';
			}

		  $string .= '},' ;

		}

		$aColsData .= substr($string, 0, -1); 
		$aColsData .= ']';

		return $aColsData;

	}
	
	//---------------------------------------------
	// https://datatables.net/forums/discussion/28407/how-to-disable-sorting-of-certain-column

	private function CreateColumnDefs() {

		$aColsDefs = '[';

		$string  = '';

		$nCols = count( $this->aCols );
		for ( $i = 0; $i < $nCols; $i++ ) {
			$ctype   = $this->aCols[ $i ]->cType;
			$nWidth  = $this->aCols[ $i ]->nWidth;

			$string .= '{' ;

		  if ( $nWidth != null ) { 
				if ( gettype($nWidth) == "string"  ) {
					 $string .= 	' width : "'. $nWidth .'",';		
				} else {
				   $string .= 	' width : '. $nWidth .',';	
				} 
			}	

			$string .= ' targets : ' . $this->aCols[ $i ]->nCol;

			if ( $ctype === IS_BUTTON) {
				$string .= ', orderable : false';
			} else {
				$string .= ', orderable : true';
			}

			$string .= '},' ;
		}

		$aColsDefs .= substr($string, 0, -1); 
		$aColsDefs .= ']';

		return $aColsDefs;

	}

	//---------------------------------------------

	private function isAlign( $cAlign ) {
		if ( $cAlign === AL_LEFT || $cAlign === AL_RIGHT || $cAlign === AL_CENTER ) {
			return true;
		} else {
			return false;	
		}	
	}

	//---------------------------------------------

	private function isFormat( $cFormat ) {
		
		$cFormat = substr($cFormat,0,1);	
		if (  $cFormat === 'N' || $cFormat === 'D' ) {	
			return true;
		} else {
			return false;	
		}	
	}

	//---------------------------------------------

	private function CreateAjaxObject() {
		
		$cJson  = '[{';
		$nCols  = count( $this->aCols );
		$string = "";
		for ( $i = 0; $i < $nCols; $i++ ) {
			$string .= '"' . $this->aCols[ $i ]->cId . '" : ' ;
			$cFormat = substr($this->aCols[ $i ]->cFormat,1,1);
			if ( $cFormat === 'N' ) {
				 $string .= '"0",' ; 	
			} else {
				 $string .= '" ",' ;
			}
		}
		
		/* 
		*		En los servidores linux se debe deshabilitar la carpeta del proyecto para lectura
		*	 	#sudo chown -R www-data:www-data /home/<namehost>/proyecto/
		*/ 
		
		$cJson .= substr($string, 0, -1); 
		$cJson .= '}]';

		if (!file_exists('./data')) {
			 mkdir('./data', 0777, true);
		}

		$fp = fopen( $this->nameJson, 'w');
    	fwrite($fp, $cJson);
		fclose($fp);

		chmod($this->nameJson, 0777);
		
		// { url: "./data/data.json", dataSrc: ""},
		$this->oAjax = '{ url: "' . $this->nameJson . '", ' . 'dataSrc: "" }';	

	}		

	//---------------------------------------------

	private function isRowFooter() {

		$lFooter = false;

		$nCols = count( $this->aCols );
		for ( $i = 0; $i < $nCols; $i++ ) {
			if ( $this->aCols[$i]->lTotal ) {
				 $lFooter = true;
				 break;
			}
		}

		return $lFooter;
		
	}

	//---------------------------------------------

	private function CreateRowFooter() {

		$this->aColsFooter = '[';

		$cHtml  = '<tfoot class="dt-footer">';
		$cHtml .= '<tr>';

		$string = '';
		$nCols  = count( $this->aCols );
		for ( $i = 0; $i < $nCols; $i++ ) {
			$cFormat = $this->aCols[ $i ]->cFormat;
			$cPrefix = $this->aCols[ $i ]->cPrefix;
			$lTotal  = $this->aCols[ $i ]->lTotal; 
			if ( $lTotal ) {
				if ( $cFormat === 'N' && !empty($cPrefix) ) {
					$string .= "{column : " . strval($i) . ', format : "' . $cPrefix . '"}, ';
				} else {
					$string .= "{column : " . strval($i) . ", format : ' '}, ";
				}	
			}
			$cHtml .= '<th></th>';
		}	

		$cHtml .= '</tr>';
		$cHtml .= '</tfoot>';
		
		$this->aColsFooter .= substr($string, 0, -1); 
		$this->aColsFooter .= ']';
		
		return $cHtml;

	}

	//---------------------------------------------

	public function SetColorHeader( $cClrTxt = 'black', $cClrBg = 'aquamarine' ) {
		$this->cClrHeadTx = $cClrTxt;
		$this->cClrHeadBg = $cClrBg;
	}
	
	//---------------------------------------------

	public function SetColorFooter( $cClrTxt = 'black', $cClrBg = 'aquamarine' ) {
		$this->cClrFootTx = $cClrTxt;
		$this->cClrFootBg = $cClrBg;
	}
	
	//---------------------------------------------

	// https://datatables.net/forums/discussion/38188/scrolly-auto
	private function createCodeStyle() {
		
		$cCSS  = '<style>' . PHP_EOL;
		
		$cCSS .= '#div-' . $this->cId . ' {' . PHP_EOL;
		$cCSS .= '	top: ' . $this->nTop  . ';' . PHP_EOL;
		$cCSS .= '	left: ' . $this->nLeft . ';' . PHP_EOL;
		$cCSS .= '	width: ' . $this->nWidth . ';' . PHP_EOL;
		$cCSS .= '	height: ' . $this->nHeight . ';' . PHP_EOL;
		$cCSS .= '	right: 1em ;' . PHP_EOL;
		$cCSS .= '	bottom: 1em ;' . PHP_EOL;
		$cCSS .= '  padding-right: 1em;' . PHP_EOL;
		$cCSS .= '}' . PHP_EOL;

		$cCSS .= 'thead tr th{' . PHP_EOL;
		$cCSS .= '    color:' . $this->cClrHeadTx  . ';' . PHP_EOL;
		$cCSS .= '		background-color:' . $this->cClrHeadBg . ';' . PHP_EOL;
		$cCSS .= '}' . PHP_EOL;
		
		$cCSS .= 'tfoot.dt-footer tr th {' . PHP_EOL;
		$cCSS .= '    color:' . $this->cClrFootTx  . ';' . PHP_EOL;
		$cCSS .= '		background-color:' . $this->cClrFootBg .';' . PHP_EOL;	
		$cCSS .= '    text-align: right;' . PHP_EOL;
		$cCSS .= '}' . PHP_EOL;	

		$cCSS .= 'tbody {' .PHP_EOL;
		$cCSS .= '	font-size: ' . $this->FontSize . ';' .PHP_EOL;
		$cCSS .= '}' .PHP_EOL;

		$cCSS .= '.dataTables_filter {' . PHP_EOL;
		$cCSS .= '	font-size: ' . $this->FontSize . ';' .PHP_EOL;
		$cCSS .= '}' .PHP_EOL; 

		if ($this->aSizeRow["cell"] > 0) {

			$cCSS .= '#'.$this->cId.'_wrapper tbody th, #dtuser_wrapper tbody td {' . PHP_EOL;
			$cCSS .= '	height: '.$this->aSizeRow["cell"].'px;' . PHP_EOL;
			$cCSS .= '	white-space: nowrap;' . PHP_EOL;
			$cCSS .= '	padding: 6px 10px;' . PHP_EOL; // e.g. change 8x to 4px here 
			$cCSS .= '}' . PHP_EOL;

			$cCSS .= '@media (max-width : 640px) {' . PHP_EOL;
			$cCSS .= '	#'.$this->cId.'_wrapper tbody th, #dtuser_wrapper tbody td {' . PHP_EOL;
			$cCSS .= '	padding-left: 30px !important;' . PHP_EOL;
			$cCSS .= '	cursor: pointer;' . PHP_EOL;
			$cCSS .= '	}' . PHP_EOL;
			$cCSS .= '}' . PHP_EOL;

		}	
	
		if ($this->aSizeRow["header"] > 0) {
			$cCSS .= '#'.$this->cId.'_wrapper thead th, #dtuser_wrapper thead td {' . PHP_EOL;
			$cCSS .= '	height: '.$this->aSizeRow["header"].'px;' . PHP_EOL;
			$cCSS .= '	white-space: nowrap;' . PHP_EOL;
			$cCSS .= '	padding: 6px 10px;' . PHP_EOL; // e.g. change 8x to 4px here 
			$cCSS .= '}' . PHP_EOL;
		}	

		// Buttons Export
		$cCSS .= '#'.$this->cId.'_wrapper button {' . PHP_EOL;
		$cCSS .= '	color: #fff;' . PHP_EOL;
		$cCSS .= '	background-color: #007bff;' . PHP_EOL;
		$cCSS .= '	border-color: #007bff;' . PHP_EOL;
		$cCSS .= '  font-size: 16px !important;' . PHP_EOL;
		$cCSS .= '	border-color: #0062cc;' . PHP_EOL;
		$cCSS .= '}' . PHP_EOL;
		
		$cCSS .= '#'.$this->cId.'_wrapper button:hover {' . PHP_EOL;
		$cCSS .= '	color: #fff;' . PHP_EOL;
		$cCSS .= '	background-color: #0069d9;' . PHP_EOL;
		$cCSS .= '	border-color: #0062cc;' . PHP_EOL;
		$cCSS .= '}' . PHP_EOL;
		
		$cCSS .= '</style>' . PHP_EOL;

		echo $cCSS;

		// console_php("size-header ".$this->aSizeRow["header"]." size-cell ".$this->aSizeRow["cell"]);

	}

	//---------------------------------------------

	private function createCodeJavaScript() {

		if ( $this->isFileJson ) { 
			$this->CreateAjaxObject(); 
		} else {
			$this->oAjax = 'null';	
		}

		$uAjax           = $this->oAjax;     
		$uColumns        = $this->CreateColumns();
		$uColumnDefs     = $this->CreateColumnDefs();
		$uInfo           = $this->aOptions[ 'info'           ] ? 'true' : 'false';
		$uPaging         = $this->aOptions[ 'pagin'          ] ? 'true' : 'false';
		$uOrdering       = $this->aOptions[ 'ordering'       ] ? 'true' : 'false';
		$uSearching      = $this->aOptions[ 'searching'      ] ? 'true' : 'false';
		$uScrollCollapse = $this->aOptions[ 'scrollCollapse' ] ? 'true' : 'false';
		$uPageLength     = $this->aOptions[ 'pageLength'     ];
		$createdRow      = $this->aOptions[ 'createdRow'     ];
		$uScrollX        = $this->aOptions[ 'scrollX'        ] ? 'true' : 'false';
		if( gettype( $this->aOptions[ 'scrollY' ] ) == 'boolean' ) {
			$uScrollY = $this->aOptions[ 'scrollY'        ] ? 'true' : 'false';
		} else {
			$uScrollY = '"'. $this->aOptions[ 'scrollY' ] . '"';
		}

		$hideBtnExport 	= ( $this->aBtnExport["show"] ) ? 'false' : 'true';
		if ( gettype( $this->aBtnExport["buttons"] ) == 'array' ) {
			$buttonsExport = json_encode( $this->aBtnExport["buttons"] );
		} else {
			$buttonsExport = '"'.$this->aBtnExport["buttons"].'"';
		}

		$hideFilter = ( $this->inputFilter ) ? 'false' : 'true';
		$uCheckBox  = ( $this->lCheckbox ) ? 'true' : 'false';  
		
		$data = null; 
		if ( gettype( $this->aData ) == 'array' && count( $this->aData ) > 0 ) {
			$data = json_encode($this->aData);
		}

		// Funciones creadas en las columnas
		$aColsFunction = json_encode( $this->aColsFunc );

		// Columnas totales
		if ( empty($this->aColsFooter) ) {
			$aColsFooter = '[]';
		} else {
			$aColsFooter = $this->aColsFooter;
		}	

		// Double Click Action
		$uDoubleClick = '"'.$this->doubleclick.'"';

		// CODIGO JAVASCRIPT -------------------------------
		$cJS  = '<script>' . PHP_EOL;
	  $cJS .= PHP_EOL;
		$cJS .=	'$(function() {'                                      . PHP_EOL;
		$cJS .= '  var container = $("#div-' . $this->cId . '");'     . PHP_EOL; 
		$cJS .= '	 var options = {};' 												        . PHP_EOL;
		$cJS .= '	 options.ajax 		  	 	= ' . $uAjax . ';'          . PHP_EOL;	
		$cJS .= '	 options.columns 	  	 	= ' . $uColumns . ';' 			. PHP_EOL;	
		$cJS .= '	 options.columnDefs 	 	= ' . $uColumnDefs . ';' 		. PHP_EOL;
		$cJS .= '	 options.info 		  	 	= ' . $uInfo . ';' 					. PHP_EOL;
		$cJS .= '	 options.paging 		 	 	= ' . $uPaging . ';' 				. PHP_EOL;
		$cJS .= '	 options.ordering 	   	= ' . $uOrdering . ';' 			. PHP_EOL;
		$cJS .= '	 options.searching 	   	= ' . $uSearching . ';' 		. PHP_EOL;
		$cJS .= '	 options.pageLength 	 	= ' . $uPageLength . ';' 		. PHP_EOL;
		$cJS .= '	 options.scrollX 		   	= ' . $uScrollX . ';' 			. PHP_EOL;
		$cJS .= '	 options.scrollY 		   	= ' . $uScrollY . ';'		  	. PHP_EOL;
		$cJS .= '	 options.scrollCollapse = ' . $uScrollCollapse . ';'. PHP_EOL;
		$cJS .= '	 options.colsFunction  	= ' . $aColsFunction . ';' 	. PHP_EOL;  
		$cJS .= '	 options.hideButtons   	= ' . $hideBtnExport . ';'	. PHP_EOL;  
		$cJS .= '	 options.exportButtons  = ' . $buttonsExport . ';'	. PHP_EOL;  
		$cJS .= '	 options.hideFilter 	 	= ' . $hideFilter . ';'		  . PHP_EOL;  
		$cJS .= '	 options.checkbox 	 	  = ' . $uCheckBox . ';'		  . PHP_EOL;  
		$cJS .= '	 options.columnsTotal 	= ' . $aColsFooter . ';'		. PHP_EOL;  
		$cJS .= '	 options.createdRow   	= "'. $createdRow . '";'		. PHP_EOL;  
		$cJS .= '	 options.doubleclick   	= '. $uDoubleClick . ';' 		. PHP_EOL;  
		$cJS .= PHP_EOL;
		$cJS .= '  var oGrid = new TDataTable2("'.$this->cId.'",options,"'.TWEB_PATH_LIBS.'",'.$data.');' . PHP_EOL;
		$cJS .= '  var oCtrl = new TControl();' . PHP_EOL;
		$cJS .= '  oCtrl.ControlInit("'.$this->cId.'", oGrid, "datatable" );' . PHP_EOL;
		$cJS .= '  container.find(".dataTables_scrollBody").css("height", container.height() - 20);' . PHP_EOL;
		
		if ( $this->cClassCss != null ) {
			$cJS .= 'document.getElementById("div-' . $this->cId . '").classList.add("' . $this->cClassCss. '");' . PHP_EOL;
		}
		
		$cJS .= '});' . PHP_EOL;
		$cJS .= PHP_EOL;
		$cJS .= '</script>' . PHP_EOL;
		
		echo  $cJS;

	}

	//---------------------------------------------

	public function Activate() {

		/* Library JAVASCRIPT */
		$cHtml  = '<link href="' . TWEB_PATH . 'core.datatable.css" rel="stylesheet">';
		$cHtml .= '<script src="' . TWEB_PATH . 'core.datatable.js"></script>';
		echo $cHtml;

		/* Codigo HTML */ 
		$lFooter = $this->isRowFooter();

		$cHtml  = '<div class="container-fluid">';
		$cHtml .= ' <div class="div-datatable" id="div-'.$this->cId.'">';

		if ($this->aTitle) {
			$cHtml .= '<div style="color:'.$this->aTitle["color"];
			$cHtml .= ' ;background-color:'.$this->aTitle["fondo"];
			$cHtml .= ' ;width: 100%;text-align:center">';
			$cHtml .= '<h3 class="'.$this->aTitle["class"].'">'.$this->aTitle["label"].'</h3>';
			$cHtml .= '</div>';	
		}

		$buttons = count($this->aButtons);
    if ( $buttons > 0 ) {
      $cHtml .= '<div class="btn-group btn-custom btn-datatable" role="group">';
      for ($i=0; $i < $buttons; $i++) { 
        $item = $this->aButtons[$i];
        $cHtml .= '<button type="button" class="btn '.$this->buttonsCss.' btn-dtcustom"';
        $cHtml .= ' id="'.$item["id"].'" onclick="'.$item["action"].'">';
        if ( !empty($item["icon"]) ) {
          $cHtml .= ' <i class="fa '.$item["icon"].'"></i> ';
        }
        $cHtml .= $item["label"];
        $cHtml .= '</button>';  
      }
      $cHtml .= '</div>';
		}  

		$cHtml .= '<table id="'.$this->cId.'" class="table table-striped table-bordered nowrap" style="width:100%"> ';
		
		if ( $lFooter ) {
			$cHtml .= $this->CreateRowFooter();
		}
		$cHtml .= '</table>';
		$cHtml .= '</div>';
		$cHtml .= '</div>';

		echo $cHtml;

		/* Codigo STYLE */
		$this->createCodeStyle();

		/* Codigo JAVASCRIPT */
		$this->createCodeJavaScript();
		
	}

} /*end class TDataTable*/

//---------------------------------------------

Class TDataTableCol {
	public $nCol    	= 0;
	public $cId     	= '';
	public $cHeader 	= '';
	public $cFormat 	= '';
	public $cType     = '';
	public $nWidth    = null;
	public $cPrefix   = '';
	public $cAlign  	= '';
	public $cHtml   	= '';
	public $lTotal    = false;
	public $cClass    = "";
	
	//---------------------------------------------

	public function __construct( $nCol, $cId, $cHeader, $nWidth, $cFormat, $cAlign, $cHtml, $lTotal, $cClass ) {
		$this->nCol    	= $nCol;
		$this->cId   		= $cId;
		$this->cHeader 	= $cHeader;
		$this->cAlign  	= ($cAlign === null) ? null : strtoupper($cAlign);
		$this->cHtml   	= $cHtml; 
		$this->lTotal  	= ( (gettype($lTotal) == 'boolean') ? $lTotal : false ) ;
		$this->cType   	= $cFormat;
		$this->nWidth		= ($nWidth) ? $nWidth : null;   
		$this->cClass		= ($cClass == null) ? "" : $cClass;

		$cFormat = ($cFormat === null) ? '' : $cFormat;
		if ( !empty($cFormat) ) {
			$this->cFormat = strtoupper(substr($cFormat,0,1));
			if ( $this->cFormat === "N" ) {
				$this->cPrefix = substr($cFormat,1);
			} else {
				$this->cPrefix = '';
			}
		} else {
			$this->cFormat = 'T';
			$this->cPrefix = '';
		}

	}

} /*End class TDataTableCol*/

//-----------------------------------------------

function console_php( $data ) {
	ob_start();
	$output  = "<script>console.log( 'PHP debugger: ";
	$output .= json_encode( print_r( $data, true ) );
	$output .= "' );</script>";
	echo $output;
}
	

//-------------------------------
//EOF
//-------------------------------
