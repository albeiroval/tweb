<?php
/*
*  CLASS TModal : para crea dialogos modales en forma dinamica con Bootstrap
*  Autor : Albeiro Valencia 
*  Fecha : 11/09/2020 Derechos Reservados
*  https://github.com/shaack/bootstrap-show-modal
*  https://www.jqueryscript.net/demo/Dynamic-Bootstrap-4-Modals/
*  https://www.codehim.com/download/dynamic-bootstrap-modals.zip
*/

// Type control
define('IS_INPUT', '_INPUT'); 
define('IS_SELECT', '_SELECT'); 
define('IS_RADIO', '_RADIO'); 
define('IS_LABEL', '_LABEL'); 
define('IS_SPINNER', '_SPINNER');
define('IS_UPLOAD', '_UPLOAD');
define('IS_MAP', '_MAP');
define('IS_IMAGE', '_IMAGE');


// Type input
define('TYPE_NUMBER', 'number');
define('TYPE_TEXT', 'text');
define('TYPE_EMAIL', 'email');
define('TYPE_PASSWORD', 'password');
define('TYPE_DATE', 'date');
define('ES_LANG', 'es_lang');
define('EN_LANG', 'en_lang');

// Align label
define('LBL_LEFT', 'text-align: left');
define('LBL_RIGHT', 'text-align: right');
define('LBL_CENTER', 'text-align: center');

// type button
define('BTN_DEFAULT', 0);
define('BTN_CANCEL', 1);

// Column Align label
if ( ! defined('AL_LEFT') ) { define('AL_LEFT', 'left'); }
if ( ! defined('AL_RIGHT') ) { define('AL_RIGHT', 'right'); }
if ( ! defined('AL_CENTER') ) { define('AL_CENTER', 'center'); }

//----------------------------------------------

class TModal {   

  private $titleHtml  = '';
  private $bodyHtml   = '';
  private $footerHtml = '';
  
  private $cId          = '';
  private $cTitleText   = '';
  private $cTitleClr    = '';
  private $cTitleBg     = '';
  private $aButtons     = []; 
  private $aControls    = [];
  private $aColumns     = [];
  private $isTable      = false;
  private $dataTable    = [];
  private $isUpload     = false;
  private $idUpload     = 'null';
  // private $clrHeaderBg  = 'yellow';
  // private $clrHeaderTxt = 'black';
  private $numRadio     = 1;
  private $lSpinner     = false;
  
  public $heigthTable  = 460;

  //----------------------------------------------
  
  function __construct( $cId = '', $cTitle = '', $cClrTxt = '', $cClrBg = '' ) {

    $cId     = ( empty( $cId ) ) ? 'modal_' . time() : $cId;
    $cTitle  = ( empty( $cTitle ) ) ? 'Test Modal' : $cTitle;
    $cClrTxt = ( empty( $cClrTxt ) ) ? 'black' : $cClrTxt;
    $cClrBg  = ( empty( $cClrBg ) ) ? 'aliceblue' : $cClrBg;

    $this->cId        = $cId;
    $this->cTitleText = $cTitle;
    $this->cTitleClr  = $cClrTxt;
    $this->cTitleBg   = $cClrBg;
   
  }  

  //----------------------------------------------

  public function AddInput( $cLabel, $cId, $cType = TYPE_TEXT, $cAlign = LBL_LEFT, $cHolder ='' ) {
    $this->aControls[] = [ IS_INPUT, 
                           new TModalInput( $cLabel, $cId, $cType, $cAlign, $cHolder ) ];
  }

  //----------------------------------------------

  public function AddSelect( $cLabel, $cId, $cAlign = LBL_LEFT, $aItems = [] ) {
    $this->aControls[] = [ IS_SELECT, 
                          new TModalSelect( $cLabel, $cId, $cAlign, $aItems ) ];     
  }

  //----------------------------------------------

  public function AddRadio( $cLabel ) {
    $this->aControls[] = [ IS_RADIO, 
                          new TModalRadio( $cLabel, $this->numRadio ) ];     
    $this->numRadio++;                          
  }

  //----------------------------------------------

  public function AddLabel( $cLabel = '', $cId = '', $size = 16 ) {
    $this->aControls[] = [ IS_LABEL, 
                           new TModalLabel( $cLabel, $cId, $size ) ];     
  }

  //----------------------------------------------

  public function AddHtml( $cFile ) {
    if ( $cFile ) {
      $this->aControls[] = [ IS_RADIO, $cFile ];     
    }
    $this->numRadio++;                          
  }

  // https://embed.plnkr.co/plunk/ZDkUYz
  // https://stackoverflow.com/questions/54657785/how-to-display-a-google-map-inside-modal-bootstrap-4/54664163

  //----------------------------------------------
 
  public function AddButton( $cId = '', $cLabel = '', $cAction = '', 
                             $cClrTxt = '', $cClrBg = '', $cType = BTN_DEFAULT ) {

                              /*
    if ( empty($cClrTxt) ) {                          
      $cClrTxt = ( $cType == BTN_DEFAULT ) ? '#fff' : 'black';
    }

    if ( empty($cClrBg) ) {                          
      $cClrBg = ( $cType == BTN_DEFAULT ) ? '#17a2b8' : '#efefef';
    }
    */

    if ( empty($cAction) ) {
      $cAction = ( $cType <> BTN_CANCEL ) ? "oModal.Self()" : "";
    }

    $this->aButtons[] = [ "id"     => ( empty($cId) ) ? 'btn_' . time() : $cId, 
                          "texto"  => ( empty($cLabel) ) ? 'Button' : $cLabel, 
                          "action" => $cAction, 
                          "color"  => $cClrTxt,
                          "fondo"  => $cClrBg ];
  }

  //----------------------------------------------

  public function AddColumn( $cId, $cTitle, $cPicture = '', $cAlign = AL_LEFT ) {
    $this->isTable = true;
    $this->aColumns[] = [ "id"      => $cId, 
                          "title"   => $cTitle,
                          "format"  => $cPicture,
                          "align"   => $cAlign ];
  }

  //----------------------------------------------

  public function SetDataTable( $aData = [] ) {
    $this->dataTable = $aData;
  }

  //----------------------------------------------

  public function ColorHeaderTable( $clrBg = 'yellow', $clrTexto = 'black' ) {
    $this->clrHeaderBg  = $clrBg;
    $this->clrHeaderTxt = $clrTexto;
  }

  //----------------------------------------------

  public function AddSpinner( $cId = 'idSpinner', $label = 'CANTIDAD :' ) {
    $this->lSpinner = true;
    $this->aControls[] = [ IS_SPINNER, [ "id" => $cId, "label" => $label ] ];     
  }

  //----------------------------------------------

  public function AddUploadBase64( $cId, $cLang = ES_LANG ) {
    // https://github.com/kartik-v/bootstrap-fileinput
    if ( !$this->isUpload ) {
      $this->isUpload = true;
      $this->idUpload = $cId;
      $this->aControls[] = [ IS_UPLOAD, [ "id" => $cId, "lang" => $cLang ] ];
    }
  }

  //----------------------------------------------

  public function AddMapWeb() {
    $this->aControls[] = [ IS_MAP ];     
  }

  //----------------------------------------------

  public function AddImage( $cId = 'idImage', $image = '', $cAlign = AL_CENTER, $width = '200px' ) {
    $this->aControls[] = [ IS_IMAGE, [ "id" => $cId, "image" => $image, "align" => $cAlign, "width" => $width ] ];     
  }
  
  //----------------------------------------------
  
  private function CreateControlsBody() {

    if ( $this->isTable ) {
      return " ";
    } 

    $cHtml  = "'";

    $nCount = count( $this->aControls );
		for ( $i = 0; $i < $nCount; $i++ ) { 
      $oCtrl = $this->aControls[$i];

      switch ( $oCtrl[0] ) {
        case IS_INPUT:
          $cHtml .= $this->CreateInput( $oCtrl[1] );
          break;

        case IS_SELECT:
          $cHtml .= $this->CreateSelect( $oCtrl[1] );
          break;  

        case IS_LABEL:
          $cHtml .= $this->CreateLabel( $oCtrl[1] );
          break;    
        
        case IS_RADIO:
          $cHtml .= $this->CreateRadio( $oCtrl[1] );
          break;  

        case IS_SPINNER:
          $cHtml .= $this->CreateSpinner( $oCtrl[1]["id"], $oCtrl[1]["label"] );  
          break;

        case IS_UPLOAD:
          $cHtml .= $this->CreateBtnUpload( $oCtrl[1]["id"], $oCtrl[1]["lang"] );
          break;      

        case IS_MAP:
          $cHtml .= $this->CreateMap();    
          break;

        case IS_IMAGE:
          $cHtml .= $this->CreateImage( $oCtrl[1]["id"], $oCtrl[1]["image"], $oCtrl[1]["align"], $oCtrl[1]["width"] );
          break;

        default:
          # code...
          break;
      }

    }  

    $cHtml .= "'";

    return $cHtml;
  }

  //----------------------------------------------

  private function CreateInput( $input ) {

    // $cId = "'". $input->cId . "'"; 
    
    $cHtml  = '<div class="item-modal">';
    $cHtml .= ' <div class="left">';
    $cHtml .= '   <label style="width: 100%;padding-right: 10px;' . $input->cAlign . ';">'; 
    $cHtml .= $input->cLabel . ' :</label>';
    $cHtml .= ' </div>';
    $cHtml .= ' <div class="rigth">';
    $cHtml .= '   <input type="' . $input->cType . '" id="' . $input->cId .'" ';
    if ( $input->cPlaceholder ) {
       $cHtml .= 'placeholder="' . $input->cPlaceholder . '" ';
    }   
    $cHtml .= 'style="width: 100%">';
    $cHtml .= ' </div>';
    $cHtml .= '</div>';

    return $cHtml;
  
  }

  //----------------------------------------------

  private function CreateRadio( $radio ) {

    $cHtml  = '<div class="radio-modal">';
    $cHtml .= '<label class="radio-inline" style="font-size: 18px;">';

    if ( $radio->numRadio == 1 ) {
      $cHtml .= '<input type="radio" name="tradio" ';
      $cHtml .= ' onclick="oModal.setindexradio(' . $radio->numRadio . ');"';
      $cHtml .= ' value="' . $radio->cLabel .' "checked>&nbsp' . $radio->cLabel ;
    } else {
      $cHtml .= '<input type="radio" name="tradio" '; 
      $cHtml .= ' onclick="oModal.setindexradio(' . $radio->numRadio . ');"';
      $cHtml .= ' value="' . $radio->cLabel .'">&nbsp' . $radio->cLabel ;
    }
    
    $cHtml .= "</label>";
    $cHtml .= "</div>";

    return $cHtml;
  }

  //----------------------------------------------
  
  private function CreateLabel( $label ) {

    $cHtml  = '<div class="item-modal">';
    $cHtml .= '<label id="' . $label->cId . '" style="font-size: ' . $label->size .' px;">' . $label->cLabel;
    $cHtml .= "</label>";
    $cHtml .= "</div>";

    return $cHtml;
  }

  //----------------------------------------------

  private function CreateSelect( $select ) {

    $cHtml  = '<div class="item-modal">';
    $cHtml .= ' <div class="left">';
    $cHtml .= '   <label style="width: 100%;padding-right: 10px;' . $select->cAlign . ';">' . $select->cLabel .' :</label>';
    $cHtml .= ' </div>';
    $cHtml .= ' <div class="rigth">';
    $cHtml .= '   <select id="' . $select->cId . '" class="form-control"' ;
    $cHtml .= '     style="width: 100%;margin-left: 0px;height: 30px !important">';
    $nItems = count( $select->aItems );
    if ($nItems > 0) {
      if (is_array($select->aItems[0])) {
        for ($item = 0; $item < $nItems; $item++) {
          $cHtml .= '   <option value="'.$select->aItems[$item][0] .'">' . $select->aItems[$item][1] . '</option>';
        }
      } else {
        for ($item = 0; $item < $nItems; $item++) {
          $cHtml .= '   <option>' . $select->aItems[$item] . '</option>';
        }
      }
    } else {
      $cHtml .= '   <option>Ninguno</option>';
    }   
    $cHtml .= '   </select>';
    $cHtml .= ' </div>'; 
    $cHtml .= '</div>'; 
    
    return $cHtml;

  }

  //----------------------------------------------

  private function CreateSpinner( $cId, $label ) {

    $cHtml  = '<div class="item-modal">';
    $cHtml .= '<div class="label-spinner">';
    $cHtml .= '  <label>' . $label .'</label>';
    $cHtml .= '</div>';
    $cHtml .= '<div class="minus-spinner">';
    $cHtml .= '<button type="button" class="btn btn-danger btn-spinner" onclick="clickMinus();">';
    $cHtml .= '  <span class="fa fa-minus"></span>';
    $cHtml .= '</button>';
    $cHtml .= '</div>';
    $cHtml .= '<div class="div-input-spinner">';
    $cHtml .= '  <input type="text" id="'. $cId .'" class="input-spinner" onkeypress="SetSpinner(this);">';
    $cHtml .= '</div>';
    $cHtml .= '<div class="plus-spinner">';
    $cHtml .= '<button type="button" class="btn btn-success btn-spinner" onclick="clickPlus();">';
    $cHtml .= '  <span class="fa fa-plus"></span>';
    $cHtml .= '</button>';
    $cHtml .= '</div>';
    $cHtml .= '</div>';
    
    return $cHtml;

  }

  //----------------------------------------------

  private function CreateBtnUpload( $cId, $cLang ) {

    $cUpload = ($cLang == ES_LANG) ? "Subir" : "Upload";
    $cChoose = ($cLang == ES_LANG) ? "Seleccione Archivo" : "Choose file";
    
    $cHtml  = '<div class="input-group">';
    $cHtml .= '  <div class="input-group-prepend">';
    $cHtml .= '    <span class="input-group-text" id="inputGroupFileAddon01">' . $cUpload .'</span>';
    $cHtml .= '  </div>';
    $cHtml .= '  <div class="custom-file">';
    $cHtml .= '    <input type="file" class="custom-file-input" id="' . $cId . '"'; 
    $cHtml .= '           onchange="oModal.uploadFile()"';
    $cHtml .= '           aria-describedby="inputGroupFileAddon01">';
    $cHtml .= '    <label class="custom-file-label" for="' . $cId . '">' . $cChoose . '</label>';
    $cHtml .= '  </div>';
    $cHtml .= '</div>';
    
    return $cHtml;

  }

  //----------------------------------------------

  private function CreateImage( $cId, $image, $cAlign, $width ) {

    $image = ($image) ? $image : TWEB_PATH . "/images/no-image.png";

    $cHtml  = '<div class="item-modal" style="margin-bottom:10px;">';
    $cHtml .= '<img class="img-responsive" id="' . $cId . '" src="' . $image .'" ';

    switch ( $cAlign ) {
      case AL_CENTER:
        $cHtml .= ' style="margin-left: auto;margin-right: auto;width :' . $width .';">';
        break;

      case AL_LEFT:
        $cHtml .= ' style="margin-left: 0;margin-right: auto;width :' . $width .';">';
        break;
          
      case AL_RIGHT:
        $cHtml .= ' style="margin-left: auto;margin-right: 0;width :' . $width .';">';
        break;    
    }    

    $cHtml .= '</div>';
    
    return $cHtml;

  }

  //----------------------------------------------

  // https://mappinggis.com/2013/06/como-crear-un-mapa-con-leaflet/
  private function CreateMap() {
    
    $cHtml  = '<div class="item-modal">';
    $cHtml .= '<div id="map_canvas" style="width: 100%;height: 480px;box-shadow: 5px 5px 5px #888;"></div>';
    $cHtml .= '</div>';
    
    return $cHtml;
  }
  
  //----------------------------------------------

  private function CreateButtons() {

    $cHtml  = "'";

    $nCount = count( $this->aButtons );

    if ( $nCount > 0 ) {
      foreach($this->aButtons as $index => $value) {
        $cHtml .= '<button type="button" class="btn btn-default" id="' . $value["id"] . '" ';
        if ( $value["action"] ) {
          $cHtml .= 'onclick="' . $value["action"] . '" ';  
        } else {
          $cHtml .= 'onclick="oModal.clearUpload()" ';
          $cHtml .= 'data-dismiss="modal" ';
        }
        if ( !empty($value["color"]) && !empty($value["fondo"]) ) {
          $cHtml .= 'style="color:' . $value["color"] . ';';
          $cHtml .= 'background-color:' . $value["fondo"] . ';'; 
          $cHtml .= 'border-color:' . $value["fondo"] . '"';
        }  
        $cHtml .= '>';
        $cHtml .= $value["texto"] . '</button>' ;
      }
    } else {
      $cHtml .= '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' ;
    }

    $cHtml .= "'";

    return $cHtml;
    
  }

  //----------------------------------------------
  // https://examples.bootstrap-table.com/#options/trim-on-search.html#view-source
  private function CreateTable() {

    $count = count($this->aColumns);

    if ( $count > 0 ) {

      $this->isTable = true;

      if ( count($this->dataTable) == 0 ) {

        $aRow = "[{";

        for ($i=0; $i < $count; $i++) { 
          $row = $this->aColumns[ $i ];  
          $aRow .= '"' . $row["id"] . '" : " ",';
        }

        $aRow = substr($aRow, 0, -1) . "}]";

        $this->dataTable = json_decode($aRow);

      }  

      // https://examples.bootstrap-table.com/#column-options/detail-formatter.html#view-source
      $cHtml  = "'";
      $cHtml .= '<table id="table"';
      $cHtml .= ' data-search="true"';
      $cHtml .= ' data-trim-on-search="false"';   // Evita eliminar el key space
      $cHtml .= ' data-search-highlight="true"';
      // $cHtml .= ' data-search-on-enter-key="true"';
      $cHtml .= ' data-click-to-select="true"';
      $cHtml .= ' data-id-field="id"';
      $cHtml .= ' data-height="' . $this->heigthTable . '"';
      $cHtml .= ' data-pagination="true"';
      $cHtml .= ' data-mobile-responsive="true"';
      $cHtml .= ' data-header-style="headerStyle"';
      $cHtml .= ' data-show-footer="true">';
      $cHtml .= ' <thead>';
      $cHtml .= '   <tr>';
      $cHtml .= '     <th data-checkbox="true"></th>';
      
      for ($i=0; $i < $count; $i++) { 
        
        $row = $this->aColumns[ $i ];  

        $cHtml .= '<th data-field="' . $row["id"]. '" ';

        if ( !empty($row["format"]) ) {
          $cHtml .= 'data-formatter="' . $row["format"]  .'"';
        }

        if ( !empty($row["align"]) ) {
          $cHtml .= 'data-align="' . $row["align"] . '"';
        }
                          
        $cHtml .= '>' . $row["title"] . '</th>';
      }

      $cHtml .= '  </tr>';  
      $cHtml .= ' </thead>';
      $cHtml .= '</table>';

      $cHtml .= "'";

    } else {
      $cHtml = '';
    }  

    return $cHtml;

  }

  //----------------------------------------------

  private function CreateCodeJs() {

    $cId      = '"' . $this->cId . '"';
    $titleclr = '{ clrtext : "' . $this->cTitleClr . '", bgtext : "' . $this->cTitleBg .'"}';
        
    $cJS  = '<script>';
    $cJS .= PHP_EOL; 
    $cJS .= 'var oModal;' . PHP_EOL;
    $cJS .= PHP_EOL; 
    $cJS .= '$(function() {'                                 . PHP_EOL;
    $cJS .= '  oModal = new TModal();'                  . PHP_EOL;
    $cJS .= '  var props = {};'                             . PHP_EOL;
    $cJS .= '  props.id       = ' . $cId                    . PHP_EOL; 
    $cJS .= '  props.title    = ' . $this->titleHtml  . ';' . PHP_EOL; 
    $cJS .= '  props.titleclr = ' . $titleclr  . ';'        . PHP_EOL; 
    $cJS .= '  props.body     = ' . $this->bodyHtml   . ';' . PHP_EOL; 
    $cJS .= '  props.footer   = ' . $this->footerHtml . ';' . PHP_EOL;
    $cJS .= '  props.options  = {backdrop : false, keyboard : false};'  . PHP_EOL;
    $cJS .= '  props.data     = ' . json_encode($this->dataTable) . ';' . PHP_EOL;
    $cJS .= '  props.istable  = ' . (($this->isTable) ? 'true' : 'false') . ';' . PHP_EOL;
    $cJS .= '  props.idupload = "' . $this->idUpload . '";' . PHP_EOL;
    $cJS .= '  props.modal    = oModal;'     . PHP_EOL;
    $cJS .= "  oModal.setmodal( props );"    . PHP_EOL;
    $cJS .= '});'                            . PHP_EOL; 
    $cJS .= PHP_EOL;
    $cJS .= '</script>';

    echo $cJS;
    
	}

  //----------------------------------------------

  private function LibsBootstrapTable() {

    $cHtml = '';

    if ( $this->isTable ) {

      $cHtml .= '<!-- PLUGIN BOOTSTRAP TABLES  -->';

      // Load BootStrap Table CSS
      $cHtml .= '<link href="' . TWEB_PATH . '/libs/bootstrap-table/bootstrap-table.css" rel="stylesheet">';

      // Load BootStrap Table JS
      $cHtml .= '<script src="' . TWEB_PATH . '/libs/bootstrap-table/bootstrap-table.js"></script>';
      $cHtml .= '<script src="' . TWEB_PATH . '/libs/bootstrap-table/bootstrap-table-locale-all.min.js"></script>';
      $cHtml .= '<script src="' . TWEB_PATH . '/libs/bootstrap-table/extensions/mobile/bootstrap-table-mobile.js"></script>';

    }  

    $cHtml .= '<!-- PLUGIN CORE.MODAL.JS -->';

    // Load JavaScript Library
		$cHtml .= '<script src="' . TWEB_PATH . 'core.modal.js"></script>';
    if ( $this->lSpinner ) {
       $cHtml .= '<link href="' . TWEB_PATH . '/modal.spinner.css" rel="stylesheet">';
       $cHtml .= '<script src="' . TWEB_PATH . 'modal.spinner.js"></script>';
    }   

    echo $cHtml ;
          
  }
  
  //----------------------------------------------

  public function Activate() {

    // Cargamos Las librerias de tables bootstrap
    $this->LibsBootstrapTable();
    
    // Agregamos el codigo a la clase
    $this->titleHtml   = "'" . '<h5 class="modal-title">' .$this->cTitleText . '</h5>' . "'";
    $this->bodyHtml    = $this->CreateControlsBody();
    $this->bodyHtml   .= $this->CreateTable();
    $this->footerHtml  = $this->CreateButtons();
    
    // codigo javascript
    $this->CreateCodeJs();
    
  }

} 

//----------------------------------------------

class TModalInput {
  public $cLabel        = '';
  public $cId           = '';
  public $cType         = TYPE_TEXT;
  public $cAlign        = LBL_LEFT;
  public $cPlaceholder  = '';
  
  function __construct( $cLabel = '', $cId = '', $cType = TYPE_TEXT, $cAlign = LBL_LEFT, $cHolder = '' ) {
    $this->cLabel       = $cLabel;
    $this->cId          = $cId;
    $this->cType        = $cType;
    $this->cAlign       = $cAlign;
    $this->cPlaceholder = $cHolder;
  }
  
}

//----------------------------------------------

class TModalSelect {
  public $cLabel = '';
  public $cId    = '';
  public $cAlign = LBL_LEFT;
  public $aItems = [];
  
  function __construct( $cLabel = '', $cId = '', $cAlign = LBL_LEFT, $aItems = [] ) {
    $this->cLabel = $cLabel;
    $this->cId    = $cId;
    $this->cAlign = $cAlign;
    $this->aItems = $aItems;
  }
  
}

//----------------------------------------------

class TModalRadio {
  public $cLabel   = '';
  public $numRadio = 0;
  
  function __construct( $cLabel = '', $numRadio = 0 ) {
    $this->cLabel   = $cLabel;
    $this->numRadio = $numRadio;
  }
  
}

//----------------------------------------------

class TModalLabel {
  public $cLabel   = '';
  public $cId      = '';
  public $size     = 18;
  
  function __construct( $cLabel = '', $cId = '', $size = 18 ) {
    $this->cLabel = $cLabel;
    $this->cId    = $cId;
    $this->size   = $size;
  }
  
}

?>
 
