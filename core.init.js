function ConfigCalendar() {
	
	$.datepicker.regional.ca = {
		firstday: 1,
		closeText: 'Cerrar',
		prevText: '<Ant',
		nextText: 'Seg>',
		currentText: 'Hoy',
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septembre', 'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
		dayNamesShort: ['Dom','Lun','Mar','Mie','Jue','Vie','Sab'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	};	 	 
	 	
	jQuery(function($){
		$.datepicker.setDefaults( $.datepicker.regional['ca'] );
	});	
}

function ConfigTime() {

	$.timepicker.regional.ca = {
		closeText: 'Cerrar',
		currentText: 'Ahora', 
		timeOnlyTitle: 'Elegir una hora',
		timeText: 'Hora',
		hourText: 'Horas',
		minuteText: 'Minutos', 
		timeFormat: 'hh:mm TT',
		stepMinute: 10,
		addSliderAccess: false,
		sliderAccessArgs: { touchonly: false }
	};
	
	jQuery(function($){ 
		$.timepicker.setDefaults( $.timepicker.regional.ca );
	});	
	
}

function ConfigCombobox() {  /* Definición del pluggin en tweb\libs\multiple-select\ */

	$.fn.multipleSelect.defaults.selectAllText 	= 'Selecciona todo'  ;	
	$.fn.multipleSelect.defaults.allSelected 	= 'Todo seleccionado'  ;	
	$.fn.multipleSelect.defaults.countSelected  = '# de % seleccionado' ;	
	$.fn.multipleSelect.defaults.noMatchesFound = "No se han encontrado coincidencias"  ;	
}

ConfigCalendar();
ConfigTime();
ConfigCombobox();

