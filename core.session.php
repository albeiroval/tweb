<?php
/*
*  https://code.tutsplus.com/es/tutorials/how-to-use-sessions-and-session-variables-in-php--cms-31839
*  https://www.php.net/manual/es/function.session-start.php
*/
if (session_status() == PHP_SESSION_NONE) {
    session_start();	
}

include_once( "core.request.php" );
include_once( "core.constant.php" );

//-----------------------------------------------

Class TSession {

	private $aVars   		= [];
	public  $nTimeOut   = 1800 ;			//  60 = 1 Minut ; 1800 = 30 min.	
	public  $lOnlyAjax  = false ;			
	public  $lExeError  = true ;			

	private $cName			= 'core';
	private $cAction		= '';
	private $cParam			= '';
	private $aLog				= [];

	//------------------------------------------------

	public function __construct( $cName = '' ) {     
		$this->cName  	= $cName;		
	}

	//------------------------------------------------
	
	public function Login() {	
	
		$aSys = [	'acces' 	=> true,
							'id'			=> session_id()	,
							'ip' 			=> $this->GetIp(),						
							'time' 		=> time(),
							'timeout'	=> $this->nTimeOut ];
				
		$_SESSION[ $this->cName ][ 'sys' ] = $aSys ;
		
		if ( count( $this->aVars ) > 0 ) {
		
			foreach ( $this->aVars as $key => $val ) {
				$_SESSION[ $this->cName ][ "vars" ][ $key ] = $val;
			}
			
		}

		return true;	
	}

	//------------------------------------------------
	
	private function GetLapsus() {
		if ( isset( $_SESSION[ $this->cName ] ) == false ) {   
			return 0;
		}	
		
		return ( time() - $_SESSION[ $this->cName ][ 'sys' ][ 'time' ] );					
	}

	//------------------------------------------------
	
	private function InitLapsus() {
	
		if ( isset( $_SESSION[ $this->cName ] ) == false ) {   
			return null;
		}	
		
		$_SESSION[ $this->cName ][ 'sys' ][ 'time' ] = time();		
	}
	
	//------------------------------------------------

	public function Logout() {	

		$_SESSION[ $this->cName ] = null;
		
		unset( $_SESSION[ $this->cName ] );		

		return true;
	}

	//------------------------------------------------
	
	public function AddVar( $cVar = '', $uValue = '' ) {	
		$this->SetVar( $cVar, $uValue ); 
	}

	//------------------------------------------------
	
	public function SetLog( $cLog = '' ) { 
		$this->aLog[] = $cLog; 
	}

	//------------------------------------------------
	/*
	* Si NO existe la variable de session de TWEB es que todavia no hemos hecho Login(). 
	* En este caso se guarda la variable en un array y al llamar Login() se crea.
	* Si existe se borra la variable y se vuelve a crear
	*/

	public function SetVar( $cVar = '', $uValue = '' ) {		
	
		if ( !isset( $_SESSION[ $this->cName ] ) ) {  
			$this->aVars[ $cVar ] = $uValue ;
		} else {		
			if ( isset( $_SESSION[ $this->cName ][ "vars" ][ $cVar ] ) ) {
				unset( $_SESSION[ $this->cName ][ "vars" ][ $cVar ] );
			}
			$_SESSION[ $this->cName ][ "vars" ][ $cVar ] = $uValue;
		}	

	}

	//------------------------------------------------
	
	public function GetVar( $cVar = '' ) {	

		if ( isset( $_SESSION[ $this->cName ] ) == false ) {   
			return null;
		}
	
		$aVars =  $_SESSION[ $this->cName ][ "vars" ];

		if ( array_key_exists( $cVar, $aVars ) ) {		
			$uValue = $_SESSION[ $this->cName ][ "vars" ][ $cVar ];
		} else {		
			print("Variable de SESION ".$cVar." No Existe");
			$uValue = null;		
		}

		return $uValue;		
	}

	//------------------------------------------------

	public function GetVars() {	

		if ( isset( $_SESSION[ $this->cName ] ) == false ) {   
			return null;
		}
		
		$aVars 	= ( isset( $_SESSION[ $this->cName ][ "vars"] ) ? $_SESSION[ $this->cName ][ "vars" ] : null );
	
		return $aVars;
	}		

	//------------------------------------------------
	
	public function GetSys() {	

		if ( isset( $_SESSION[ $this->cName ] ) == false ) {   
			return null;
		}
		
		$aSys 	= $_SESSION[ $this->cName ][ 'sys' ];	
	
		return $aSys;
	}		

	//------------------------------------------------
	
	public function Valid( $uAction = null ) {			

		//	Init Vars....
		
		$nLapsus = $this->GetLapsus();
	
		//	Inicialitzo accio...
		
		$this->cAction 	= '';
		$this->cParam 	= '';		
		
		$cType = gettype( $uAction );			
		switch ( $cType ) {
		
			case 'array':
				reset( $uAction );
				$this->cAction 	= key( $uAction );
				$this->cParam 	= $uAction[ $this->cAction ];	// 'url', 'function'
				break;
			
			case 'string':
				$this->cAction = 'url';			
				$this->cParam  = $uAction ;			
				break;						

		}
		
		$lAjax = $this->Is_Ajax(); 

		if ( $this->lOnlyAjax && $lAjax == false ) {
			die();
		}					

		//	NO existe Session
		if ( isset( $_SESSION[ $this->cName ] ) == false ){				
	
			if ( $this->lExeError ) {						
			
				$this->Kill( $lAjax, TWEB_ERROR_SESSION_101 );
				
			} else {
			
				return false;
			}
		}	

		//	Timeout !!!
		$aSys 			= $this->GetSys();
		$nTimeOut 	= $aSys[ 'timeout' ];

		if ( $nLapsus > $nTimeOut ){				

			if ( $this->lExeError ) {						
			
				$this->Kill( $lAjax, TWEB_ERROR_SESSION_102 );
				
			} else {
			
				return false;
			}
		}			

		//	Session erronea !
		if ( $_SESSION[ $this->cName ][ 'sys'][ 'id' ] <> session_id() ) {
		
			if ( $this->lExeError ) {				

				$this->Kill( $lAjax, TWEB_ERROR_SESSION_103 );		
				
			} else {
			
				return false;				
			}
		}

		$this->InitLapsus(); 

		return true;		
	}

	//------------------------------------------------

	public function	Kill( $lAjax, $cError = TWEB_ERROR_SESSION ) {

		if ( $lAjax ) {	
						
			switch( $this->cAction ) {			
				case 'url':					
					header("HTTP/1.0 901 " . $this->cParam );
					break;
					
				case 'function':
					header("HTTP/1.0 902 " . $this->cParam );
					break;	

				default:
					header("HTTP/1.0 900 " . $cError );
					break;													
			}
			
		} else {

			$cUrl = '';
			
			switch( $this->cAction ) {
			
				case 'url':
					$cUrl = $this->cParam ;
					break;												
			}			
	
	
			/*	Quan la crida la fem que NO es Ajax, tenim 2 solucions:
				1.- Executarem un javascript que redirijirà a algun lloc, p.e. index.php 
				2.- Senzillament matem el proces amb un die(). Això és util per si s'executa a pel desde la URL
			*/						

			if ( $cUrl !== '' ) {
			
				/*
				* Eso iniciará la caché del búfer de salida y las cabeceras no se enviarán hasta finalizar el script.
				*/
				ob_start();
				$cCmd  = "<script type='text/javascript'>";
				$cCmd .= '   window.location.href = "' . $cUrl . '" ;';
				$cCmd .= "</script>";				
					
				echo $cCmd ;
				
				// header( "Location: " . $cUrl );

				die();
				
			} else {						

				die();
			}
			
		}

		exit();		
	}	

	//------------------------------------------------
	
	public function GetUser() { 
		return $this->GetVar( 'user' ); 
	}

	//------------------------------------------------
	
	public function GetIp() { 

		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		return $ip;		
	}

	//------------------------------------------------

	public function Is_Ajax() {
	
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
			return true;
		else
			return false;		
	}
	
}

?>