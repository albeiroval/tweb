/*
*   TWEB PLUS : Class TSelectPicker
*   AUTOR     : Albeiro Valencia
*   FECHA     : 01/02/2021
*   DOCS      : https://github.com/snapappointments/bootstrap-select
*/

class TSelect {

  constructor(cId) {
    this.cId = cId;
    $('#' + this.cId).selectpicker();
  }  

  //----------------------------------
  getIndex() {
    return $('#' + this.cId + ' option:selected').val();
  }

  //----------------------------------
  getValue() {
    return $('#' + this.cId + ' option:selected').text();
  }

  //----------------------------------
  setValue = function (value) {
    $('#' + this.cId).selectpicker('val', value);
  }

  //----------------------------------
  hide() {
    $("#" + this.cId).selectpicker('hide');
  }

  //----------------------------------
  show() {
    $("#" + this.cId).selectpicker('show');
  }

  //----------------------------------
  disabled() {
    $("#" + this.cId).prop('disabled', true);
    $("#" + this.cId).selectpicker('refresh');
  }

  //----------------------------------
  enabled() {
    $("#" + this.cId).prop('disabled', false);
    $("#" + this.cId).selectpicker('refresh');
  }

  //----------------------------------
  addOptions(items) {
    var options = [];
    var _options;
    if (Array.isArray(items)) {
      for (var i = 0; i < items.length; i++) {
        var option = '<option value="' + items[i] + '">' + items[i] + '</option>';
        options.push(option);
      }
      _options = options.join('');
      $("#" + this.cId).html(_options).selectpicker('refresh');
    } else {
      JMsgError("items debe ser un array");
    }
  }

}
