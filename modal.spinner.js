/*
*   TVALWEB Version 1.0
*   AUTOR : ALBEIRO VALENCIA
*   FECHA : 30/09/2021
*/

var idInput = null;

function isNumber(e) {
  e = e || window.event;
  var charCode = e.which ? e.which : e.keyCode;
return /\d/.test(String.fromCharCode(charCode));
}

function SetSpinner( element ) {
  idInput = element.id;
}

function clickMinus() {
  if (idInput) {
    var elem = document.getElementById(idInput);
    if (elem) {
      let numero = parseInt( elem.value );
      numero = numero - 1 ;
      elem.value = numero;
      elem.focus();
    }  
  }  
}

function clickPlus() {
  if (idInput) {
    var elem = document.getElementById(idInput);
    if (elem) {
      let numero = parseInt( elem.value );
      numero = numero + 1 ;
      elem.value = numero;
      elem.focus();
    }  
  }
}

// FINAL
