<?php
include "config.php";
include "utils.php";

$dbConn =  connect($db);
$todook = false;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $input = file_get_contents('php://input');
      $data = json_decode($input, true);
      if (!isset($data['ciaid'])) {
            header("HTTP/1.1 401 Unauthorized");
            $resultar["estado"] = "-1";
            $resultar["mensaje"] = "falta parametro 'ciaid'";
            echo json_encode($resultar);
            exit();
      }

      if (!isset($data['datos'])) {
            header("HTTP/1.1 400 error");
            $resultar["estado"] = "-3";
            $resultar["mensaje"] = "falta parametro 'datos'";
            echo json_encode($resultar);
            exit();
      }
      if (isset($data['datos'])) {
            if (strlen(trim($data['datos'])) == 0) {
                  header("HTTP/1.1 400 error");
                  $resultar["estado"] = "-1";
                  $resultar["mensaje"] = "'datos' no puede estar vacio";
                  echo json_encode($resultar);
                  exit();
            }
            if (!validBase64($data['datos'])) {
                  header("HTTP/1.1 400 error");
                  $resultar["estado"] = "-1";
                  $resultar["mensaje"] = "'datos' no esta encriptado base64";
                  echo json_encode($resultar);
                  exit();
            }
            $decoded = base64_decode($data['datos'], true);
            $registros = json_decode($decoded, true);
            if (!is_array($registros)) {
                  header("HTTP/1.1 400 error");
                  $resultar["estado"] = "-1";
                  $resultar["mensaje"] = "'datos' no es un arreglo";
                  echo json_encode($resultar);
                  exit();
            } else {
                  if (count($registros) > 500) {
                        header("HTTP/1.1 400 error");
                        $resultar["estado"] = "-1";
                        $resultar["mensaje"] = "'datos' no debe exceder de 500 registros";
                        echo json_encode($resultar);
                        exit();
                  }
            }
            $rucemi = $data['ciaid'];
            $nomarchivo = 'regdoce' . $data['ciaid'] . '';
            $keydata = array();
            foreach($registros as $arrayIndex => $row){
                  $coddoc = $row['coddoc'];
                  $numdoc = $row['numdoc'];
                  if(!isset($keydata[$coddoc])) {
                        $keydata[$coddoc] = array();
                  }
                  $keydata[$coddoc][] = $numdoc;
            }
            //echo json_encode($keydata),"\n";
            //exit();
            foreach($keydata as $arrayIndex => $key){
                  $param1 = ":coddoc" ;
                  $toBind1 = $arrayIndex; 
                  $param2 = "" ;
                  $ids = $keydata[$arrayIndex];
                  //$toBind2 = implode("','", $ids);
                  $comma = '';
                  for($i=0; $i<count($ids); $i++){
                    $param2 .= $comma.':p'.$i;       // :p0, :p1, ...
                    $comma = ',';
                  } 
                                   
                  $sql = "DELETE FROM `$nomarchivo` WHERE coddoc= ".$param1." and numdoc IN( ".$param2. " )";
                  $sql_array[] = $sql;
                  $statement = $dbConn->prepare($sql);
                  
                  $statement->bindValue($param1, $toBind1, PDO::PARAM_STR);
                  for($i=0; $i<count($ids); $i++){
                        $statement->bindValue(':p'.$i, $ids[$i]);
                  }
                  
                  try {
                        $statement->execute();
                  } catch (Exception $e) {
                        header("HTTP/1.1 400 error al ejecutar query");
                        $resultar["estado"] = "-1";
                        $resultar["mensaje"] = $e;
                        echo json_encode($resultar);
                        exit();
                  }
      
            }
            $fecha  = date('Y-m-d');
            $hora   = date("H:i");

            $columnNames = array_keys($registros[0]);
            //agregamos campos nuevos que no vienen de origen
            if (!in_array("datsta", $columnNames)) {
               $columnNames[] = "datsta";
            }
            if (!in_array("timsta", $columnNames)) {
                  $columnNames[] = "timsta";
            }
            //echo json_encode($columnNames),"\n";

            $toBind = array();
            $rowsSQL = array();

            $i = 0;
              
            foreach($registros as $arrayIndex => $row){
                $params = array();
                if(!isset($row['datsta'])) {
                  $row['datsta'] = $fecha;
            }
                if(!isset($row['timsta'])) {
                  $row['timsta'] = $hora;
            }
                //echo json_encode($row),"\n";
                foreach($row as $columnName => $columnValue){
                    $param = ":" . $columnName . $arrayIndex;
                    $params[] = $param;
                    $toBind[$param] = $columnValue; 
                }
                $rowsSQL[] = "(" . implode(", ", $params) . ")";
            }
            $sql = "INSERT INTO `$nomarchivo` (" . implode(", ", $columnNames) . ") VALUES " . implode(", ", $rowsSQL);
            $statement = $dbConn->prepare($sql);
            //echo json_encode($sql), "\n";

            foreach($toBind as $param => $val){
                $statement->bindValue($param, $val);
            }
           
            try {
                  $statement->execute();
                  $todook = true;
            } catch (Exception $e) {
                  header("HTTP/1.1 400 error al ejecutar query");
                  $resultar["estado"] = "-1";
                  $resultar["mensaje"] = $e;
                  echo json_encode($resultar);
                  exit();
            }


      }
} else {      
      header("HTTP/1.1 400 Metodo no permitido");
      $resultar["estado"] = "-1";
      $resultar["mensaje"] = "Metodo no permitido";
      echo json_encode($resultar);
}
$dbConn = null;

if ($todook) {
      header("HTTP/1.1 200 OK");
      $resultar["estado"] = "1";
      $resultar["mensaje"] = "OK";
      echo json_encode($resultar);
}