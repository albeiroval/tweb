/*
*  Author and copyright: Albeiro Valencia (http://avcsistemas.com/foro)
*  CORE para manejo de la clase TModal.
*  Fecha : 11/09/2020 Derechos Reservados
*  https://github.com/shaack/bootstrap-show-modal
*  https://www.jqueryscript.net/demo/Dynamic-Bootstrap-4-Modals/
*  https://www.codehim.com/download/dynamic-bootstrap-modals.zip
*/

/* jshint esversion: 8 */

var aTModal    = [];
var curTModal  = null;

class TModal {

  constructor( _idModal = "" ) {

    this.header   = { color: '', backGround: '' };

    this.aModal   = aTModal; 

    this.map      = { idmap: null, latitud: null, longitud: null };
    this.image    = { witdh: '50%', height: '50% ' };

    // Upload Files
    this.validFile    = false;
    this.uploadBase64 = null;
    this.uploadFile   = null;
    this.uploadType   = null;

    // Array de datos 
    this.tableData = [];

    // Manejo de controles del Modal
    this.propmodal  = {};
    this.propmodal.idfocus     = ''; 
    this.propmodal.setvalue    = [];
    this.propmodal.imgBase64   = [];
    this.propmodal.image       = [];
    this.propmodal.disabled    = [];
    this.propmodal.enabled     = [];
    this.propmodal.btnfunction = [];
    this.propmodal.onvalid     = [];
    this.propmodal.onchange    = [];
    this.propmodal.footerClr   = '';
    
    // Propiedades para trabajar con la calse Modal
    this.props = {  };
    this.props.idmodal  = _idModal;   
    this.props.title    = "";
    this.props.body     = "";
    this.props.footer   = "";
    this.props.options  = null;
    this.props.data     = null;
    this.props.istable  = false;
    this.props.idupload = null;
    this.props.width    = null;
    
    this.listbox  = [];
    this.checked  = [];
    
    // Array's de TModal
    
    // Load getProperties
    this.getprops();

  }

  //-----------------------------------------------------

  setmodal(props) {
    aTModal.push(props);
  }
  
  //-----------------------------------------------------

  getprops() {
    if ( this.props.idmodal ) {
      var length = this.aModal.length;
      for (let index = 0; index < length; ++index) {
        if (this.aModal[index].id == this.props.idmodal ) {
          this.props.idmodal      = this.aModal[index].id;
          this.props.title        = this.aModal[index].title;
          this.props.body         = this.aModal[index].body;
          this.props.footer       = this.aModal[index].footer;
          this.props.options      = this.aModal[index].options;
          this.props.data         = this.aModal[index].data;
          this.props.istable      = this.aModal[index].istable;
          this.props.idupload     = this.aModal[index].idupload;
          this.props.width        = this.aModal[index].width;
          this.header.color       = this.aModal[index].titleclr.clrtext;
          this.header.backGround  = this.aModal[index].titleclr.bgtext;
          break;
        }
      }
    }
  }

  //-----------------------------------------------------

  show() {
    var self = this;

    // create props
    var props = {};
    props.idModal          = this.props.idmodal;
    props.body             = this.props.body;
    props.footer           = this.props.footer;
    props.options          = this.props.options;
    props.istable          = this.props.istable;
    props.data             = this.props.data;
    props.modalDialogClass = this.props.width + " modal-shadow";

    if (this.props.title) {
      props.title = this.props.title;
    }

    // onShow
    props.onShown = function() {
    
      // set focus
      if (self.propmodal.idfocus) {
        $(self.propmodal.idfocus).focus();
      }
    
      // set's values
      if (self.propmodal.setvalue) {
        $.each(self.propmodal.setvalue, function (index, obj) {
          let elem = document.getElementById(obj.id);
          if ( elem ) {
            let atr = elem.getAttribute("data-name") ;
            if ( atr == "select" ) {  
              let cId = "#" + obj.id ;
              $(cId).selectpicker('val', obj.value);
            } else {
              // setTimeout(function() { elem.value = obj.value; }, 50);
              elem.value = obj.value;
            }  
          }  
        });
      }
    
      // Set images base64
      if (self.propmodal.imgBase64) {
        $.each(self.propmodal.imgBase64, function (index, obj) {
          self.showImage(obj);
        });
      }
    
      // Disabled id's
      if (self.propmodal.disabled) {
        $.each(self.propmodal.disabled, function (index, id) {
          $(id).prop('disabled', true);
        });
      }
    
      // Enabled id's
      if (self.propmodal.enabled) {
        $.each(self.propmodal.enabled, function (index, id) {
          $(id).prop('disabled', false);
        });
      }
    
      if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        $('.selectpicker').selectpicker('mobile');
      } else {
        $('.selectpicker').selectpicker({ size: 10 });
      }
    };

    // onCreate
    props.onCreate = function() {
     
      // button's onClick function
      if (self.propmodal.btnfunction) {
        var aItems = self.propmodal.btnfunction;
        for (let index = 0; index < aItems.length; index++) {
          const item = aItems[index];
          let elem = document.getElementById(item.id);
          if ( elem ) {
            elem.removeAttribute("onclick");
            elem.onclick = function(event) { 
              event.preventDefault();
              self.executeFunction(item.action);
            };  
          }
        }
      }
     
      // onvalid input's function
      // https://keycode.info/
      if (self.propmodal.onvalid) {
        var aItems = self.propmodal.onvalid;
        for (let index = 0; index < aItems.length; index++) {
          const item = aItems[index];
          let elem = document.getElementById(item.id);
          if ( elem ) {
            elem.addEventListener("keydown", event => {
              event.preventDefault();
              if (event.code === "Tab") {
                self.executeFunction(item.action);
              } else if (event.code === "Enter") {
                self.executeFunction(item.action);
              }
            });
          }
        }
      }
     
      // onchange function
      if (self.propmodal.onchange) {
        var aItems = self.propmodal.onchange;
        for (let index = 0; index < aItems.length; index++) {
          const item = aItems[index];
          $("select").on('change', function(){
            self.executeFunction(item.action, this.value);
          });
        }  
      }
    };  

    if (props) {
      curTModal = new Modal(props);
    }

    // Set css header and footer
    this.setHeaderCss();
    this.setFooterCss();

    // Show Table 
    if (props.istable) {
      var $table = $('#table');
      $table.bootstrapTable('destroy');
      $table.bootstrapTable({ data: this.tableData, locale: 'es-ES' });
    }

    // Show Map
    if (this.map.idmap) {
      this.createMap();
    }

    // Show Image's
    this.createimages();

    // Show listbox
    for (let index = 0; index < this.listbox.length; index++) {
      let item = this.listbox[index];
      this.createlistbox(item.id, item.data);
    }

    //Show checked
    for (let index = 0; index < this.checked.length; index++) {
      let item = this.checked[index];
      this.createcheked( item.id, item.value );
    }
    
  }

  //-----------------------------------------------------

  showImage(obj) {
    // 'data:image/png;base64,'
    const dataType = "data:" + this.uploadType + ";base64,";
    $(obj.id).attr('src', dataType + obj.src);
    const elem = document.getElementById(obj.id.substring(1));
    if (elem) {
      const css = "width : " + this.image.witdh + "; height: " + this.image.height + "; margin : auto";
      elem.style.cssText = css;
    }
  }

  //-----------------------------------------------------

  setHeaderCss() {
    var elem = document.getElementById('modal-header');
    if (elem) {
      var css = "color: " + this.header.color + "; background-color: " + this.header.backGround;
      elem.style.cssText = css;
    }
  }

  //-----------------------------------------------------

  setFooterColor(cColor) {
    if (typeof this.propmodal.footerClr === "undefined") {
      this.propmodal.footerClr = '';
    }
    this.propmodal.footerClr = cColor;
  }

  //-----------------------------------------------------

  setFooterCss() {
    var elem = document.getElementById('modal-footer');
    if (elem && this.propmodal.footerClr) {
      elem.style.backgroundColor = this.propmodal.footerClr;
    }
  }

  //-----------------------------------------------------
  // https://stackoverflow.com/questions/359788/how-to-execute-a-javascript-function-when-i-have-its-name-as-a-string

  executeFunction(functionName, params) {
    if (typeof functionName == 'function') {
      if ( params) {
        functionName.apply(null, [params]);
      } else {
        functionName.apply(null, null);
      }  
    } else {
      alert('Error function No Existe: ' + functionName);
    }  
  }

  //-----------------------------------------------------
  
  ontitle(title, clrTxt, clrBack) {
    if (title == null || title == '') { title = 'TEST TMODAL'; }
    if (clrTxt == null || clrTxt == '') { clrTxt = this.header.color; }
    if (clrBack == null || clrBack == '') { clrBack = this.header.backGround; }
    this.props.title = '<h5 class="modal-title">' + title + '</h5>';
    this.header.color = clrTxt;
    this.header.backGround = clrBack;
  }

  //-----------------------------------------------------
  
  hide() {
    if (curTModal) {
      this.map = { idmap: null, latitud: null, longitud: null };
      this.clearUpload();
      curTModal.hide();
    }
  }

  //-----------------------------------------------------
  
  get(cId) {
    var value = "";
    let elem = document.getElementById(cId)
    if ( elem ) {
      value = elem.value;
    }
    return value;
  }

  //-----------------------------------------------------
  
  iniset(cId, value) {
    let element = document.getElementById(cId);
    if (element) {
      element.value = value;
    } else {
      if (typeof this.propmodal.setvalue === "undefined") {
        this.propmodal.setvalue = [];
      }
      if (value) {
        var idCtrl = cId;  
        var index = this.propmodal.setvalue.findIndex(p => p.id === idCtrl);
        if (index === -1) {
          this.propmodal.setvalue.push({ id: idCtrl, value: value });
        } else {
          this.propmodal.setvalue[index].value = value;
        }
      }
    }  
  }
  
  //-----------------------------------------------------
  
  set(cId, value) {
    this.iniset(cId, value);
  }

  //-----------------------------------------------------
  
  setlistbox(cId, aData, cDefault) {
    var self = this;
    var elem = document.getElementById(cId);
    if ( elem ) {
      setTimeout(function() { 
          self.createlistbox(cId, aData);  
          if ( cDefault ) {
            $("#"+cId).selectpicker('val', cDefault); 
          }  
        }, 300 );  
    } else {
      this.listbox.push({ id: cId, data: aData });  
    }  
  }

  //-----------------------------------------------------
  
  createlistbox(cId, aData) {
    if ( Array.isArray(aData) ) {
      var aItems;
      var options = [];
      for (let i = 0; i < aData.length; i++) {
        var option = '<option value="'+aData[i]+'" data-token="'+aData[i]+'">'+aData[i]+'</option>';
        options.push(option);
      }
      aItems = options.join('');
      $("#"+cId).html(aItems).selectpicker('refresh');
    } else {
      JMsgError("items debe ser un array");
    }
  }

  //-----------------------------------------------------
  
  setvalue_listbox = function(cId, value ) {
    setTimeout( function() { 
        $("#"+cId).selectpicker('val', value); 
      }, 300 );
  }

  //-----------------------------------------------------
  
  enabled(cId) {
    let element = document.getElementById(cId);
    if ( element ) {
      element.disabled = false;
    } else {  
      if (typeof this.propmodal.enabled === "undefined") {
        this.propmodal.enabled = [];
      }
      var idCtrl = "#" + cId;
      var index = this.propmodal.enabled.indexOf(idCtrl);
      if (index === -1) {
        this.propmodal.enabled.push(idCtrl);
      } else {
        this.propmodal.enabled[index] = idCtrl;
      }
    }  
  }

  //-----------------------------------------------------
  
  disabled(cId) {
    let element = document.getElementById(cId);
    if ( element ) {
      element.disabled = true;
    } else {  
      if (typeof this.propmodal.disabled === "undefined") {
        this.propmodal.disabled = [];
      }
      var idCtrl = "#" + cId;
      var index = this.propmodal.disabled.indexOf(idCtrl);
      if (index === -1) {
        this.propmodal.disabled.push(idCtrl);
      } else {
        this.propmodal.disabled[index] = idCtrl;
      }
    }  
  }

  //-----------------------------------------------------
  
  inifocus(cId) {
    let elem = document.getElementById(cId);
    if ( elem ) {
      elem.focus();
    } else {
      this.propmodal.idfocus = "#" + cId;
    }  
  }

  //-----------------------------------------------------

  focus(cId) {
    this.inifocus(cId);
  }

  //-----------------------------------------------------

  onvalid(cId, bfunc) {
    if (bfunc) {
      var index = this.propmodal.onvalid.findIndex(p => p.id === cId);
      if (index === -1) {
        this.propmodal.onvalid.push({ id: cId, action: bfunc });
      } else {
        this.propmodal.onvalid[index].action = bfunc;
      }
    }
  }

  //-----------------------------------------------------
  
  onchange(cId, bfunc) {
    if (bfunc) {
      var index = this.propmodal.onchange.findIndex(p => p.id === cId);
      if (index === -1) {
        this.propmodal.onchange.push({ id: cId, action: bfunc });
      } else {
        this.propmodal.onchange[index].action = bfunc;
      }
    }
  }

  //-----------------------------------------------------

  btnfunction(cId, bfunc) {
    if (bfunc) {
      var index = this.propmodal.btnfunction.findIndex(p => p.id === cId);
      if (index === -1) {
        this.propmodal.btnfunction.push({ id: cId, action: bfunc });
      } else {
        this.propmodal.btnfunction[index].action = bfunc;
        let elem = document.getElementById(cId);
        if (elem) {
          elem.removeEventListener("click", bfunc); 
        }  
      }
    }
  }

  //-----------------------------------------------------

  setTable = function( aData ) {
    this.tableData = aData;
  }

  //-----------------------------------------------------
  
  getTable() {
    var $table = $('#table');
    return $table.bootstrapTable('getData');
  }

  //-----------------------------------------------------
  
  getTableSelec() {
    var $table = $('#table');
    return $table.bootstrapTable('getSelections');
  }

  //------------------------------------------------------
  
  addTableRow = function( aRow ) {
    var $table = $('#table');
    $table.bootstrapTable('append', aRow );
  }

  //------------------------------------------------------

  delTableRow = function() {
    var $table = $('#table');
    var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
      return row.id
    })
    $table.bootstrapTable('remove', {
      field: 'id',
      values: ids
    })
  }

  //-----------------------------------------------------
  
  tablerowselect = function() {
    var $table = $('#table');
    var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
      return row;
    });
    if ( ids.length > 0 ) {
       return true; 
    } else {
      return false;
    }
  };

  //-----------------------------------------------------
  
  getvalueradio(cId) {
    var value = '';
    var radios = document.getElementsByName('radio-' + cId);
    for (let i = 0; i < radios.length; i++) {
      if (radios[i].checked) {
        value = radios[i].value;
        break;
      }
    }
    return value;
  }

  //-----------------------------------------------------
  
  getchecked = function(cId) {
    var value = 0;
    var elem = document.getElementById(cId);
    if (elem) {
      if (elem.checked) {
        value = 1;
      }
    }
    return value;
  }

  //-----------------------------------------------------
  
  setchecked = function(cId, lCheck = true) {
    var data = (typeof lCheck === "boolean") ? lCheck : true;
    this.checked.push({ id : cId, value : data }); 
  } 

  //-----------------------------------------------------
  
  createcheked(cId, value) {
    let elem = document.getElementById(cId);
    if (elem) {
      elem.checked = value;
    }
  }

  //-----------------------------------------------------
  
  setimage(cId, image) {
    this.propmodal.image.push({ id : cId, src : image});
  }

  //-----------------------------------------------------
  
  createimages = function() {
    for (let index = 0; index < this.propmodal.image.length; index++) {
      var item = this.propmodal.image[index];
      var elem = document.getElementById(item.id);
      if (elem) {
        if (this.checkimage(item.src)) { 
          elem.setAttribute('src', item.src);
        } else {
          elem.setAttribute('src', TWEB_PATH_IMAGES + '/no-image.jpg');
        }
      }
    }  
  } 
  
  //-----------------------------------------------------
  
  checkimage(urlFile) {
    var xhr = new XMLHttpRequest();
    xhr.open('HEAD', urlFile, false);
    xhr.send();
   
    if (xhr.status == "404") {
      return false;
    } else {
      return true;
    }
  }
  
  //-----------------------------------------------------
  
  setImageBase64(cId, base64, w, h) {
    if (typeof this.propmodal.imgBase64 === "undefined") {
      this.propmodal.imgBase64 = [];
    }
    if (base64) {
      var idCtrl = "#" + cId;
      var index = this.propmodal.imgBase64.findIndex(p => p.id === idCtrl);
      if (index === -1) {
        this.propmodal.imgBase64.push({ id: idCtrl, src: base64 });
      } else {
        this.propmodal.imgBase64[index].src = base64;
      }
      if (w) {
        if (h == null)
          h = w;
      } else {
        w = '50%';
        h = '50%';
      }
      this.image = { witdh: w, height: h };
    }
  }

  //-----------------------------------------------------
  
  setMapWeb(latitud, longitud) {
    this.map.idmap = 'map_canvas';
    this.map.latitud = latitud;
    this.map.longitud = longitud;
  }

  //-----------------------------------------------------
  
  createMap() {
    var map = L.map(this.map.idmap).setView([this.map.latitud, this.map.longitud], 15);
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
      maxZoom: 18
    }).addTo(map);
    L.control.scale().addTo(map);
    L.marker([this.map.latitud, this.map.longitud], { draggable: true }).addTo(map);
  }

  //-----------------------------------------------------
  
  clearUpload() {
    if (this.props.idupload) {
      var elem = document.getElementById(this.props.idupload);
      if (elem) {
        elem.value = "";
      }
      this.validFile = false;
    }
  }

  //-----------------------------------------------------

  uploadFile(event) {
    this.getfileBase64("#" + this.props.idupload);
  }

  //-----------------------------------------------------
  
  getUploadFile() {
    return this.uploadFile;
  }

  //-----------------------------------------------------
  
  setUploadFile(cfiletype) {
    this.uploadFile = cfiletype;
  }

  //-----------------------------------------------------
  
  async getfileBase64(cId) {
    var file = document.querySelector(cId).files[0];
    var namefile = " ";
    this.uploadFile = file;
    if (this.typeimage(file.type)) {
      this.uploadBase64 = await toBase64(file);
      this.validFile = true;
      this.uploadType = file.type;
      namefile = file.name;
    } else {
      this.validFile = false;
      this.uploadType = null;
      namefile = "error de tipo de archivo ";
      MsgNotify("Tipo de Archivo debe ser BMP/JPG/JPEG/PNG", "error");
    }
    var elem = document.getElementsByClassName('custom-file-label')[0];
    elem.innerText = namefile;
  }

  //-----------------------------------------------------
  
  typeimage(type) {
    if (type == "image/bmp" || type == "image/png" || type == "image/jpg" ||
      type == "image/jpeg" || type == "application/pdf") {
      return true;
    }
    return false;
  }

  //-----------------------------------------------------
  
  getUploadBase64() {
    return this.onlyBase64(this.uploadBase64);
  }

  //-----------------------------------------------------
  
  onlyBase64(str) {
    var newstr = str.replace('data:application/pdf;base64,', '');
    newstr = newstr.replace('data:image/png;base64,', '');
    newstr = newstr.replace('data:image/jpg;base64,', '');
    newstr = newstr.replace('data:image/jpeg;base64,', '');
    newstr = newstr.replace('data:image/bmp;base64,', '');
    newstr = newstr.replace(' ', '+');
    return newstr;
  }

  //-----------------------------------------------------
  
  isUploadBase64() {
    return this.validFile;
  }

  //-----------------------------------------------------
  
  getUploadType() {
    return this.uploadType;
  }

  //-----------------------------------------------------
  
  sendFileServer(cPhp, bCallback, params, uCargo) {
    var cUrl = cPhp + '?' + Math.random();
    try {
      $.ajax({
        url: cUrl,
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: params,
        type: 'post',
        beforeSend: function () {
          loaddingOn();
        },
        complete: function () {
          loaddingOff();
        }
      })
        .done(function (data, textStatus, jqXHR) {
          var respond = JSON.parse(data);
          loaddingOff();
          if (typeof bCallback == 'function') {
            var fnparams = [respond, uCargo, textStatus, jqXHR];
            bCallback.apply(null, fnparams);
          }
        })
        .fail(function (jqXhr, textStatus, errorThrown) {
          //	Si ha saltado un error pero hay un status 200 mostraremos el mensaje 
          if (jqXhr.status == 200) {
            Msg_AjaxError(cPhp, jqXhr, textStatus);
          } else {
            TWeb_Error_Dispatcher(jqXhr, cPhp);
          }
        });
    } catch (ex) {
      MsgError(ex, 'Error Ajax');
    }
  }

  //-----------------------------------------------------
  
  Self() {
    return null;
  }

  //-----------------------------------------------------

  version() {
    return $.fn.tooltip.Constructor.VERSION;
  }  
    
}

//-----------------------------------------------------

const toBase64 = file => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => resolve(reader.result);
  reader.onerror = error => reject(error);
});

//------------------- FINAL -----------------------------