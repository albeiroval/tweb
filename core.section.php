<?php
/*
  CLASS : Clases para usar con core.dashboard.php 
  AUTOR : Albeiro Valencia
  FECHA : 07/11/2021
*/

class TSection extends TPanel {
  
  private $colorBG = '#d5f3f3';

  public function __construct( $cId = 'section', $nTop = 0, $nLeft = 0, $nWidth = 0, $nHeight = 0, $cColor = null ) {

    $cColor = TDefault( $cColor, $this->colorBG );

    parent::__construct( null, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor );

		$this->cClass			= 'tweb-section';	
		$this->cControl 	= 'tsection';		

  }

  public function colorSection( $cColor ) {
    $this->cColor = $cColor;
  }

}

//-----------------------------------------

class TFooter extends TPanel {

  private $colorBG      = '#d5f3f3';
  private $aCtrlFooter  = [];

  public function __construct( $cColor = null ) {

    $cColor = TDefault( $cColor, $this->colorBG );

    parent::__construct( null, 'footer', 0, 0, 0, 0, $cColor );

		$this->cClass			= 'tweb-footer';	
		$this->cControl 	= 'tfooter';		

  }

  public function colorFooter( $cColor ) {
    $this->cColor = $cColor;
  }

  public function addText( $cText, $cClass ) {
    $this->aCtrlFooter[] = new TextFooter( $cText, $cClass );
  }

  public function addIcon( $cIcon, $cAction, $cClass, $cColor, $colorBG ) {
    $this->aCtrlFooter[] = new IconFooter( $cIcon, $cAction, $cClass, $cColor, $colorBG );
  }

  public function Activate() {

    $cHtml  = '<div id="footer" name="footer" class="tweb-footer" data-control="tfooter" '; 
    $cHtml .= '   style="position: absolute;';
    $cHtml .= '   margin:0px;'; 
    $cHtml .= '   top:95%;';
    $cHtml .= '   left:0px;';  
    $cHtml .= '   width:100%;';  
    $cHtml .= '   height:70px;'; 
    $cHtml .= '   background-color:'.$this->colorBG.';'; 
    $cHtml .= '   overflow: hidden;">';
    
    $count = count($this->aCtrlFooter);
    for ($i=0; $i < $count; $i++) { 
      $item = $this->aCtrlFooter[$i];
      $cHtml .= $item->Activate();
    }

    $cHtml .= '</div>';

    echo $cHtml;

  }  
    

}

//-----------------------------------------

class TextFooter {

  private $cText  = "";
  private $cClass = "";

  public function __construct( $cText, $cClass ) {
    $this->cText  = TDefault($cText, "");
    $this->cClass = TDefault($cClass, "footer-text");
  }

  public function Activate() {

    $cHtml  = '<div class="' . $this->cClass . '" style="float: left;margin : 5px">' . PHP_EOL;
    $cHtml .= '  <p>' . $this->cText . '</p>' . PHP_EOL;
    $cHtml .= '</div>' . PHP_EOL;

    return $cHtml;

  }

}

//-----------------------------------------

class IconFooter {

  public $cIcon     = 'fa fa-home';
  public $cAction   = null;

  private $cClass   = 'tfooter-icon';
  private $colorBG  = '#d5f3f3';
  private $colorTXT = 'black';
  private $nWidth   = 35;
  private $nHeight  = 35;                                 

  public function __construct( $cIcon, $cAction, $cClass, $colorBG, $colorTXT ) {

    $cIcon    = TDefault( $cIcon, $this->cIcon );                            
    $cAction  = TDefault( $cAction, $this->cAction );                            
    $cClass   = TDefault( $cClass, $this->cClass );
    $colorBG  = TDefault( $colorBG, $this->colorBG );
    $colorTXT = TDefault( $colorTXT, $this->colorTXT );
    
    $this->cIcon     = $cIcon;
    $this->cAction   = $cAction;
    $this->colorBG   = $colorBG;
    $this->colorTXT  = $colorTXT;
    $this->cClass		 = $cClass;	
		
  }

  public function Activate() {

    $cHtml = '';

    $cHtml .= '<div style="float : left;';
    $cHtml .= ' margin: 3px 5px 5px 5px;';
    $cHtml .= ' width : ' . $this->nWidth .';';
    $cHtml .= ' height : ' . $this->nHeight .';';
    $cHtml .= ' color : ' . $this->colorTXT .';';
    $cHtml .= ' text-align : center;';
    $cHtml .= ' background-color : ' . $this->colorBG . ';';
    $cHtml .= ' cursor: pointer;';
    $cHtml .= ' border-radius: 50px;';
    $cHtml .= ' border: 1px solid #d0d0cd;';
    $cHtml .= ' -webkit-box-shadow: 0px 10px 13px -7px #000000, 5px 5px 15px 15px rgb(0 0 0 / 0%);"';
    $cHtml .= ' onclick="' . $this->cAction .'">';
    $cHtml .= '<i class="' . $this->cIcon .'" style="position: relative;top: calc(50% - 10px);"></i>';
    $cHtml .= '</div>';

    return $cHtml;

  }  

}

//-----------------------------------------

class TIcon extends TControl {

  public $cIcon    = 'fa fa-home';
  public $cAction  = '';
  
  private $colorBG  = '#d5f3f3';
  private $colorTXT = 'black';

  public function __construct( $oWnd = null, $cIcon = null, $cAction = null,
                               $nTop = 0, $nLeft = 0, 
                               $cClass = 'tfooter-icon', $colorBG = null, $colorTXT = null ) {

    $nWidth   = 35;
    $nHeight  = 35;                                 
    $cIcon    = TDefault( $cIcon, $this->cIcon );                            
    $cAction  = TDefault( $cAction, $this->cAction );                            
    $cClass   = TDefault( $cClass, $this->cClass );
    $colorBG  = TDefault( $colorBG, $this->colorBG );
    $colorTXT = TDefault( $colorTXT, $this->colorTXT );
    
    parent::__construct( $oWnd, 'footer', $nTop, $nLeft, $nWidth, $nHeight, null );

    $this->cIcon     = $cIcon;
    $this->cAction   = $cAction;
    $this->colorBG   = $colorBG;
    $this->colorTXT  = $colorTXT;
    $this->nWidth    = $nWidth; 
    $this->nHeight   = $nHeight; 
    $this->cClass		 = $cClass;	
		$this->cControl  = 'ticon';		
    
  }

  public function Activate() {

    $cHtml = '';

    $cHtml .= '<div style="position : absolute;';
    $cHtml .= ' top : ' . $this->nTop . ';';
    $cHtml .= ' left : ' . $this->nLeft .';';
    $cHtml .= ' width : ' . $this->nWidth .';';
    $cHtml .= ' height : ' . $this->nHeight .';';
    $cHtml .= ' color : ' . $this->colorTXT .';';
    $cHtml .= ' text-align : center;';
    $cHtml .= ' background-color : ' . $this->colorBG . ';';
    $cHtml .= ' cursor: pointer;';
    $cHtml .= ' border-radius: 50px;';
    $cHtml .= ' border: 1px solid #d0d0cd;';
    $cHtml .= ' -webkit-box-shadow: 0px 10px 13px -7px #000000, 5px 5px 15px 15px rgb(0 0 0 / 0%);"';
    $cHtml .= ' onclick="' . $this->cAction .'">';
    $cHtml .= '<i class="' . $this->cIcon .'" style="position: relative;top: calc(50% - 10px);"></i>';
    $cHtml .= '</div>';

    echo $cHtml;

  }  

}

?>

<style> 

  .tweb-section {
    border-radius : 5px;
  }

  @media (max-width : 640px) {

    .tweb_window {
      overflow: scroll !important;
    }

    .tweb-footer {
      top : 93% !important;
    }
    
  }

</style>
