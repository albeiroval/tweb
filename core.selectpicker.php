<?php
/*
*  CLASS TSelectPicker
*  Author : Albeiro Valencia
*  Fecha  : 30/08/2021
*  Derechos Reservador(C)(R)
*/

/*---------------------------------------------------------------------------------------------*/

class TSelectPicker extends TControl {

  public $cId	      = '';
  public $aData     = array();
  public $nTop      = 0;
  public $nLeft     = 0;
  public $nWidth    = null;
  public $cHeader   = null;
  public $nItems    = null;
  public $lSearch   = true;
  public $fontSize  = 14;    

  private $bChanged  = '';
  private $bKeyEnter = '';
  
	//----------------------------------------
	
  public function __construct( $oWnd, $cId = '', $nTop = 0, $nLeft = 0, $nWidth = null, $aData = null ) {

    $cId      = TDefault( $cId,     'selectpicker' );
    $nWidth   = TDefault( $nWidth,  null );
    $aData    = TDefault( $aData,   array() );  
    
    parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, null );  

    $this->cId      = $cId;
    $this->cControl = 'tselect';
    $this->nTop     = $nTop;
    $this->nLeft    = $nLeft;                     
    $this->aData    = $aData;           

    // Esto para que se vea los select sin limite de altura en windows
    $oWnd->cOverflow = 'visible';

  }

  //----------------------------------------

  //	uDummy es un 2 variable para ser compatible con el SetData() de TControl 
  //  que tiene 2 parametros...
  public function SetData( $aData = array(), $uDummy = null  ) {
    $this->aData = $aData;
  }
  
  //----------------------------------------

  public function onChanged( $cChanged = '' ) {
    if ( !empty($cChanged) ) {
      $this->bChanged = $cChanged;
    }    
  }

  public function SetKeyEnter( $cAction = '') {
		$this->bKeyEnter = $cAction;
  }	
  
  //----------------------------------------

  private function StyleContainer() {

    /*
    Con position: absolute, los margins top y left no funcionan y tienes que usar top, left.
    Con position: relative justo al revés.
    */

    $cStyle   = 'style="position: absolute;';
    $cStyle  .= 'top: ' . $this->nTop . ';';
    $cStyle  .= 'left: ' . $this->nLeft . ';';
    $cStyle  .= 'width: ' . $this->nWidth . ';';
    $cStyle  .= '"';

    return $cStyle;
  }

  //----------------------------------------

  private function AddOptions() {

    $cHtml = "";
    $nLen  = 0;

    if ( $this->aData == null ) {
      return $cHtml;
    } else if ( is_array( $this->aData ) ) {
      $nLen  = count($this->aData);
    }

    $lMulti = is_array( $this->aData[0] );
    
    for ( $i = 0; $i < $nLen; $i++) {
      $cHtml .= '<option';
      if ( $lMulti ) {
        $cHtml .= ' data-tokens="' . $this->aData[$i][1] . '"'; 
        $cHtml .= ' value="' . $this->aData[$i][0] . '"'; 
        $cHtml .= '>' . $this->aData[$i][1] ;
      } else {
        $cHtml .= ' data-tokens="' . $this->aData[$i] . '"'; 
        $cHtml .= ' value="' . $i . '"'; 
        $cHtml .= '>' . $this->aData[$i] ;
      }
      $cHtml .= '</option>';
    }

    return $cHtml;
  
  }

  //----------------------------------------

  private function EventKeyEnter() {

    /*
    $('input#search').keypress(function(e) {
      if (e.keyCode == 13) {
        $('#btn-search').click();
      }
    */  

    $cJS = '';

    if ( !empty($this->bKeyEnter) ) {
      $cJS  = PHP_EOL;  
      $cJS .= '$( "#' . $this->cId . '" ).on("keyup", function(e) { ' . PHP_EOL;
      $cJS .= '   console.log("keypress ENTER");' . PHP_EOL;  
      $cJS .= '   if (e.keyCode == 13) { ' . PHP_EOL;
      $cJS .= '         ' . $this->bKeyEnter . ';' . PHP_EOL;  
      $cJS .= '   }' . PHP_EOL;
      $cJS .= '});' . PHP_EOL;
      $cJS .= PHP_EOL;
      
    } 
    
    return $cJS;

	}

  //----------------------------------------

	public function Activate() {

    // HTML -------------------------------------
    $cHtml  = '<div class="select-control" style="overflow: hidden;">';
    $cHtml .= '<div ' . $this->StyleContainer() . '">';

    $cHtml .= '<select class="form-control selectpicker" id="' . $this->cId . '"'; 
    
    if ( gettype ($this->lSearch) === 'boolean') {
      if ( $this->lSearch ) {
        $cHtml .= ' data-live-search="true" '; 
      }  
    }

    if ($this->cHeader) {
      $cHtml .= ' data-header="' . $this->cHeader . '" ';
    }

    if ($this->nItems) {
      $cHtml .= ' data-size="' . $this->nItems . '"';
    }

    $cHtml .= ' ' . $this->Datas();		

    $cHtml .= '>';

    $cHtml .= $this->AddOptions();
    $cHtml .= '</select>';

    $cHtml .= '</div>';
    $cHtml .= '</div>';

    echo $cHtml;

    // CSS ------------------------
    $cCSS  = '<style>';
    $cCSS .= ' .filter-option {' . PHP_EOL;
    $cCSS .= '   font-size :' . $this->fontSize . ';' . PHP_EOL;
    $cCSS .= ' }' . PHP_EOL;
    $cCSS .= '</style>';

    echo $cCSS;

    // JAVASCRIPT ----------------------------
    $cJS  = '<script>' . PHP_EOL;
    $cJS .= PHP_EOL;
    $cJS .= '$(function() {' . PHP_EOL;
    $cJS .= "  var oSel = new TSelect('".$this->cId."');" . PHP_EOL;
    $cJS .= "  var o = new TControl();" . PHP_EOL;
    $cJS .= "  o.ControlInit('".$this->cId."', oSel, 'tselect');" . PHP_EOL;
    $cJS .= "});" . PHP_EOL;
    $cJS .= PHP_EOL;

    if ( !empty($this->bChanged) ) {
      $cJS .= "$('#" . $this->cId . "').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) { " . PHP_EOL;
      $cJS .= $this->bChanged . ';' . PHP_EOL;
      $cJS .= ' });' . PHP_EOL; 
    }
    $cJS .= PHP_EOL;
    $cJS .= '</script>' . PHP_EOL;

    // $cJS .= $this->EventKeyEnter();
    
    echo $cJS;
    
  }
 
}  

?>

