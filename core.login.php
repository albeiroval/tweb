<?php
/*
*  CLASS TLogin
*  SOLO PRA USO DE LA LIBRERIA TVALWEB
*	 AUTOR : ALBEIRO VALENCIA
*  FECHA : 06/10/2021
*/

class TLogin {

	public 	$cId	    				= '';
	public	$cTitle  					= '';
	public 	$cImage        		= './images/avatar.png';
	public 	$cImageCss				= null;
	public 	$cTextUser 				= 'Username';
	public 	$cTextPassword 		= 'Password';
	public  $lShowPassword    = false;
	public 	$cTextLogin  			= 'Login'; 
	public 	$cTextRegister 		= 'Registro';
	public 	$cTextForgot   		= 'Olvido Password ?';
	public 	$bActionLogin  		= '';
	public 	$bActionRegister 	= '';
	public 	$bActionForgot 		= '';
	public  $clrTapiz  			  = 'aliceblue';

	private  $recaptcha  	= [ "active" 	=> false, "key" 		=> "" ];
	private  $aCompany   	= [ "label" => " ", "name" 	=> " ", "link" 	=> " " ];
	private  $lLinkAutor	= false;

  //----------------------------------
	
	public function __construct( $cId = null, $cTitle = null ) {
		$this->cId 			= TDefault( $cId, 'tweb_login' );
		$this->cTitle  	= TDefault( $cTitle, 'Usuario Login');
	}

  //----------------------------------

	public function Developer( $cLabel = 'Developed By', $cAutor = 'tvalweb', $cAction = '' ) {
    $this->aCompany["label"] 		= $cLabel;
    $this->aCompany["name"]  		= $cAutor;
		$this->aCompany["action"]  	= $cAction;
		$this->lLinkAutor = true;
	}

  //----------------------------------

  public function Recaptcha( $cKey = "" ) {
    $this->recaptcha["active"] = true;
  	$this->recaptcha["key"]    = $cKey;
  }
   
  //----------------------------------

  public function Activate() {

		$this->bActionRegister 	= str_replace( '"', "'" , $this->bActionRegister );
		$this->bActionForgot 		= str_replace( '"', "'" , $this->bActionForgot );

    // Carga librerias  JS - CSS
    $cHtml  = '<link href="'.TWEB_PATH.'core.login.css" rel="stylesheet">';
		$cHtml .= '<script src="'.TWEB_PATH.'/core.login.js"></script>' ;
		$cHtml .= '<script type="text/javascript" src="'.TWEB_PATH.'/MaskedPassword.js"></script>' ;
      
		// Codigo HTML
    $cHtml .= $this->createHTML();

    echo $cHtml;

	}  

   //----------------------------------

   private function createHTML() {
      
  	// Agrega el script del Recaptcha
    $cHtml = "";

    if ( $this->recaptcha["active"] ) {
    	// https://www.google.com/recaptcha/admin/create
      // https://developers.google.com/recaptcha/docs/v3
      // https://www.wdb24.com/integrate-google-recaptcha-v3-example-in-php-using-ajax/
      $cHtml  = '<script src="https://www.google.com/recaptcha/api.js"></script>';
    }   
      		
    // Bloque HTML
		$cHtml .= '<div id="'.$this->cId.'" class="modal fade" style="background-color:'.$this->clrTapiz.';padding: 8.5px;">';
		$cHtml .= '<div class="modal-dialog modal-login" role="document">';
		$cHtml .= '<div class="modal-content">';

		// Modal Header
		$cHtml .= '<div class="modal-header">';
		$cHtml .= '	<div class="avatar">';

		if ( $this->cImageCss ) {
			$cHtml .= '<img src="'.$this->cImage.'" class="'.$this->cImageCss.'" alt="Avatar">';	
		} else {
			$cHtml .= '<img src="'.$this->cImage.'" alt="Avatar">';	
		}

		$cHtml .= '	</div>				';
		$cHtml .= '	<h4 class="modal-title">'.$this->cTitle.'</h4>	';
    $cHtml .= '	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
		$cHtml .= '</div>';

		// Modal Body
		// https://codepen.io/Sohail05/pen/yOpeBm
		$cHtml .= '<div class="modal-body">';
		$cHtml .= '	<form id="form-login" action="javascript:'.$this->bActionLogin.';" method="post">';
		$cHtml .= '		<div class="form-group">';
		$cHtml .= '			<input type="text" class="form-control" id="username" name="username" placeholder="'.$this->cTextUser.'"/>';
		$cHtml .= '		</div>';
		$cHtml .= '		<div class="form-group">';
		// $cHtml .= '			<input type="password" class="form-control" id="password" name="password" placeholder="'.$this->cTextPassword. '" required autocomplete="off"/>';
		$cHtml .= '			<input type="password" class="form-control password" id="password" name="password" placeholder="'.$this->cTextPassword.'"/>';

		if ( $this->lShowPassword ) {
			$cHtml .= '<span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>';
		}

		$cHtml .= '		</div>     ';   
		$cHtml .= '		<div class="form-group">';
		$cHtml .= '       <button class="g-recaptcha btn btn-primary btn-lg btn-block login-btn" ';
    $cHtml .= '               data-sitekey="'. $this->recaptcha["key"].'"'; 
    $cHtml .= '               data-callback="onSubmit" data-action="submit">';
    $cHtml .=             $this->cTextLogin;
    $cHtml .= '       </button>';
		$cHtml .= '		</div>';
		$cHtml .= '	</form>';
		$cHtml .= '</div>';

		// Modal Footer
		$cHtml .= '<div class="modal-footer">';
		$cHtml .= '	<div class="d-inline-block" style="margin : auto">';    
		$cHtml .= '		<a href="#" class="modal-register" onclick="'.$this->bActionRegister.';">'.$this->cTextRegister.'</a>';
		$cHtml .= '	</div>';    
		$cHtml .= '	<div class="d-inline-block" style="margin : auto">';    
		$cHtml .= '		<a href="#" class="modal-forgot" onclick="'.$this->bActionForgot.';">'.$this->cTextForgot.'</a>';
    $cHtml .= '	</div>';    
		$cHtml .= '</div>';
		
		// Creditos
		if ( $this->lLinkAutor ) {
			$cHtml .= '<div class="company" style="padding:6px;">';
			$cHtml .= ' <div class="d-inline-block" style="margin : auto">';
			$cHtml .= '  <p class="company-center">'.$this->aCompany["label"].'</p>';
			$cHtml .= ' </div>'; 
			$cHtml .= ' <div class="d-inline-block" style="margin : auto">';
			$cHtml .= '	 <a class="company-link company-center" href="#" onclick="return '.$this->aCompany["action"].';">';
			$cHtml .= '  	<span>'.$this->aCompany["name"].'</span>';
			$cHtml .= '	 </a>';
			$cHtml .= ' </div>'; 
			$cHtml .= ' <div class="d-inline-block" style="margin : auto">';
			$cHtml .= '    <i class="fa fa-copyright company-center"></i>';
			$cHtml .= ' </div>'; 
			$cHtml .= '</div>';
		}	

    $cHtml .= '</div>';  
    $cHtml .= '</div>';
		$cHtml .= '</div>';

		return $cHtml;

   }
  
} 

?>