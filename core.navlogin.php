<?php

class TNavLogin {

  public $lShow = true;
  
  protected $cId       = '';
  protected $cTitle    = '';
  protected $nTop      = '';

  protected $btnLogin  = [ "text"        => 'Ingresar', 
                           "action"      => '', 
                           "color"       => '', 
                           "background"  => '' ];

  protected $btnCancel = [ "text"        => 'Cancelar', 
                           "action"      => 'oNavlogin.close()', 
                           "color"       => 'white', 
                           "background"  => 'red' ];

  //------------------------------------------------

  public function __construct( $cId = 'navlogin', $nTop = '',  $cTitle = 'INGRESO AL SISTEMA' ) {
		$this->cId		  = $cId;
    $this->cTitle   = $cTitle;
    $this->nTop     = $nTop;
  }

  //------------------------------------------------

  public function ButtonLogin( $cText = '', $cAction = '', $cClrText = '', $cClrBg = '' ) {
    $this->btnLogin = [ "text"        => $cText, 
                        "action"      => $cAction, 
                        "color"       => $cClrText, 
                        "background"  => $cClrBg ];
  }

  //------------------------------------------------

  public function ButtonCancel( $cText = '', $cAction = '', $cClrText = '', $cClrBg = '' ) {
    
    if ( !empty($cText) ) { $this->btnCancel["text"] = $cText;  }
    if ( !empty($cAction) ) { $this->btnCancel["action"] = $cAction;  }
    if ( !empty($cClrText) ) { $this->btnCancel["color"] = $cClrText;  }
    if ( !empty($cClrBg) ) { $this->btnCancel["background"] = $cClrBg;  }

  }

  //------------------------------------------------

  private function CreateButtons() {

    $cHtml = '';
    
    // Button Login
    $cHtml .= '<div class="divbutton">' . PHP_EOL;
    $cHtml .= '   <a href="#" onclick="' . $this->btnLogin["action"] . '"'. PHP_EOL;
    $cHtml .= '   class="btn btn-success" style="width: 100%;' ;
    if ( !empty($this->btnLogin["color"] ) ) {
       $cHtml .= 'color: ' . $this->btnLogin["color"] . ';';
    } 
    if ( !empty($this->btnLogin["background"]) ) {
       $cHtml .= 'background: ' . $this->btnLogin["background"] . ';';
    }
    $cHtml .= '">'. $this->btnLogin["text"] .' </a>' . PHP_EOL;
    $cHtml .= '</div>' . PHP_EOL;  

    // Button Cancel
    $cHtml .= '<div class="divbutton">' . PHP_EOL;
    $cHtml .= '   <a href="#" onclick="' . $this->btnCancel["action"] . '"'. PHP_EOL;
    $cHtml .= '   class="btn btn-success" style="width: 100%;' ;
    if ( !empty($this->btnCancel["color"] ) ) {
       $cHtml .= 'color: ' . $this->btnCancel["color"] . ';';
    } 
    if ( !empty($this->btnCancel["background"]) ) {
       $cHtml .= 'background: ' . $this->btnCancel["background"] . ';';
    }
    $cHtml .= '">'. $this->btnCancel["text"] .' </a>' . PHP_EOL;
    $cHtml .= '</div>' . PHP_EOL;  

    return $cHtml;
  }


  //------------------------------------------------

  public function Activate() {
    
    // HTML ------------------
    $cHtml  = '<div class="container nav-login" id="' . $this->cId . '" style="display: block;';
    if ( $this->nTop ) {
       $cHtml .= 'margin-top : ' . $this->nTop;
    }
    $cHtml .= '">' . PHP_EOL;
    $cHtml .= ' <div class="card nav-card">' . PHP_EOL;
    $cHtml .= '   <div class="card-header" style="text-align: center;">' . $this->cTitle . PHP_EOL; 
    $cHtml .= '   </div>' . PHP_EOL;
    $cHtml .= '   <div class="card-body">' . PHP_EOL;
    $cHtml .= '     <div class="card-title" style="text-align: center;">' . PHP_EOL;
    $cHtml .= '       <i class="fa fa-user fa-3x"></i>' . PHP_EOL;
    $cHtml .= '     </div>' . PHP_EOL;
    $cHtml .= '    <div class="divcard">' . PHP_EOL;
    $cHtml .= '       <p class="label">Usuario :</p>' . PHP_EOL;
    $cHtml .= '       <input type="text" name="user" class="input" id="user">' . PHP_EOL;
    $cHtml .= '     </div> ' . PHP_EOL;
    $cHtml .= '    <div class="divcard">' . PHP_EOL;
    $cHtml .= '       <p class="label">Password :</p>' . PHP_EOL;
    $cHtml .= '       <input type="password" name="password" class="input" id="password">' . PHP_EOL;
    $cHtml .= '     </div> ' . PHP_EOL;

    $cHtml .= $this->CreateButtons();

    $cHtml .= '   </div>' . PHP_EOL;
    $cHtml .= ' </div>' . PHP_EOL;
    $cHtml .= '</div>' . PHP_EOL;  

    echo $cHtml;

    // STYLE  ------------------
    $cStyle = '<style>'  . PHP_EOL;

    $cStyle .= PHP_EOL;
    
    $cStyle .= '.nav-login {'              . PHP_EOL;
    $cStyle .= '   width: 100%!important;' . PHP_EOL;
    $cStyle .= '   margin-top: 15%;'       . PHP_EOL;
    $cStyle .= '}'                         . PHP_EOL;

    $cStyle .= PHP_EOL;

    $cStyle .= '.nav-card {'       . PHP_EOL;
    $cStyle .= '   width: 50%;'    . PHP_EOL;
    $cStyle .= '   margin: auto;'  . PHP_EOL;
    $cStyle .= '}'                 . PHP_EOL;

    $cStyle .= PHP_EOL;

    $cStyle .= '@media (max-width : 720px) {' . PHP_EOL;
    $cStyle .= '  .nav-card {'                . PHP_EOL;
    $cStyle .= '     width: 80%;'             . PHP_EOL;
    $cStyle .= '     margin: auto;'           . PHP_EOL;
    $cStyle .= '  }'                          . PHP_EOL;
    $cStyle .= '}'                            . PHP_EOL;

    $cStyle .= PHP_EOL;

    $cStyle .= '.divcard {'             . PHP_EOL;
    $cStyle .= '  display: flex;'       . PHP_EOL;
    $cStyle .= '  margin: auto;'        . PHP_EOL;
    $cStyle .= '  height: 30px;'        . PHP_EOL;
    $cStyle .= '  text-align: center;'  . PHP_EOL;
    $cStyle .= '  margin-bottom: 5px;'  . PHP_EOL;
    $cStyle .= '}'                      . PHP_EOL;

    $cStyle .= PHP_EOL;

    $cStyle .= '.label {'               . PHP_EOL; 
    $cStyle .= '  float: left;'         . PHP_EOL; 
    $cStyle .= '  width: 50%;'          . PHP_EOL;
    $cStyle .= '  text-align: right;'   . PHP_EOL;
    $cStyle .= '  padding-right: 10px;' . PHP_EOL;
    $cStyle .= '}'                      . PHP_EOL;

    $cStyle .= PHP_EOL;

    $cStyle .= '.input {'             . PHP_EOL; 
    $cStyle .= '  float: right;'      . PHP_EOL; 
    $cStyle .= '  width: 50%;'        . PHP_EOL;
    $cStyle .= '  margin-left: auto;' . PHP_EOL;
    $cStyle .= '}'                    . PHP_EOL;

    $cStyle .= PHP_EOL;

    $cStyle .= '.divbutton {'           . PHP_EOL;
    $cStyle .= '  margin: auto;'        . PHP_EOL;
    $cStyle .= '  height: auto;'        . PHP_EOL;
    $cStyle .= '  text-align: center;'  . PHP_EOL;
    $cStyle .= '  margin-bottom: 5px;'  . PHP_EOL;
    $cStyle .= '}'                      . PHP_EOL;

    $cStyle .= PHP_EOL;

    $cStyle .= '</style>' . PHP_EOL;

    echo $cStyle;

    // JAVASCRIPT  ------------------
    $cJS  = '<script>'; 
    $cJS .= PHP_EOL;
    $cJS .= 'var oNavlogin;' . PHP_EOL;
    $cJS .= '$(function() {' . PHP_EOL;
    $cJS .= '   oNavlogin = new TNavLogin("'. $this->cId .'");' . PHP_EOL;     
    if ( $this->lShow ) { 
      $cJS .= 'oNavlogin.open();' . PHP_EOL;  
    } else {
      $cJS .= 'oNavlogin.close();' . PHP_EOL;   
    }
    $cJS .= ' });' . PHP_EOL;
    $cJS .= PHP_EOL;
    $cJS .= '</script>';
    
    echo $cJS;
    
  }
  
}  

?>

<script>

var TNavLogin = function( cId ) {

  var idLogin = "#" + cId;

  this.open = function() {
    $(idLogin).show();
  };

  this.close = function() {
    console.log("close", idLogin);
    $(idLogin).hide();
  };

  this.getuser = function() {
    return $("#user").val();
  };

  this.getpassword = function() {
    return $("#password").val();
  };

  this.focususer = function() {
    $("#user").focus();
  };

  this.focuspassword = function() {
    $("#password").focus();
  };

}

</script>