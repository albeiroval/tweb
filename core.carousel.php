<?php

//------------------------------------

class TCarousel extends TControl {
  public $cId         = '';
  public $cIdControl  = '';
  public $aImages     = null; 
  public $nTop        = 0;
  public $nLeft       = 0;
  public $nWidth      = 0;
  public $nHeight     = 0;

  private $disableNextPrev = false;

  //------------------------------
  
  function __construct( $oWnd, $cId = '', $nTop = 0, $nLeft = 0, $nWidth = 100, $nHeight = 100 ) {

    $cId      = TDefault( $cId, 'div_carousel' );
    $nTop     = $nTop;
    $nleft    = $nLeft;
    $nWidth   = $nWidth;
    $nHeight  = $nWidth;

    parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, null );  

    $this->cId        = $cId;
    $this->nTop       = $nTop;
    $this->nLeft      = $nLeft;
    $this->nWidth     = $nWidth;
    $this->nHeight    = $nHeight;
    $this->cIdControl = 'tweb_carousel_' . $this->cId ; 

  }

  //------------------------------

  public function AddImage( $cImage, $cTitle = '', $cDescri = '' ) {
    $oImage = new TCarouselImage( $cImage, $cTitle, $cDescri );
		$this->aImages[] = $oImage;
		return $oImage;
  }

  /**/

  public function NextPrevDisable() {
    $this->disableNextPrev = true;
  }

  /**/

  private function Indicators() {

    $cHtml = '';

    $cHtml .= '<ol class="carousel-indicators">';
    
    $nLen  = count($this->aImages);
    
    for ( $i = 0; $i < $nLen; $i++) {
      $cHtml .= '<li data-target="#' . $this->cIdControl . '" data-slide-to="' . $i . '"';
      $cHtml .= ( $i === 0 ) ? 'class="active">' : '>';
      $cHtml .= '</li>';
    }

    $cHtml .= '</ol>';

    return $cHtml;
  
  }

  private function WrapperSlides() {

    $cHtml = '';

    $cHtml .= '<div class="carousel-inner" role="listbox">';

    $nLen  = count($this->aImages);

    for ( $i = 0; $i < $nLen; $i++) {

      $oImage = $this->aImages[$i];

      $cHtml .= '<div';
      $cHtml .= ($i === 0) ? ' class="carousel-item active">' : ' class="carousel-item">';
      $cHtml .= ' <img src="' . $oImage->cImage . '" alt="' . $oImage->cTitle . '">';
      $cHtml .= ' <div class="carousel-caption d-none d-md-block">';
      $cHtml .= '   <h5>' . $oImage->cTitle .  '</h5>';
      $cHtml .= '   <p>' . $oImage->cDescri . '</p>';
      $cHtml .= ' </div>';
      $cHtml .= '</div>';
    }  

    $cHtml .= '</div>';

    return $cHtml;
  
  }

  //------------------------------

  public function Activate() {

    // div Contenedor
    $cHtml  = '<div id="' . $this->cId  . '" ';
    $cHtml .= ' style="position: absolute; ';
    $cHtml .= ' top: ' . $this->nTop . 'px; left: ' . $this->nLeft . 'px;'; 
    $cHtml .= ' width: ' . $this->nWidth . 'px; height:' . $this->nHeight . 'px;">';

    // div carousel bootstrap
    $cHtml .= '<div id="tweb_carousel_' . $this->cId . '" class="carousel slide" data-ride="carousel">';

    // Indicators
     $cHtml .= $this->Indicators();

    //  Wrapper for slides
    $cHtml .= $this->WrapperSlides();
    

    // Control previus 
    $cHtml .= '<a class="carousel-control-prev" href="#' . $this->cIdControl . '" role="button" data-slide="prev">';
    $cHtml .= '  <span class="carousel-control-prev-icon" aria-hidden="true"></span>';
    $cHtml .= '  <span class="sr-only">Previous</span>';
    $cHtml .= '</a>';

    // Control next 
    $cHtml .= '<a class="carousel-control-next" href="#' . $this->cIdControl . '" role="button" data-slide="next">';
    $cHtml .= '  <span class="carousel-control-next-icon" aria-hidden="true"></span>';
    $cHtml .= '  <span class="sr-only">Next</span>';
    $cHtml .= '</a>';

    // end div carousel
    $cHtml .= '</div>';

    // end div contenedor
    $cHtml .= '</div>';

    echo $cHtml;

    $cStyle = '<style>';

    $cStyle .= ' img {'             . PHP_EOL;  
    $cStyle .= '    display:block;' . PHP_EOL;
    $cStyle .= '    width: 100%;'   . PHP_EOL;  
    $cStyle .= '    height: 100%;'  . PHP_EOL;  
    $cStyle .= '    margin:auto;  ' . PHP_EOL;
    $cStyle .= ' }'                 . PHP_EOL; 

    if ( $this->disableNextPrev ) {
      $cStyle .= ' .carousel-control-next, .carousel-control-prev {' . PHP_EOL;
      $cStyle .= '    display: none;'   . PHP_EOL;
      $cStyle .= '}'                    . PHP_EOL;
    }
    
    $cStyle .= '</style>'; 

    echo $cStyle;

    $cJS  = '<script>';
    $cJS .= '   $(".carousel").carousel({ interval: 2000 });';
    $cJS .= '</script>';

    echo $cJS;		
     
  }

}

//------------------------------------

class TCarouselImage {
  public $cImage  = '';
  public $cTitle  = '';
  public $cDescri = '';

  function __construct( $cImage = '', $cTitle = '', $cDescri = '' )
  {
    $this->cImage   = $cImage;  
    $this->cTitle   = $cTitle;
    $this->cDescri  = $cDescri;
  }

}

//------------------------------------ FINAL


