<?php

//------------------------------------------

class TSidebar {
	public $aOptions 		= null;
	public $nWidth 			= 0;
	public $cTitle 			= '';
	public $ColorTitle 	= 'yellow';
	public $FontTitle   = '16px';
	public $cId 				= '';
	public $cImage 			= null;
	public $ColorBack 	= '';
	public $ColorItem 	= '';
	public $OverColor   = 'cyan';
	public $OverBack    = 'navy';

	private $cImageCss = '';
	private $cIdWnd    = ''; 

	//-----------------------------------
	
	public function __construct( $cId = '', $cTitle = 'MAIN MENU', $nWidth = 200, $cImage = null ) {
		$this->cId 				= $cId;
		$this->nTop 			= 0;
		$this->nLeft 			= 0;
		$this->nWidth 		= $nWidth;
		$this->nHeight 		= '100%';
		$this->cTitle 		= $cTitle;
		$this->aOptions 	= [];
		$this->cImage 		= $cImage;
		$this->cImageCss  = '';
		$this->ColorBack  = '#151719';
		$this->ColorItem  = 'rgba(230,230,230,0.9';
	}

	//-----------------------------------

	public function Title( $cTitle = 'MAIN MENU', $cColor = 'yellow', $FontSize = '16px' ) {
		$this->cTitle 		= $cTitle;
		$this->ColorTitle = $cColor;	
		$this->FontTitle  = $FontSize;
	}

	//-----------------------------------

	public function OverItem( $Color = 'cyan', $Background = 'navy' ) {
		$this->OverColor 	= $Color;
		$this->OverBack 	= $Background;	
	}
	
	//-----------------------------------

	public function AddOption( $cId, $cText, $cFunction, $cIcon = '') {
		$oOption = new TSidebarOption( $cId, $cText, $cFunction, $cIcon );
		$this->aOptions[] = $oOption;
		return $oOption;
	}

	//-----------------------------------

	public function SetImageCss( $cSS = null ) {
		$this->cImageCss 	= $cSS;
	}

	//-----------------------------------

	public function AddWindow( $Wnd = '' ) {
		$this->cIdWnd = $Wnd;
	}

	//-----------------------------------

	private function CreateImage() {

		$cHtml = '';

		if ( !empty( $this->cImage ) ) {
			$cHtml .= ' <div class="sbimage" style="width :200px; height: 100px">' . PHP_EOL;
			$cHtml .= ' <img src="' . $this->cImage . '" alt="sidebar_image" '; 
			if ( !empty($this->cImageCss) ) {
				$cHtml .= 'style="object-fit: cover; ' . $this->cImageCss .'">' . PHP_EOL;	
			} else {
				$cHtml .= 'style="width: 200px; height: 100px; object-fit: cover;">' . PHP_EOL;
			}	
			$cHtml .= ' </div>' . PHP_EOL;
		}

		return $cHtml;

	}

	//-----------------------------------

	private function CreateItems() {

		$cHtml 	= '<ul>' . PHP_EOL;

		$nCount = count( $this->aOptions );
		for ( $i = 0; $i < $nCount; $i++ ) {
			$option = $this->aOptions[ $i ];
			if ( ! $option->dropDown ) {
				$cHtml .= $this->item_li( [	'id'   => $option->cId,
																		'text' => $option->cText,
																		'func' => $option->cFunction,
																		'icon' => $option->cIcon ] );		
			}	else {
				$cHtml .= $this->dropDown( $option );
			}
		}	

		$cHtml .= '</ul>' . PHP_EOL;

		return $cHtml;

	}

	//-----------------------------------

	private function dropDown( $option ) {

		$cHtml  = '<li class="dropdown">';
		$cHtml .= ' <a href="#" data-toggle="dropdown">' ;
		$cHtml .= '		<i class="fa fa-caret-down" style="margin-right:8px;font-size:20px;"></i>';
		$cHtml .= $option->cText;
		$cHtml .= '	</a>';
		$cHtml .= ' <ul class="dropdown-menu" style="padding-inline-start: 10px !important;">';
		$nCount = count( $option->aItems );
		for ( $i = 0; $i < $nCount; $i++ ) {
			$item  = $option->aItems[$i];
			$cIcon = ( $item[2] ) ? $item[2] : "fa-sitemap";
			$cHtml .= $this->item_li( [	'id'   => $option->cId . "-" . $i,
																	'text' => $item[0],
																	'func' => $item[1],
																	'icon' => $cIcon ] );		
		}	
    $cHtml .= ' </ul>';
		$cHtml .= '</li>';
		
		return $cHtml;
	} 

	//-----------------------------------

	private function item_li( $aitem ) {

		if ( !empty( $aitem['icon'] ) ) { 
			$cHtml  = '<li id="' . $aitem['id'] . '"'; 
			$cHtml .= ' onclick="' . $aitem['func'] . '">'; 
			$cHtml .= ' <i class="fa ' . $aitem['icon'] . '" style="margin-right: 10px;"></i>';
			$cHtml .= $aitem['text']; 
			$cHtml .= '</li>' . PHP_EOL;
		} else { 
			$cHtml  = '<li id="' . $aitem['id'] . '"'; 
			$cHtml .= ' onclick="' . $aitem['func'] . '">' . $aitem['text'] ; 
			$cHtml .= '</li>' . PHP_EOL;
		}

		return $cHtml;
	}
	
	//-----------------------------------

	public function Activate() {

		// Cdigo HTML
		$this->CreateHtml();
		
		// Codigo CSS
		$this->CreateCss();
		
		// Codigo JAVASCRIPT 
		$this->CreateJS();
		
	}

	//-----------------------------------

	private function CreateHtml() {

		$cHtml  = '<div id="sidebar" style="height:100%;">' . PHP_EOL;

		$cHtml .= $this->CreateImage();
		
		$cHtml .= '<div class="sbtitle">' . $this->cTitle . '</div>' . PHP_EOL;
		
		$cHtml .= ' <div class="toogle-btn" onclick="oMenuSide.toogle()">' . PHP_EOL;
		$cHtml .= '  <span>&#9776;</span>'                              . PHP_EOL;
		$cHtml .= ' </div>'                                             . PHP_EOL;

		// class="treeview-animated w-20 border mx-4 my-4"
	
		$cHtml .= ' <div id="items-side">' . PHP_EOL;

		$cHtml .= $this->CreateItems();

		$cHtml .= ' </div>' . PHP_EOL;
		
		$cHtml .= '</div>' . PHP_EOL;

		echo $cHtml;

	}

	//-----------------------------------

	private function CreateCss() {

		$cWidth = $this->nWidth . 'px';
		$nPosToggleBtn = ( $this->nWidth + 10 ) . 'px';

		$cCss  = '<style>' . PHP_EOL;

		$cCss .= '#sidebar {'                     								. PHP_EOL;
		$cCss .= ' font-family: sans-serif;'      								. PHP_EOL;
		$cCss .= ' position : fixed;'             								. PHP_EOL;
		$cCss .= ' width : ' . $cWidth . ';'      								. PHP_EOL;
		$cCss .= ' height : 100%;'                								. PHP_EOL;
		$cCss .= ' left : -200px;'                								. PHP_EOL;		
		$cCss .= ' background-color: ' . $this->ColorBack . ';' 	. PHP_EOL;
		$cCss .= ' box-shadow: 5px 0 5px -2px #888;' 							. PHP_EOL;
		$cCss .= ' transition: all 500ms linear;' 								. PHP_EOL;
		$cCss .= '}'                              								. PHP_EOL;

		$cCss .= '#sidebar.active {'  . PHP_EOL;
		$cCss .= ' left: 0px;'        . PHP_EOL;
		$cCss .= '}'                  . PHP_EOL;

		$cCss .= '#sidebar .sbtitle {'  									. PHP_EOL;
		$cCss .= ' color: ' . $this->ColorTitle . ';' 		. PHP_EOL;
		$cCss .= ' font-size: ' . $this->FontTitle . ';' 	. PHP_EOL;
		$cCss .= ' padding: 10px 20px;' 									. PHP_EOL;
		$cCss .= '}'                    									. PHP_EOL;

		$cCss .= '#sidebar .sbimage {'    . PHP_EOL;
		$cCss .= ' display: block;'       . PHP_EOL;
		$cCss .= ' padding-top: 10px;'    . PHP_EOL;
		$cCss .= ' padding-bottom: 10px;' . PHP_EOL;
		$cCss .= ' text-align: center;'   . PHP_EOL;	
		$cCss .= '}'                      . PHP_EOL;
	
		$cCss .= '#sidebar .toogle-btn {'           . PHP_EOL;
		$cCss .= ' position: absolute;'             . PHP_EOL;
		$cCss .= ' left: ' . $nPosToggleBtn . ';'   . PHP_EOL;
		$cCss .= ' top: 10px;'                      . PHP_EOL;
		$cCss .= ' cursor: pointer;'                . PHP_EOL;
		$cCss .= '}'                                . PHP_EOL;

		$cCss .= '#sidebar .toogle-btn span {' . PHP_EOL;
		$cCss .= ' display: block;'            . PHP_EOL;
		$cCss .= ' width: 40px;'               . PHP_EOL;
		$cCss .= ' text-align: center;'        . PHP_EOL;
		$cCss .= ' font-size: 30px;'           . PHP_EOL;
		$cCss .= ' border: 1px solid #000;'    . PHP_EOL;
		$cCss .= '}'                           . PHP_EOL;

		$cCss .= '#sidebar ul {'                                 . PHP_EOL;
		$cCss .= ' background-color: ' . $this->ColorBack . ';'  . PHP_EOL;	
		$cCss .= ' color: white;'                								 . PHP_EOL;
		$cCss .= ' padding-inline-start: 30px;'									 . PHP_EOL;
		$cCss .= '}'                                             . PHP_EOL;

		$cCss .= '#sidebar ul li {'                                 . PHP_EOL;
		$cCss .= ' color: ' . $this->ColorItem . ';'                . PHP_EOL;
		$cCss .= ' list-style: none;'                               . PHP_EOL;
		$cCss .= ' padding: 15px 10px;'                             . PHP_EOL;
		$cCss .= ' border-bottom: 1px solid rgba(100,100,100,0.3);' . PHP_EOL;
		$cCss .= ' cursor: pointer;'                                . PHP_EOL;
		$cCss .= '}'                                                . PHP_EOL;

		$cCss .= '#sidebar a {'                                 . PHP_EOL;
		$cCss .= ' color: ' . $this->ColorItem . ';'            . PHP_EOL;
		$cCss .= ' list-style: none;'                           . PHP_EOL;
		$cCss .= ' padding: 0px 10px 0px 0px;'                  . PHP_EOL;
		$cCss .= ' cursor: pointer;'                            . PHP_EOL;
		$cCss .= '}'                                            . PHP_EOL;

		$cCss .= '#sidebar ul li:hover {'            						. PHP_EOL;
		$cCss .= ' color: ' . $this->OverColor . ';' 						. PHP_EOL;
		$cCss .= ' background-color: ' . $this->OverBack	. ';' . PHP_EOL;
		$cCss .= ' border-left: 4px solid #f1c40f;'  						. PHP_EOL;
		$cCss .= ' padding-left: 6px;'               						. PHP_EOL;
		$cCss .= '}'                                 						. PHP_EOL;

		$cCss .= '#items-side {' 					. PHP_EOL;                        
		$cCss .= '	overflow-y: auto;' 	. PHP_EOL;                        
		$cCss .= '	height: 80%;' 				. PHP_EOL;                        
		$cCss .= '}' 											. PHP_EOL;                        

		if ( !empty($this->cIdWnd) ) {

			$cCss .= '.tweb_window {' 							  		. PHP_EOL;
			$cCss .= '  top: 60px !important;'						. PHP_EOL;
    	$cCss .= '  left: 219px !important;'					. PHP_EOL;
			$cCss .= '	margin-left: -209px !important;'  . PHP_EOL;
			$cCss .= '	width: 98.4% !important;'         . PHP_EOL;
			$cCss .= '	height: 91% !important;'          . PHP_EOL;	
			$cCss .= '}'                  					      . PHP_EOL;	
		
			$cCss .= '#' . $this->cIdWnd . ' {'  				. PHP_EOL;
			$cCss .= '  transition: all 500ms linear; ' . PHP_EOL; 
			$cCss .= '}'                         				. PHP_EOL;

			$cCss .= '#' . $this->cIdWnd . '.open {' 		. PHP_EOL;
			$cCss .= '  top: 60px;'											. PHP_EOL;
    	$cCss .= '  left: 219px;'										. PHP_EOL;
			$cCss .= '  width: 80% !important;'   			. PHP_EOL;
			$cCss .= '  margin-left: -8px !important;'  . PHP_EOL;
			$cCss .= '}'                             		. PHP_EOL;

			$cCss .= '@media (max-width : 720px) {'      		. PHP_EOL;
			$cCss .= '  .tweb_window {' 							   		. PHP_EOL;
			$cCss .= '	   margin-left: -209px !important;' . PHP_EOL;
			$cCss .= '	   width: 97% !important;'			 		. PHP_EOL;
			$cCss .= '  }'                  					   		. PHP_EOL;	
			$cCss .= '}'                  					   	 		. PHP_EOL;	
						
			$cCss .= '@media (max-width : 720px) {'  				. PHP_EOL;
			$cCss .= 		'#' . $this->cIdWnd . '.open {' 		. PHP_EOL;
			// $cCss .= '  		margin-left: -7px !important;' 	. PHP_EOL;
			// $cCss .= '  		width: 69% !important;'					. PHP_EOL;
			$cCss .= '			display: none;'									. PHP_EOL;
			$cCss .= '	}'																	. PHP_EOL;
			$cCss .= '}'                  					   			. PHP_EOL;	

		}	

		$cCss .= '</style>' . PHP_EOL;

		echo $cCss;

	}

	//-----------------------------------

	private function CreateJS() {

		$cJS  = '<script>' 	. PHP_EOL;
		$cJS .= 'var oMenuSide = new TSideMenu("sidebar", "' . $this->cIdWnd . '");' . PHP_EOL;	
		$cJS .= '</script>' . PHP_EOL;
		
		echo $cJS;

	}
	
}

//------------------------------------------

Class TSidebarOption {
	public $dropDown  = false;
	public $cId     	= '';
	public $cText 		= '';
	public $cFunction = '';
	public $cIcon			= '';
	public $aItems    = [];

	//-----------------------------------

	public function __construct( $cId, $cText, $cFunction, $cIcon ) {
		$this->cId  			= $cId;
		$this->cText 			= $cText;
		$this->cFunction 	= $cFunction;
		$this->cIcon 			= $cIcon;
		
		if ( is_array($cFunction) ) {
			$this->dropDown = true;
			$this->aItems   = $cFunction;
		}	
		 else {
      $this->cFunction = str_replace( '"', "'", $cFunction);
    }

	}

} /*End class TSideBar*/

?>

<script>

var TSideMenu = function( cId, cIdWin ) {

	this.cId 		= "#" + cId;
	this.cIdWnd = "#" + cIdWin;
	
	this.show = function() {
	
		$("li a").click(function(e) {
			e.preventDefault();
			$(this).closest('li').find('[class=dropdown-menu]').slideToggle();
		});

	}
	
	this.toogle = function() {
		$(this.cId).toggleClass('active');
		if ( !empty(this.cIdWnd) ) {
			$(this.cIdWnd).toggleClass('open');
		}
	};

}

</script>
