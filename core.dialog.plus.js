/*
* Para loadding gif 
* controla la creacion y eliminacion
* del swal loaddingOn(), loaddingOff()
*/
var oSwal = null;

function loaddingOn() {
	oSwal = Swal.fire({
		imageUrl: TWEB_PATH_IMAGES + "/ajax-loader.gif",
		imageWidth: 98,									
		imageHeight: 98,
		showConfirmButton: false,
		allowOutsideClick: false,
		customClass: 'load-gif'
	});
}	

function loaddingOff() {
	if ( oSwal !== null ) {
		oSwal.close();
	}	
	oSwal = null;
}	

//--------------------------------

function __NewId() {
	__NewId.count = ++__NewId.count || 1 ; // f.count is undefined at first
	
	var cId = 'tweb-' + __NewId.count;
	return cId;
}

//--------------------------------

function Msg( cMsg ) {
	alert( cMsg );
}

//--------------------------------

function MsgYesNo( cMsg, cTitle ) {
	cTitle = ( typeof( cTitle ) === 'string') ? cTitle : _( '_dlg_title_confirm' );
	return confirm( cMsg, cTitle );
}

//--------------------------------

function MsgGet( uValue, cTitle ) {
	cTitle = ( typeof( cTitle ) === 'string') ? cTitle : _( '_dlg_title_get' );

	var uVal = prompt( cTitle, uValue );

	if ( typeof uVal !== 'string' ) {
		uVal = '';
	}	

	return uVal;
}

$.widget( "ui.dialog", $.ui.dialog, {
	options: {
		headerIcon: _( '_msg_image_new' )	
	},
	_setHeaderIcon: function() {
		var title;
		if ( this.options.headerIcon && this.options.headerIcon !== '' ) {
		
			title = this.uiDialogTitlebar
				.find( ".ui-dialog-title" );
			title.find( "span" ).remove();

			var c = '<span><img src="' + this.options.headerIcon + '" class="ui-img" /></span>';

			title.prepend( c );			
		}        
	},
	_create: function() {
		this._super();
		this._setHeaderIcon();
	},
	_setOption: function( key, value ) {
		this._super( key, value );
		if ( key === "headerIcon" ) {
			this._setHeaderIcon();
		}
	}
});	

//--------------------------------

function MsgInfo( cMsg, oOptions ) {

	var oCfg = {};
	
	oCfg.id 			 = '_Wnd_Info';
	oCfg.msg 			 = cMsg;
	oCfg.title 		 = _( '_dlg_title_info' );
	oCfg.icon 		 = _( '_msg_image_info' );	
	oCfg.clr_title = '#25619b';	

	_Msg( oCfg, oOptions );
	
}

//--------------------------------

function MsgError( cMsg, oOptions ) {

	var oCfg = {};
	
	oCfg.id    		 = '_Wnd_Error';
	oCfg.msg   		 = cMsg;
	oCfg.title 		 = _( '_dlg_title_error' );
	oCfg.icon  		 = _( '_msg_image_error' );	
	oCfg.clr_title = '#da0810';	
	
	_Msg( oCfg, oOptions );

}

//--------------------------------

function _Msg( oCfg, oOptions ) {

	var lButtons = false;

	switch ( $.type( oOptions ) ) {

		case 'object':
		
			cTitle 		= ( 'title' in oOptions ) ? oOptions.title : oCfg.title;	
			
			lButtons	= ( 'buttons' in oOptions ) ? true : false;			
			break;
				
		case 'string':
		
			cTitle = oOptions; 
			break;

		default:
		
			oOptions = {};
			cTitle 	= oCfg.title;

	}	
	
	//	Si no hay Button lo ponemos. En movil cuesta pulsar la cruz de cerrar ventana
	
	if ( lButtons == false ) {
	
		var oBtn 	= { text: _( '_dlg_btn_close' ),  click : function() { $(this).dialog("close"); } };

		if ( $.type( oOptions )	!== 'object' )
			oOptions = {};
		
		oOptions.buttons = [ oBtn ];
	}			

	var cHtml 		= '<div class="_msg_error">' + oCfg.msg + '</div>';
	var cIcon 		= oCfg.icon;

	var div			= document.createElement( "div" );		

	div.id 			= oCfg.id;	
	div.title		= cTitle; 
	div.innerHTML	= '';		
	
	document.body.appendChild( div );	

	//	Màxim de la finestra serà el 70% de la ventana
	var nMaxWidth = $(window).width() * 70 / 100;

	var oWnd = $( '#' + oCfg.id ).html( cHtml );
	
	var aProp = {
		closeText: _( '_dlg_btn_close' ),
		autoOpen: false,
		modal: true,
		headerIcon: cIcon,
		maxWidth: nMaxWidth,
		minWidth: 115,
		minHeight: 75,
		width: 'auto', 		//400, 
		height: 'auto', 			
		resizable: true,
		closeOnEscape: true,
		open: function(){
		
		},				
		show : function () {$(this).fadeIn();},		// 'blind', 'scale'	//show : { effect: "scale",  easing: "easeOutBack" },
		//hide : function () {$(this).fadeOut();},	// 'fold', 'scale'					
		close: function() {					
			$(this).dialog().dialog('destroy');																	
			$(this).remove();												
												
			var fn = oOptions.close;													
			
			if (typeof fn === "function") {										
				var fnparams = null;
				fn.apply(null, fnparams );								
			}								
		},
	  create: function( event, ui ) {
			
			//	Los diálogos con jquery crea un id en -> aria-describedby 
			
				var oWnd = $( "[aria-describedby=" + oCfg.id );
				
				//	Ocultar boton close...
					//	oWnd.find( '.ui-dialog-titlebar-close' ).hide();
				
				oWnd.find( '.ui-dialog-titlebar' ).css( 'background', oCfg.clr_title );					
				oWnd.addClass("ui-dialog-shadow");			  
	  
				
			//	Refrescamos variables
				$(this).css("minWidth", 150 );	
				$(this).css("maxWidth", nMaxWidth );
	  }							
	};
	
	//	Carraguem dintre de aProp les posibles claus de oOptions

		for(var key in oOptions) {			
			aProp[ key ] = oOptions[ key ]		;				
		}		
	
	oWnd.dialog( aProp );
	oWnd.dialog( "open" );		
	
	return oWnd;
}

//--------------------------------

/*	MsgConnect() es una funcion de la lib y tiene la función de mostrar un dialogo
	modal en pantalla mientras hay una comunicación con el servidor. Es importante que cuando
	hacemos un MsgConnect( true ) despues hacer un MsgConnect( false ) para
	cerrala. 
	
	Tambien conocer que tiene un efecto pila, es decir, si abrimos 3 veces seguidas
	se habra de cerrar 3 veces para que cierre el dialogo. Esto es por si se
	realizamos varias peticiones asyncronas en el mismo momento. Cuando se cierre
	la última se cerrara el diálogo.
	
	La funcion MsgSignal( lOnOff ) controla todo el proceso automáticamente
*/		
	
var _TWeb_oWnd_Connect = null;

function MsgConnect( lOnOff ) {	

	lOnOff 	= ( typeof( lOnOff ) === 'boolean' ) ? lOnOff : true;
	
	if ( lOnOff ) {
	
		MsgConnect.count = ++MsgConnect.count || 0 ; // f.count is undefined at first
		
		if ( $.type( _TWeb_oWnd_Connect ) == 'null' ) {
		
			MsgConnect.count = 1;
			
			_TWeb_oWnd_Connect = _MConnect()			;
		} 
	
	} else {
	
		if ( $.type( _TWeb_oWnd_Connect ) == 'object' ) {	
		
			if ( MsgConnect.count == 1 ) {
			
				_TWeb_oWnd_Connect.dialog( 'close' );
				_TWeb_oWnd_Connect = null;
			
				MsgConnect.count = 0;
				
			} else {

				MsgConnect.count--;
			}				
		}			
	}
}

//--------------------------------

function _MConnect() {

	var cMsg 		= ( typeof( cMsg ) === 'string') ? cMsg : _( '_dlg_connect' );
	var cHtml 		= '<div class="_msg_loading"><img src="' + _( '_msg_gif_loading' ) + '" /><p>' + cMsg + '</p></div>';

	var div			= document.createElement( "div" );		
	div.id 			= '_Wnd_Connect';	
	div.title		= cMsg; 
	div.innerHTML	= '';		
	
	document.body.appendChild( div );	

	var oWnd = $( '#_Wnd_Connect' ).html( cHtml )			;
	
	var aProp = {
		closeText: '',
		autoOpen: false,
		modal: true,		
		width: 200,
		height: 50,		
		resizable: false,
		closeOnEscape: false,					
		open: function(){ },						
		close: function() {					
			$(this).dialog().dialog('destroy');																	
			$(this).remove();																				
		},
		create: function( event, ui ) {
			
			//	Los diálogos con jquery crea un id en -> aria-describedby 
			
				var oWnd = $( "[aria-describedby=_Wnd_Connect]" );
					
				//	Ocultar barra de titul
					oWnd.find( '.ui-dialog-titlebar' ).hide();
				
				//oWnd.find( '.ui-dialog-titlebar' ).css( 'background', '#da0810');					
				oWnd.addClass("ui-dialog-shadow");			  
		}							
	};
	
	oWnd.dialog( aProp );
	oWnd.dialog( "open" );
	
	return oWnd;
}

//--------------------------------

/*	MsgLoading() es para mostrar mensajes de proceso en una ventana de dialogo modal.
	Devolvera un objeto de la ventana que nos permitirá cerrarla cuando queramos
	haciendo o.dialog('close')
*/
function MsgLoading( cMsg, cTitle, cIcon ) {

	cMsg 	= ( typeof( cMsg ) === 'string') ? cMsg : _( '_dlg_loading' );
	cTitle 	= ( typeof( cTitle ) === 'string') ? cTitle : _( '_dlg_title_loading' );
	cIcon  	= ( typeof( cIcon ) === 'string') ? cIcon : _( '_msg_image_loading' );

	var cHtml 		= '<div class="_msg_loading"><img src="' + _( '_msg_gif_loading' ) + '" /><p>' + cMsg + '</p></div>';

	var div			= document.createElement( "div" );		

	div.id 			= '_Wnd_Loading';	
	div.title		= cTitle; 
	div.innerHTML	= '';		
	
	document.body.appendChild( div );	

	var oWnd = $( '#_Wnd_Loading' ).html( cHtml )			;
	
	var aProp = {
		closeText: '',
		autoOpen: false,
		modal: true,
		headerIcon: cIcon,
		width: 300,
		height: 80,		
		resizable: false,
		closeOnEscape: false,					
		open: function(){ 
		},				
		//show : function () {$(this).fadeIn();},		// 'blind', 'scale'	//show : { effect: "scale",  easing: "easeOutBack" },
		//hide : function () {$(this).fadeOut();},	// 'fold', 'scale'					
		close: function() {					
			$(this).dialog().dialog('destroy');
			$(this).remove();
		},
		create: function( event, ui ) {
			
			//	Los diálogos con jquery crea un id en -> aria-describedby 
			
				var oWnd = $( "[aria-describedby=_Wnd_Loading]" );
				
				//	Ocultar boton close...
					oWnd.find( '.ui-dialog-titlebar-close' ).hide();
				
				//oWnd.find( '.ui-dialog-titlebar' ).css( 'background', '#da0810');					
				oWnd.addClass("ui-dialog-shadow");			  
		}							
	};
	
	oWnd.dialog( aProp );
	oWnd.dialog( "open" );		
	
	return oWnd;

}

//--------------------------------

function MsgCombo( aKey, cFunc, aKeyTxt, cTitle, cDefault, oOptions ) {

//	cTxt = (typeof cTxt == 'string' ) ? cTxt : '';	
	aKeyTxt = (typeof aKeyTxt == 'object' ) ? aKeyTxt : [];	// $.isArray(aKeyTxt)

	if (typeof cFunc === "string") {	
	
		// find object
		var fn = window[cFunc];
							
		// is object a function?
		if (typeof fn !== "function") {	
			alert( 'Funcion Callback no existe: ' + cFunc );
			return null;
		}
	
	} else {
		alert( 'MsgCombo: Indicar funcion Callback' );
		return null;
	}
	
	var nLen 	= aKey.length;
	var nLenTxt = aKeyTxt.length;

	var div = document.createElement( "div" );
	
	var cId 		= __NewId();
	var cId_Memo 	= '__ComboSelect_' + cId;
	var cBtn_Accept = _( '_dlg_btn_accept' );
	var cBtn_Cancel = _( '_dlg_btn_close' );
	
	div.style.cssText = 'padding: 5px; overflow:hidden; ';

	div.id 			= cId;	
	div.title		= (typeof cTitle == 'string' ) ? cTitle : 'Select';		
	div.innerHTML	= '';
	
	document.body.appendChild( div );	


	var html = 	'<select id="' + cId_Memo + '" class="_msg_combo" >' ;

	for ( var i = 0; i < nLen; i++) {	

		html += '<option value="' + aKey[i] + '" ';
		
		if ( typeof cDefault == 'string' && cDefault == aKey[i] ) {
			html += 'selected';
		}
		
		html += '>'		;
		
		if ( nLen == nLenTxt ) {				
			html += aKeyTxt[i] ;
		} else {
			html += aKey[i] ;
		}
		
		html += '</option>'		;
	}
	
	html += '</select>'	;						
				

	var oBtns = new Object();
	
		oBtns[ cBtn_Accept ] = function() {		
					
						var c 		= $('#' + cId_Memo ).val();
						var nIndex 	= $('#' + cId_Memo ).prop('selectedIndex');
						
						$( this ).dialog( "close" ).remove();	
					
						// find object
						var fn = window[cFunc];
											
						// is object a function?
						if (typeof fn === "function") {				
							var fnparams = [c, nIndex];
							fn.apply(null, fnparams );								
						}
					};
					
		oBtns[ cBtn_Cancel ] = function() {
						$( this ).dialog( "close" ).remove();
					}	;

	$("#" + cId )
		.html( html )
		.dialog({
			modal: true,
			width: 400,
			height: "auto",
			resizable: true,
			closeOnEscape: true,
			dialogClass: "dlg_combo",
			buttons: oBtns,		
			open: function(){				
				//$(".dlg_combo .ui-widget-content").css("background-color", "#ddd");																																										
			}, 
			close: function() {	
				$( "#" + cId ).dialog( "close" ).remove();
			}
		
		});	

		$("#" + cId ).dialog( oOptions );
		//$( "#" + cId).multipleSelect()		
}

//	DIALOG	----------------------------------------------------------------------------

function TDialog( cId, html, oMyOptions ) {
	
		//	Propiedades ----------------------------------------------------------------------------	

		if ( $.type( oMyOptions ) !== 'object' ) {
			oMyOptions = {}	;
		}			

		//	Validamos que no exista el id

		var o = $( '#' + cId  );

		if ( o.length > 0 ) {
			MsgError( _( '_error_dlg_exist' ) + cId );
			return null;
		}	

		//	Creamos el div 	

		var div			= document.createElement( "div" );		
		div.id 			= cId;	
		div.title		= ''; 
		div.innerHTML	= '';		
		document.body.appendChild( div );	

		var oDlg = $("#" + cId );
		oDlg.html( html );

		//	Check si el html recibido es de la clase tpanel...

		var cId_Container = '#tdialog_' + cId;

		var oDlg_Container 	=  $( cId_Container );


		//	Ponemos el color del Panel en el diàlogo, para que tenga el mismo al hacer resizes

		cPanel_Background = oDlg_Container.css( 'background' );

		oDlg.css( 'background-color', cPanel_Background );

		//	Recuperamos parámetros del diálogo
		//	Dimensiones	

		var win 	= $(window);  

		var nWidth 	=  oDlg_Container.width(); 
		var nHeight =  oDlg_Container.height();	

		//	Si pasamos como parametro width/height, adaptamos

		if ( $.type( oMyOptions.width ) == 'number'  ) {
				nWidth = oMyOptions.width;
		}		

		if ( $.type( oMyOptions.height ) == 'number'  ) {
				nHeight = oMyOptions.height;
		}				

		//	Otras definiciones...

		var cIcon 	= ( $.type( oMyOptions.icon ) == 'string' ) ? oMyOptions.icon : _( '_msg_image_dlg' );

		//	Centrar aleatoriamente la ventana...
	
		var nTop;
		var nLeft;

		if ( $.type( oMyOptions.centered ) == 'undefined' || oMyOptions.centered == false ) {
	  
			var nSigne 		= Math.random() < 0.5 ? -1 : 1;
			var nTopOffset	= Math.floor(Math.random() * 200 ) - 150;
			var nLeftOffset	= Math.floor(Math.random() * 400 ) - 200;			
			nTop		= (( win.height() / 2 ) - ( nHeight / 2 )) - ( nTopOffset * nSigne );
			nLeft		= (( win.width() / 2 ) - ( nWidth / 2 )) - ( nLeftOffset * nSigne );
		
		} else {
		
			if ( $.type( oMyOptions.top ) == 'number' )
				nTop 	= oMyOptions.top;
			else
				nTop 	= (( win.height() / 2 ) - ( nHeight / 2 ));
				
			if ( $.type( oMyOptions.left ) == 'number' )
				nLeft	= oMyOptions.left;
			else
				nLeft	= (( win.width() / 2 ) - ( nWidth / 2 ));
		}
		
		//	Show
	
		var oShow;
		if ( $.type( oMyOptions.show ) !== 'undefined' )
			oShow = oMyOptions.show;
		else
			oShow = _( '_dlg_show' );
			
		//	Hide
	
	  var oHide;
		if ( $.type( oMyOptions.hide ) !== 'undefined' )
			oHide = oMyOptions.hide;
		else
			oHide = _( '_dlg_hide' );		

		//	Definimos propiedades del diálogo ----------------------------------------------------------

		var aProp = {
			closeText: _( '_dlg_btn_close' ),
			autoOpen: false,
			modal: false,
			headerIcon: cIcon,
			height: nHeight,
			width: nWidth,
			minWidth: 115,
			minHeight: 160,				
			resizable: true,
			resizeStop: function(event, ui) { Check_TMaps( this ); },
			closeOnEscape: true,
			//position: { my: 'center', at: 'center' },             
			position: [ nLeft, nTop ],
			drag: function(){					
				//	Si sistema de menus existeix i esta visible algun submenu, el tanquem al moure dialeg
				if(  $('.iw-contextMenu').is(':visible') ) {
					$('.iw-contextMenu').css('display', 'none');
				}
			},					
			open: function(){ 
			
				Check_TMaps( this ) ;
				
				//	Como realizamos un efecto show, no podemos crear un proceso justo cuando acabe esta
				//	animacion. COn este tip, cuando acaba la animación ejecutaremos...
				
				$(this).parent().promise().done(function () {
						
					if ( 'bInit' in oMyOptions ) {
						
						fn = oMyOptions.bInit;
						/*
						fn = oMyOptions.bInit;
						if (typeof fn === "function") {	
							fnparams = null;
							fn.apply(null, fnparams );
						}
						*/						
						
						var cType = typeof fn;
						
						switch ( cType ) {
						
							case 'function':
							
								fnparams = null;
								fn.apply(null, fnparams );							
								break;
								
							case 'string':
							
								var uFunction = window[ fn ];

								if (typeof uFunction === "function") {	
									fnparams = null;
									uFunction.apply(null, fnparams );								
								}								
								break;					
						}						
					}
				});					
			},				
			show : function () { 

				$(this).fadeIn(); // 'blind', 'scale'	//show : { effect: "scale",  easing: "easeOutBack" }, 
				
			},		
			hide : function () {$(this).fadeOut();},	// 'fold', 'scale'					
			close: function() {	
				
				$(this).dialog().dialog('destroy');																	
				$(this).remove();
				
			},
			create: function( event, ui ) {
				
				//	Reajustamos la altura en el dialogo. El ancho ya se ajusta bien
				
					oDlg.css("height", nHeight );
					
				//	Titulo del diálogo...

					var cTitle = '';
			
					if (  oMyOptions.hasOwnProperty('title') ) {
					
						cTitle = oMyOptions.title;
						
 					} else if ( $.type(oDlg_Container.attr( 'data-title' )) == 'string' ) {
					
						cTitle = oDlg_Container.attr( 'data-title' );
						
					} else {
								
						cTitle = _( '_dlg_title' );
					}
					
						
					oDlg.dialog('option', 'title',  cTitle );									
					
				//	Ajustamos width y height al 100%. Si hacemnos un resize se readaptará
					oDlg_Container.css("height", '100%' );
					oDlg_Container.css("width", '100%' );
					
				//	Panel hide en paneles con muchos controles. Lo mostramos en la apertura
					oDlg_Container.css('display', '');								 

				//	PRENDENT !!! de fer ho a nivell sols d'aquest dialog NO Generic
					$(".ui-dialog").addClass("ui-dialog-shadow"); 
					
					
					$( this ).attr( 'data-control'		, 'tdialog' )	  ;
					$( this ).attr( 'data-id_container'	, cId )	  ;

			}			
		};
		
		//	Definimos propiedades extendidas del diálogo 

		var dialogExtendOptions = {
			"closable" : true,
			"maximizable" : true,
			"minimizable" : true,	
			"collapsable" : true,
			"dblclick" : 'maximize',
			//"animation" : true,
			"titlebar" : false, 				// "none"  "transparent"			
			beforeClose: function() {
			},
			beforeMaximize: function() {
				$(".ui-dialog > [aria-describedby='" + cId + "'] " ).removeClass("ui-dialog-shadow"); //Pendent de resoldre
			},
			restore: function() {
				Check_TMaps( this );				
			},
			maximize: function() {
				Check_TMaps( this )	;															
			}
		};
		
		function Check_TMaps( oThis ) {
		
			//	Cuando resize la ventana se ha de refrescar el contenedor del mapa.
			//	Chequeamos si existe en la ventana un control de tipo TMAPS
			//		Si existe capturamos su ID y recuperamos el objeto TMAPS
			//		El objeto tiene una data MAP. Forzaremos que haga un RESIZE

			var oMaps 	= $(oThis).dialog().find( '[data-control=tmaps]' );

			if ( oMaps.length > 0 ) {
			
				var cId_Maps =  $(oMaps).attr( 'id' );
				
				var o = new TControl();
				var oM = o.GetControl( cId_Maps );		
				
				google.maps.event.trigger( oM.map, 'resize');
			}					
		}

				
		//	Carraguem dintre de aProp les posibles claus de oOptions
		
		var key;

		for( key in oMyOptions) {			
			aProp[ key ] = oMyOptions[ key ];						
		}
		
		//	Carraguem dintre de dialogExtendOptions les posibles claus de oOptions
	
		for( key in oMyOptions) {			
			dialogExtendOptions[ key ] = oMyOptions[ key ];					
		}			
			

		//	Iniciamos diálogo...
	
		oDlg.dialog( aProp );
		
		if ( oMyOptions.dialogextend )
			oDlg.dialogExtend(dialogExtendOptions);	

		oDlg.dialog( "open" );
}

	
//	----------------------------------------------------------------------------

function TLoadPanel ( cId, cUrl, oParam ) {
	TLoad( cId, cUrl, oParam, null, 'panel' );
}

function TLoadDialog( cId, cUrl, oParam, oOptions ) {

	if ( $.type( cId ) == 'null' ) {
		cId = __NewId();
	}	
		
	if( $.type( oOptions ) !== 'object' ) {
		oOptions = {};		
	}	
		
	if ( $.type( oParam ) == 'string' ) {	
		oOptions.title = oParam 	;
	}

	//if ( ! array_key_exists ( 'dialogextend', oOptions ) ){
	if ( $.type( oOptions.dialogextend ) == 'undefined'  ) {
		oOptions.dialogextend = true;
	}

	//	Validamos que no exista el id

	var o = $( '#' + cId  );
		
	if ( o.length > 0 ) {
		//MsgError( _( '_error_dlg_exist' ) + cId )
		console.error( _( '_error_dlg_exist' ), cId );
		o.focus();
		return null;
	}
			
	TLoad( cId, cUrl, oParam, oOptions, 'dialog' );
}

function TLoad( cId, cUrl, oParam, oOptions, cMetode ) {

	/*
	*  oParam.id sera leido desde core.php en la clase TDialog (Linea 1057)
	*  usando isset( $_POST[ 'id' ]
	*/
	if( $.type( oParam ) !== 'object' ) {
		oParam = {};
	}	
	oParam._method = cMetode ;
	oParam._id 		 = cId ;       

	var oControl = new TControl();

	loaddingOn();		

	var oSrv = $.ajax({
		async			 : true,			// Si true farem multitask, sino sequencial...
		type			 : 'post',
		data			 : oParam,
		url				 : cUrl ,									
		dataType	 : 'html',
		beforeSend : function(){	},
		timeout: 10000,
		error: function( XMLHttpRequest, textStatus, errorThrown) {		}
	});
	
	oSrv.error( function(  XMLHttpRequest, textStatus, errorThrown) {
	
		if ( oSwal != null ) loaddingOff();
		
		TWeb_Error_Dispatcher( cUrl, XMLHttpRequest );			
	});
	
	oSrv.done( function( html, x, XMLHttpRequest ){ 

		if ( oSwal != null ) loaddingOff();

		//	Si no llega un code status == 200 dara un error
		
		if ( TWeb_Error_Dispatcher( cUrl, XMLHttpRequest ) ) {
			return null;
		}
	
		//	Parece ser, q puede haber un error de php, pero llega aqui con un
		//	html normal con la descripcion pero sin marcar un status de error.
		//	Intentaremos chequear los principales.

			if ( html.indexOf( '<b>Parse error</b>:') >= 0 || 
				 html.indexOf( '<b>Fatal error</b>:') >= 0 ) {

				MsgError( html );			
				
			} else { 

				switch ( cMetode ) {
					
					case 'panel':			
					  $( '#' + cId ).css( 'display', '' );						
						$( '#' + cId ).html( html );
						break;
						
					case 'dialog':
						TDialog( cId, html, oOptions ) ;
						break;
						
					default:
						MsgError( 'Metodo load desconocido: '  + cMetode );
				
				}
				
			} 
			
			return null ;				
			
	});

	oSrv.complete( function(){ 	

		if ( oSwal != null ) loaddingOff();
		
	});	
}

//--------------------------------

function MsgSignal( lOnOff ) {

	var oControl 	= new TControl();
	var lSignal;
	
	lOnOff 	= (typeof lOnOff == 'boolean' ) ? lOnOff : false;	

	if ( lOnOff ) {
	
		lSignal = oControl.SetSignal( true )	;	
	
		if ( lSignal == false ) {
			MsgConnect( true );
		}		

	} else {
	
		lSignal = oControl.SetSignal( false ); 

		if ( lSignal == false ) {		
			MsgConnect( false );
		}
	}
}

/*
Cambiar el core de MSgServer por fectch
fetch('https://reactnativecode.000webhostapp.com/user_registration.php', {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
 
    name: UserName,
 
    email: UserEmail,
 
    password: UserPassword
 
  })
 
}).then((response) => response.json())
      .then((responseJson) => {
 
// Showing response message coming from server after inserting records.
        Alert.alert(responseJson);
 
      }).catch((error) => {
        console.error(error);
      });
 
 
  }
*/

/*
*	AJAX MsgServer()
*
*	Para crear conexion con server
*	Func 	  -> Puntero a la funcion callback. Recibe ( Data, status )	OPCIONAL
*	oParam  -> Parametros que se desea pasar. Para crear parametros:	OPCIONAL
*			    			o = {}
*			    			o.param1 = 1234
*		     				o.param2 = 'Pol'
*	uCargo 	-> Parámetro (string,array,objecto,...) opcional que sera devuelto a la funcion bCallback
*/

function MsgServer( cPhp, bCallback, oParam, uCargo, lMsgSignal ) {	

	lMsgSignal	= ( typeof lMsgSignal  === 'boolean' ) ? lMsgSignal : true;
	
	/*
	*	Se me ocurrio este tip para forzar al servidor que haga una recarga y que no lee la cache, 
	* porque sino parece ser que si hay muchas peticiones seguidas al detectar la misma IP y uRL 
	* el server desconecta y produce un ERR_EMPTY_RESPONSE. 
	* En la red no he visto solucion alguna, pero como lo aplico en la recarga de imágenes y funciona
	*	lo he dejado y parece que el error ha desaparecido
	*/
	
	cPhp = cPhp + '?' + Math.random();
		
	//	--------------------------------------------------------------------------------------------------------------------

	try {		
		$.ajax({
			  url					: cPhp, 	// el modulo que nos proveera de la informacion que solicitamos
				type				: "post",	// el tipo de consulta, puede ser "get" y "post".
				dataType		: "json",
				data				: oParam,	// los datos relacionados a la consulta Ajax
				context			: this,		// un contexto u objeto con informacion complementaria, este no viaja al servidor				
				success			: __AjaxSucces, 	  // que rutina se ejecuta si esto funciona 								
				complete		: __AjaxComplete, 	// que rutina se ejecuta si esto funciona 								
				beforeSend	: __AjaxBeforeSend,	 
				error				: __AjaxError,		  // que rutina se ejecuta si esto falla
			});
	} catch( ex ) {
		console.log("error ajax en " + cPhp, ex);
		MsgError( ex, 'Error Ajax');			
	}
	
	function __AjaxBeforeSend() { 
		if ( lMsgSignal ) {
			loaddingOn(); 
		}	
	}
	
	function __AjaxComplete() { 
		if ( lMsgSignal ) {
			loaddingOff(); 
		}	
	}	

	function __AjaxSucces ( dat, state, o ) {
		if ( lMsgSignal ) loaddingOff();	
		if ( typeof bCallback == 'function' ) {	
			var fnparams = [ dat, uCargo, state, o ];
			bCallback.apply(null, fnparams );	
		}
	}
	
	function __AjaxError( XMLHttpRequest, textStatus, errorThrown ) {
		if ( lMsgSignal ) loaddingOff();
		
		//	Si ha saltado un error pero hay un status 200 
		//	mostraremos el mesajede error
		if ( XMLHttpRequest.status == 200 ) {
			Msg_AjaxError( cPhp, XMLHttpRequest, textStatus );
		} else {
			TWeb_Error_Dispatcher( cPhp, XMLHttpRequest );		
		}
	}

}

//----------------------------------------------

function Msg_AjaxError( cUrl, XMLHttpRequest, textStatus ) {
	
	cUrl = cUrl.substring(0, cUrl.indexOf("?"));
	
	console.log("error ajax", cUrl, XMLHttpRequest, textStatus);
	
	var newLine = "<br>"; 
	msg  = "url : " + cUrl + newLine;
	msg += "status : " + XMLHttpRequest.status + newLine; 
	msg += "type : " + textStatus + newLine;
	msg += "description  : " + XMLHttpRequest.responseText + newLine; 

	Msg_Swal( msg );
	
}

//----------------------------------------------

function TWeb_Error_Dispatcher( cPhp, XMLHttpRequest ) {

	cPhp = ( typeof( cPhp ) === 'string') ? cPhp : '';
	
	var lError 			= true;
	var cErrorTxt  	= "";

	//	Siempre que se ponga un codigo error ira acompañado de un texto:
	//	header("HTTP/1.0 902 Error );

	switch ( XMLHttpRequest.status  ) {

		case 900:

			cErrorTxt =  XMLHttpRequest.statusText;	

			if ( cErrorTxt == '' ) {
				cErrorTxt =  XMLHttpRequest.status + ' '  + XMLHttpRequest.statusText ;				
			}	

			Msg_Swal( cErrorTxt );
			
			break;
			
		case 901:

			var cUrl =  XMLHttpRequest.statusText;			
			
			window.location.href = cUrl;												
			
			break;	
			
		case 902:	

			//	statusText ->  mifuncion var1 var2
			//		cFunction será mifuncion
			//		aParam será un array con [ var1, var2 ] 

			var cParam			= XMLHttpRequest.statusText;				
			var aParam  		= cParam.split(' ' ) ;			
			var cFunction 	= aParam[0];
			
			aParam = aParam.splice(1) ;

			if ( cFunction !== ''  ) {				
		
				var fn = window[ cFunction ];
							
				// is object a function?
				if (typeof fn === "function") {				
		
					//var fnparams = [ cErrorTxt, XMLHttpRequest, textStatus ];
					//fn.apply(null, fnparams );	
					fn.apply( null, aParam );	
			
				} else {
		
					//if ( oThis.lLog ) console.error( cFunction + ', no es una funcion valida' );				
					console.error( 'TWeb', 'Error asignando ' + cFunction + ' a code 902' );				
				}				
			}															
			
			break;					
			
		case 200:
		
			lError = false;
			
			break;
			
		case 404:	// No file

			cPhp = cPhp.substring(0, cPhp.indexOf("?"));
			Msg_Swal( 'Archivo no existe : ' + cPhp );

			break;
			
		case 500:
		
			cErrorTxt = XMLHttpRequest.status + ' '  + XMLHttpRequest.statusText ;		
			Msg_Swal( cErrorTxt );
			
			break;			
			
		default: 
	
			cErrorTxt = XMLHttpRequest.status + ' '  + XMLHttpRequest.statusText ;		
			Msg_Swal( cErrorTxt );
			
			break;							
	}

	return lError;

}

//----------------------------------------------

function Msg_Swal( msg ) {
	Swal.fire({
		// icon: 'error',
		title: 'Error ajax',
		html: msg
		// text: msg,
	});
}
