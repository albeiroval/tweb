<?php
/*
 * CLASS : TDashboard
 * Author : Albeiro Valencia / TVALWEB 
 * Fecha : 16/08/2021 
 * Documentacion : 
 * https://github.com/chniter/bstreeview
 * https://github.com/jonmiles/bootstrap-treeview
 * https://github.com/chniter/bstreeview/releases/tag/v1.2.0
 * https://htmx.org/
 * https://javascript.plainenglish.io/why-developers-prefer-vanilla-javascript-over-jquery-e707b249d421
 */

define("ITEM_JS", "JS");
define("ITEM_PHP", "PHP");

class TDashboard extends TControl {

  public $fontSize = '12px';

  private $idImage      = 'tree-image';
  private $cImage 	    = null;
  private $cImageClass  = 'tree-image';
  private $aTitle       = [ "text" => 'MAIN MENU', "clrbg" => '#fff', "clrtxt" => '#212529' ];
  private $numItem      = 1;
  private $aItems       = [];
  private $cStrMenu     = ''; 
  
  private $aItemsAction  = [];    
  private $aFunctionPHP  = [];

  private $menuBackClr   = "#eff7f7";
  
  private $clrNormal  = [ "color" => '#212529', "bg" => 'whitesmoke' ];
  private $clrFocus   = [ "color" => '#212529', "bg" => 'coral' ];
  private $clrOver    = [ "color" => '#da6a0d', "bg" => '#adf7ed' ];

  private $aHeader     = [ "create" => false, "height" => '52px', "color" => '#d5f3f3' ];
  private $aTitHeader  = [ "title" => "DASHBOARD", "color" => 'black' ];
  private $aBtnHeader  = [ "text"  => "Logout", "action" => '', "class" => '' ];
  private $PanelFooter = [ "id" => "footer", "action" => null,  "params" => [], "class" => "mobil-footer" ];  
  
  private $aWindPos = null ;   

  //-----------------------------------------------

  public function __construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  ) {
  
    $nTop     = TDefault( $nTop, 10 );
    $nLeft    = TDefault( $nLeft, 10 );
    $nWidth   = TDefault( $nWidth, 300 );
    $nHeight  = TDefault( $nHeight, 300 );

    parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  ); 

    $this->aWindPos = [ "id" => $oWnd->cId, "top" => $oWnd->nTop, "left" => $oWnd->nLeft, 
                        "width" => $oWnd->nWidth, "height" => $oWnd->nHeight ];
  
  }

  //-----------------------------------------------

  public function Header( $nHeight = '52px', $color = '#d5f3f3' ) {
    $this->aHeader = [ "create" => true, "height" => $nHeight, "color" => $color ];
  }

  //-----------------------------------------------

  public function TitleHeader( $title = 'DASHBOARD', $color = '#d5f3f3' ) {
    $this->aTitHeader = [ "title" => $title, "color" => $color ];
  }

  //-----------------------------------------------

  public function ButtonHeader( $text = '', $action = null, $class = null  ) {
    $text  = TDefault($text, 'Logout');
    $this->aBtnHeader = [ "text"   => $text, "action" => $action, "class"  => $class ];

  }

  //-----------------------------------------------

  public function Footer( $cId = 'footer',  $cAction = null, $params = [], $cCss = 'mobil-footer'  ) {
    $this->PanelFooter = [ "id" => $cId, "action" => $cAction, "params" => $params, "class" => $cCss ];
  }

  //-----------------------------------------------

  public function ImageMenu( $cId = 'tree-image', $cImage = null, $cClass = '' ) {
		$this->idImage      = $cId;
    $this->cImage       = $cImage;
    $this->cImageClass  = $cClass;
	}

  //-----------------------------------------------

  public function ImageMenuCss( $cClass = '' ) {
		$this->cImageClass = $cClass;
	}

  //-----------------------------------------------

  public function TitleMenu( $Text = null, $Colortext = null, $Colorbg = null ) {

    $Text       = TDefault( $Text, $this->aTitle["text"] ); 
    $Colorbg    = TDefault( $Colorbg, $this->aTitle["clrbg"] ); 
    $Colortext  = TDefault( $Colortext, $this->aTitle["clrtxt"] ); 
    
    $this->aTitle["title"]   = $Text;
    $this->aTitle["clrbg"]   = $Colorbg;
    $this->aTitle["clrtxt"]  = $Colortext;

	}

  //-----------------------------------------------

  public function itemHome( $text, $action = null, $params = null ) {

    $type = ITEM_PHP;
    $icon = "fa fa-home";
    $oItem = new TItemMenu( $text, $icon, $action );
    $this->aItems[] = $oItem; 

    $this->getItemID( $action, $type, true );

    if ( $action ) {
      $this->aFunctionPHP[] = [ "action" => $action, "params" => $params ]; 
    }
    
    return $oItem;

  }

  //-----------------------------------------------

  public function addMenu( $text, $icon, $action = '', $params = null, $type = ITEM_JS ) {

    $oItem = new TItemMenu( $text, $icon, $action );
    $this->aItems[] = $oItem; 

    $this->getItemID( $action, $type );

    if ( $type == ITEM_PHP ) {
      if ( $action ) {
         $this->aFunctionPHP[] = [ "action" => $action, "params" => $params ]; 
      }  
    }
    
    return $oItem;
  }

  //-----------------------------------------------

  public function addSubmenu( $oItem, $text, $icon, $action = '', $params = null, $type = ITEM_JS ) {

    $this->getItemID( $action, $type );

    $oItem->addItem( $text, $icon );
    if ( $type == ITEM_PHP ) {
      if ( $action ) {
         $this->aFunctionPHP[] = [ "action" => $action, "params" => $params ]; 
      }  
    }
  
  }

  //-----------------------------------------------

  private function getItemID( $action, $type, $home = false ) {

    $cId = "item" . rtrim(strval($this->numItem));
    $this->aItemsAction[] = [ "id" => $cId, "action" => $action, "type" => $type, "home" => $home ];
    
    $this->numItem++;

    return;
  } 

  //-----------------------------------------------

  public function BGColorMenu( $color = "#eff7f7" ) 
  {
    $this->menuBackClr = $color;
  }

  //-----------------------------------------------

  public function ColorNormalMenu( $Colortext = null, $Colorbg = null ) {

    $Colortext  = TDefault( $Colortext, $this->clrNormal["color"] ); 
    $Colorbg    = TDefault( $Colorbg, $this->clrNormal["bg"] ); 
    
    $this->clrNormal["color"] = $Colortext;
    $this->clrNormal["bg"]    = $Colorbg;

	}

  //-----------------------------------------------

  public function ColorFocusMenu( $Colortext = null, $Colorbg = null ) {

    $Colortext  = TDefault( $Colortext, $this->clrFocus["color"] ); 
    $Colorbg    = TDefault( $Colorbg, $this->clrFocus["bg"] ); 
    
    $this->clrFocus["color"] = $Colortext;
    $this->clrFocus["bg"]    = $Colorbg;

	}

  //-----------------------------------------------

  public function ColorOverMenu( $Colortext = null, $Colorbg = null ) {

    $Colortext  = TDefault( $Colortext, $this->clrOver["color"] ); 
    $Colorbg    = TDefault( $Colorbg, $this->clrOver["bg"] ); 
    
    $this->clrOver["color"] = $Colortext;
    $this->clrOver["bg"]    = $Colorbg;

	}

  //-----------------------------------------------

  private function CreateHeader() {

    $cHtml = '';

    if ( $this->aHeader["create"] ) {
      $cHtml  = '<header class="side-header">';
      $cHtml .= '  <div class="header-say">';
      $cHtml .= '    <h5 style="' . $this->aTitHeader["color"] .';">' . $this->aTitHeader["title"] . '</h5>';
      $cHtml .= '  </div>';
      $cHtml .= '  <div class="header-btn">';
      $cHtml .= '    <button class="btn btn-primary '.$this->aBtnHeader["class"].'" aria-label="Left Align"'; 
      $cHtml .= '            onclick="' . $this->aBtnHeader["action"] .';">' . $this->aBtnHeader["text"];
      $cHtml .= '       <span class="fas fa-sign-in-alt" style="margin-left: 8px;" aria-hidden="true"></span>';
      $cHtml .= '    </button>';
      $cHtml .= '  </div>';
      $cHtml .= '</header>';
      $this->nTop = (int)$this->aHeader["height"] + $this->nTop;
    }

    return $cHtml;

  }
  
  //-----------------------------------------------

  private function CreateToogleButton() {

    $cHtml  = ' <div class="toogle-btn" id="toogle-btn" onclick="oMenu.toogle();">';
		$cHtml .= '  <i class="fas fa-align-justify"></i>';
		$cHtml .= ' </div>';

    return $cHtml;

  }

  //-----------------------------------------------

  private function CreateImage() {

		$cHtml = '';

		if ( $this->cImage ) {
			$cHtml .= '<div id="' . $this->idImage . '" class="' . $this->cImageClass . '">';
			$cHtml .= '  <img src="' . $this->cImage . '" style="width: 100%;height: 100%;" alt="sidebar_image">'; 
			$cHtml .= '</div>';
		}

		return $cHtml;

	}

  //-----------------------------------------------

  private function CreateTitle() {

    $cHtml  = '<div style="border: solid 0.01em #cec3c4 !important;width: auto;"></div>';
    $cHtml .= '<div class="tree-title">';
		$cHtml .= '  <h6>' . $this->aTitle["title"] . '</h6>'; 
		$cHtml .= '</div>';
		
		return $cHtml;

	}

  //-----------------------------------------------

  private function CreateTree() {
    
    $cHtml  = '<div id="tree" style="';
    $cHtml .= ' overflow-y:auto;';
    $cHtml .= ' height:450px;';
    $cHtml .= ' border:1px solid #ccc;';
    $cHtml .= ' width:'.$this->nWidth.';';
    $cHtml .= ' background-color: '.$this->menuBackClr.'">';
    $cHtml .= '</div>';

    return $cHtml;

  }

  //-----------------------------------------------
  
  private function CreateMenu() {

    $numItem = 0; 

    $cMenu = 'var tree = [';

    $nItems = count( $this->aItems );
    for ($i=0; $i < $nItems; $i++) { 
      $item = $this->aItems[ $i ];
      $numItem++;
      $cMenu .= '{';
      $cMenu .= 'id : "item' . strval($numItem) . '",';
      $cMenu .= 'text: "' . $item->text . '",';
      $cMenu .= 'icon: "' . $item->icon . '",';
      $cMenu .= "class: 'mymenu'";
      $nItem = count($item->submenu);
      if ( $nItem > 0) {
        $cMenu .= ', nodes: [';
        for ($x=0; $x < $nItem; $x++) { 
          $numItem++;
          $subitem = $item->submenu[$x];
          $cMenu .= '{';
          $cMenu .= 'id : "item' . strval($numItem) . '",';  
          $cMenu .= 'text : "' . $subitem["text"] . '",';
          $cMenu .= 'icon : "' . $subitem["icon"] . '",';
          $cMenu .= "class: 'mymenu',";
          $cMenu .= '},';
        }
        $cMenu  = rtrim($cMenu, ",");
        $cMenu .= ']';  
      }
      $cMenu .= '},';
    }

    $cMenu  = rtrim($cMenu, ",");
    $cMenu .= '];';

    $this->cStrMenu = $cMenu;

  }

  //-----------------------------------------------

  private function CreateFooter() {

    $footer = $this->PanelFooter;

    if ( $footer["action"] != null )  {
      if ( function_exists( $footer["action"] ) ) {
        if ( $footer["params"] ) {
          call_user_func_array( $footer["action"], [ $footer["params"] ] );
        } else {
          call_user_func( $footer["action"] );
        }  
      } else {
        var_dump("Error : No Existe la Funcion PHP : " . $footer["action"] );
        exit;
      }  
    }

  }

  //-----------------------------------------------
  // https://stackoverflow.com/questions/43870211/highlight-and-scroll-inside-bootstrap-treeview

  public function Activate() {
    
    $this->LoadLibs();

    $this->CreateMenu();

    $cHtml  = $this->CreateHeader();
    $cHtml .= $this->CreateToogleButton();
    $cHtml .= '<div class="side-bar" id="side-bar">';
    $cHtml .=   $this->CreateImage();
    $cHtml .=   $this->CreateTitle();
    $cHtml .=   $this->CreateTree();
    $cHtml .= '</div>';
    
    echo $cHtml;

    $this->createCSS();

    // Crea los Panels del Dashboard
    $this->createItemsPHP();

    // Crea el Footer
    $this->CreateFooter();

    $cJS  = '<script>';

    $cJS .= PHP_EOL;
    $cJS .= $this->cStrMenu . PHP_EOL;
    $cJS .= 'var aItems = ' . json_encode( $this->aItemsAction ) . PHP_EOL;
    $cJS .= 'var btnLeftPos = ' . ($this->nWidth + 10) . PHP_EOL;
    $cJS .= 'var oMenu;' . PHP_EOL;
    $cJS .= '$(function() {' . PHP_EOL;
    $cJS .= '  oMenu = new TTreeView();' . PHP_EOL;
    $cJS .= '  oMenu.init();' . PHP_EOL; 
    $cJS .= '  oMenu.footerCss("' . $this->PanelFooter["id"] . '",';
    $cJS .= '                  "' . $this->PanelFooter["class"] .   '",'; 
    $cJS .= '                  "' . $this->aWindPos["id"] . '");' . PHP_EOL;
    $cJS .= '});' . PHP_EOL;
    $cJS .= PHP_EOL;
    
    $cJS .= '</script>';

    echo $cJS;
    
  }

  //-----------------------------------------------

  private function LoadLibs() {

    $cHtml  = '<!-- LIBRERIAS core.dashborad  -->';
    $cHtml .= '<link href="' . TWEB_PATH . 'core.dashboard.css" rel="stylesheet">';
    $cHtml .= '<script src="' . TWEB_PATH . '/core.dashboard.js"></script>';
    
    echo $cHtml;

  }

  //-----------------------------------------------

  private function createItemsPHP() {

    $count = count($this->aFunctionPHP);
    for ($i=0; $i < $count; $i++) { 
      $item = $this->aFunctionPHP[$i];
      if ( function_exists( $item["action"] ) ) {
        if ( $item["params"] ) {
          call_user_func_array( $item["action"], $item["params"] );
        } else {
          call_user_func( $item["action"] );
        }  
      } else {
        var_dump("Error : No Existe la Funcion PHP : " . $item["action"] );
        exit;
      }  
    }

  }  

  //-----------------------------------------------

  private function createCSS() {

    $nPosToggleBtn = ( $this->nWidth + 10 ) . 'px';

    $clrBg  = $this->aTitle["clrbg"];
    $clrTxt = $this->aTitle["clrtxt"];

    $cSS  = '<style>' . PHP_EOL;

    $cSS .= '.side-bar {'      . PHP_EOL;
    $cSS .= '  position: absolute;' . PHP_EOL;  
    $cSS .= '  border-color: #343a40;' . PHP_EOL;  
    $cSS .= '  top : auto;' . PHP_EOL;  
    $cSS .= '  left : ' . $this->nLeft . ';' . PHP_EOL;  
    $cSS .= '  width : ' . $this->nWidth . ';' . PHP_EOL;  
    $cSS .= '  height : auto;' . PHP_EOL; 
    $cSS .= '  border: 1px solid rgba(0, 0, 0, 0.125);' . PHP_EOL; 
    $cSS .= '  border-radius: 0.25rem;' . PHP_EOL; 
    $cSS .= '  overflow-y:auto;' . PHP_EOL; 
    $cSS .= '}' . PHP_EOL;

    $cSS .= '.toogle-btn {'                   . PHP_EOL;
    $cSS .= ' position: absolute;'            . PHP_EOL;
    $cSS .= ' left: ' . $nPosToggleBtn . ';'  . PHP_EOL;
    $cSS .= ' top: ' . $this->nTop . ';'      . PHP_EOL;
    $cSS .= ' width: 35px;'                   . PHP_EOL;
    $cSS .= ' height: 35px;'                  . PHP_EOL;
    $cSS .= ' padding: 5px;'                  . PHP_EOL;
    $cSS .= ' font-size: 22px;'               . PHP_EOL;
    $cSS .= ' text-align: center;'            . PHP_EOL;
    $cSS .= ' cursor: pointer;'               . PHP_EOL;              
    $cSS .= ' border-radius: 0.25rem;'        . PHP_EOL; 
    $cSS .= ' border: 1px solid rgba(0, 0, 0, 0.125);' . PHP_EOL;
    $cSS .= '}'                               . PHP_EOL;

    
    $cSS .= '.mymenu {' . PHP_EOL;
    $cSS .= '  color: ' . $this->clrNormal["color"] . ';' . PHP_EOL;  
    $cSS .= '  background-color: ' . $this->clrNormal["bg"] . ';' . PHP_EOL;
    $cSS .= '}' . PHP_EOL;
    
    $cSS .= '.mymenu:hover {'   . PHP_EOL;
    $cSS .= '  color: ' . $this->clrOver["color"] . ';' . PHP_EOL;
    $cSS .= '  background-color: ' . $this->clrOver["bg"] . '!important;' . PHP_EOL;
    $cSS .= '}' . PHP_EOL;

    $cSS .= '.active {' . PHP_EOL;
    $cSS .= '  color: ' . $this->clrFocus["color"] . ';' . PHP_EOL;  
    $cSS .= '  background-color: ' . $this->clrFocus["bg"] . ';' . PHP_EOL;
    $cSS .= '}' . PHP_EOL;

    $cSS .= '.tree-image {'         . PHP_EOL;
    $cSS .= '  object-fit: cover;'  . PHP_EOL;	
    $cSS .= '  width: ' . $this->nWidth . ';' . PHP_EOL; 
    $cSS .= '  max-width: 100%;' .PHP_EOL;
    $cSS .= '  max-height: 100%;' .PHP_EOL;
    $cSS .= '  height: 200px;'       . PHP_EOL;
    $cSS .= '  padding: 6px;'       . PHP_EOL;
    $cSS .= '  text-align: center;' . PHP_EOL;
    $cSS .= '}'                     . PHP_EOL;

    $cSS .= '.tree-title {'         . PHP_EOL;
    $cSS .= '  width: ' . $this->nWidth . ';' . PHP_EOL; 
    $cSS .= '  height: auto;'       . PHP_EOL;
    $cSS .= '  padding: 6px;'       . PHP_EOL;
    $cSS .= '  margin-top: 0.5px;'       . PHP_EOL;
    $cSS .= '  margin-bottom: 0.5px;'       . PHP_EOL;
    $cSS .= '  color: ' . $clrTxt .';' . PHP_EOL;
    $cSS .= '  background-color: ' . $clrBg . ';' . PHP_EOL;
    $cSS .= '  text-align: center;' . PHP_EOL;
    $cSS .= '  border-radius: 3px;' . PHP_EOL;
    $cSS .= '}'                     . PHP_EOL;

    $cSS .= '.side-header {' . PHP_EOL;
    $cSS .= '  display: flex;' .PHP_EOL;
    $cSS .= '  justify-content: space-between;'. PHP_EOL;
    $cSS .= '  height : ' . $this->aHeader["height"] . ';' . PHP_EOL;
    $cSS .= '  background-color : ' . $this->aHeader["color"] . ';' . PHP_EOL;
    $cSS .= '  padding: 8px;' . PHP_EOL;
    $cSS .= '}'. PHP_EOL;

    $cSS .= '.' . $this->PanelFooter["class"] .'{' . PHP_EOL;
    $cSS .= '  display: flex;' . PHP_EOL;
    $cSS .= '  justify-content: space-between;'. PHP_EOL;
    $cSS .= '}' . PHP_EOL;
    
    $cSS .= '</style>' . PHP_EOL;

    echo $cSS;

  }

}

//------------------------------------------------------

class TItemMenu {

  public  $text    = '';
  public  $icon    = '';
  public  $submenu = [];

  public function __construct( $text, $icon ) {
    $this->text     = $text;
    $this->icon     = $icon;
  }

  public function addItem( $text, $icon ) {
    $this->submenu[] = [ "text" => $text, "icon" => $icon ];
  } 

}

?>
