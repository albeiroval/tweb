<?php
//	Si creamos un modulo dentro del directorio de la libreria, debemos inicializar TWEB_PATH a ''
	define( 'TWEB_PATH'		,  '' );
	
	include ( 'core.php' );

//	Leemos el fichero log

	$cFitxer	= dirname(__FILE__) . '/log/log.txt';	
	$cLine 		= '';

	if ( file_exists( $cFitxer ) ) {
	
		$file = fopen( $cFitxer, "r"); 

		while(!feof($file)){
			$cLine .= htmlspecialchars( fgets($file) ). "</br>" ;
		}
		
		fclose($file);
	}	
	
//	Creamos la vista	

	$oWnd = new TDialog( null, 500, 300 );
			
			$oSay = new TSay( $oWnd, null, 0, 0, $cLine );	
				$oSay->lMemo 			= true;
				$oSay->lScrollbar		= true;	
				$oSay->SetRight( 0 );
				$oSay->SetBottom( 0 );
			
	$oWnd->Activate();
?>