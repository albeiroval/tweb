<?php
include_once ( 'libs/jwt/JWT.php' );

/*	NOTES Carles 

Un JWT técnicamente es un mecanismo para verificar el propietario de algunos datos JSON. Es una 
cadena codificada, que es segura para URL, que puede contener una cantidad ilimitada de datos (a 
diferencia de una cookie), y está firmada criptográficamente.

Cuando un servidor recibe un JWT, puede garantizar que los datos que contiene pueden ser confiables 
porque están firmados por la fuente. Ningún intermediario puede modificar un JWT una vez que se envía.

Es importante tener en cuenta que un JWT garantiza la propiedad de los datos pero no el cifrado: 
cualquier persona que intercepte el token puede ver los datos JSON que almacena en un JWT, ya que 
simplemente se serializa, no se cifra.

Por esta razón, es altamente recomendable utilizar HTTPS con JWT (y, por cierto, HTTPS en general).

Un JWT debe almacenarse en un lugar seguro dentro del navegador del usuario.

Si lo almacena en localStorage, se puede acceder a él mediante cualquier secuencia de comandos 
dentro de su página (lo cual es tan malo como suena, ya que un ataque XSS puede permitir que un 
atacante externo tenga acceso al token).

No lo almacene en almacenamiento local (o almacenamiento de sesión). Si alguno de los scripts de la 
tercera parte que incluye en su página se ve comprometido, puede acceder a todos los tokens de sus 
usuarios.

El JWT debe almacenarse dentro de una cookie HttpOnly, un tipo especial de cookie que solo se envía 
en solicitudes HTTP al servidor, y nunca es accesible (tanto para leer o escribir) desde JavaScript 
que se ejecuta en el navegador.

Dado que JWT están firmados, el receptor puede estar seguro de que el cliente realmente es quien 
cree que es.

Los JWT se pueden usar como un mecanismo de autenticación que no requiere una base de datos. 

Puede almacenar cualquier tipo de información de usuario en el cliente. el servidor puede confiar 
en el cliente, ya que el JWT está firmado y no es necesario llamar a la base de datos para recuperar
la información que ya almacenó en el JWT

//	ESTA BE	--------------------------
Cualquiera que tenga acceso al PC del cliente, a su sesión y a su navegador. Llegado a este punto es 
responsabilidad del usuario proteger su computadora y sus cuentas, no del desarrollador. Si permite al 
navegador que guarde su usuario y contraseña (es algo muy habitual), cualquiera que pueda leer el 
localStorage podría también simplemente entrar al navegador y abrir una nueva sesión.

En conclusión: no hay problema por usar el localStorage, siempre y cuando borres el token cuando el 
usuario decida cerrar la sesión (podrías usar sessionStorage, pero el concepto de sesión del navegador 
puede ser un poco restrictivo si quieres que el token dure horas o incluso días). Lo que sí te aconsejo 
es mandar el token como un header, no como cookie, para evitar ataques CSRF.
//	-----------------------------------

*/

/*
Check Session Storage (sols per pestanya y mentres navegador obert y LocalStorage
*/


class TToken {

  private $secret_key 	= 'SonGoku!#2019';
  private $encrypt 		= ['HS256'];
  private $aud 			= null;
  public  $nLapsus		= 3600; 	//	3600;	//	(60*60);  //	0 -> No caduca
	private $cName			= '';
	private $aVars   		= array();
	public  $lCookie  		= true;
	private $cToken  		= '';
	public  $cError  		= '';
	public  $lOnlyAjax  	= false ;		
	public  $iss  			= null ;		
	public  $nbf  			= null ;		
	public  $sub  			= null ;		
	public  $jti  			= null ;		
	
	
	public function __construct( $cName = 'TWEB' ) {     

		$this->cName  	= $cName;				
	}

	public function SetVar( $cVar = '', $uValue ) {	$this->aVars[ $cVar ] = $uValue; }
	public function AddVar( $cVar = '', $uValue ) {	$this->SetVar( $cVar, $uValue ); }	

	public function Login() {
	
        $time 	= time();		
		$token 	= array();
		
		$token[ 'iat' 	] = $time;						//	claim: Fecha de creación
		$token[ 'exp' 	] = $time + $this->nLapsus;		//	claim: Caducitat del token
		$token[ 'aud' 	] = $this->Aud();				//	Capa seguridad: Auditoria
		$token[ 'lapsus'] = $this->nLapsus	;			//	Tiempos en seg. max. de validez
		$token[ 'data'	] = $this->aVars	;			//	Datas de usuario
		
		if ( $this->iss )
			$token[ 'iss'	] = $this->iss	;			//	claim: creador del jwt
			
		if ( $this->nbf )
			$token[ 'nbf'	] = $this->nbf	;			//	claim: Not before
			
		if ( $this->sub )
			$token[ 'sub'	] = $this->sub	;			//	claim: razón de la creación del jwt			
		
		if ( $this->jti )
			$token[ 'jti'	] = $this->jti	;			//	claim: Json Token Id: un unico identificador para el token							
		
        $this->cToken = JWT::encode($token, $this->secret_key);	
		
		if ( $this->lCookie )			
			$this->SendCookie();
			
		return $this->cToken;
	
	}
	
	public function Logout() {	

		if ( $this->lCookie )			
			$this->SendCookie( false );
			
		return '';	
	}	
	
	function SendCookie( $lSend = true ) {
	
		$nExpire 		= time() + $this->nLapsus;
		
		$name 			= $this->cName;
		$value 			= $this->cToken;		
		$expirationTime = $nExpire;    // Session cookie.
		$path 			= '/';
		$domain 		= 'localhost';
		$isSecure 		= false;
		$isHttpOnly 	= true;
	
		if ( $lSend ) {

			setcookie($name, $value, $expirationTime, $path, $domain, $isSecure, $isHttpOnly);	
			
		} else {
		
			unset( $_COOKIE[ $name ] );
		
			setcookie($name, '', -1, $path, $domain, $isSecure, $isHttpOnly);		
		}
	
	}
	
	function Valid( $cToken = '', $uAction = null ) {

		$lAjax 		= $this->Is_Ajax(); 

		if ( $this->lOnlyAjax && $lAjax == false ) {
			die();
		}

		if ( $this->lCookie )	{
		
			$this->cToken = isset( $_COOKIE[ $this->cName ] ) ? $_COOKIE[ $this->cName ] : '';	
			
		} else {
		
			$this->cToken = $cToken ;		
		}
		
		if ( empty( $this->cToken ) ) { 
			$this->Kill( $lAjax, $uAction, 'No token...' );			
			exit();
		}
		
		try {
			$this->Check();
		} catch ( Exception $e) {	

			$this->cError = $e->getMessage();

				$this->Kill( $lAjax, $uAction, $this->cError );			
				
			//	-----------------------
			
			return false;
		}						
	
		return true;
	}		

    private function Check()  {
	
        if(empty($this->cToken)) {
            throw new Exception("No existe token.");
        }

        $decode = JWT::decode(
            $this->cToken,
            $this->secret_key,
            $this->encrypt
        );

        if($decode->aud !== $this->Aud()) {
            throw new Exception("Invalid user logged in.");
        }

    }
	
	//	Refrescarem el token amb la data d'expiracio
	
    public function Refresh() {
	
		$aToken = (array) $this->GetToken();

		//	Refresco 
		
		$nTime = time();
		
		$aToken[ 'iat' ] = $nTime;
		$aToken[ 'exp' ] = $nTime + $aToken[ 'lapsus' ];
		
		$this->cToken = JWT::encode($aToken, $this->secret_key);
		
		if ( $this->lCookie )			
			$this->SendCookie();						
	
		return $this->cToken;	
	}
	
    public function GetData() {

        return JWT::decode(
            $this->cToken,
            $this->secret_key,
            $this->encrypt
        )->data;
    }	
	
    public function GetToken() {
	
        return JWT::decode(
            $this->cToken,
            $this->secret_key,
            $this->encrypt
        );
    }

	public function GetLapsus() {
	
		$aToken = $this->GetToken();
		
		return $aToken->lapsus;
	}
	
	public function GetPendingTime() {
	
		$aToken = $this->GetToken();
		
		$nLapsus = $aToken->exp - time();
		
		return $nLapsus;		
	}

    private function Aud() {
	
        $aud = '';

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $aud = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $aud = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $aud = $_SERVER['REMOTE_ADDR'];
        }

        $aud .= @$_SERVER['HTTP_USER_AGENT'];
        $aud .= gethostname();

        return sha1($aud);
    }
	
	private function Kill( $lAjax, $uAction = null, $cError = '' ) {
	
		$cError_Action 	= '';
		$cError_Param 	= '';		

		$cType 			= gettype( $uAction );			

		switch ( $cType ) {
		
			case 'array':
			
				reset( $uAction );
				$cError_Action 	= key( $uAction );
				$cError_Param	= $uAction[ $cError_Action ];	// 'url', 'function'
			
				break;						
			
			case 'string':					
			
				$cError_Action  = 'url';			
				$cError_Param 	= $uAction ;

				break;								
		}			

		if ( $lAjax ) {	
						
			switch( $cError_Action ) {		
			
				case 'url':							
					header("HTTP/1.0 901 " . $cError_Param );
					break;
					
				case 'function':
					header("HTTP/1.0 902 " . $cError_Param );
					break;					

				default:
					header("HTTP/1.0 900 " . $cError );
					break;													
			}
			
		} else {

			$cUrl = '';
			
			switch(  $cError_Action ) {
			
				case 'url':
					$cUrl = $cError_Param ;
					break;												
			}			
	
	
			/*	Quan la crida la fem que NO es Ajax, tenim 2 solucions:
				1.- Executarem un javascript que redirijirà a algun lloc, p.e. index.php 
				2.- Senzillament matem el proces amb un die(). Això és util per si s'executa a pel desde la URL
			*/						

			if ( $cUrl !== '' ) {
			
				/*				
					$cCmd  = "<script type='text/javascript'>";
					$cCmd .= 'window.location.href = "' . $cUrl . '" ;';
					$cCmd .= "</script>";				
					
					echo $cCmd ;
				*/
				
				header( "Location: " . $cUrl );

			} else {						

				die();
			}
			
		}

		exit();		
	}	


	private function Is_Ajax() {
	
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
			return true;
		else
			return false;		
	}	
}
?>