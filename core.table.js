/*
*	 Class TABLE
*  Fecha  : 10-07-2021
*  Autor  : Antonio Albeiro Valencia
*  (C)(R) : Derechos Reservados, se prohibe el uso de esta libreria para otros fines distintos
*						al uso del framework VALWEB
*/

class BTable {

  constructor(id, options) {
    var table     = $("#" + id);
    var row_index = -1;
    var onClick   = options; 

    //-------------------------------------
    this.create = function( aData = [], lExport = false ) {
      table.bootstrapTable("destroy");
    
      var aParam = {};

      if ( aData.length > 0 ) {
        aParam.data   = aData;
      }

      if ( lExport ) {
        aParam.exportDataType = 'all';
        aParam.exportTypes    = ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf']
      } 

      aParam.locale = "es-ES";

      aParam.onClickRow = function (row) {
        if ( onClick.clickrow ) {
          eval(onClick.clickrow);
        }

      };

      aParam.onDblClickRow = function (row) {
        if ( onClick.dblclickrow ) {
          eval(onClick.clickrow);
        }  
      };
      
      table.bootstrapTable(aParam);
    
    };
    
    //-------------------------------------
    this.getData = function() {
      return JSON.stringify(table.bootstrapTable('getData'));
    }; 

    //-------------------------------------
    this.setData = function( aData ) {
      table.bootstrapTable('load', aData);
    };

    //-------------------------------------
    this.addRow = function( aRow ) {
      table.bootstrapTable('append', aRow );
    };

    //-------------------------------------
    this.getindex = function() {
      return row_index;
    };

    //-------------------------------------
    // https://github.com/wenzhixin/bootstrap-table/issues/3834#issuecomment-395985149
    this.delRowColumn = function( column ) {
      if (column) {
        var ids = $.map(table.bootstrapTable('getSelections'), function (row) {
          return row[column];
        });
        if ( ids.length > 0 ) {
          table.bootstrapTable('remove', {
            field:  "" + column,
            values: ids
          });
        } else {
          MsgNotify("Haga click en la fila a Eliminar", "error");
        } 
      } else {
        MsgNotify("Falta parametro en method : delRowColumn()", "error");
      }
    };

    //-------------------------------------
    this.delRowIndex = function( nRow ) {
      if ( nRow ) {
        table.bootstrapTable('remove', {
          field: '$index',
          values: nRow
        });
      } 
    };

    //-------------------------------------
    this.updateRow = function( aData ) {
      if (aData) {
        if ( this.rowselect() ) {
          table.bootstrapTable('updateRow', {
            index: row_index,
            row: aData
          });
        } else {
          MsgNotify("Haga click en la fila a Modificar", "error");
        }
      } else {
        MsgNotify("Falta parametro en method : upadateRow()", "error");
      }    
    };

    //-------------------------------------
    this.rowselect = function() {
      var ids = $.map(table.bootstrapTable('getSelections'), function (row) {
        return row;
      });
      if ( ids.length > 0 ) {
         return true; 
      } else {
        return false;
      }
    }; 

    //-------------------------------------
    this.getrow = function( index = 0) {
      
      var ids = $.map(table.bootstrapTable('getSelections'), function (row) {
        return row;
      });

      if ( ids.length > 0 ) {
        return ids[0];
      } else {
        return null;
      }
      
    }

    //-------------------------------------
    this.check = function( index = 0 ) {
      table.bootstrapTable('check', index)
    }

    //-------------------------------------
    this.uncheck = function( index = 0 ) {
      table.bootstrapTable('uncheck', index)
    }
    
    //-------------------------------------
    this.showloading = function( lShow = true ) {
      if ( lShow ) {
        table.bootstrapTable('showLoading');
      } else {
        table.bootstrapTable('hideLoading');
      }
    };

    //-------------------------------------
    this.init = function() {

      /*
      // actualiza el index del row seleccionado 
      table.on('click-row.bs.table', function (e, row, $element) {
        row_index = $element.index();
        console.log("click on ", row_index);
      });
      */

      // https://bootstrap-table.com/docs/api/events/
      table.on('check.bs.table', function (e, row, $element) {
        row_index = $element.attr('data-index');
      });
      
    }
    
    /** INICIA LA CLASE **/
    this.init(); // Inicia el Control
  
  }  

}

//-------------------------------------------------//

function createKeyEvent( cId, cAction ) {
  let elem = document.getElementById(cId);
  elem.addEventListener("keyup", function(event) {;
    if (event.key === 'Enter') {;
      event.preventDefault();  
      let func = new Function( cAction );
      func(cId);
    };
  });
}

function totalTextFormatter(data) {
  return 'Total';
}         

function totalPriceFormatter(data) {
  var field = this.field;
  var value = data.map(function(row) { return +row[field] }).reduce(function(sum, i) { return sum + i }, 0);
  return '$ ' + value.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}

// FINAL