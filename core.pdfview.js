/*
*  CLASS TPDFView Javscript
*  AUTOR  : Albeiro Valencia
*  FECHA  : 03/09/2021 
*  https://stackoverflow.com/questions/23115581/can-i-explicitly-specify-a-mime-type-in-an-img-tag
*
*/

var TViewPDF = function( cId ) {
  
  this.cId = "#" + cId;

  const __base64 = "base64";
  const __file   = "file";
  
  //--------------------------------------

  this.show = function() {
    $(this.cId).modal('show');
  };
  
  //--------------------------------------

  this.loadFromBase64 = function( cUrl, oParam ) {

    this.doCallAjax( cUrl, oParam, __base64 );

    
  };

  //--------------------------------------

  this.loadFromFile = function( cUrl, oParam ) {
    
    this.doCallAjax( cUrl, oParam, __file );
    
  };

  //--------------------------------------

  this.doCallAjax = function( cUrl, oParam, cType ) {

    $.ajax({
      type     : 'POST',
      url      : cUrl + "?" + Math.random(),
      // cache    : false,
      data     : oParam,
      dataType : 'json'
    })
    .done( function( data ) {
      if ( data != null ) {
        initializePDFJS( { id : "myCanvas", type : cType, data : data, scale : 5 } );
      } else {
        JMsgError("No se encontro base64 / Archivo");
      }
    })
    .fail( function (jqXhr, textStatus, errorMessage) {
      if ( jqXhr.status == 200 ) {
        Msg_AjaxError( cUrl, jqXhr, textStatus );
      } else {
        Msg_Dispatcher( jqXhr, cUrl );		
      }
    });

  };
  
  //--------------------------------------
  
  function Msg_Dispatcher( jqXhr, cUrl ) {

    cUrl = ( typeof( cUrl ) === 'string') ? cUrl : '';

    var lError 			= true;
    var cErrorTxt  	= "";

    //	Siempre que se ponga un codigo error ira acompañado de un texto:
    //	header("HTTP/1.0 902 Error );

    switch ( jqXhr.status  ) {

      case 900:

        cErrorTxt =  jqXhr.statusText;	

        if ( cErrorTxt == '' ) {
          cErrorTxt =  jqXhr.status + ' '  + jqXhr.statusText ;				
        }	

        Msg_Swal( cErrorTxt );

        break;

      case 901:

        cUrl =  jqXhr.statusText;			

        window.location.href = cUrl;												

        break;	

      case 902:	

        var cParam			= jqXhr.statusText;				
        var aParam  		= cParam.split(' ' ) ;			
        var cFunction 	= aParam[0];

        aParam = aParam.splice(1) ;

        if ( cFunction !== ''  ) {				
        
          var fn = window[ cFunction ];

          if (typeof fn === "function") {				
            fn.apply( null, aParam );	
          } else {
            console.error( 'TWeb', 'Error asignando ' + cFunction + ' a code 902' );				
          }				
        }															

        break;					

      case 200:
      
        lError = false;

        break;

      case 404:	

        cUrl = cUrl.substring(0, cUrl.indexOf("?"));
        Msg_Swal( 'Archivo no existe : ' + cUrl );

        break;

      case 500:
      
        cErrorTxt = jqXhr.status + ' '  + jqXhr.statusText ;		
        Msg_Swal( cErrorTxt );

        break;			

      default: 

        cErrorTxt = jqXhr.status + ' '  + jqXhr.statusText ;		
        Msg_Swal( cErrorTxt );

        break;							
    }

    return lError;

  }	

  //--------------------------------------
  
  function Msg_AjaxError( cUrl, jqXhr, textStatus ) {
    cUrl = cUrl.substring(0, cUrl.indexOf("?"));
  
    console.log("error ajax", cUrl, jqXhr, textStatus);
  
    var newLine = "<br>"; 
    msg  = "url : " + cUrl + newLine;
    msg += "status : " + jqXhr.status + newLine; 
    msg += "type : " + textStatus + newLine;
    msg += "description  : " + jqXhr.responseText + newLine; 
  
    Msg_Swal( msg );
  }

  //--------------------------------------
  
  function Msg_Swal( msg ) {
	  Swal.fire({
		  title: 'Error ajax',
		  html: msg
		});
  }

};

