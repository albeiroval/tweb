<?php 
/*
* Libreria   				: TWEB (FrameWork for Web)
* Autor							: Carles Aubia
* Versio						: 1.3
* Data Inici  			: 24/12/2014
* Ult. Modificacio	: 11/10/2018
* Descripcio				: Módulo para crear diálogos desde un fichero *.RC
*					  				  Basado en la estructura generada por ResEdit
*/

include_once ( "core.constant.php" );

class TRc {

	public  $cFile		= '';
	private $cDialog	= '';
	private $cCaption	= '';
	private $lFound		= false;
	private $aCtrl		= array();
	private $nTop		= 0;
	public  $nLeft		= 0;
	public  $nWidth		= 0;
	public  $nHeight	= 0;	
	public  $oFont		= null;	

	public function __construct( $cFile = '', $cDialog = '' ) {     
   
		$this->cFile	= TDefault( $cFile, '' );
		$this->cDialog	= TDefault( $cDialog, '' );

		if ( !file_exists( $this->cFile ) ) {
			MsgError( TWEB_RC_NO_FILE . ' ' . $this->cFile );
			exit();		
		}		
		
		$this->Load_Rc();

	}
	
	private function Load_Rc() {
	
		if ( $this->cDialog == '' ){
			MsgError( TWEB_RC_NO_DIALOG );
			exit();
		}
	
		$lExit 			= false;
		$this->cDialog 	= strtolower( $this->cDialog );
		
		$this->nHandler = fopen( $this->cFile, "r") or exit( TWEB_RC_NO_OPEN );

		while( (!feof($this->nHandler)) and ( $lExit == false ) )	{
		
			$cLinea 	= fgets($this->nHandler);
			$lDialog	= strpos($cLinea, ' DIALOG ' );
			
			if ( ! $lDialog )
				$lDialog	= strpos($cLinea, ' DIALOGEX ' );	// Pelles C
			
			if ( $lDialog ) {
			
				$cName = strtolower( strtok( $cLinea, " ") );

				if ( $cName ==  $this->cDialog ) {
					$this->lFound = true;
				    $this->Read_Dialog( $cName, $cLinea );				
					$lExit = true;
				}
			}
	
		}
		
		fclose($this->nHandler);
		
		if ( $this->lFound == false ) {
			MsgError( TWEB_RC_NO_RESOURCE . ' ' . $this->cDialog );
			exit();
		}

	}


	private function Read_Dialog( $cName, $cLinea ){
	
		$nPos			= strpos( $cLinea, " DIALOG " ) + 8;		
		$cLinea			= substr( $cLinea, $nPos );
		
//		Si s'edita amb PELLES C, La linea te un contingut => DLG_SAY DIALOG DISCARDABLE 6, 15, 207, 111 
//		Si tiene DISCARDABLE, lo eliminamos

		$nPos = strpos( $cLinea, 'DISCARDABLE' );		

		if ( $nPos !== false ) {
			$cLinea	= substr( $cLinea, 12 );
		}
		
//		------------------------------------------------------------------------------------------		
	
		$this->nTop 	= (int) strtok( $cLinea, ",");				
		$this->nLeft	= (int) strtok( "," );
		$this->nWidth	= (int) strtok( "," );
		$this->nHeight	= (int) strtok( "," );				
		
		$cToken	= '';
		
		while( !feof($this->nHandler) and ( $cToken <> '}' ))	{
		
			$cLinea 	= fgets($this->nHandler);
			$cToken 	= trim( strtok( $cLinea, " ") );
			
			//echo '<br>Token: ' . $cToken ;

//			Pelles C pone muchos controles bajo el token CONTROL (say->static, group->BS_GROUPBOX,...)
		
			switch ( $cToken )
			{
			   case 'CONTROL'  			: $this->Read_Control ( $cLinea	)			;	break;			
			   case 'EDITTEXT'			: $this->Read_Get	  ( $cLinea	)			;	break;   
			   case 'LTEXT'				: $this->Read_Say	  ( $cLinea, 'left' )	;	break; 
			   case 'RTEXT'				: $this->Read_Say	  ( $cLinea, 'right' )	;	break; 
			   case 'CTEXT'				: $this->Read_Say	  ( $cLinea, 'center' )	;	break; 
			   case 'PUSHBUTTON'		: $this->Read_Button  ( $cLinea, false)		;	break;
			   case 'CHECKBOX'			: $this->Read_Checkbox( $cLinea )			;	break;
			   case 'COMBOBOX'			: $this->Read_Combobox( $cLinea )			;	break;			   
			   case 'GROUPBOX' 			: $this->Read_Groupbox( $cLinea	)			;	break;			   
			   case 'DEFPUSHBUTTON'		: $this->Read_Button  ( $cLinea, true )		;	break;
			   case 'CAPTION'  			: $this->Read_Caption ( $cLinea	)			;	break;
			   case 'FONT'  			: $this->Read_Font    ( $cLinea	)			;	break;
			   
			   case 'AUTOCHECKBOX'  	: $this->Read_AutoCheckbox( $cLinea )		;	break;	// ResEdit;
			   case 'AUTORADIOBUTTON' 	: $this->Read_AutoRadioButton( $cLinea )	;	break;	// ResEdit;
			}			

		}							
	}	
	
	
	private function Init_Static_Control( $oWnd ){
	
//		Una vez inicializado el Dialogo/Child, miraremos si existe algun control con id == -1,
//		por ejemplo los TSAY. Si existen los creamos.			
		
		$nCtrl = count( $this->aCtrl );
				
		for ( $i = 0; $i < $nCtrl; $i++) {
		
			$cType 		= $this->aCtrl[$i][ 'cType' ];
			$cIdCtrl	= $this->aCtrl[$i][ 'nId'   ];
			
//				echo '<br>Type: ' . $cType . '/ Id:' . $cIdCtrl ;
			
			if ( ( $cType == 'TGROUP' ) and ( $cIdCtrl == '-1' ) ){
			
				$nTop 		= $this->aCtrl[$i][ 'nTop' 		];
				$nLeft 		= $this->aCtrl[$i][ 'nLeft' 	];												
				$nWidth		= $this->aCtrl[$i][ 'nWidth' 	];												
				$nHeight	= $this->aCtrl[$i][ 'nHeight' 	];												
				$cCaption 	= $this->aCtrl[$i][ 'cCaption'	];	
				
				$cCaption 	= utf8_encode( $cCaption );	

				$nTop		= ( $nTop * 14 ) / 8 ;	
				$nLeft		= ( $nLeft * 7 ) / 4 ;	
				$nWidth		= ( $nWidth * 7 ) / 4 ;	
				$nHeight	= ( $nHeight * 14 ) / 8 ;					
			
				$o = new TGroup( $oWnd, '-1' , $nTop, $nLeft, $cCaption, $nWidth, $nHeight );		
				
			}	

			if ( ( $cType == 'TBOX' ) and ( $cIdCtrl == '-1' ) ){
			
				$nTop 		= $this->aCtrl[$i][ 'nTop' 		];
				$nLeft 		= $this->aCtrl[$i][ 'nLeft' 	];												
				$nWidth		= $this->aCtrl[$i][ 'nWidth' 	];												
				$nHeight	= $this->aCtrl[$i][ 'nHeight' 	];												
				$cCaption 	= $this->aCtrl[$i][ 'cCaption'	];	

				$nTop		= ( $nTop * 14 ) / 8 ;	
				$nLeft		= ( $nLeft * 7 ) / 4 ;	
				$nWidth		= ( $nWidth * 7 ) / 4 ;	
				$nHeight	= ( $nHeight * 14 ) / 8 ;					
			
				$o = new TBox  ( $oWnd, '-1', $nTop, $nLeft, $nWidth, $nHeight ); 															
			}				
			
			if ( ( $cType == 'TSAY' ) and ( $cIdCtrl == '-1' ) ){
			
				$nTop 		= $this->aCtrl[$i][ 'nTop' 		];
				$nLeft 		= $this->aCtrl[$i][ 'nLeft' 	];												
				$nWidth		= $this->aCtrl[$i][ 'nWidth' 	];												
				$nHeight	= $this->aCtrl[$i][ 'nHeight' 	];						
				$cCaption 	= $this->aCtrl[$i][ 'cCaption'	];						
				$cAlign 	= $this->aCtrl[$i][ 'cAlign'	];						
				$lBorder 	= $this->aCtrl[$i][ 'lBorder'	];						

				$nTop		= ( $nTop * 14 ) / 8 ;	
				$nLeft		= ( $nLeft * 7 ) / 4 ;	
				$nWidth		= ( $nWidth * 7 ) / 4 ;	
				$nHeight	= ( $nHeight * 14 ) / 8 ;					
			
				$o = new TSay( $oWnd, '-1' , $nTop, $nLeft, $cCaption );		
				
					$o->nWidth  = $nWidth;
					$o->nHeight = $nHeight;
					$o->cAlign 	= $cAlign;
					$o->lBorder	= $lBorder;
			}
			
		}					
	
	}	
	
	private function Read( $cId ) {
	
		$oCtrl = null;
		$nCtrl = count( $this->aCtrl );			
				
		for ( $i = 0; $i < $nCtrl; $i++) {

			$cIdCtrl	= $this->aCtrl[$i][ 'nId' ];
			
			if ( $cIdCtrl == $cId ){
			
				$oCtrl = $this->aCtrl[$i];				
				
				$oCtrl[ 'nTop' 		] = ( $oCtrl[ 'nTop' 	] * 14 ) / 8 ;	
				$oCtrl[ 'nLeft'	 	] = ( $oCtrl[ 'nLeft' 	] * 7 ) / 4 ;		
				$oCtrl[ 'nWidth' 	] = ( $oCtrl[ 'nWidth' 	] * 7 ) / 4 ;	
				$oCtrl[ 'nHeight'	] = ( $oCtrl[ 'nHeight'	] * 14 ) / 8 ;	
				
				break;	
			}			
		}	
		
		if ( gettype( $oCtrl ) == 'NULL' ) {
			MsgError( TWEB_RC_DIALOG . ' ' . $this->cDialog . '<br>' .
					  TWEB_RC_NO_CONTROL . ' ' . $cId  );
			exit();			
		}		
		
		return $oCtrl;
		
	}
	
	private function Read_Caption( $cLinea ){
		
		$nPos 			= strpos( $cLinea, "CAPTION" ) + 8 ;
		
		$cLinea			= trim( substr( $cLinea, $nPos ) );
		
		$cText			= strtok( $cLinea, ",");
		$cText			= substr( $cText, 1 );
		$cText			= substr( $cText, 0, strlen($cText)-1);		
	
		$this->cCaption = utf8_encode( $cText );
	}

	private function Read_Font( $cLinea ){
		
		$nPos 			= strpos( $cLinea, "FONT" ) + 5;

		$cLinea			= substr( $cLinea, $nPos );			

		$nSize			= (int) trim( strtok( $cLinea, ",") ); 
		$cName			= trim( strtok( "," ) ); 
		
		$this->oFont 	= array( 'cFaceName'	=> $cName,
								 'nSize'		=> $nSize );	

	}	

	private function Read_Control( $cLinea ){
		
		$nPos = strpos( $cLinea, "CONTROL" ) + 8;
		
		$cLinea		= substr( $cLinea, $nPos );	
		
		$cText 		= trim( strtok( $cLinea, ",") );
		$cText		= substr( $cText, 1 );
		$cText		= substr( $cText, 0, strlen($cText)-1);		

		$nId 		= trim(strtok( "," )); 		

		$cCtrl		= trim(strtok( ","));
		$cCtrl		= substr( $cCtrl, 0 );
		$cCtrl		= substr( $cCtrl, 0, strlen($cCtrl));		
		$cCtrl		= trim(strtoupper( $cCtrl ));
		
		$cDummy 	= strtok( ",");
		
		switch ( $cCtrl ) {
		
			case 'STATIC' :					
			case '"STATIC"' :
				$cCtrl = 'TSAY';
				break;				
				
			case 'WC_TABCONTROL' :		// ResEdit -> Folder
				$cCtrl = 'TFOLDER';
				break;	
		
			case 'BUTTON' :				
			case '"BUTTON"' :
			
				$nPos = strpos( $cDummy, 'BS_GROUPBOX' );
				
				if ( $nPos !== false ) {
					$cCtrl = 'TGROUP';
				}
					
				break;	

			case '"TBOX"' :
				$cCtrl = 'TBOX';
				break;		

			case "TPROGRESBAR" :
				$cCtrl = 'TBOX';
				break;					
		}
		
		$nLeft		= (int) trim(strtok( "," ));
		$nTop 		= (int) trim(strtok( "," ));
		$nWidth		= (int) trim(strtok( "," ));
		$nHeight	= (int) trim(strtok( "," ));

		$this->aCtrl[] = array( 'cType'		=> $cCtrl,
								'nId'		=> $nId,
								'cCaption'	=> $cText,								
								'nTop'		=> $nTop,
								'nLeft'		=> $nLeft,
								'nWidth'	=> $nWidth,								
								'nHeight'	=> $nHeight,
								'cAlign'	=> 'left',		// Default
								'lBorder'	=> false		// Default
								);
	}		

	private function Read_Get( $cLinea ){	
	
		$nPos 		= strpos( $cLinea, "EDITTEXT" ) + 8;
	
		$cLinea		= substr( $cLinea, $nPos );		
		$nId 		= trim( strtok( $cLinea, ",") ); 
	
		$nLeft		= (int) trim(strtok( "," ));
		$nTop 		= (int) trim(strtok( "," ));
		$nWidth		= (int) trim(strtok( "," ));
		$nHeight	= (int) trim(strtok( "," ));
		$cAlign		= trim(strtok( "," ));			

//		Puede contener cadenas tipo: ES_RIGHT | ES_AUTOHSCROLL

		$nPosCenter	= strpos( $cAlign, 'ES_CENTER' );
		$nPosRight	= strpos( $cAlign, 'ES_RIGHT'  );
	
		if  ( gettype( $nPosCenter ) == 'integer' )  {
		
			$cAlign = 'center';

		} elseif ( gettype( $nPosRight ) == 'integer' ) {
		
			$cAlign = 'right';
	
		}				
		
//		---------------------------------------------------------		
		
		$this->aCtrl[] = array( 'cType'		=> 'TGET',
								'nId'		=> $nId,
								'nTop'		=> $nTop,
								'nLeft'		=> $nLeft,
								'nWidth'	=> $nWidth,								
								'nHeight'	=> $nHeight,
								'cAlign'	=> $cAlign );								
								
	}
	
	private function Read_Say( $cLinea, $cAlign = 'left'){
	
		$cLinea 	= trim( $cLinea );		
		$cLinea		= trim( substr( $cLinea, 5 ) );  // OffSet de LTEXT/CTEXT/RTEXT

		$cLinea		= trim( substr( $cLinea, 1 ) );

		$nCometa 	= strpos( $cLinea, '"' ); 
	
		$cText		= trim( substr( $cLinea, 0, $nCometa ) );
	
		$cText	    = utf8_encode($cText);  	// Tema a acentos, ñ, ...

		$cLinea 	= trim( substr( $cLinea, $nCometa ) );	

		$nComa 		= strpos( $cLinea, ',' ); // Primera , despres del TEXTE
	
		$cDummy 	= trim(strtok( $cLinea, "," ));	
		
		$nId 		= trim(strtok( "," ));
		$nLeft		= (int) trim(strtok( "," ));
		$nTop 		= (int) trim(strtok( "," ));
		$nWidth		= (int) trim(strtok( "," ));
		$nHeight	= (int) trim(strtok( "," ));
		$cStyle		= trim(strtok( "," ));
		$nPos 		= strpos( $cStyle, 'WS_BORDER');
		$lBorder 	= !($nPos === false) ;
	
		$this->aCtrl[] = array( 'cType'		=> 'TSAY',
								'nId'		=> $nId,
								'cCaption'	=> $cText,
								'nTop'		=> $nTop,
								'nLeft'		=> $nLeft,
								'nWidth'	=> $nWidth,								
								'nHeight'	=> $nHeight,
								'cAlign'	=> $cAlign,
								'lBorder'	=> $lBorder
								);														
	}

	private function Read_Groupbox( $cLinea ){
	
		$nPos 		= strpos( $cLinea, "GROUPBOX" ) + 9;
		
		$cLinea		= trim( substr( $cLinea, $nPos ) );

		$cText		= strtok( $cLinea, ",");
		$cText		= substr( $cText, 1 );
		$cText		= substr( $cText, 0, strlen($cText)-1);	

		$nId 		= trim(strtok( "," ));  
	
		$nLeft		= (int) trim(strtok( "," ));
		$nTop 		= (int) trim(strtok( "," ));
		$nWidth		= (int) trim(strtok( "," ));
		$nHeight	= (int) trim(strtok( "," ));
		
		$this->aCtrl[] = array( 'cType'		=> 'TGROUP',
								'nId'		=> $nId,
								'cCaption'	=> $cText,
								'nTop'		=> $nTop,
								'nLeft'		=> $nLeft,
								'nWidth'	=> $nWidth,								
								'nHeight'	=> $nHeight );
	}

	private function Read_Button( $cLinea, $lDefault = false ){
		
		if ( $lDefault == false )
			$nPos = strpos( $cLinea, "PUSHBUTTON" ) + 11;
		  else	
			$nPos = strpos( $cLinea, "DEFPUSHBUTTON" ) + 14;

		$cLinea		= substr( $cLinea, $nPos );
		
		$cText		= trim(strtok( $cLinea, ","));
		$cText		= substr( $cText, 1 );
		$cText		= substr( $cText, 0, strlen($cText)-1);	
		$cText		= utf8_encode($cText); 	// Tema a acentos, ñ, ...		

		
		$nId 		= trim(strtok( "," ));        
		$nLeft		= (int) trim(strtok( "," ));
		$nTop 		= (int) trim(strtok( "," ));
		$nWidth		= (int) trim(strtok( "," ));
		$nHeight	= (int) trim(strtok( "," ));
		
		$this->aCtrl[] = array( 'cType'		=> 'TBUTTON',
								'nId'		=> $nId,
								'cCaption'	=> $cText,								
								'nTop'		=> $nTop,
								'nLeft'		=> $nLeft,
								'nWidth'	=> $nWidth,								
								'nHeight'	=> $nHeight );
	}

	private function Read_Checkbox( $cLinea ){		

		$nPos 		= strpos( $cLinea, "CHECKBOX" ) + 9;

		$cLinea		= substr( $cLinea, $nPos );
		
		$cText		= strtok( $cLinea, ",");
		$cText		= substr( $cText, 1 );
		$cText		= substr( $cText, 0, strlen($cText)-1);
		$cText		= utf8_encode($cText); 	// Tema a acentos, ñ, ...


		$nId 		= trim(strtok( "," ));        
		$nLeft		= (int) trim(strtok( "," ));
		$nTop 		= (int) trim(strtok( "," ));
		$nWidth		= (int) trim(strtok( "," ));
		$nHeight	= (int) trim(strtok( "," ));
		
		$this->aCtrl[] = array( 'cType'		=> 'TCHECKBOX',
								'nId'		=> $nId,
								'cCaption'	=> $cText,								
								'nTop'		=> $nTop,
								'nLeft'		=> $nLeft,
								'nWidth'	=> $nWidth,								
								'nHeight'	=> $nHeight );
	}
	
	private function Read_Combobox( $cLinea ){	
	
		$nPos 		= strpos( $cLinea, "COMBOBOX" ) + 9;
	
		$cLinea		= substr( $cLinea, $nPos );
		
		$nId 		= trim( strtok( $cLinea, ",") ); 
		$nLeft		= (int) trim(strtok( "," ));
		$nTop 		= (int) trim(strtok( "," ));
		$nWidth		= (int) trim(strtok( "," ));
		$nHeight	= (int) trim(strtok( "," ));
		
		$this->aCtrl[] = array( 'cType'		=> 'TCOMBOBOX',
								'nId'		=> $nId,
								'nTop'		=> $nTop,
								'nLeft'		=> $nLeft,
								'nWidth'	=> $nWidth,								
								'nHeight'	=> $nHeight );
	}	

//	Version ResEdit !
	
	private function Read_AutoCheckbox( $cLinea ){		

		$nPos 		= strpos( $cLinea, "AUTOCHECKBOX" ) + 13;

		$cLinea		= trim( substr( $cLinea, $nPos ));
		
		$cText		= strtok( $cLinea, ",");
		$cText		= substr( $cText, 1 );
		$cText		= substr( $cText, 0, strlen($cText)-1);
		$cText		= utf8_encode($cText); 	// Tema a acentos, ñ, ...
		
		$nId 		= trim(strtok( "," ));        
		$nLeft		= (int) trim(strtok( "," ));
		$nTop 		= (int) trim(strtok( "," ));
		$nWidth		= (int) trim(strtok( "," ));
		$nHeight	= (int) trim(strtok( "," ));
		
		$this->aCtrl[] = array( 'cType'		=> 'TCHECKBOX',
								'nId'		=> $nId,
								'cCaption'	=> $cText,								
								'nTop'		=> $nTop,
								'nLeft'		=> $nLeft,
								'nWidth'	=> $nWidth,								
								'nHeight'	=> $nHeight );							
	}

	private function Read_AutoRadioButton( $cLinea ){		

		$nPos 		= strpos( $cLinea, "AUTORADIOBUTTON" ) + 16;

		$cLinea		= trim( substr( $cLinea, $nPos ) );
		
		$cText		= strtok( $cLinea, ",");
		$cText		= substr( $cText, 1 );
		$cText		= substr( $cText, 0, strlen($cText)-1);
		$cText		= utf8_encode($cText); 	// Tema a acentos, ñ, ...		
		
		$nId 		= trim(strtok( "," ));        
		$nLeft		= (int) trim(strtok( "," ));
		$nTop 		= (int) trim(strtok( "," ));
		$nWidth		= (int) trim(strtok( "," ));
		$nHeight	= (int) trim(strtok( "," ));
		
		$this->aCtrl[] = array( 'cType'		=> 'TRADIO',
								'nId'		=> $nId,
								'cCaption'	=> $cText,								
								'nTop'		=> $nTop,
								'nLeft'		=> $nLeft,
								'nWidth'	=> $nWidth,								
								'nHeight'	=> $nHeight );
	}	
	
	//	Controles Públicos...		    
		
	public function TWindow( $cId = null, $nTop = 0, $nLeft = 0, $nWidth = null, $nHeight = null, $cColor = CLR_HGRAY   ) {	
	
		if ( gettype( $nWidth ) == 'NULL' )	
			$nWidth		= ( $this->nWidth * 7 ) / 4 ;
			
		if ( gettype( $nHeight ) == 'NULL' )			
			$nHeight	= ( $this->nHeight * 14 ) / 8 ;	
	
		$oWnd = new TWindow( $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor );
		
		$this->Init_Static_Control(	$oWnd );	
		
		return $oWnd;	
	}
	
	public function TDialog( $cId = null, $cTitle = '', $cColor = CLR_HGRAY  ) {
	
		$cTitle = ! empty( $cTitle ) ? $cTitle : $this->cCaption;			
	
		if ( $this->lFound ) {		

			$nWidth		= ( $this->nWidth * 7 ) / 4 ;
			$nHeight	= ( $this->nHeight * 14 ) / 8 ;	
			
			$nHeight 	= $nHeight + 35;	//	35 = Offset title bar...
	
			$oWnd = new TDialog( $cId, $nWidth, $nHeight, $cColor );
			
			$oWnd->SetData( 'data-title', $cTitle );
			
			$this->Init_Static_Control(	$oWnd );			
		} 		
		
		return $oWnd;	
	}
	
	//	DialogChild es el diàlogo que leemos desde el RC y que va en un Panel
	//	Esto significa que tendran unas dimensiones de 0,0,100%,100%
	
	//public function TDialogChild( $cId = null, $cColor = CLR_HGRAY  ) {
	public function TPanel( $cId = null, $cColor = CLR_HGRAY  ) {
	
		if ( $this->lFound ) {
			/*
			$nLeft		= ( $this->nLeft * 7 ) / 4 ;
			$nWidth		= ( $this->nWidth * 7 ) / 4 ;
			$nHeight	= ( $this->nHeight * 14 ) / 8 ;
			$nTop		= ( $this->nTop * 14 ) / 8 ;						

			$oWnd = new TPanel( null, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor );
			*/
			$oWnd = new TPanel( null, $cId, 0, 0, '100%', '100%', $cColor );
				// Se habria de mirar que cargase una fuente por defecto...

				//$oWnd->oFont 	= new TFont( 'Verdana', 12, true );				
				//$oWnd->oFontGet	= new TFont( 'Verdana', 12, false, true );
				//$oWnd->rc_width =  $nWidth ;
				//$oWnd->rc_height = $nHeight;			

			$this->Init_Static_Control(	$oWnd );

			//$oWnd->SetCoors( 0, 0, '100%', '100%' );		

/*
//			Key Enter jump next field input	

			$cFunction  = '$(document).ready(function() {' ;
			
			$cFunction .= '      FWeb_EvalWhen();';
			
//			$cFunction .= '     $( document ).tooltip();' ;		// Activamos sistema Tootltip		
	
			$cFunction .= '}); ' ;
			
			ExeJS( $cFunction );				
			*/
			
			return $oWnd;		
			
		} else {			
		
			$oWnd = new TPanel( null, $cId, 0, 0, '100%', '100%' );

			// $oWnd->SetCoors( 0, 0, '100%', '100%' );				
			
			return $oWnd;							
		}
	}

	public function TTabs( $cId = null, $cColor = CLR_HGRAY  ) {

		$oWnd = new TTabs( $cId, $cColor );
			//$oWnd->nHeight = '600px';
		
			$this->Init_Static_Control(	$oWnd );				
					
		return $oWnd;						

	}		
	

	public function TGet( $oWnd = null, $cId = '', $uValue = '', $cPicture = '', $bChange = '', $cClass = '' ) {	

		if ( gettype( $oWnd ) <> 'object' )
			return false;	
			
		$aCtrl = $this->Read( $cId );
				
		$nTop 		= $aCtrl[ 'nTop' 		];
		$nLeft 		= $aCtrl[ 'nLeft'	 	];		
		$nWidth		= $aCtrl[ 'nWidth' 		];
		$nHeight	= $aCtrl[ 'nHeight'	 	];	
		$cAlign		= $aCtrl[ 'cAlign'	 	];				
		
		$o = new TGet( $oWnd , $cId, $nTop, $nLeft, $uValue );
		
			$o->nWidth 	= $nWidth;
			$o->nHeight	= $nHeight;
			$o->cAlign	= $cAlign;					
			$o->SetPicture( $cPicture );
			$o->SetChange ( $bChange );
			$o->SetClass( $cClass );
		
		return $o;							
	}

	// Adicion AV parametro $cClass  
	public function TSay( $oWnd = null, $cId = null, $uValue = null , $oFont = null, $cClass = '' ) {	

		if ( gettype( $oWnd ) <> 'object' )
			return false;	
			
		$aCtrl = $this->Read( $cId );
				
		$cCaption	= $aCtrl[ 'cCaption'	];
		$nTop 		= $aCtrl[ 'nTop' 		];
		$nLeft 		= $aCtrl[ 'nLeft'	 	];		
		$nWidth		= $aCtrl[ 'nWidth' 		];
		$nHeight	= $aCtrl[ 'nHeight'	 	];				
		$cAlign		= $aCtrl[ 'cAlign'	 	];		
		$lBorder	= $aCtrl[ 'lBorder'	 	];			

//			$uValue 	= empty( $uValue ) ? $cCaption : $uValue ;
		$uValue 	= ( gettype( $uValue ) == 'NULL' ) ? $cCaption : $uValue ;
		
		$o = new TSay( $oWnd, $cId, $nTop, $nLeft, $uValue );
	
			$o->nWidth 	= $nWidth;
			$o->nHeight	= $nHeight;
			$o->cAlign	= $cAlign;				
			$o->lBorder	= $lBorder;	
			$o->SetFont( $oFont );
			$o->SetClass( $cClass );
		
		return $o;							
	}
	
	public function TGroup( $oWnd = null, $cId = '', $uValue = null ) {	

		if ( gettype( $oWnd ) <> 'object' )
			return false;	
			
		$aCtrl = $this->Read( $cId );
				
		$cCaption	= $aCtrl[ 'cCaption'	];
		$nTop 		= $aCtrl[ 'nTop' 		];
		$nLeft 		= $aCtrl[ 'nLeft'	 	];		
		$nWidth		= $aCtrl[ 'nWidth' 		];
		$nHeight	= $aCtrl[ 'nHeight'	 	];				

//		$uValue 	= ( gettype( $uValue ) == 'NULL' ) ? utf8_encode( $cCaption ): $uValue ;
		$uValue 	= ( gettype( $uValue ) == 'NULL' ) ? $cCaption : $uValue ;
					
		$o = new TGroup( $oWnd, $cId, $nTop, $nLeft, $uValue, $nWidth, $nHeight );
		
		return $o;							
	}
	
	public function TButton( $oWnd = null, $cId = '', $cCaption = '', $bClick = '', $cImage = '' ) {	

		if ( gettype( $oWnd ) <> 'object' )
			return false;	
			
		$aCtrl = $this->Read( $cId );
	
		$nTop 		= $aCtrl[ 'nTop' 		];
		$nLeft 		= $aCtrl[ 'nLeft'	 	];		
		$nWidth		= $aCtrl[ 'nWidth' 		];
		$nHeight	= $aCtrl[ 'nHeight'	 	];				
		$cCaption 	= ( empty( $cCaption ) || gettype( $cCaption ) == 'NULL' ) ? $aCtrl[ 'cCaption'	] : $cCaption ;
	
		$o = new TButton( $oWnd , $cId, $nTop, $nLeft, $cCaption, $bClick );
		
			$o->nWidth 	= $nWidth;
			$o->nHeight	= $nHeight;
			$o->cImage	= $cImage;
		
		return $o;							
	}
	
	public function TButtonFiles( $oWnd = null, $cId = '', $cCaption = '', $bClick = '', $cImage = '', $lMultiple = true ) {	

		if ( gettype( $oWnd ) <> 'object' )
			return false;	
			
		$aCtrl = $this->Read( $cId );
	
		$nTop 		= $aCtrl[ 'nTop' 		];
		$nLeft 		= $aCtrl[ 'nLeft'	 	];		
		$nWidth		= $aCtrl[ 'nWidth' 		];
		$nHeight	= $aCtrl[ 'nHeight'	 	];				
		$cCaption 	= ( empty( $cCaption ) || gettype( $cCaption ) == 'NULL' ) ? $aCtrl[ 'cCaption'	] : $cCaption ;
	
		$o = new TButton( $oWnd , $cId, $nTop, $nLeft, $cCaption, $bClick );
		
			$o->nWidth 		= $nWidth;
			$o->nHeight		= $nHeight;
			$o->cImage		= $cImage;
			$o->lFiles 		= true;		
			$o->lMultiple	= $lMultiple;	
		
		return $o;							
	}	
	
	public function TCheckbox( $oWnd = null, $cId = '', $uValue = '', $cCaption = null,  $bAction = null ) {	

		if ( gettype( $oWnd ) <> 'object' )
			return false;	
			
		$aCtrl = $this->Read( $cId );
				
		$cCaption	= ( empty( $cCaption ) || gettype( $cCaption ) == 'NULL' ) ? $aCtrl[ 'cCaption'	] : $cCaption ;			
		$nTop 		= $aCtrl[ 'nTop' 		];
		$nLeft 		= $aCtrl[ 'nLeft'	 	];		
		$nWidth		= $aCtrl[ 'nWidth' 		];
		$nHeight	= $aCtrl[ 'nHeight'	 	];							

		$o = new TCheckbox( $oWnd , $cId, $nTop, $nLeft, $uValue );
			$o->bAction 	= $bAction ;
			$o->cCaption	= $cCaption;
			$o->nWidth 		= $nWidth;
			$o->nHeight		= $nHeight;
		
		return $o;							
	}
	
	public function TCombobox( $oWnd = null, $cId = '', $aKey = array(), $aTxt = array(), $uValue = '' ) {	

		if ( gettype( $oWnd ) <> 'object' )
			return false;	
			
		$aCtrl = $this->Read( $cId );

		$nTop 		= $aCtrl[ 'nTop' 		];
		$nLeft 		= $aCtrl[ 'nLeft'	 	];		
		$nWidth		= $aCtrl[ 'nWidth' 	];
		$nHeight	= $aCtrl[ 'nHeight'	];							

		$o = new TCombobox( $oWnd, $cId, $nTop, $nLeft, $aKey, $aTxt, $uValue );		
		
			$o->nWidth 	= $nWidth;
			$o->nHeight	= $nHeight;

		return $o;							
	}

	public function TSelectPicker( $oWnd = null, $cId = '', $aData = [] ) {	

		if ( gettype( $oWnd ) <> 'object' ) {
			return false;	
		}	
			
		$aCtrl = $this->Read( $cId );
				
		$nTop 		= $aCtrl[ 'nTop' 		];
		$nLeft 		= $aCtrl[ 'nLeft'	 	];		
		$nWidth		= $aCtrl[ 'nWidth' 	];
		$nHeight	= $aCtrl[ 'nHeight'	];	
		
		$o = new TSelectPicker( $oWnd, $cId, $nTop, $nLeft, $nWidth, $aData );
		$o->nWidth 	= $nWidth;
		$o->nHeight	= $nHeight;

		return $o;							
	}
	
	public function TRadio( $oWnd = null, $cId = '', $aIds = array(), $aItemsText = array(),  $nOption = 0, $bChange = '' ) {	

		if ( gettype( $oWnd ) <> 'object' )
			return false;	

		$oRadio 	= new TRadio( $oWnd, $cId, $nOption, $bChange );
		
		$nTotalId 	= count( $aIds ); 

		for ( $i = 0; $i < $nTotalId; $i++) {						
		
			$aCtrl = $this->Read( $aIds[$i] );
			
			if ( gettype( $aCtrl ) == 'array' ) {
	
				$cCaption	= $aCtrl[ 'cCaption'	];
				$nTop 		= $aCtrl[ 'nTop' 		];
				$nLeft 		= $aCtrl[ 'nLeft'	 	];	
			
				
				if 	( count( $aItemsText ) == $nTotalId ) {
					$cCaption = $aItemsText[$i];				
				}
			
				$oRadio->AddItem( $cCaption, $nTop, $nLeft );				
			}											
		}
		
		return $oRadio;							
	}

	public function TImage( $oWnd = null, $cId = '', $cFile = '', $cFileZoom = '', $cTitle = '') {	

		if ( gettype( $oWnd ) <> 'object' )
			return false;	
			
		$aCtrl = $this->Read( $cId );
				
		$nTop 		= $aCtrl[ 'nTop' 		];
		$nLeft 		= $aCtrl[ 'nLeft'	 	];		
		$nWidth		= $aCtrl[ 'nWidth' 		];
		$nHeight	= $aCtrl[ 'nHeight'	 	];							

		$o = new TImage ( $oWnd, $cId, $nTop, $nLeft, $cFile, $nWidth, $nHeight, $cFileZoom ); 
		$o->cTitle 	= $cTitle ;				
			
		
		return $o;							
	}
	
	public function TProgressBar( $oWnd = null, $cId = '', $nValue = 0 ) {	

		if ( gettype( $oWnd ) <> 'object' )
			return false;	
			
		$aCtrl = $this->Read( $cId );
				
		$nTop 		= $aCtrl[ 'nTop' 		];
		$nLeft 		= $aCtrl[ 'nLeft'	 	];		
		$nWidth		= $aCtrl[ 'nWidth' 		];
		$nHeight	= $aCtrl[ 'nHeight'	 	];							

		$o = new TProgressBar( $oWnd, $cId, $nTop, $nLeft, $nValue, $nWidth, $nHeight ); 
		
		return $o;							
	}
	
	public function TCalendar( $oWnd = null, $cId = '', $nValue = 0 ) {	

		if ( gettype( $oWnd ) <> 'object' )
			return false;	
			
		$aCtrl = $this->Read( $cId );
				
		$nTop 		= $aCtrl[ 'nTop' 		];
		$nLeft 		= $aCtrl[ 'nLeft'	 	];		
		$nWidth		= $aCtrl[ 'nWidth' 		];
		$nHeight	= $aCtrl[ 'nHeight'	 	];							

		$o = new TCalendar( $oWnd, $cId, $nTop, $nLeft, $nValue, $nWidth, $nHeight ); 
		
		return $o;							
	}	

	public function TMaps( $oWnd = null, $cId = '' ) {	

			if ( gettype( $oWnd ) <> 'object' )
				return false;	
				
			$aCtrl = $this->Read( $cId );
					
			$nTop 		= $aCtrl[ 'nTop' 		];
			$nLeft 		= $aCtrl[ 'nLeft'	 	];		
			$nWidth		= $aCtrl[ 'nWidth' 		];
			$nHeight	= $aCtrl[ 'nHeight'	 	];							
	
			$o = new TMaps( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight ); 			
			
			return $o;							
	}

	public function TYouTube( $oWnd = null, $cId = '', $cSource = '' ) {	

			if ( gettype( $oWnd ) <> 'object' )
				return false;	
				
			$aCtrl = $this->Read( $cId );
					
			$nTop 		= $aCtrl[ 'nTop' 		];
			$nLeft 		= $aCtrl[ 'nLeft'	 	];		
			$nWidth		= $aCtrl[ 'nWidth' 		];
			$nHeight	= $aCtrl[ 'nHeight'	 	];							
	
			$o = new TYouTube( $oWnd, $cId, $nTop, $nLeft, $nWidth, $cSource, $nHeight, null ); 			
			
			return $o;							
	}
	
	public function TTree( $oWnd = null, $cId = '', $aData = null ) {	

			if ( gettype( $oWnd ) <> 'object' )
				return false;	
				
			$aCtrl = $this->Read( $cId );
			
			$nTop 		= $aCtrl[ 'nTop' 		];
			$nLeft 		= $aCtrl[ 'nLeft'	 	];		
			$nWidth		= $aCtrl[ 'nWidth' 		];
			$nHeight	= $aCtrl[ 'nHeight'	 	];							
	
			$o = new TTree( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, null, $aData ); 			
			
			return $o;							
	}
	
	public function TFolder( $oWnd = null, $cId = '' ) {	

			if ( gettype( $oWnd ) <> 'object' )
				return false;	
				
			$aCtrl = $this->Read( $cId );
			
			$nTop 		= $aCtrl[ 'nTop' 		];
			$nLeft 		= $aCtrl[ 'nLeft'	 	];		
			$nWidth		= $aCtrl[ 'nWidth' 		];
			$nHeight	= $aCtrl[ 'nHeight'	 	];							
	
			$o = new TFolder( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight ); 			
			
			return $o;							
	}	

	public function TBar( $oWnd = null, $cId = '' ) {	

		if ( gettype( $oWnd ) <> 'object' )
			return false;	
			
		$aCtrl = $this->Read( $cId );
		
		$nTop 		= $aCtrl[ 'nTop' 		];
		$nLeft 		= $aCtrl[ 'nLeft'	 	];		
		$nWidth		= $aCtrl[ 'nWidth' 		];
		$nHeight	= $aCtrl[ 'nHeight'	 	];							

		$o = new TBar( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight ); 
		
		return $o;							
	}
	
	public function TGrid( $oWnd = null, $cId = '', $aData = array() ) {	

		if ( gettype( $oWnd ) <> 'object' )
			return false;	
			
		$aCtrl = $this->Read( $cId );
		
		$nTop 		= $aCtrl[ 'nTop' 		];
		$nLeft 		= $aCtrl[ 'nLeft'	 	];		
		$nWidth		= $aCtrl[ 'nWidth' 		];
		$nHeight	= $aCtrl[ 'nHeight'	 	];							

		$o = new TGrid( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $aData ); 
		
		return $o;							
	}	
	
	public function TFrame( $oWnd = null, $cId = '', $cSrc = '' ) {	

		if ( gettype( $oWnd ) <> 'object' )
			return false;	
			
		$aCtrl = $this->Read( $cId );
		
		$nTop 		= $aCtrl[ 'nTop' 		];
		$nLeft 		= $aCtrl[ 'nLeft'	 	];		
		$nWidth		= $aCtrl[ 'nWidth' 		];
		$nHeight	= $aCtrl[ 'nHeight'	 	];							

		$o = new TFrame( $oWnd, $cId, $nTop, $nLeft, $cSrc, $nWidth, $nHeight ); 
		
		return $o;							
	}	
	
	
}