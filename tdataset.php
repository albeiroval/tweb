<?php
include_once( 'core.tools.php' );
include_once( 'tdatabase.php' );

class TDataset extends TDatabase {

	private $aFields 		= array();
	public  $cTable 		= '';
	public  $cPK 			= '';
	public  $lAutoincrement	= true;
	private $lFound 		= false;
	public  $lTrigger 		= true;
	public  $lLog 			= false;
	public  $bError			= null;

	function __construct() { }

	public function Init( $cServer, $cUser, $cPsw, $cDatabase, $nPort = null ){

		parent::__construct( $cServer, $cUser, $cPsw, $cDatabase, $nPort );
	}

	public function __set( $cField, $uValue ) {

		if ( array_key_exists( $cField, $this->aFields ) ) {

			$this->aFields[ $cField ][ 'value'  ] = $uValue;
			$this->aFields[ $cField ][ 'edited' ] = true;

		} else {

			if ( $this->lTrigger ) {

				$cMsg = TWEB_DS_FIELD_WRONG . $cField;

				if ( gettype( $this->bError ) == 'string' && function_exists( $this->bError ) ) {
					call_user_func( $this->bError, $cMsg );
				} else {
					$this->ShowError( $cMsg );
				}
			}
		}
	}

	public function __get( $cField ) {

		if ( array_key_exists( $cField, $this->aFields ) ) {
			return $this->aFields[ $cField ][ 'value' ] ;
		} else {
			if ( $this->lTrigger ) {

				$cMsg = TWEB_DS_FIELD_WRONG . $cField;

				if ( gettype( $this->bError ) == 'string' && function_exists( $this->bError ) ) {
					call_user_func( $this->bError, $cMsg );
				} else {
					$this->ShowError( $cMsg );
				}
			}
		}
	}

	public function SetError( $bError ) { $this->bError = $bError; }


	public function info() { return $this->aFields; }

	public function TField( $uField, $uValue = '' ) {

		$cTypeField = gettype( $uField );

		switch ( $cTypeField ) {

			case 'string':

				$this->aFields[ $uField ] = array(  'value' => $uValue,
													'name' 	=> $uField,
													'default' => '',
													'type'	=> '' ,
													'edited' => false );

				break;

			case 'array':

				if( array_key_exists( 'field', $uField ) ){

					$cField 	= $uField[ 'field' ];

					$cName 		= array_key_exists( 'name'   , $uField ) ? $uField[ 'name'    ] : $uField[ 'field' ] ;
					$cDefault 	= array_key_exists( 'default', $uField ) ? $uField[ 'default' ] : '';
					$cType 		= array_key_exists( 'type'   , $uField ) ? $uField[ 'type'    ] : '';

					$this->aFields[ $cField ] = array(  'value' 	=> $cField,
														'name' 		=> $cName,
														'default'	=> $cDefault,
														'type'		=> $cType,
														'edited'	=> false
													);
				}

				break;
		}

	}

	public function GetId( $uId ) {


		$cSql 	= 'SELECT * FROM ' . $this->cTable . ' WHERE ' . $this->cPK . ' = "' . $uId . '" LIMIT 1';

		$this->Query( $cSql );

		$this->lFound = ( $this->RecCount() > 0 ) ? true : false;


		if ( $this->lFound ) {
			$this->Load();
		} else {
			$this->Blank();
		}

		return $this->lFound;
	}

	public function GetTotal() {

		$cSql 	= 'SELECT count(*) as total FROM ' . $this->cTable . ' LIMIT 1';

		$this->Open( $cSql );

		return $this->total;
	}

	public function GetTable() {

		$cSql 	= 'SELECT * FROM ' . $this->cTable ;

		$this->Query( $cSql );

		return $this->GetAll();
	}

	private function GetFields() {

		$nFields	= count( $this->aFields );
		$cFields 	= '';
		$nIdx		= 1;

		foreach( $this->aFields as $key => $value ) {

			$cFields .= $key;

			if ( $nIdx < $nFields )
				$cFields .= ', ';

			$nIdx++;
		}

		return $cFields;
	}

	public function Search( $aFilter, $cOperator = '', $uValue = '', $nMax = '' ) {

		$cFields 		= $this->GetFields();
		$lMultiSearch 	= false;

		if ( gettype( $aFilter ) == 'array' ) {

			$nFilter = count( $aFilter );

			if ( $nFilter == 0 ) {
				return false;
			}

			$lMultiSearch = true;

			$cLogic = '';

			$cSql 	= 'SELECT ' . $cFields . ' FROM ' . $this->cTable . ' WHERE ' ;

			for ($i = 0; $i < $nFilter; $i++) {

				if ( $i > 0 ) {
					if ( array_key_exists( 'logic',  $aFilter[$i] ) ) {
						$cLogic = ' ' . $aFilter[$i][ 'logic' ] . ' ' ;
					} else {
						$cLogic = ' AND ';
					}
				} else {
					$cLogic = '';
				}

				$cOp = $aFilter[$i][ 'op' ] ;

				$cSql .= $cLogic  ;
				$cSql .= $aFilter[$i][ 'field' ] . ' ' . $cOp . ' '  ;

				$uValue = $aFilter[$i][ 'value' ] ;

				if ( $cOp == 'LIKE' ) {

					$cSql .= '"%' . $uValue . '%" ';

				} else {

					$cSql .= '"' . $uValue . '" ';

				}

			}

			//	Si usamos multiples searchs, podemos usar el 2 parámetro (cOperator) como order

			if ( !empty( $cOperator ) ) {
				$cSql .= ' ORDER BY ' . $cOperator . ' ' ;
			}

		} else {

			$cField 	= $aFilter;

			$cSql  	 = 'SELECT ' .$cFields . ' FROM ' . $this->cTable . ' WHERE ' ;
			$cSql 	.= $cField . ' ' . $cOperator ;

			if ( $cOperator == 'LIKE' ) {

				$cSql .= '"%' . $uValue . '%" ';

			} else {

				$cSql .= '"' . $uValue . '" ';
			}

			$cSql .= ' ORDER BY ' . $cField . ' ' ;


		}

		if ( $nMax !== '' )
			$cSql .= ' LIMIT ' . $nMax ;


		if ( $this->lLog ) _Log( 'Search SQL: ' . $cSql );

        //var_dump($cSql);
		$this->Query( $cSql );

		return  (( $this->RecCount() > 0 ) ? true : false );

	}

/*

	SI $aFilter es de tipo array tendra esta estrcutura:

		$aFilter = array ( ;
							array ( 'field' => 'name', 'value' = 'Pep', 'op' => 'LIKE' ),
							array ( 'field' => 'estat', 'value' = 'A', 'op' => '=', logic = 'AND' ),
							array ( 'field' => 'id_carrer', 'value' = '123', 'op' => '=', logic = 'AND' )
						 );

	SINO sera considerado como el nombre de un campo

*/
	public function BuildWhere( $aFilter, $cOperator = '', $uValue = '' ) {

		if ( gettype( $aFilter ) == 'array' ) {

			$nFilter = count( $aFilter );

			if ( $nFilter == 0 ) {
				return '';
			}

			$cLogic = '';

			$cSql 	= '' ;

			for ($i = 0; $i < $nFilter; $i++) {

				if ( $i > 0 ) {
					if ( array_key_exists( 'logic',  $aFilter[$i] ) ) {
						$cLogic = ' ' . $aFilter[$i][ 'logic' ] . ' ' ;
					} else {
						$cLogic = ' AND ';
					}
				} else {
					$cLogic = '';
				}

				$cOp = $aFilter[$i][ 'op' ] ;

				$cSql .= $cLogic  ;
				$cSql .= $aFilter[$i][ 'field' ] . ' ' . $cOp . ' '  ;

				$uValue = $aFilter[$i][ 'value' ] ;

				if ( $cOp == 'LIKE' ) {

					$cSql .= '"%' . $uValue . '%" ';

				} else {

					$cSql .= '"' . $uValue . '" ';

				}
			}

		} else {

			$cSql 	= $cField . ' ' . $cOperator ;

			if ( $cOperator == 'LIKE' ) {

				$cSql .= '"%' . $uValue . '%" ';

			} else {

				$cSql .= '"' . $uValue . '" ';
			}

		}

		return $cSql;

	}

	public function Skip() { return $this->Load(); }

	public function Load() {

		$aReg 	= $this->Fetch();
		$aItem	= array();

		if ( $aReg ) {

			$this->lFound = true;

//			$aReg 		= array_map('utf8_encode', $aReg );

//			$this->SetRecord( $aReg );

			foreach( $aReg as $key => $value ) {

				//	Transformació si toca...

					if ( array_key_exists( $key, $this->aFields ) ) {

						$aReg = $this->aFields[ $key ];

						if ( array_key_exists( 'type', $aReg ) ){

							if ( $aReg[ 'type' ] !== '' ){

								switch ( $aReg[ 'type' ] ) {

									case 'D':

										$value = DTOC( $value );

										break;

									case 'L':

										$value = ( $value == 1 ) ? true : false;

										break;
								}
							}
						}
					}

				//	--------------------------

				//$this->__Set( $key, $value );	// false x q son valors originals NO EDITATS

				$this->aFields[ $key ][ 'value' ] = $value;
				$this->aFields[ $key ][ 'edited'] = false;

				$aItem[ $key ] = $value ;

			}

		} else {

			$this->lFound 	= false;
			$this->Blank();
		}

		if ( $this->lFound ) {

			$this->SetRecord( $aItem );
		}

		return $this->lFound;
	}


	public function GetAll(){

		$aData = array();

		while ( $this->Fetch() ){

			$aReg = $this->GetRecord();

			foreach( $aReg as $key => $value ) {

				//	Transformació si toca...

					if ( array_key_exists( $key, $this->aFields ) ) {

						$aField = $this->aFields[ $key ];

						if ( array_key_exists( 'type', $aField ) ){

							if ( $aField[ 'type' ] !== '' ){

								switch ( $aField[ 'type' ] ) {

									case 'D':

										$aReg[ $key ] = DTOC( $value );

										break;

									case 'L':

										$aReg[ $key ] = ( $value == 1 ) ? true : false;

										break;
								}
							}
						}
					}

				//	--------------------------

			}

			$aData[] = $aReg;
		}

		return $aData;
	}


	public function Blank() {

		$this->SetRecord();

		foreach( $this->aFields as $key => $value ) {

			$this->aFields[ $key ][ 'value' ] =  $this->aFields[ $key ][ 'default' ];
		}
	}

	public function Record() {

		$aReg = array();

		foreach( $this->aFields as $key => $aData ) {

			$aReg[ $key ] = $aData[ 'value' ];
		}

		return $aReg;
	}

	public function Update() { return $this->Save( false ); }
	public function Insert() { return $this->Save( true ); }

	//	Si lAll forcem a desar TOTS els fitxers independentment de si s'han modificat

	public function Save( $lInsert = null, $lAll = false ) {

		//	Si no especifico /insertar/Modificar, mirare si existeix el registre...

		if ( gettype( $lInsert ) !== 'boolean' ) {

			$uValue = $this->aFields[ $this->cPK ][ 'value' ];

			$cSql 	= 'SELECT ' . $this->cPK . ' FROM ' . $this->cTable . ' WHERE ' . $this->cPK . ' = "' . $uValue . '" LIMIT 1';

			$this->Query( $cSql );

			$lFound = ( $this->RecCount() > 0 ) ? true : false;

			$lInsert = ( $lFound ) ? false : true;
		}

		$nEdited	= 0;


		if ( $lInsert ) {

			$cSql 		= 'INSERT INTO ' . $this->cTable . ' ( ';

			$nFields 	= count( $this->aFields );
			$lFirst		= true;

			foreach( $this->aFields as $key => $aData ) {

				if ( $aData[ 'edited' ] || ( $key == $this->cPK  ) || $lAll ) {

					if ( $lFirst ) {
						$lFirst = false;
					} else {
						$cSql .= ', ' ;
					}

					$cSql .= $key;

				}

			}

			$cSql .= ') VALUES ( ';

			$cValuePK 	= '';
			$lFirst		= true;

			foreach( $this->aFields as $key => $aData ) {

				if ( $aData[ 'edited' ]  || ( $key == $this->cPK  ) || $lAll ) {

					$uValue =  addslashes( $aData[ 'value' ] ) ;		// Convertimos las " en \"

					switch ( $aData[ 'type' ] ) {

						case 'D';
							$uValue = CTOD( $uValue );   //RDC
							break;

					}

					if ( $lFirst ) {
						$lFirst = false;
					} else {
						$cSql .= ', ' ;
					}

					if ( $key == $this->cPK ) {

						if ( $this->lAutoincrement ) {

							if ( gettype( $uValue ) == 'integer'  ) {
								$cSql .=  '"' . $uValue . '" ';
							} else {
								$cSql .= ' NULL ' ;
							}

						} else {
							$cSql .=  '"' . $uValue . '" ';
						}

					} else {
						$cSql .=  '"' . $uValue . '" ';
					}

					$nEdited++;
				}

			}

			$cSql .= ') ';

		} else {

			$cSql 		= 'UPDATE ' . $this->cTable . ' SET ';

			$nFields 	= count( $this->aFields );
			$cValuePK 	= '';
			$lFirst		= true;
			$uValue 	= '';


			foreach( $this->aFields as $key => $aData ) {

				if ( $aData[ 'edited' ] || $lAll ) {

					$uValue =  addslashes( $aData[ 'value' ] ) ;		// Convertimos las " en \"

					switch ( $aData[ 'type' ] ) {

						case 'D';
							$uValue = CTOD( $uValue );  //RDC
							break;

					}

					if ( $lFirst ) {
						$lFirst = false;
					} else {
						$cSql .= ', ' ;
					}

					$cSql .= $key . ' = "' . $uValue . '" ';

					$nEdited++;

				}


				if ( $key == $this->cPK ) {
					$uValue = addslashes( $aData[ 'value' ] );
					$cValuePK = $uValue ;
				}

			}

			$cSql .= 'WHERE ' . $this->cPK . ' = "' . $cValuePK . '"';

		}



		if ( $nEdited == 0 ) {
			return 0;
		}

		//var_dump($cSql);
		$this->Execute( $cSql );

		$nRows = $this->affected_rows;

		if ( $nRows > 0 ) {


			if ( $lInsert && ( $this->lAutoincrement ) ) {

				if ( $this->cPK !== '' ) {

					$nId = $this->lastInsertId();

					$oField = $this->aFields[ $this->cPK ];

					$oField[ 'value' ] = $nId;

					$this->aFields[ $this->cPK ] = $oField;

				}
			}
		}

		return ( $nRows > 0 ? true : false ) ;
	}

	public function Delete( $cId = '' ) {

		$cSql 		= 'DELETE FROM ' . $this->cTable ;

		if ( $cId == '' ) {

			$nFields 	= count( $this->aFields );
			$nIdx   	= 1;

			foreach( $this->aFields as $key => $aData ) {

				if ( $key == $this->cPK ) {
					$cValuePK = $aData[ 'value' ];
				}

				$nIdx++;
			}

			$cSql .= ' WHERE ' . $this->cPK . ' = "' . $cValuePK . '"';

		} else {

			$cSql .= ' WHERE ' . $this->cPK . ' = "' . $cId . '"';
		}


		$this->Execute( $cSql );

		$nRows 		= $this->affected_rows;
		$lDelete 	= ( ( $nRows > 0 ) ? true : false );

		if ( $lDelete )
			$this->Blank();

		return $lDelete;
	}


	public function ToArray() {

		$aData = array();

		foreach( $this->aFields as $key => $value ) {

			$aData[ $key ] =  $value[ 'value' ];
		}

		return $aData;
	}


	public function GridToSave( $o ){

		$nAffected_Rows = 0;


		foreach( $o as $oReg ) {

			$cType	= $oReg[0];		// [U]pdate, [I]nsert, [D]elete
			$oObj 	= $oReg[1];

			$nFields 	= 0;
			$cFields 	= 'SET ';
			$uValuePk	= '';

			foreach( $oObj as $key => $uValue ) {


				if ( array_key_exists( $key, $this->aFields ) ) {

					$uValue = addslashes($uValue);	// Convertimos las " en \"

					if ( $nFields > 0 ) {

						$cFields .= ', ' ;
					}

					$nFields++;

					$aField = $this->aFields[ $key ];


					if ( $aField [ 'type' ] !== '' ) {

						switch ( $aField[ 'type' ] ) {

							case 'D';

								$uValue = CTOD( $uValue );
								break;
						}
					}

					if ( $key == $this->cPK ) {
						$uValuePK = $uValue;
					}

					$cFields .= $key . ' = "' . $uValue . '" ';
				}
			}

			switch ( $cType ) {

				case 'U':

					$cSql  = 'UPDATE ' . $this->cTable . ' ' ;
					$cSql .= $cFields;
					$cSql .= 'WHERE ' . $this->cPK . ' = "' . $uValuePK . '" ';

					break;

				case 'I':
					$cSql  = 'INSERT INTO ' . $this->cTable . ' ' ;
					$cSql .= $cFields;
					break;

				case 'D':
					$cSql  = 'DELETE FROM ' . $this->cTable . ' ' ;
					$cSql .= 'WHERE ' . $this->cPK . ' = "' . $uValuePK . '" ';
					break;
			}

 			//var_dump($cSql);
			$this->Execute( $cSql );

			$nAffected_Rows = $nAffected_Rows + $this->affected_rows;

		}

		return $nAffected_Rows;

	}

	public function GridAllToSave( $o ){

		$nAffected_Rows = 0;


		foreach( $o as $oReg ) {

			$cType	= "I";   // [U]pdate, [I]nsert, [D]elete
			$oObj 	= $oReg;

			$nFields 	= 0;
			$cFields 	= 'SET ';
			$uValuePk	= '';

			foreach( $oObj as $key => $uValue ) {


				if ( array_key_exists( $key, $this->aFields ) ) {

					$uValue = addslashes($uValue);	// Convertimos las " en \"

					if ( $nFields > 0 ) {

						$cFields .= ', ' ;
					}

					$nFields++;

					$aField = $this->aFields[ $key ];


					if ( $aField [ 'type' ] !== '' ) {

						switch ( $aField[ 'type' ] ) {

							case 'D';

								$uValue = CTOD( $uValue );
								break;
						}
					}

					if ( $key == $this->cPK ) {
						$uValuePK = $uValue;
					}

					$cFields .= $key . ' = "' . $uValue . '" ';
				}
			}

			switch ( $cType ) {

				case 'I':
					$cSql  = 'INSERT INTO ' . $this->cTable . ' ' ;
					$cSql .= $cFields;
					break;

			}

 			//var_dump($cSql);
			$this->Execute( $cSql );

			$nAffected_Rows = $nAffected_Rows + $this->affected_rows;

		}

		return $nAffected_Rows;

	}

//	TOOLS ---------------------------------------------------------------------------------------



	public function ShowReg( $cTitle = '' ) {

		if ( !empty( $cTitle ) )
			echo '<h2>' . $cTitle . '<hr></h2>';

		echo '<table border="1" style="width:100%">';

		echo '<tr style="background-color:#ddd">';

		foreach( $this->aFields as $key => $value ) {

			echo '<td><b>';
			echo $value[ 'name' ];
			echo '</b></td>';
		}

		echo '</tr>';

		echo '<tr>';

		foreach( $this->aFields as $key => $value ) {

			echo '<td>';
			echo $value[ 'value' ];
			echo '</td>';
		}

		echo '</tr>';

		echo '</table>';
	}


	public function ShowTable( $aTab ) {

		$nTotal = count( $aTab );

		echo '<table border="1" style="width:100%">';

		echo '<tr style="background-color:#ddd">';

		foreach( $this->aFields as $key => $value ) {

			echo '<td><b>';
			echo $value[ 'name' ];
			echo '</b></td>';
		}

		echo '</tr>';

		for ($i = 0; $i < $nTotal; $i++) {

			echo '<tr>';

			foreach( $aTab[ $i ] as $index => $value ){

				echo '<td>';
				echo $value;
				echo '</td>';
			}

			echo '</tr>';

		}

		echo '</table>';
	}

	private function ShowError( $cError ) {

		$arrTrace 	= debug_backtrace();
		$cTrace 	= '';
		$nTrace     = count( $arrTrace );

		for ( $i = 1; $i < $nTrace; $i++) {	//	i=0 es la llamada a ShowError

			if (!isset ($arrTrace[ $i ]['file']))
				$arrTrace[ $i ]['file'] = '[PHP Kernel]';

			if (!isset ($arrTrace[ $i ]['line']))
				$arrTrace[ $i ]['line'] = '';
/*
			if ( empty( $arrTrace[ $i ][ 'class' ] ) ){
				$cClass = '';
			} else {
				$cClass = $arrTrace[ $i ][ 'class' ] . '->' .
							$arrTrace[ $i ][ 'function' ] . '()' ;
			}
*/
			$cTrace .=  '<br>' . TWEB_DS_CALL . ' ' . $arrTrace[ $i ]['file'] .
						' (' .  $arrTrace[ $i ]['line'] . ')' .
						PHP_EOL ;

		}

		echo '<br><b>' . TWEB_DS_TABLE . '</b> ' . $this->cTable . '';
		echo '<br><b>' . TWEB_DS_ERROR . '</b> ' . $cError . '';
		echo '<br><u>' . TWEB_DS_TRACE . '</u>';
		echo $cTrace;

	}

}

//	Entrara en format 2016-03-18 i ha de sortir -> 18/03/2016
function DTOC( $cDate = '', $cSep = '/' ) {


//	$aData = preg_split( '/.-', $cDate );  NO xuta però he de mirar com ferho q serveixi per varis delimitdors

	$aData = explode( '-', $cDate );

	if ( count( $aData ) == 3 ) {
		$cData = $aData[2] . $cSep . $aData[1] . $cSep . $aData[0];
		return $cData;
	} else {
		return '00/00/0000';
	}
}

#function CTOD( $cDate = '', $cSep = '-' ) {
#
#	$aData = explode( '/', $cDate );
#
#	if ( count( $aData ) == 3 ) {
#		$cData = $aData[2] . $cSep . $aData[1] . $cSep . $aData[0];
#	} else {
#		$cData = '0000-00-00';
#	}
#
#	return $cData;
#}

function CTOD( $cDate = '', $cSep = '-' ) {

	$aData = preg_split( "^[-./]+^", $cDate );

	if ( count( $aData ) == 3 ) {
	    if (strlen($aData[2]) == 4 ) {
		   $cData = $aData[2] . $cSep . $aData[1] . $cSep . $aData[0];
		} else {
		   $cData = $aData[0] . $cSep . $aData[1] . $cSep . $aData[2];
		}
	} else {
		$cData = '0000-00-00';
	}

	return $cData;
}

function Filter2Where( $aFilter = array() , $cPrefix = '' ) {

		$cSql = '';

	//	Montem el filtre...

		$nFilter = count( $aFilter );

		if ( $nFilter > 0 ) {

			$cSql .= 'WHERE ' ;

			for ($i = 0; $i < $nFilter - 1 ; $i++) {

				if ( $i > 0 ) {

					if ( array_key_exists( 'logic',  $aFilter[$i] ) ) {
						$cLogic = ' ' . $aFilter[$i][ 'logic' ] . ' ' ;
					} else {
						$cLogic = ' AND ';
					}
				} else {
					$cLogic = '';
				}

				$cSql .= $cLogic . ' ';

				$cOp = $aFilter[$i][ 'op' ] ;

				$cSql .= $cPrefix . $aFilter[$i][ 'field' ] . ' ' . $cOp . ' '  ;

				$uValue = $aFilter[$i][ 'value' ] ;

				if ( $cOp == 'LIKE' ) {

					$cSql .= '"%' . $uValue . '%" ';

				} else {

					$cSql .= '"' . $uValue . '" ';

				}


			}

			if ( $i > 0 ) {

				if ( array_key_exists( 'logic',  $aFilter[$nFilter-1] ) ) {
					$cLogic = ' ' . $aFilter[$nFilter-1][ 'logic' ] . ' ' ;
				} else {
					$cLogic = ' AND ';
				}

			} else {
				$cLogic = '';
			}

			$cSql .= $cLogic;

			$cOp = $aFilter[$nFilter-1][ 'op' ] ;

			$cSql .= $cPrefix . $aFilter[$nFilter-1][ 'field' ] . ' ' . $cOp . ' '  ;

			$uValue = $aFilter[$nFilter-1][ 'value' ];

			if ( $cOp == 'LIKE' ) {

				$cSql .= '"%' . $uValue . '%" ';

			} else {

				$cSql .= '"' . $uValue . '" ';

			}

		}

	//	-------------------------------------------------------------

	return $cSql;
}


	function _d( $u ) {

		$uValue = print_r( $u, true );
		echo '<pre>';
		echo $uValue;
		echo '</pre>';

	}


?>