<?php 
/*
Libreria			: TWEB (FrameWork for Web)
Autor				: Carles Aubia
Versio				: 1.5
*/


class TValidator {

	private  $cParameter	= null;
	private  $cRules		= '';
	private  $aRules		= null;
	private  $aFilters		= null;
	private  $aMsg			= null;
	private  $uValue		= null;
	private  $lError		= false;
	private  $cError		= '';
	private  $aError		= array();
	public   $lSatinize 	= true;
	private  $lHas_Satinize = false;
	
	public function __construct( $cLang = 'ES' ) { 
	
		$this->cLang = strtoupper( $cLang );
		
		$this->LoadMsg();		
	}
	
	public function GetError() { return $this->aError; }	

	public function Post( $cVarName = '', $cRules = '', $cFilters = '' ) {	

		$this->cParameter 		= $cVarName;
		$this->uValue 			= $this->Request( 'POST', $cVarName );	
		
		$this->Proc( $cRules, $cFilters );		

		return $this->uValue;
	}
	
	public function Get( $cVarName = '', $cRules = '', $cFilters = '' ) {	

		$this->cParameter 		= $cVarName;
		$this->uValue 			= $this->Request( 'GET', $cVarName );	
	
		$this->Proc( $cRules, $cFilters );
		
		return $this->uValue;
	}

	private function Proc( $cRules, $cFilters ) {
	
		$this->lHas_Satinize 	= false;
		
		//	Cargamos Reglas...
			$this->Parse( 'RULES', $cRules );
			
		//	Cargamos Filtros...
			$this->Parse( 'FILTERS', $cFilters );			
		
		//	Ejecutamos Reglas...
			$this->Exe_Rules();
			
		//	Ejecutamos Filtros...
			$this->Exe_Filters();			
			
		if ( ( $this->lSatinize ) && ( gettype( $this->uValue ) == 'string' ) && ( ! $this->lHas_Satinize ) ){ 
			
			$this->uValue = $this->Satinize( $this->uValue );			
		}		
	}
	
	private function Request( $cVerb = 'POST', $cVarName = '' ) {
	
		switch ( $cVerb ) {
		
			case 'POST':
			
				$uValue = isset( $_POST[ $cVarName ] ) ? $_POST[ $cVarName ] : null;		
				break;
		
			case 'GET':
			
				$uValue = isset( $_GET[ $cVarName ] ) ? $_GET[ $cVarName ] : null;
				break;						
				
			default:
			
				$uValue = null;
		}		
			
		return $uValue;
	}
	
	private function Parse( $cType = '', $cCommand = '' ) {
	
		$aCommands = explode('|', $cCommand );
		$a = array();	

		foreach ( $aCommands as $cCmd ) {										
			
			$params = null;

			if (strstr($cCmd, ',') !== false) {
			
				$aCmd = explode( ',', $cCmd );

				$cCmd 		= strtolower(trim($aCmd[0]));
				$cParams 	= trim($aCmd[1]);

				if ( ( $cCmd == 'contain' ) || ( $cCmd == 'boolean' ) ) {
				
					//	Extrae palabras entre comillas simples 
					if (preg_match_all('#\'(.+?)\'#', $cParams, $matches, PREG_PATTERN_ORDER)) {
						$aParams = $matches[1];
					} else { // o extrae en un array palabras separadas por espacios
						$aParams = explode(chr(32), $cParams);
					}

					$a[] = array( $cCmd, $aParams );
					
				} else {
				
					$a[] = array( $cCmd, $cParams );			
				}
				
			} else {
			
				$cCmd = strtolower(trim($cCmd));
				
				$a[] = array( $cCmd, null );			
			}					
		}

		switch( $cType ) {
		
			case 'RULES':
			
				$this->aRules = $a;
				break;

			case 'FILTERS':
			
				$this->aFilters = $a;
				break;				
		}
		
		return null;	
	}
	
	private function Exe_Rules() {
	
		$nRules = count( $this->aRules );
		
		for ( $i = 0; $i < $nRules; $i++) {
		
			$cCmd 	= $this->aRules[ $i ][0];
			$uParam = $this->aRules[ $i ][1];
		
			switch ( $cCmd ) {
			
				case 'default':
					
					if ( ! $this->uValue ) {						
					
						$this->uValue = $uParam;
					}
					
					break;				
			
				case 'required':
					
					if ( $this->uValue == '' ) {						
						
						$this->aError[] = array( $this->cParameter, $this->MsgError( 'required', array( 'parameter' => $this->cParameter) ), 'required' );
					}
					
					break;	

				case 'is_numeric':
					
					if ( is_numeric( $this->uValue ) == false ) {						
						
						$this->aError[] = array( $this->cParameter, $this->MsgError( 'is_numeric', array( 'parameter' => $this->cParameter) ), 'is_numeric' );
					}
					
					break;						

				case 'min_len':
				
					$nLen 		= strlen( $this->uValue );
					$nLen_Min 	= $uParam;
					
					if ( $nLen < $nLen_Min ) {
					
						$this->aError[] = array( $this->cParameter, $this->MsgError( 'min_len', array( 'parameter' => $this->cParameter, 'len' => $nLen_Min ) ), 'min_len' );
						
					}									
				
					break;
					
				case 'max_len':
				
					$nLen 		= strlen( $this->uValue );
					$nLen_Max 	= $uParam;
					
					if ( $nLen > $nLen_Max ) {
					
						$this->aError[] = array( $this->cParameter, $this->MsgError( 'max_len', array( 'parameter' => $this->cParameter, 'len' => $nLen_Max ) ), 'max_len' );
						
					}									
				
					break;					
					
				case 'min':
				
					$nMin 	= $uParam;
					
					if ( $this->uValue < $nMin ) {
					
						$this->aError[] = array( $this->cParameter, $this->MsgError( 'min', array( 'parameter' => $this->cParameter, 'min' => $nMin ) ), 'min' );
						
					}									
				
					break;					
					
				case 'contain':				
					
					$aItems 	= $uParam;

					if ( in_array( $this->uValue, $aItems ) == false ) {
					
						$this->aError[] = array( $this->cParameter, $this->MsgError( 'contain', array( 'parameter' => $this->cParameter) ), 'contain' );

					}									
				
					break;	

				case 'url':
				
					$cUrl	= $this->uValue;
					
					//	Si tiene caracters raros, salta !
					
					if ( filter_var( $cUrl, FILTER_VALIDATE_URL ) === FALSE ){
					
						$this->aError[] = array( $this->cParameter, $this->MsgError( 'url', array( 'parameter' => $this->cParameter) ), 'url' );					
					}
					
					//$this->uValue = filter_var( $cUrl, FILTER_SANITIZE_URL );
					
					break;

				case 'mail':
				
					$cMail	= $this->uValue;
					
					//	Si tiene caracters raros, salta !
					
					if ( filter_var( $cMail, FILTER_VALIDATE_EMAIL ) === FALSE ){
					
						$this->aError[] = array( $this->cParameter, $this->MsgError( 'mail', array( 'parameter' => $this->cParameter) ), 'mail' );					
					}
					
					//$this->uValue = filter_var( $cUrl, FILTER_SANITIZE_URL );
					
					break;

				case 'ip':
				
					$cIp	= $this->uValue;
					
					//	Si tiene caracters raros, salta !
					
					if ( filter_var( $cIp, FILTER_VALIDATE_IP ) === FALSE ){
					
						$this->aError[] = array( $this->cParameter, $this->MsgError( 'ip', array( 'parameter' => $this->cParameter) ), 'ip' );					
					}
					
					//$this->uValue = filter_var( $cUrl, FILTER_SANITIZE_URL );
					
					break;					
			}

		}		
		
		$this->lError = ( count( $this->aError ) > 0 );		
	}
	
	private function Exe_Filters() {

		$nFilters = count( $this->aFilters );
		
		for ( $i = 0; $i < $nFilters; $i++) {
		
			$cCmd 	= $this->aFilters[ $i ][0];
			$uParam = $this->aFilters[ $i ][1];	
			
			if ( $cCmd == 'default' ) {	
				
				if ( ! $this->uValue ) {						
				
					$this->uValue = $uParam;
				}
			}

			if ( $cCmd == 'filter_satinize' ) {	
			
					$this->uValue = filter_var( $this->uValue, FILTER_SANITIZE_STRING );
					$this->lHas_Satinize = true;
			}
					
			if ( $cCmd == 'trim' ) {	
			
					$this->uValue = trim( $this->uValue );	
			}
					
			if ( $cCmd == 'len' ) {
		
					$nLen	= intval( $uParam );
					$this->uValue = substr( $this->uValue, 0, $nLen );					
			}
			
			if ( $cCmd == 'upper' ) {			
			
					$this->uValue = strtoupper( $this->uValue );															
			}
			
			if ( $cCmd == 'lower' ) {			
			
					$this->uValue = strtolower( $this->uValue );															
			}	

			if ( $cCmd == 'int' ) {			
			
					$this->uValue = intval( $this->uValue );															
			}

			if ( $cCmd == 'float' ) {			
			
					$this->uValue = floatval( $this->uValue );															
			}	

			if ( $cCmd == 'boolean' ) {			

				$aItems 	= $uParam;

				$this->uValue = ( in_array( strtolower($this->uValue), $aItems ) == true ) ? true : false;		
			}				
		}		
	}

	private function MsgError( $cCmd, $aTags = array() ) {
	
		$cError = null;
		
		if ( array_key_exists ( $cCmd, $this->aMsg ) ){

			$cError = $this->aMsg[ $cCmd ] ;			
					
			foreach( $aTags as $key => $value) {
				$cSearch 	= '{' . $key . '}';
				$cReplace 	= $value;
				$cError = str_replace( $cSearch, $cReplace, $cError );
			}				
		} 

		return $cError;
	}
	
	public function Valid( $bError = null ) {							
	
		if ( $this->lError ) {
		
			if ( $bError ) {
			
				if( is_callable( $bError ) ) {
			
					call_user_func( $bError, $this->aError );
					
				} else {
				
					//die( "Function doen't exist: " . $bError );
					echo "Function doen't exist: " . $bError ;
				}
			
			}						
		}
		
		return ! $this->lError;
		
	}
	
	private function Satinize( $uValue ) {

		//	Pendent de Estudi...
		
		//	a) Aquest posa simbols davant els tags i el resultat es q per pantalla es veuren pero estan codificats
			$uValue = stripslashes( $uValue ); // Elimina backslashes \
			$uValue = htmlspecialchars( $uValue ); // Traduce caracteres especiales en entidades HTML
			
		//	b) Aquest extreu de la cadena caracters especials, <, <script>,...		
			//	$uValue = filter_var($uValue, FILTER_SANITIZE_STRING);
		
		return $uValue;		
	}
	
	private function LoadMsg() {
	
		switch ( $this->cLang ) {
		
			case 'ES':	//	Español
			
				$this->aMsg = include ( 'lang/core.request.es.php' );			
				break;
				
			case 'CT':	//	Català
			
				$this->aMsg = include ( 'lang/core.request.ct.php' );			
				break;				
				
			default:
			
				$this->aMsg = include ( 'lang/core.request.es.php' );
		}
		
	}

}


function TPost( $cVarName, $uDefault = '', $cType = '' ) 		{ return TWeb_Request( 'post'	, $cVarName, $uDefault, $cType  ); }
function TGet ( $cVarName, $uDefault = '', $cType = ''  ) 		{ return TWeb_Request( 'get' 	, $cVarName, $uDefault, $cType  ); }
function TRequest( $cVarName, $uDefault = '', $cType = ''  ) 	{ return TWeb_Request( 'request', $cVarName, $uDefault, $cType  ); }

function TWeb_Request( $cVerb = 'post', $cVarName = '', $uDefault = '', $cType = '' ) {

	switch ( $cVerb ) {
	
		case 'post':
		
			$uValue = isset( $_POST[ $cVarName ] ) ? $_POST[ $cVarName ] : $uDefault;
		
			break;
	
		case 'get':
	
			$uValue = isset( $_GET[ $cVarName ] ) ? $_GET[ $cVarName ] : $uDefault;
			
			break;

		case 'request':
	
			$uValue = isset( $_REQUEST[ $cVarName ] ) ? $_REQUEST[ $cVarName ] : $uDefault;
			
			break;			

		default:

			$uValue = '';
	}

	switch ( $cType ) {
	
		case 'D':
	
			$uValue = CToD( $uValue );				
			break;		
		case 'L':
	
			$uValue = ( $uValue == 'true' ) ? true : false ;				
			break;	
	}
	

	
	
	return $uValue;
}

?>