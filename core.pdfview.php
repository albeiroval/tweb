<?php
/*
* Class  : TXFrame( <id>, <with> );
* Autor  : Antonio Albeiro Valencia
* Fecha  : 18/01/2021
*/

class TPDFView {
  
  public $textBtn = 'Close';
  public $nWidth  = 551;

  protected $cId    = 'viewpdf';
  protected $cTitle = 'Load PDF from base64';

  //--------------------------------------
  
  public function __construct( $cId = 'viewpdf', $cTitle = '' ) {
    if ( !empty($cId) ) { $this->cId = $cId; }
    if ( !empty($cTitle) ) { $this->cTitle = $cTitle; }
  }

  //--------------------------------------

  protected function CreateCanvasHTML() {

    $cHtml = "";

    $cHtml .= '<div id="outerContainer">' . PHP_EOL;

	  $cHtml .= ' <div id="toolbarViewer">' . PHP_EOL;

		$cHtml .= '   <div id="toolbarViewerLeft">' . PHP_EOL;
    $cHtml .= '     <button id="prev" class="toolbarButton pageUp" onclick="onPrevPage()" title="anterior"></button>'. PHP_EOL;
    $cHtml .= '     <button id="next" class="toolbarButton pageDown" onclick="onNextPage()" title="siguiente"></button>'. PHP_EOL;
		$cHtml .= '   </div>' . PHP_EOL;

    $cHtml .= '   <div id="toolbarViewerMiddle">' . PHP_EOL;
    $cHtml .= '     <div id="idmidle">' . PHP_EOL;
    $cHtml .= '       <button id="zoomOut" class="toolbarButton zoomOut" onclick="zoomMin()" title="Reducir" ></button>' . PHP_EOL;
    $cHtml .= '       <button id="zoomIn" class="toolbarButton zoomIn" onclick="zoomPlus()" title="Aumentar" ></button>' . PHP_EOL;
    $cHtml .= '     </div>' . PHP_EOL;
    $cHtml .= '     <div id="idpage" class="page">' . PHP_EOL;
    $cHtml .= '       <span style="font-weight: bold;">Pagina : <span id="page_num"></span> / <span id="page_count"></span></span>' . PHP_EOL;
    $cHtml .= '     </div>' . PHP_EOL;	
    
		$cHtml .= '   </div>' . PHP_EOL;

		$cHtml .= '   <div id="toolbarViewerRight">' . PHP_EOL;
    $cHtml .= '     <div style="position: relative;">' . PHP_EOL;
    $cHtml .= '       <button id="print" class="toolbarButton print" onclick="printPDF()" style="float: right;" title="Imprimir"></button>' . PHP_EOL;
    $cHtml .= '       <button id="download" class="toolbarButton download" onclick="downloadPDF()" style="float: right;" title="Descargar"></button>' . PHP_EOL;
    $cHtml .= '     </div>' . PHP_EOL;
		$cHtml .= '   </div>' . PHP_EOL;

    $cHtml .= ' </div>' . PHP_EOL;
		
    $cHtml .= ' <canvas id="myCanvas" class="canvasWrapper"></canvas>' . PHP_EOL; 

    $cHtml .= '</div>' . PHP_EOL;
    
    return $cHtml;
    
  }

  //--------------------------------------
  
  public function Activate() {

    $this->createHTML();
    $this->createCSS();
    $this->createJS();
    
  }

  //--------------------------------------

  private function createHTML() {

    $cStyle = '';

    if ( $this->nWidth > 0 ) {
      $cStyle .= 'style="width: 100%;max-width: ' . $this->nWidth .'px;"'; 
    }
    
    // Modal 
    $cHtml  = '<div id="'.$this->cId.'" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">' . PHP_EOL;	
		$cHtml .= ' <div class="modal-dialog modal-xl"' . $cStyle .'>' . PHP_EOL;

    // Modal content 
    $cHtml .= ' <div class="modal-content">' . PHP_EOL;

    // Modal Header
    $cHtml .= ' <div class="modal-header" style="height: 50px;">' . PHP_EOL;
    $cHtml .= '   <h5 class="modal-title" style="margin: auto;">' . $this->cTitle . '</h4>' . PHP_EOL;
    $cHtml .= ' </div>' . PHP_EOL;

    // Modal Body
    $cHtml .= ' <div class="modal-body">' . PHP_EOL;
    $cHtml .= $this->CreateCanvasHTML();
    $cHtml .= ' </div>' . PHP_EOL;

    // Modal footer
    $cHtml .= ' <div class="modal-footer" style="height: auto;">' . PHP_EOL;
    $cHtml .= '   <button id="btnexit" type="button" class="btn btn-default" data-dismiss="modal">'.$this->textBtn.'</button>' . PHP_EOL;
    $cHtml .= ' </div>' . PHP_EOL;

    // End Div's
    $cHtml .= ' </div>'  . PHP_EOL;
    $cHtml .= ' </div>'  . PHP_EOL;
    $cHtml .= '</div>'   . PHP_EOL;
    
    echo $cHtml;

  }

  //--------------------------------------

  private function createCSS() {

    $cHtml  = '<style>';
    $cHtml .= PHP_EOL;
    $cHtml .= '#btnexit {' . PHP_EOL;
    $cHtml .= '  width: 200px;' . PHP_EOL;
    $cHtml .= '  font-size: 14px;' . PHP_EOL;
    $cHtml .= '  background-color: #cdc9c9;' . PHP_EOL;
    $cHtml .= '  color: #0d0404;' . PHP_EOL;
    $cHtml .= '}' . PHP_EOL;
    $cHtml .= PHP_EOL;
    $cHtml .= '#btnexit:hover {' . PHP_EOL;
    $cHtml .= '  background-color: rgb(150, 196, 196);' . PHP_EOL;
    $cHtml .= '}' . PHP_EOL;
    $cHtml .= PHP_EOL;
    $cHtml .= '@media (max-width : 420px) {' . PHP_EOL;
    $cHtml .= '  .modal-content {' . PHP_EOL;
    $cHtml .= '    width: 96%  !important;' . PHP_EOL;
    $cHtml .= '  } ' . PHP_EOL;
    $cHtml .= '  #btnexit {' . PHP_EOL;
    $cHtml .= '    width: 30%;' . PHP_EOL;
    $cHtml .= '  }' . PHP_EOL;
    $cHtml .= '}' . PHP_EOL;
    $cHtml .= PHP_EOL;
    $cHtml .= '</style>';

    echo $cHtml;

  }

  //--------------------------------------

  private function createJS() {
 
    $cHtml  = '<!-- PLUGIN CORE.PDFVIEW.JS  -->';
    $cHtml .= '<script src="' . TWEB_PATH . '/core.pdfview.js"></script>';
    
    echo $cHtml;
    
    $cJS  = '<script>' . PHP_EOL;
    $cJS .= '$(function() {'                                            . PHP_EOL;
    $cJS .= '  var oFrame = new TViewPDF("' . $this->cId . '");'        . PHP_EOL;
		$cJS .= '  var o = new TControl();'                                 . PHP_EOL;
    $cJS .= "  o.ControlInit('" . $this->cId . "', oFrame, 'pdfview');" . PHP_EOL;
    $cJS .= '});'                                                       . PHP_EOL;
    $cJS .= '</script>' . PHP_EOL;

    echo $cJS;

  }
    
}

?>
