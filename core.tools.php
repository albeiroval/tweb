<?php
include_once ( 'core.constant.php' );	
	
//	Funcions de suport...

function _log( $uValue = '', $cTitle = '', $cFile = ''  ) {

	$cFitxer	= ( $cFile == '' ) ? ( TWEB_PATH_LOG . '/log.txt' ) : $cFile ;	
	$cType 		= gettype( $uValue );
	
	switch ( $cType ) {
	
		case 'array':
		case 'object':
			
			$uValue = print_r( $uValue, true );	
			break;
			
		case 'boolean':
		
			$uValue = ( $uValue ) ? 'true' : 'false';
			break;
			
		default: 
		
			if ( $uValue == '_DEL' || $uValue == '_NEW') {			
			
				if ( file_exists ( $cFitxer ))
					unlink( $cFitxer );	

				return false;
			}		
	}


	if ( !file_exists( TWEB_PATH_LOG ) ) {

		if ( ! mkdir( TWEB_PATH_LOG, 0777, true ) ){
			echo TWEB_ERROR_CREATE_TMPDIR ;
			exit();
		}
	}			

	$cHora		= date("d-m-y H:i:s", time());

	$nHandle 	= fopen( $cFitxer, "a");
	
	if ( $cTitle !== '' )
		fwrite( $nHandle, $cHora . ' ' . $cTitle . PHP_EOL);
		
	fwrite( $nHandle, $cHora . ' ' . $uValue . PHP_EOL);
	fclose( $nHandle ); 			
}

function _lapsus( $nStartTime = null ) {
	
	$cType = gettype( $nStartTime );
	
	switch ( $cType ) {
			
		case 'double':

			// Get the difference between start and end in microseconds, as a float value
			$diff 	= microtime(true) - $nStartTime;
			
			// Break the difference into seconds and microseconds
			$sec 	= intval($diff);
			$micro 	= $diff - $sec;
			
			// Format the result as you want it
			// $nFinal will contain something like "00:00:02.452"
			
			$nFinal = strftime('%T', mktime(0, 0, $sec)) . str_replace('0.', '.', sprintf('%.3f', $micro));		
		
			break;
	
		case 'NULL':
		default:
		
			$nFinal = microtime( true );	//	Starttime
			
			break;			
	}		
	
	return $nFinal;
}
?>