<?php
/*
* Libreria			: TVALWEB
* Autor					: Albeiro Valencia
* Versio				: 1.0
* Data Inici 		: 15/11/2019
* Ult. 
* Modificacio   : 17/08/2021
* Descripcion		: Framework para ayudar a contruir de manera rapida, facil y productiva una
* 					  		web de mantenimiento, diseñando incluso pantallas con workshop acelerando
* 					  		asi su tiempo de diseño.
*  http://vanilla-js.com/
*/

if ( ! defined( 'TWEB_PATH' ) ) {
	// define( 'TWEB_PATH'		, dirname(__FILE__) . '/' );
	define( 'TWEB_PATH', dirname(__DIR__) . '/' );
} 

if ( ! defined( 'TWEB_DATATABLE') ) {
	define( 'TWEB_DATATABLE', TWEB_PATH . "/libs/dataTables" );
}	

//	Includes Class TWeb ...
require_once ( 'core.config.php' );
require_once ( 'core.constant.php' );
require_once ( 'core.session.php' );
require_once ( 'core.tools.php' );
require_once ( 'core.rc.php' );

//  Includes New Class  ...
require_once ( 'core.datatable.php' );
require_once ( 'core.selectpicker.php' );
require_once ( 'core.carousel.php' );

//************************************************************************//

Class TWeb {

	public  $cTitle 			= '';
	public  $cIcon 				= '';	
	public  $cCss					= '';
	public  $cJQueryCss		= '';
	public  $cBackground 	= '';
	public  $cBrush 			= '';
	public  $cColor		 		= '';	
	public  $cLang		 		= TWEB_HTML_LANG;	
	public  $lLibs		 		= true;

	public  $lMaps		 				= false;	//	Carga plugin Maps
	public  $lUpload	 				= false;	//	Carga plugin Upload
	public  $lEditor	 				= false;	//	Carga plugin Editor
	public  $isLoading	 			= true;		//  Muestra el Gif Animado en la carga		
	public  $lAwesome 				= true;		//	Carga fonts Awesome
	public  $ltableBootstrap 	= true;   //  Tipo de datatable / Original o BootStrap  
	public  $fontfamily 		 	= "";  		//  Font para toda la aplicacion
	
	private $aCss 				= [];
	private $aJs 			  	= [];
	private $aMeta				= [];	

	//-----------------------------------------------------

  public function __construct( $cTitle = 'TWeb' ) {     
		$this->cTitle			= $cTitle;	
		$this->cIcon 			= TWEB_PATH_IMAGES 	. '/tweb.png';
		$this->cCss 			= TWEB_PATH . '/core.css';
		$this->cJQueryCss	= TWEB_PATH_LIBS	. '/jquery/css/tweb/jquery-ui-1.10.4.custom.css';
	}

	//-----------------------------------------------------
	
	public function Activate() {

		$cHtml  = '<!DOCTYPE html>';
		$cHtml .= '<html lang="' . $this->cLang . '">';
		$cHtml .= $this->documentHead();
		$cHtml .= $this->documentBody();

		echo $cHtml ;

	}

	//-----------------------------------------------------

	private function documentHead() {
		
		$cHtml  = '<head>';		

		$cHtml .= '<meta charset=utf-8">';
		$cHtml .= '<meta http-equiv="Content-Type" content="text/html">';
		$cHtml .= '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
		
		$cHtml .= '<title>' . $this->cTitle . '</title>';

		$cHtml .= '<link rel="icon" href="' . $this->cIcon . '" type="image/x-icon" >';
		$cHtml .= '<link rel="shortcut icon" href="'  . $this->cIcon .  '" type="image/x-icon">';

		$cHtml .= $this->loadPlugins();

		/** META's **/
		$nMeta = count( $this->aMeta );

		for ( $i = 0; $i < $nMeta; $i++) {
			$cName 		  = $this->aMeta[$i][0];		
			$cContent 	= $this->aMeta[$i][1];		
			$cHtml .= '<meta name="' . $cName . '" content="' . $cContent . '" >';					
		}

		/** Css TWeb **/
		$cHtml .= '<link type="text/css" rel="stylesheet" href="' . $this->cCss . '" media="screen">';		
		//	$cHtml .= '<link type="text/css" rel="stylesheet" href="' . TWEB_PATH . '/reset.css' . '" media="screen">';		

		/** JS Public **/
		$nJs = count( $this->aJs );
		for ( $i = 0; $i < $nJs; $i++) {
			$cJs = $this->aJs[$i];		
			$cHtml .= '<script src="' . $cJs . '"></script>';					
		}

		/** CSS Public **/
		$nCss = count( $this->aCss );
		for ( $i = 0; $i < $nCss; $i++) {
			$cCss = $this->aCss[$i];			
			$cHtml .= '<link type="text/css" rel="stylesheet" href="' . $cCss . '" media="screen">';					
		}		
	
		/* Loading page 
     * https://smallenvelop.com/display-loading-icon-page-loads-completely/
		 * https://loading.io/ 
     */
    if ( $this->isLoading ) {

			$cHtml .= '<script>';
      $cHtml .= '$(window).on("load", function() {';
      $cHtml .= '  $(".se-pre-con").fadeOut("slow");';

			// Cambiamos el font para toda la aplicacion
			if ( !empty($this->fontfamily) ) {
				$cHtml .= 'document.body.setAttribute("style", "font-family: '.$this->fontfamily.' !important");'; 
			}

			$cHtml .= '});';
      $cHtml .= '</script>'; 
			
    }
			
		$cHtml .= '</head>';	

		return $cHtml;

	}

	//-----------------------------------------------------

	private function documentBody() {
	
		$cHtml  = '<body>';

		/** Loading  */
    $cHtml .= '<div class="se-pre-con"></div>';

		/** Style para Pantalla principal **/
		$cHtml .= '<div class="tweb_screen" id="tweb_screen"';	
    
		if ( !empty( $this->cColor ) ) {
				$cHtml .= ' style="background-color: ' . $this->cColor . ';"';
		} else if ( !empty( $this->cBrush ) ) {
				$cHtml .= ' style="background: url( ' . $this->cBrush . ')"';		
		} else if ( !empty( $this->cBackground )) {
				/* The image used */
				$cHtml .= ' style="background: url(' . $this->cBackground . ');';

				/* Full height */
				$cHtml .= ' height: 100%;';
				
				/* Center and scale the image nicely */
				$cHtml .= ' background-position: center;';
				$cHtml .= ' background-repeat: no-repeat;';
				$cHtml .= ' background-size: cover;';
				
		}
		
		$cHtml .= '>';					

		$cHtml .= '<!-- Inicializamos variables globales para TVALWEB -->';
		$cHtml .= '<script>'; 
		$cHtml .= ' var TWEB_PATH        = "' . TWEB_PATH . '";';
		$cHtml .= ' var TWEB_PATH_LIBS   = "' . TWEB_PATH_LIBS . '";';
		$cHtml .= ' var TWEB_PATH_IMAGES = "' . TWEB_PATH_IMAGES . '";';
		$cHtml .= ' var TWEB_PATH_SOUND  = "' . TWEB_PATH_SOUND . '";';
		$cHtml .= '</script>'; 
		
		$cHtml .= '<!-- CORE de TVALWEB javascript -->';
		$cHtml .= '<script src="' . TWEB_PATH . '/core.js"></script>' ;
		$cHtml .= '<script src="' . TWEB_PATH . '/core.msg.js"></script>' ;
		$cHtml .= '<script src="' . TWEB_PATH . '/core.dialog.js"></script>' ;
		$cHtml .= '<script src="' . TWEB_PATH . '/core.selectpicker.js"></script>' ;

		return $cHtml;

	}

	//-----------------------------------------------------

	private function LoadPlugins() {

		/** JQuery **/
		// https://github.com/jquery/jquery-migrate
		$cHtml  = '<!-- PLUGIN JQUERY -->' . PHP_EOL;
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/jquery/jquery-3.6.0.min.js"></script>'; 
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/jquery/jquery-migrate-3.0.0.min.js"></script>';
		
		if ( $this->lLibs ) {
		
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/jquery/jquery-ui-1.10.4.custom.js"></script>';	
			$cHtml .= '<link rel="stylesheet" href="' . $this->cJQueryCss . '">';			
			
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/jquery/jquery.ui.progressbar.js"></script>';		// ProgressBar		
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/jquery/jquery.ui.datepicker-es.js"></script>';	// Calendario
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/jquery/jquery.event.drag-2.3.0.js"></script>';		// Drags		
			
			/** Dialog Extend **/
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/timepicker/jquery-ui-timepicker-addon.min.js"></script>';	// Timepicker
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/timepicker/jquery-ui-sliderAccess.js"></script>';			// Timepicker
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/timepicker/jquery-ui-timepicker-addon.min.css">';			
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/dialogextend/jquery.dialogextend.js"></script>';   // Original 
						
			/**	Multiselect **/
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/multiple-select/multiple-select.js"></script>'; 	
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/multiple-select/multiple-select.css" type="text/css" media="screen" />';			

			/** Notificacions **/
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/notify/notify.js"></script>'; 
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/notify/styles/bootstrap/notify-bootstrap.js"></script>'; 		

			/** Cookies **/
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/cookies/jquery.cookie.js"></script>'; 
			
			/** Trees **/
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/jstree/jstree.js"></script>'; 
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/jstree/themes/default/style.css" type="text/css" media="screen" />';			
			
			/** Mask Input **/
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/masked/jquery.maskedinput.min.js"></script>';   // Original 		
		
			/** Image **/
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/lightbox/lightbox.js"></script>' ;
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/lightbox/css/lightbox.css">';			
			
			/** Menus **/
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/menu/contextMenu.js"></script>'; 	
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/menu/contextMenu.css" type="text/css" media="screen" />';	
						
			/** Resize **/
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/resize/jquery.ba-resize.js"></script>' ;
			
			/** Upload **/
			if ( $this->lUpload ) {
				$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/fileupload/jquery.ui.widget.js"></script>' ;
				$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/fileupload/jquery.iframe-transport.js"></script>' ;
				$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/fileupload/jquery.fileupload.js"></script>' ;
			}

			/** SlickGrid **/
			$cHtml .= $this->LibrarySlickGrid();
			
			/** Touch **/
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/touch/jquery.touch.min.js"></script>' ;
			
			/** Maps **/
			if ( $this->lMaps )	 {
				$cHtml .= '<script src="https://maps.googleapis.com/maps/api/js?key=' . TWEB_KEY_MAP . '"></script>';
			}			

			/** Editor **/
			if ( $this->lEditor ) {			
				$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/sceditor/minified/themes/default.min.css" id="theme-style" />';
				$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/sceditor/minified/sceditor.min.js"></script>' ;
				$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/sceditor/minified/icons/monocons.js"></script>' ;
				$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/sceditor/minified/formats/bbcode.js"></script>' ;
			}

			// https://fontawesome.com/download
			if ( $this->lAwesome ) {			
				$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/fontawesome-5.15.1/css/all.min.css">';	
			}
			/** Library bootstrap **/
			$cHtml .= $this->LibraryBootStrap();

			/** Library bootstrap tables**/
			$cHtml .= $this->LibraryBootStrapTable();
			
			/** Library datatables **/
			$cHtml .= $this->LibraryDatatables();

			/* Sweetalert2  **/
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/sweetalert2/sweetalert2.min.js"></script>' ; 
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/sweetalert2/sweetalert2.min.css" type="text/css" media="screen" />';

			/** bootbox **/
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/bootbox/bootbox.all.min.js"></script>' ;   	

			/** Library PDFVIEW  */
			$cHtml .= $this->LibraryPDFViewer();

			/** Library Leaflet **/
			$cHtml .= $this->LeafLetMaps();

			/** Library datepicker  **/
			$cHtml .= $this->loadDatePickerLibs();

			/** Library Bootstrap-treeview **/
			$cHtml .= $this->BstreeView();

			/** Library's creadas por el Author : Albeiro Valencia */
			$cHtml .= $this->CustomLibrary();

			return $cHtml;

		}	

	}

	//-----------------------------------------------------
	
	public function SetBackground( $cBackground = '' ) { $this->cBackground = $cBackground;	}
	public function SetIcon( $cIcon = '' ) { $this->cIcon = $cIcon ;	}
	public function SetBrush( $cBrush = '' ) { $this->cBrush = $cBrush ;	}
	public function SetColor( $cColor = '' ) { $this->cColor = $cColor ;	}
	public function AddJs( $cJs = '' ) { $this->aJs[] = $cJs ;	}
	public function AddCss( $cCss = '' ) { $this->aCss[] = $cCss ;	}
	public function AddMeta( $cName = '', $cContent = '' ) { $this->aMeta[] = array( $cName, $cContent );	}	
	public function InitMaps() { $this->lMaps = true ;	}	
	public function InitUpload() { $this->lUpload = true ;	}	
	public function InitPreloader() { $this->isLoading = true ;	}	

	//-----------------------------------------------------

	public function SetFontFamily( $fontfamily = '"Segoe UI", Arial, sans-serif;'  ) { 
		$this->fontfamily = $fontfamily; 
	}
	
	//-----------------------------------------------------

	public function End() {	
	
		$cHtml   = '</div>';
		$cHtml  .= '<script src="' . TWEB_PATH . '/core.init.js"></script>' ;
		$cHtml	.= '</body>';
		$cHtml	.= '</html>';		

		echo $cHtml ;
	}

	//-----------------------------------------------------
	
	public function Device() {		
	
	// Fuente: http://7sabores.com/blog/detectar-tipo-dispositivo-mobile-tablet-desktop-php 	
	
		$tablet_browser = 0;
		$mobile_browser = 0;
		$body_class 	  = 'desktop';
		 
		if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
			$tablet_browser++;
			$body_class = "tablet";
		}
		 
		if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
			$mobile_browser++;
			$body_class = "mobile";
		}
		 
		if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
			$mobile_browser++;
			$body_class = "mobile";
		}
		 
		$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
		$mobile_agents = array(
			'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
			'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
			'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
			'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
			'newt','noki','palm','pana','pant','phil','play','port','prox',
			'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
			'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
			'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
			'wapr','webc','winw','winw','xda ','xda-');
		 
		if (in_array($mobile_ua,$mobile_agents)) {
			$mobile_browser++;
		}
		 
		if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
			$mobile_browser++;
			//Check for tablets on opera mini alternative headers
			$stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
			if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
			  $tablet_browser++;
			}
		}
		
		if ($tablet_browser > 0) {			//	Tablet
		   return 1;
		} else if ($mobile_browser > 0) {	//	Browser
		   return 2;
		} else {							// 	Desktop
		   return 0;
		}		
	}

	//-----------------------------------------------------

	public function ViewLog( $cPhp_View_Log = '' ) {

		$cPhp_View_Log = empty( $cPhp_View_Log ) ? ( TWEB_PATH . 'dlg_log.php' ) : $cPhp_View_Log ;
		
		echo "<div id='_log'><span class='_log'>Log</span></div>";
		
		ExeJsReady( "InitViewLog( '" . $cPhp_View_Log . "' )" );		
	}

	/*
	* Library's BootStrap
	*/
	//-----------------------------------------------------

	private function LibraryBootStrap() {

		// Bootstrap v4.6.0
		$cHtml  = '<!-- PLUGIN BOOTSTRAP v4.6.0 -->' . PHP_EOL;
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/bootstrap-4.6.0/js/popper.min.js"></script>' ;  
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/bootstrap-4.6.0/js/bootstrap.min.js"></script>' ;  
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/bootstrap-4.6.0/js/bootstrap.bundle.js"></script>' ;  
		$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/bootstrap-4.6.0/css/bootstrap.min.css" type="text/css" media="screen" />';	
		
		// Bootstrap-select-1.14
		$cHtml .= '<!-- PLUGIN BOOTSTRAP-SELECT v1.14 -->' . PHP_EOL;
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/bootstrap-select-1.14/js/bootstrap-select.min.js"></script>' ;   	
		$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/bootstrap-select-1.14/css/bootstrap-select.min.css" type="text/css" media="screen" />';	

		return $cHtml;

	}

	//-----------------------------------------------------

	private function LibraryBootStrapTable() {

    $cHtml = '<!-- PLUGIN BOOTSTRAP TABLES  -->';

    // Load BootStrap Table CSS
    $cHtml .= '<link href="' .TWEB_PATH. '/libs/bootstrap-table/bootstrap-table.css" rel="stylesheet">';

    // Load BootStrap Table JS
    $cHtml .= '<script src="' .TWEB_PATH. '/libs/bootstrap-table/bootstrap-table.min.js"></script>';
    $cHtml .= '<script src="' .TWEB_PATH. '/libs/bootstrap-table/bootstrap-table-locale-all.min.js"></script>';

    // Load Extensions
    $cHtml .= '<script src="' .TWEB_PATH. '/libs/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js"></script>';
    $cHtml .= '<script src="' .TWEB_PATH. '/libs/bootstrap-table/extensions/print/bootstrap-table-print.min.js"></script>';
    $cHtml .= '<script src="' .TWEB_PATH. '/libs/bootstrap-table/extensions/export/bootstrap-table-export.min.js"></script>';
    $cHtml .= '<script src="' .TWEB_PATH. '/libs/bootstrap-table/extensions/export-two/jspdf.min.js"></script>';
    $cHtml .= '<script src="' .TWEB_PATH. '/libs/bootstrap-table/extensions/export-two/jspdf.plugin.autotable.js"></script>';
    $cHtml .= '<script src="' .TWEB_PATH. '/libs/bootstrap-table/extensions/export-two/tableExport.min.js"></script>';
    $cHtml .= '<script src="' .TWEB_PATH. '/libs/bootstrap-table/extensions/key-events/bootstrap-table-key-events.min.js"></script>';
		// $cHtml .= '<script src="' .TWEB_PATH. '/libs/bootstrap-table/extensions/editable/bootstrap-table-editable.js"></script>';

    return $cHtml;
  
  }

	/*
	* Library Datatables
	*/
	//-----------------------------------------------------

	// https://datatables.net/plug-ins/dataRender/datetime
	// https://datatables.net/forums/discussion/39800/how-do-you-solve-uncaught-typeerror-fn-datatable-moment-is-not-a-function-exception
	private function LibraryDatatables() {
		
		/* moments DateTime */
		$cHtml  = '<!-- PLUGIN MOMENTS  -->' . PHP_EOL;
		$cHtml .= '<script src="' . TWEB_DATATABLE . '/moments/moment.min.js"></script>' ;
		
		if ( $this->ltableBootstrap ) {

			$cHtml .= '<!-- PLUGIN DATATABLES BOOTSTRAP -->' . PHP_EOL;

			/** Plugin DataTables **/
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_DATATABLE . '/twitter-bootstrap/4.5.2/css/bootstrap.css" type="text/css" />';	
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_DATATABLE . '/1.10.24/css/dataTables.bootstrap4.min.css" type="text/css" />';	
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/1.10.24/js/jquery.dataTables.min.js"></script>';   
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/1.10.24/js/dataTables.bootstrap4.min.js"></script>';   	

			/* 
			* DataTables Extensions
			*/

			/* Buttons */
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_DATATABLE . '/buttons/1.7.0/css/buttons.bootstrap4.min.css" type="text/css" />';	
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/buttons/1.7.0/js/dataTables.buttons.min.js"></script>' ;
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/buttons/1.7.0/js/buttons.bootstrap4.min.js"></script>' ;

			/* DataTables Column visibility control */
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/buttons/1.7.0/js/buttons.colVis.min.js"></script>' ;

			/* DataTables Flash export buttons */
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/buttons/1.7.0/js/buttons.flash.min.js"></script>' ;

			/* DataTables HTML5 export buttons */
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/buttons/1.7.0/js/buttons.html5.min.js"></script>' ;

			/* DataTables Print button */
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/buttons/1.7.0/js/buttons.print.min.js"></script>' ;

			/* DataTables JZip */
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/JSZip/3.6.0/jszip.min.js"></script>' ;

			/* DataTables pdfmake */
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/pdfmake/0.1.70/pdfmake.min.js"></script>' ;
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/pdfmake/0.1.70/vfs_fonts.js"></script>' ;

			/* DataTables KeyTable */
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_DATATABLE . '/keyTable/2.6.1/css/keyTable.dataTables.min.css" type="text/css" />';	
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/keyTable/2.6.1/js/dataTables.keyTable.min.js"></script>' ;

			/* DataTables Responsive Bootstrap */ 
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_DATATABLE . '/responsive/2.2.7/css/responsive.bootstrap.min.css" type="text/css"/>';	
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/responsive/2.2.7/js/dataTables.responsive.min.js"></script>' ;
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/responsive/2.2.7/js/responsive.bootstrap.min.js"></script>' ;

			/* Fixed Header */
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_DATATABLE . '/fixedheader/3.1.8/css/fixedHeader.bootstrap4.min.css" type="text/css" />';	
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/fixedheader/3.1.8/js/dataTables.fixedHeader.min.js"></script>' ;

			/* DataTables Scroller */
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_DATATABLE . '/scroller/2.0.3/css/scroller.dataTables.min.css" type="text/css" />';	
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/scroller/2.0.3/js/dataTables.scroller.min.js"></script>' ;
			
			/* DataTables Select */
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_DATATABLE . '/select/1.3.3/css/select.dataTables.min.css" type="text/css" />';	
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/select/1.3.3/js/dataTables.select.min.js"></script>' ;


		} else {

			$cHtml .= '<!-- PLUGIN DATATABLES -->' . PHP_EOL;

			/** Plugin DataTables **/
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_DATATABLE . '/1.10.24/css/jquery.dataTables.min.css" type="text/css" />';	
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/1.10.24/js/jquery.dataTables.min.js"></script>';   	

			/* 
			* DataTables Extensions
			*/

			/* Buttons */
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_DATATABLE . '/buttons/1.7.0/css/buttons.dataTables.min.css" type="text/css" />';	
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/buttons/1.7.0/js/dataTables.buttons.min.js"></script>' ;

			/* DataTables Column visibility control */
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/buttons/1.7.0/js/buttons.colVis.min.js"></script>' ;

			/* DataTables Flash export buttons */
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/buttons/1.7.0/js/buttons.flash.min.js"></script>' ;

			/* DataTables HTML5 export buttons */
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/buttons/1.7.0/js/buttons.html5.min.js"></script>' ;

			/* DataTables Print button */
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/buttons/1.7.0/js/buttons.print.min.js"></script>' ;

			/* DataTables JZip */
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/JSZip/3.6.0/jszip.min.js"></script>' ;

			/* DataTables pdfmake */
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/pdfmake/0.1.70/pdfmake.min.js"></script>' ;
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/pdfmake/0.1.70/vfs_fonts.js"></script>' ;

			/* DataTables KeyTable */
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_DATATABLE . '/keyTable/2.6.1/css/keyTable.dataTables.min.css" type="text/css" />';	
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/keyTable/2.6.1/js/dataTables.keyTable.min.js"></script>' ;

			/* DataTables Responsive Datatables */
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_DATATABLE . '/responsive/2.2.7/css/responsive.dataTables.min.css" type="text/css" />';	
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/responsive/2.2.7/js/dataTables.responsive.min.js"></script>' ;

			/* Fixed Header */
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_DATATABLE . '/fixedheader/3.1.8/css/fixedHeader.dataTables.min.css" type="text/css" />';	
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/fixedheader/3.1.8/js/dataTables.fixedHeader.min.js"></script>' ;

			/* DataTables Scroller */
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_DATATABLE . '/scroller/2.0.3/css/scroller.dataTables.min.css" type="text/css" />';	
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/scroller/2.0.3/js/dataTables.scroller.min.js"></script>' ;
			
			/* DataTables Select */
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_DATATABLE . '/select/1.3.3/css/select.dataTables.min.css" type="text/css" />';	
			$cHtml .= '<script src="' . TWEB_DATATABLE . '/select/1.3.3/js/dataTables.select.min.js"></script>' ;			
			
		}	

		$cHtml .= '<script src="' . TWEB_DATATABLE . '/dataRender/datetime.js"></script>' ;
		$cHtml .= '<script src="' . TWEB_DATATABLE . '/sorting/datetime-moment.js"></script>' ;

		return $cHtml;

	}

	/* 
	* https://mozilla.github.io/pdf.js/ 
	* https://github.com/mozilla/pdf.js
	*/
	private function LibraryPDFViewer() {

		$cHtml = '<!-- PLUGIN PDFVIEW -->' . PHP_EOL;

		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/pdfview/js/pdf.js"></script>';  
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/pdfview/js/pdf.worker.js"></script>';
	
		// <!-- CUSTOM TVALTWEB	-->
		$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/pdfview/css/viewer.css" type="text/css" media="screen" />';	
	
		// <!-- PRINT PDF https://printjs.crabbly.com/ -->
		$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/pdfview/css/print.css" type="text/css" media="screen" />';	
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/pdfview/js/print.js"></script>';  

		// <!-- DOWNLOAD PDF https://github.com/eligrey/FileSaver.js -->
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/pdfview/js/FileSaver.js"></script>';  

		// <!-- PLUGIN TVALWEB -->
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/pdfview/js/custom.js"></script>';  

		return $cHtml;

	}

	/*
	* Library SlickGrid
	*/
	//------------------------------------------------------
	private function LibrarySlickGrid() {

		$cHtml = '<!-- PLUGIN SLICKGRID  -->' . PHP_EOL;

		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/slick.core.js"></script>' ;
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/slick.formatters.js"></script>' ;
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/slick.editors.js"></script>' ;
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/slick.grid.js"></script>' ;
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/slick.dataview.js""></script>' ;
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/controls/slick.pager.js"></script>' ;
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/plugins/slick.rowselectionmodel.js"></script>' ;
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/plugins/slick.autotooltips.js"></script>' ;
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/plugins/slick.headerbuttons.js"></script>' ;
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/plugins/slick.cellexternalcopymanager.js"></script>' ;			
			
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/plugins/footer/TotalsDataView.js"></script>' ;			
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/plugins/footer/TotalsPlugin.js"></script>' ;			
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/plugins/footer/jquery.getscrollbarwidth.js""></script>' ;			
			
		$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/slickgrid/plugins/footer/TotalsPlugin.css" type="text/css" media="screen" />';	
		$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/slickgrid/plugins/footer/styles.css" type="text/css" media="screen" />';	
						
		$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/slickgrid/slick.grid.css" type="text/css" media="screen" />';	
		$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/slickgrid/examples.css" type="text/css" media="screen" />';	
		$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/slickgrid/controls/slick.pager.css" type="text/css" media="screen" />';				
		$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/slickgrid/plugins/slick.headerbuttons.css" type="text/css" media="screen" />';				

		return $cHtml;

	}

	/* Library LeafLet 
	 * an open-source JavaScript library, for mobile-friendly interactive maps
	 * https://leafletjs.com/download.html
	 */
	//------------------------------------------------------
	private function LeafLetMaps() {

		$cHtml  = '<!-- PLUGIN LEAFLET INTERACTIVE MAPS OPEN SOURCE  -->' . PHP_EOL;
		$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/leaflet/leaflet.css" />';
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/leaflet/leaflet.js"></script>';

		return $cHtml;

	}

	//------------------------------------------------------

	/*
   * DatePicker
   */
  private function loadDatePickerLibs() {

    $cHtml  = '<!-- PLUGIN DATEPICKER -->' . PHP_EOL;
    $cHtml .= '<link href="' .TWEB_PATH_LIBS . '/datepicker/datepicker.css" rel="stylesheet" type="text/css" media="screen">';
    $cHtml .= '<script src="' .TWEB_PATH_LIBS . '/datepicker/script.js"></script>';
    $cHtml .= '<script src="' .TWEB_PATH_LIBS . '/datepicker/datepicker.js"></script>';

    return $cHtml;
  
  }

	//------------------------------------------------------

	/*
	 * Bootstrap-treeview
	 */

	private function Bstreeview() {

		$cHtml  = '<!-- PLUGIN Bootstrap-treeview  -->' . PHP_EOL;
		$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/bstreeview-1.2.0/css/bstreeview.css" />';
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/bstreeview-1.2.0/js/bstreeview.js"></script>';

		return $cHtml;

	}

	//------------------------------------------------------

	/*
	 * Librerias Propias creadas por el Author : Albeiro Valencia
	 * Funciones Propias
	 */

	private function CustomLibrary() { 
	
		$cHtml  = '<!-- LIBRERIAS DE FUNCIONES CREADAS POR EL AUTOR  -->' . PHP_EOL;

		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/mylibs/jquery.maskMoney.min.js"></script>' ;   
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/mylibs/sweetalert.messages.js"></script>' ;   
		// $cHtml .= '<script src="' . TWEB_PATH_LIBS . '/mylibs/bootbox.messages.js"></script>' ;   
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/mylibs/funciones.js"></script>' ;   

		// Custom extend  
		$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/custom/css/sweetalert.css" type="text/css" media="screen" />';
		$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/custom/css/tweb.css" type="text/css" media="screen" />';

		// gps
		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/mylibs/gps.js"></script>' ;   

		return $cHtml;

	}

}

//************************************************************************//

class TCode {

	private $cCode	= '';
	
    public function __construct( $oWnd, $cCode = ''  ) {  	
	
		$this->cCode = $cCode;
		
		if ( gettype( $oWnd ) == 'object' )
			$oWnd->AddControl( $this );			
	}
	
	public function Activate() {	
	
		echo $this->cCode; 	
	}	
}

//************************************************************************//

class TFont {

	public $cFaceName 	= '';
	public $nSize 		= 0;
	public $lBold 		= false;
	public $lItalic		= false;
	public $lUnderline	= false;
	public $nClrFore	= null;
	public $nClrBack	= null;


    public function __construct( $cName = 'Verdana' , $nSize = 12, $lBold = false, $lItalic = false, $lUnderline = false, $nClrFore = null, $nClrBack = null ) {  
	
		$this->cFaceName	= $cName;
		$this->nSize		= $nSize;
		$this->lBold		= ( gettype( $lBold ) == 'boolean' ) ? $lBold : false;
		$this->lItalic		= ( gettype( $lItalic ) == 'boolean' ) ? $lItalic : false;
		$this->lUnderline	= ( gettype( $lUnderline ) == 'boolean' ) ? $lUnderline : false;
		$this->nClrFore		= $nClrFore ;	
		$this->nClrBack		= $nClrBack ;	
	}
}

//************************************************************************//

class TControl { 

  public  $oWndParent		= null;
  public  $cId					= '';
  public  $nTop					= 0;
  public  $nLeft				= 0;
  public  $nWidth				= 100;
  public  $nHeight			= 100;
  public  $nRight				= -1;
  public  $nBottom			= -1;
  public  $lBorder			= false;
  public  $lHide				= false;
  public  $lDisabled		= false;	
  public  $lReadOnly		= false;	
	public  $cCss					= '';	
	public  $cClass				= '';	
	public  $cControl			= '';	
	public  $cColor				= '';	
	public  $cBackground	= '';	
	public  $cBrush				= '';	
	public  $cCaption			= '';	
	public  $cTooltip			= '';	
	public  $bAction			= '';	
	public  $bClick				= '';	
	public  $bChange			= '';	
	public  $bWhen				= '';	
	public  $hRef			  	= '';	
	public  $cAlign				= '';	
	public  $cOverflow		= 'hidden';	
	public  $cPosition		= 'absolute';
	public  $cBorderType	= '';	// 'inset white'; //'solid black'; // 'groove'; // 'ridge' ; // 'outset'; //'inset';
	public  $oFont 				= '';
	public  $twebclass		= '';
	public  $cMessage			= '';
	public  $cData  			= '';
	public  $aKeys				= [];	
	public  $nDeepShadow	= 0;	
	public  $cColorShadow	= 'grey';	

	// Albeiro Valencia
	protected $aVarJS     = [];   // Crea variables en JavaScript

  function __construct( $oWnd, $cId, $nTop = 0, $nLeft = 0, $nWidth = 0, $nHeight = 0, $cColor = '' ) {
	
		if ( $cId == null ) {
			$cId = NewId();
		}	
	
		$this->oWndParent 	= $oWnd;
		$this->cId					= $cId;
		$this->nTop					= $nTop;
		$this->nLeft				= $nLeft;
		$this->nWidth				= $nWidth;
		$this->nHeight			= $nHeight;					
		$this->cColor				= $cColor;	
			
		if ( gettype( $oWnd ) == 'object' ) {

			// $oWndParent = $oWnd;
			$oWnd->AddControl( $this );			// $this->oWndParent
		}

  }
 
  function __destruct() {  }	
	
		/*	
		public function GetDefaultClass( $cControl = '') {
	
			$cClass = '';
	
			if ( gettype( $this->oWndParent ) == 'object' ) {	
		
				if ( array_key_exists( $cControl, $this->oWndParent->aClass ) ) {
			
					$cClass = $this->oWndParent->aClass[ $cControl ];
				
				} 
			}
		
			return $cClass;	
		}
		*/

	public function AddVarJS( $name = '', $value = '' ) {
		if ( !empty($name) && !empty($value) ) {
			$this->aVarJS[] = [ "name" => $name, "value" => $value ];
		}
	}

	// Adicion Albeiro Valencia
	public function DefVariablesJS() {

		$aVars = $this->aVarJS;
		$len = count( $aVars );

		if ( $len > 0 ) {
			$cJS = PHP_EOL;
			foreach( $aVars as $item ) { 
				$cJS .= 'var ' . $item["name"] . " = " . $item["value"] . ";" . PHP_EOL;
			}
			ExeJS( $cJS ); 
		}

	}

	public function DefClass() {

		$cHtml = '';
	
		if ( !empty( $this->cClass ) ) {
			$cHtml .= 'class="' . $this->cClass . '" ';	
		} 						
			
		return $cHtml;	
	}	
	
	public function SetData( $cKey = '', $cValue = '' ) {
	
		$this->cData = $this->cData . ' ' . $cKey . '="' . $cValue . '" ';
	}

	public function Datas() {	
		
		$cHtml  = 'data-control="' . $this->cControl . '" ';		
		$cHtml .= 'data-message="' . $this->cMessage . '" ';

		if ( !empty( $this->bWhen ) )
			$cHtml .= 'data-when="' . $this->bWhen . '" ';		
		
		if ( !empty( $this->cTooltip ) )
			$cHtml .= 'title="' . $this->cTooltip . '" ';		
			
		$cHtml .= $this->cData ;		
	
		return $cHtml;	
	}
	
	public function DefMessage( $cId = null ) {
	
		$cHtml = '';
	
		if ( !empty( $this->cMessage ) ) { 
		
			$cId = ( gettype( $cId ) == 'NULL' ) ? $this->cId : $cId ;
		
			$cFunction  = "var o = new TControl();" ;			

			$cFunction .= "o.InitCtrlMessage( '" . $cId . "' );" ;
			
			ExeJSReady( $cFunction, 'Activando ControlMessage -> ' . $cId );				
		}
			
		return $cHtml;	
	}
	
	public function DefTooltip() {
  
    if ( empty( $this->cTooltip ) ) {
				return null;
		}		
        
    //$cImage = 'images/info.png';
    $cImage = TWeb_SetupTooltips::$cImage;
        
    $cFunction  = '$( "#' . $this->cId . '" ).tooltip({ ';
    $cFunction .= '		position: { ';
    $cFunction .= '   my: "center top",';
    $cFunction .= '   at: "center bottom+5"';
    $cFunction .= '	},';
    //$cFunction .= '                show: { duration: "fast"  },';
    $cFunction .= '		show: ' . TWeb_SetupTooltips::$cShow . ',';
    //$cFunction .= ' hide: { effect: "hide"  },';
    $cFunction .= '   hide: ' . TWeb_SetupTooltips::$cHide . ',';

    if ( ! empty ( $cImage ) )
        $cFunction .= 'content: "<img style=\'vertical-align: middle;\' src=\'' . $cImage . '\' /><span style=\'vertical-align: middle;\'>&nbsp' . $this->cTooltip  . '</span> "';
    else
        $cFunction .= 'content: "' . $this->cTooltip . '</span> "';
    
    $cFunction .= ' });';             
		
    // $cFunction .= ' console.log( $( "#' . $this->cId . '" ).attr( \'title\' ));';             
        
    ExeJSReady( $cFunction );        
  }
	
	public function DefKeys() {

		$cFunction  = '$( "#' . $this->cId . '" ).bind("keydown", "", function (evt) { ' ;
		$cFunction .= '  switch( evt.keyCode ) {';
		
		$nTotalKey = count( $this->aKeys );
		
		for ( $i = 0; $i < $nTotalKey; $i++) {
		
			$nKey 		= $this->aKeys[$i][0];
			$cAction 	= $this->aKeys[$i][1];
			$cId_Jump 	= $this->aKeys[$i][2];
			
			$cFunction .= '  case ' . $nKey . ':';	
			$cFunction .= '    ' . $cAction . '; ' ;	
			$cFunction .= '    evt.preventDefault();' ;	
			
			if ( !empty( $cId_Jump ) ) {
				$cFunction .= '      $( "#' . $cId_Jump . '" ).focus(); ';
			}

			$cFunction .= '    break;' ;				
		}
		
		$cFunction .= '  }';
		$cFunction .= '});' ;

		ExeJS( $cFunction );		
	}
	
	public function StyleBorder() {	

		$cHtml = '';
			
		if ( $this->lBorder ) {
		
			$cHtml .= 'border: 1px ';
			
			if ( $this->cBorderType == '' ) {
				$cHtml .= 'solid; border-color: black; ';
			} else {
				$cHtml .= $this->cBorderType . '; '  ;				
				$cHtml .= 'border-style: ' . $this->cBorderType. '; '  ;
			}
		}
		
		return $cHtml;
	}
	
	public function StyleDim() {
	
		$cHtml  = 'margin:0px; ';
	
		if ($this->nTop >= 0 ) 			
			$cHtml .= 'top:' . $this->nTop .'; ';
			
		if ($this->nLeft >= 0 ) 			
			$cHtml .= 'left:' . $this->nLeft . '; ';		
		
		if ($this->nWidth > 0 ) 			
			$cHtml .= ' width: ' . $this->nWidth  . '; ';
			
		if ($this->nHeight > 0 ) 			
			$cHtml .= ' height:' . $this->nHeight . '; ';

		if ($this->nBottom >= 0 ) 			
			$cHtml .= ' bottom:' . $this->nBottom . '; ';				
			
		if ($this->nRight >= 0 ) 			
			$cHtml .= ' right:' . $this->nRight . '; ';				
			
		return $cHtml;	
	}
	
	public function StyleCss() {
	
		$cHtml = '';
	
		if ( $this->lHide ) 
			$cHtml .= 'display:none; ';		
	
		if ( ! empty( $this->cCss ) )
			$cHtml .= $this->cCss;
			
		return $cHtml;
	
	}
	
	public function StyleColor() {	
		
		$cHtml = '';
		
		if ( !empty( $this->cBrush ) )
			$cHtml .= 'background: url( ' . $this->cBrush . ')" ';	
		else if ( !empty( $this->cBackground ) )
			$cHtml .= 'background: url( ' . $this->cBackground . ') center center no-repeat; background-size: cover; '; 	
		else if ( !empty( $this->cColor ) )
			$cHtml .= 'background-color:' . $this->cColor . '; '; 

		return $cHtml;
	}	
	
	//	Styles ...
	
	public function SetBorderInset() {
		
		$this->lBorder		= true;	
		$this->cBorderType 	= 'inset white';	
	}
	
	public function SetBorderOutset() {
		
		$this->lBorder		= true;	
		$this->cBorderType 	= 'outset white';	
	}
	
	public function SetShadow( $nDeep = 3, $cColor = 'grey' ) {
		$this->nDeepShadow = $nDeep; 
		$this->cColorShadow = $cColor; 
	}
	
	//	----------------------------------------------------------------------------
	
	public function StyleShadow() {
	
		$cHtml = '';
		
		if ( $this->nDeepShadow > 0 ) {
			//$cHtml = 'box-shadow: 3px 3px 3px gray; ';
			$cHtml = 'box-shadow: ' . $this->nDeepShadow . 'px ' .  $this->nDeepShadow . 'px '.  $this->nDeepShadow . 'px  ' . $this->cColorShadow . '; ';
		}
		
		return $cHtml;
	}
	
	
	public function StyleFont() {
	
		$oFont = null;
		$cHtml = '';
		
		if ( gettype( $this->oFont ) == 'object' )	{
			$oFont = $this->oFont ;
		} else if ( gettype( $this->oWndParent ) == 'object' && gettype( $this->oWndParent->oFont ) == 'object') {
			$oFont = $this->oWndParent->oFont ;			
		}
		
		if ( $oFont ) {
		
			if ( !empty( $oFont->cFaceName ) )
				$cHtml .= "font-family:" . $oFont->cFaceName . "; ";	//	$cHtml .= "font-family:'" . $oFont->cFaceName . "'; ";
				

			if ( $oFont->lItalic ) 	
				$cHtml .= 'font-style: italic; '; 
			  else
				$cHtml .= 'font-style: normal; ';  		
				
			if ( $oFont->nSize > 0 ) 				
				$cHtml .= 'font-size: ' . $oFont->nSize . 'px; ';  	
				
			if ( $oFont->lBold ) 						
				$cHtml .= 'font-weight: bold; ';  		
			  else 	
				$cHtml .= 'font-weight: normal; ';  		
				
			if ( !empty( $oFont->nClrFore ) ) 				
				$cHtml .= 'color: ' . $oFont->nClrFore . '; ';  

			if ( $oFont->lUnderline ) 				
				$cHtml .= 'text-decoration: underline; ';  				
				
			if ( !empty( $oFont->nClrBack ) ) 									
				$cHtml .= 'background-color: ' . $oFont->nClrBack  . '; '; 		
		}			
		
		return $cHtml;
	}		
	
	public function OnClick() {
	
		$cHtml = '';

		if ( !empty( $this->bClick ) ) {
			$cHtml = 'onClick="' . $this->bClick . '" ';
		} else if ( !empty( $this->hRef ) ) {
			$cHtml = 'onClick="window.location.href=\'' . $this->hRef . '\'" ';		
		}
		
		return $cHtml;	
	}
	
	public function OnChange() {
	
		$cHtml = '';
	
		if ( !empty( $this->bChange ) ) {
			$cHtml = 'onchange="' . $this->bChange . '" ';			
		}		
		
		return $cHtml;	
	}	
	
	
	public function OnMessage() {
		
	}
	
	public function SetClass( $cClass = '' ) { $this->cClass = $cClass ;	}	
	public function SetCss( $cCss = '' ) { $this->cCss .= $cCss ;	}	
	public function SetAction( $bAction = '' ) { $this->bAction = $bAction; }
	public function SetClick( $bClick = '' ) { $this->bClick = $bClick; }
	public function SetFont( $oFont ) { $this->oFont = $oFont;	}
	public function SetBrush( $cBrush = '' ){ $this->cBrush = $cBrush ;	}
	public function SetBackground( $cBackground = '' ){ $this->cBackground = $cBackground ;	}
	
	public function SetColor( $cColor = null, $cBackground = null ) {
		
		if ( $cColor )		
			$this->cColor = $cColor;
			
		if ( $cBackground )
			$this->cBackground = $cBackground;			

	}
		
	public function SetKey( $nKey = 0, $cAction = '' , $cId_Jump = '') {
		$this->aKeys[] = array( $nKey, $cAction, $cId_Jump );
	}	
		
	//	O bien especificamos el ancho de control 'width' con un valor en positivo 
	//	o la distancia hasta el ancho total del panel 'right' en negativo.	
	
	public function SetWidth( $nWidth = 0 ) {
		if ( gettype( $nWidth ) == 'integer' ) {
		
			if ( $nWidth < 0 ) {
				$this->nWidth = 0;
				$this->nRight = Abs( $nWidth );			
			}						
		}			
	}
	
	//	O bien especificamos la altura de control 'height' con un valor en positivo 
	//	o la distancia hasta la altura total del panel 'bottom' en negativo.
	
	public function SetHeight( $nHeight = 0 ) {
	
		if ( gettype( $nHeight ) == 'integer' ) {
		
			if ( $nHeight < 0 ) {
				$this->nHeight = 0;
				$this->nBottom = Abs( $nHeight );			
			}						
		}			
	}
	
	/*
		+------+-------+-------+
		| nTop |nHeight|nBottom| 
		+------+-------+-------+
		|      |   X   |   X   |
		+------+-------+-------+
		|  X   |   X   |       |
		+------+-------+-------+
		|  X   |       |   X   |
		+------+-------+-------+			
	*/
	public function SetBottom( $nBottom = null, $nHeight = null ) {	
	
		if ( gettype( $nBottom ) == 'integer' ) {		
		
			$this->nBottom = $nBottom;	
			
			if ( gettype( $nHeight ) == 'integer' ) {
		
				$this->nTop 	= null;
				$this->nHeight 	= $nHeight;			
			} else {
			
				$this->nHeight 	= null;			
			}
		} else if ( $nBottom == '100%' ) {

			$this->nTop 	= ( gettype( $this->nTop ) !== 'NULL' ) ? $this->nTop : 0;
			$this->nHeight 	= null;		
			$this->nBottom 	= 0;		
		}		
	}
	
	public function SetRight( $nRight = null, $nWidth = null ) {	
	
		if ( gettype( $nRight ) == 'integer' ) {		
		
			$this->nRight = $nRight;	
			
			if ( gettype( $nWidth ) == 'integer' ) {
		
				$this->nLeft 	= null;
				$this->nWidth 	= $nWidth;			
			} else {
			
				$this->nWidth 	= null;			
			}
		} else if ( $nRight == '100%' ) {

			$this->nLeft 	= ( gettype( $this->nLeft ) !== 'NULL' ) ? $this->nLeft : 0;
			$this->nWidth 	= null;		
			$this->nRight 	= 0;		
		}		
	}	
		
	public function CoorsUpdate() {			

		$this->nTop 	= $this->Unit( $this->nTop );
		$this->nLeft	= $this->Unit( $this->nLeft );
		$this->nWidth	= $this->Unit( $this->nWidth );
		$this->nHeight	= $this->Unit( $this->nHeight );		
		$this->nRight	= $this->Unit( $this->nRight );		
		$this->nBottom	= $this->Unit( $this->nBottom );		
	}
	
	// 	Se trata	de cuando pase una coordenada, saber si se especifica unidades que podran ser
	// 	% o px. Si en la variable hay unidad (p.e. 100px o 100%), la funcion devuelve la misma var. Si en la variable 
	// 	no existe unidades devolveremos la variable + px , que es la unidad que tratamos por defecto	
	
	public function Unit( $uVar ) {
	
		//if ( gettype( $uVar ) == 'NULL'  || $uVar == 'auto' )
		//	return 'auto';
	
		if ( strpos( $uVar, '%' ) !== false ) 
			return $uVar;
		else if ( strpos( $uVar, 'px' ) !== false ) 
			return $uVar;
		else 
			return $uVar . 'px';				
	}
	

}

//************************************************************************//

class TWindow extends TPanel {

    function __construct( $cId = null, $nTop = 0, $nLeft = 0, $nWidth = '100%', $nHeight = '100%', $cColor = CLR_HGRAY ) {
	
		parent::__construct( null, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor );

		$this->cClass		= 'tweb_window';	
		$this->cControl = 'twindow';							
	}		
}

//************************************************************************//

class TDialog extends TPanel {

    function __construct( $cId = null, $nWidth = 300, $nHeight = 200, $cColor = CLR_HGRAY ) {
		
		/*
		*  El method POST traera el valor definido desde core.dialog.js en la funcion TPanel (Linea 851)
		*  en la creacion del objeto ===> oParam._id = cId ;
		*/
		if ( isset( $_POST[ '_id' ] ) ) {
			$cId = 'tdialog_' . $_POST[ '_id' ];
		}	else  {
			$cId = ( empty( $cId ) ) ? time() : $cId ;
		}	
			
		parent::__construct( null, $cId, 0, 0, $nWidth, $nHeight, $cColor );

		$this->cClass			= 'tweb_dialog';	
		$this->cControl 	= 'tdialog_container';			
		
		//	El dialogo (panel) NO estará visible. Cuando se active el diálogo lo cambia 
		
		$this->lHide  			= true;			
	}
}

//************************************************************************//

class TTabs extends TPanel {

    function __construct( $cId = null, $cType = '', $cColor = CLR_HGRAY ) {
	
		parent::__construct( null, $cId, 0, 0, '100%', '100%', $cColor );

		$this->cClass			= 'tweb_tabs';	
		$this->cControl 	= 'ttabs';												
		
		$cType = strtolower( $cType );
		
		switch ( $cType ) {
		
			case 'folder':
				$this->cPosition = 'initial';		//	Folders...											
				break;
				
			case 'accordion':
				$this->cPosition = 'relative';		//	Accordions...
				break;	

			default:
		
				$this->cPosition = 'initial';				
								
		}				
	}		
}

//************************************************************************//

class TPanel extends TControl {

  private $aControls	 = [];
	private $nControls	 = 0;
	private $PanelFooter = [ "action" => null, "params" => null ];

	public  $aDefaults	= [];
	public  $aClass 		= [];
	public  $aDlgs      = [];	
	public  $cId_Init  	= '';	

	//-------------------------------------

	function __construct( $oWnd, $cId, $nTop = 0, $nLeft = 0, $nWidth = null, $nHeight = null, $cColor = '' ) 
	{
		$nWidth  = TDefault( $nWidth , TWEB_PANEL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_PANEL_HEIGHT_DEFAULT );		

		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  );
		
		$this->cClass		= 'tweb_panel';	
		$this->cControl = 'tpanel';			
	}
	
	//-------------------------------------

	public function AddDialog( $cId = '', $cFunction = '' ) {
		$this->aDlgs[] = array( $cId, $cFunction ) ;	
	}	
	
	//-------------------------------------

	public function AddControl( $oControl ) {
		$this->aControls[] = $oControl ;
		$this->nControls++ ;
	}	

	//-------------------------------------

	public function SetDefault( $cControl = '', $cProperty = '' , $uValue = null ) {	
	
		if ( array_key_exists( $cControl,  $this->aDefaults ) ) 
			$aProp = $this->aDefaults[ $cControl ];	
		else
			$aProp = [];
			
		$aProp[ $cProperty ] = $uValue;
		
		$this->aDefaults[ $cControl ] = $aProp;				
	}	
	
	//-------------------------------------

	public function InitDefault( $o ) {
		if ( $this->cId !== -1 ) {	//	Si NO es un control estático
		
			$cControl = $o->cControl;		
		
			if ( array_key_exists( $cControl,  $this->aDefaults ) ) {
			
				$aProp 	= $this->aDefaults[ $cControl ];		

				foreach ($aProp as $key => $value) {		

					$uValue 	= $aProp[ $key ];
			
					switch ( $key ) {
						case 'SetClass()':  		 $o->cClass = $uValue; break;
						case 'SetBorderInset()': $o->SetBorderInset(); break;
						case 'SetShadow()':  		 $o->SetShadow(); break;
						case 'SetFont()':  			 $o->SetFont( $uValue ); break;
					}
				}			
			}		
		}
	}

	//-------------------------------------

	public function footer( $action = null, $params = null ) {

		$this->PanelFooter["action"] = $action;
		$this->PanelFooter["params"] = $params;

	}

	//-------------------------------------

	public function Activate() {
	
		$this->CoorsUpdate();	

		$cHtml	= '<div id="' . $this->cId . '" name="' .  $this->cId . '" ';
			
		$cHtml .= $this->DefClass();	
		$cHtml .= $this->Datas();	
		
		//	STYLE -----------------------------------------
		
		$cHtml .= 'style="position: ' . $this->cPosition . '; ';
		
			$cHtml .= $this->StyleDim();
			$cHtml .= $this->StyleBorder();			
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleFont();
			$cHtml .= $this->StyleCss();		
			$cHtml .= $this->StyleShadow();		
		
			$cHtml .= 'overflow: ' . $this->cOverflow . '; ' ;	
		
			//	Això pot ser important. Quan hi han estils a vegades tardan milésimes en 
			//	refrescarse. En canvi si ocultem la finestra, en el moment q estigui
			//	la finestra preparada la posem visible i desapareix l'efecte...
		
				//$cHtml .= 'display:none; ' ;	
			
		//	-------------------------------------------------------------------------

		$cHtml .= $this->StyleBorder();

		//	if ( !empty( $this->cColor ) and  empty( $this->cClass ) )
		//	if ( !empty( $this->cBackground ) )
		//		$cHtml .= 'background:' . $this->cBackground . '; '; 		

		if ( $this->lHide ) {
			$cHtml .= 'display:none; ';		
		}	
		
		$cHtml .= '"> ' ;			

		echo $cHtml;	

		//	Pintado de Diálogos definidos en el TPanel ---------------------------------------------
		
		$nLen = count( $this->aDlgs );

		for ( $i = 0; $i < $nLen; $i++) {				

			$cId 	= $this->aDlgs[$i][0];
			$cFunc 	= $this->aDlgs[$i][1];
	
			if ( !empty( $cFunc ) ) {
			
				if ((int) function_exists( $cFunc ) > 0) {	
		
					call_user_func( $cFunc, $this ) ;					

					if ( $this->cId_Init !== $cId ) {
						$cJS 	 = '$( "#' . $cId . '" ).css( "display", "none" );';
						ExeJS( $cJS );	
					}				
					
				} else { 					
           //	$cHtml .= $cCode ;	
				}
			}
		}			
		
		
		//	Pintado de Controles definidos en el TPanel --------------------------------------------
		for ( $i = 0; $i < $this->nControls; $i++) {
		
			$o = $this->aControls[$i] ;	
			
			$o->Activate();		
		}			

		// Agrega el footer
		$this->CreateFooter();
		
		echo '</div>';
		
		$this->DefKeys();
		
		//	Activar EvalWhen...
		
		// $cFunction = 'TWeb_EvalWhen();';		//Pendent de revisar...

		// ExeJSReady( $cFunction );

		// Agrega las variables publicas a JavaScript
		$this->DefVariablesJS(); 

	}
	
	//-------------------------------------

	public function InitMessage( $bFunction = '' ) {
	
		$cFunction  = "var o = new TControl();" ;
		//$cFunction .= "o.InitMessage( '" . $bFunction . "' );" ;

		$cFunction .= "o.InitMessage( " . $bFunction . " );" ;
		
		ExeJSReady( $cFunction, 'Activando Message' );		
	}
	
	//-------------------------------------

	public function InitSignal( $bFunction = '' ) {	
	
		$cFunction  = "var o = new TControl();" ;
		$cFunction .= "o.InitSignal( '" . $bFunction . "' );" ;
		
		ExeJSReady( $cFunction, 'Activando Signal' );		
	}	

	//-------------------------------------

	private function CreateFooter() {

		$action = $this->PanelFooter["action"];
		$params = $this->PanelFooter["params"];

    	if ( $action != null )  {
      	if ( function_exists( $action ) ) {
        		if ( $params ) {
          		call_user_func_array( $action, [ $params ] );
        		} else {
          		call_user_func( $action );
        		}  
      	} else {
        		var_dump("Error : No Existe la Funcion PHP : " . $action );
        		exit;
      	}  
		 }	

   }

}
/*	---------------------------------------------------------------------------
	La clase TDiv pretende ser un TPanel pero sin ninguna clase ni css, pero
	con la capacidad de crear objetos dentro del div usando la misma manera 
	que se usa en toda la libreria. El control del div será a cargo del usuario
	via aplicación de su propio css... 
	--------------------------------------------------------------------------- */	

class TDiv extends TControl {

  private $oWnd				= null;
	private	$aControls	= [];
	private $nControls	= 0;
	public  $aDlgs      = [];	
	public  $cId_Init  	= '';	
	
  function __construct( $oWnd, $cId, $cClass = '' ) {		

		parent::__construct( $oWnd, $cId  );
		
		$this->cClass		= $cClass;	
		$this->cControl 	= 'tdiv';			
	}
	
	public function AddDialog( $cId = '', $cFunction = '' ){
		$this->aDlgs[] = array( $cId, $cFunction ) ;	
	}	
	
	public function AddControl( $oControl ){
		$this->aControls[] = $oControl ;
		$this->nControls++ ;
	}

	public function InitDefault() {}
	
	public function Activate() {
		
		$cHtml	= '<div id="' . $this->cId . '" name="' .  $this->cId . '" ';	
		$cHtml .= 'data-control="tdiv" ';	
		
			if ( !empty( $this->cClass ) )
				$cHtml .= 'class="' . $this->cClass . '" ';
				
			//	Si hay controles en el interior del DIV marcaremos la position: relativa para
			//	que se situen correctament. Si el usuario no lo quiere ya lo podrá cambiar
			//	posteriormente via css...
				
				if ( $this->nControls > 0 ) 
					$cHtml .= 'style="position:relative; ';
		
		$cHtml .= '"> ' ;			

		echo $cHtml;	

		//	Pintado de Diálogos definidos en el TPanel ---------------------------------------------
		
		$nLen = count( $this->aDlgs );

		for ( $i = 0; $i < $nLen; $i++) {				

			$cId 	= $this->aDlgs[$i][0];
			$cFunc 	= $this->aDlgs[$i][1];
	
			if ( !empty( $cFunc ) ) {
			
				if ((int) function_exists( $cFunc ) > 0) {	
		
					call_user_func( $cFunc, $this ) ;					

					if ( $this->cId_Init !== $cId ) {
						$cJS 	 = '$( "#' . $cId . '" ).css( "display", "none" );';
						ExeJS( $cJS );	
					}				
					
				} else { 					
            //	$cHtml .= $cCode ;	
				}
			}
		}					
		
		//	Pintado de Controles definidos en el TDiv --------------------------------------------
		
		for ( $i = 0; $i < $this->nControls; $i++) {
		
			$o = $this->aControls[$i] ;	
			
			$o->Activate();		
		}			
		
		echo '</div>';
	}
}

//************************************************************************//

class TBar extends TControl 
{
	public  $oWnd				= false;
	private	$aControls	= [];
	private $nControls	= 0;
	public  $nBtnWidth  = 0;
	public  $cColorBg   = '';
	
	private $cBtnColor  = '';
	private $cBtnFocus  = '';
	private $cBtnHover  = '';
	
	public function __construct( $oWnd , $cId = null, $nTop = 0, $nLeft = 0, $nWidth = null, $nHeight = null )
	{  
	
		$nWidth  = TDefault( $nWidth , '100%' );
		$nHeight = TDefault( $nHeight, 'auto' );	
		// $nHeight = TDefault( $nHeight, TWEB_BAR_HEIGHT_DEFAULT );	
		
		$this->cClass  		= 'tweb_bar';
		$this->cControl  	= 'tbar';
		$this->oWnd 		  = $oWnd;
		$this->cId  		  = $cId;
		$this->nTop 		  = $nTop;
		$this->nLeft 		  = $nLeft;
		$this->nWidth 		= $nWidth;
		$this->nHeight		= $nHeight;
		$this->nBtnWidth	= TWEB_BAR_HEIGHT_DEFAULT;
		
		if ( gettype( $oWnd ) == 'object' ) {
			$oWnd->AddControl( $this );			// $this->oWndParent
		}		
	}

	public function AddControl( $oControl )
	{
		$this->aControls[] = $oControl ;
		$this->nControls++ ;
	}	
	
	public function AddButton( $cId, $cPrompt, $bClick = '', $cImage = '', $cTooltip = '' )
	{
		$oBtn = new TButton( $this, $cId, 0, 0 , $cPrompt, $bClick );
		$oBtn->cImage  		 = $cImage; // 'images.app/go.png';
		$oBtn->cClass 		 = 'tweb_btnbar';		
		$oBtn->cControl		 = 'tbtnbar';		
		$oBtn->cPosition	 = '';		
		$oBtn->nHeight		 = '35px';		
		$oBtn->nWidth		   = $this->nBtnWidth;		
		$oBtn->cStyleColor = $this->cBtnColor;
		$oBtn->cStyleFocus = $this->cBtnFocus;
		$oBtn->cStyleHover = $this->cBtnHover;
		
		return $oBtn;				
	}

	public function AddButtonFiles( $cId, $cPrompt, $bClick = '', $cImage = '', $lMultiple = true  )
	{
	  $oBtn = new TButton( $this, $cId, 0, 0 , $cPrompt, $bClick );
		$oBtn->cImage  		 = $cImage; // 'images.app/go.png';
		$oBtn->cClass 		 = 'tweb_btnbar';		
		$oBtn->cControl		 = 'tbtnbar';		
		$oBtn->cPosition	 = '';		
		$oBtn->nHeight		 = null;		
		$oBtn->nWidth		   = $this->nBtnWidth;		
		$oBtn->lFiles 		 = true;		
		$oBtn->lMultiple	 = $lMultiple;		
		$oBtn->cStyleColor = $this->cBtnColor;
		$oBtn->cStyleFocus = $this->cBtnFocus;
		$oBtn->cStyleHover = $this->cBtnHover;
		
		return $oBtn;				
	}

	public function SetBackColor( $cColor = '' ) { 
		$this->cColorBg = $cColor; 
	}

	// Albeiro Valencia
	public function SetColor( $cCrlText = '', $cClrBack = '' ) {
		
		$this->cBtnColor = '';

		if ( !empty($cCrlText) ) {
			$this->cBtnColor .= 'color : ' . $cCrlText . ' !important;';
		}

		if ( !empty($cClrBack) ) {
			$this->cBtnColor .= 'background : ' . $cClrBack . ' !important;';
		}

	}

	public function SetColorfocus( $cCrlText = '', $cClrBack = '' ) {
		
		$this->cBtnFocus = '';

		if ( !empty($cCrlText) ) {
			$this->cBtnFocus .= 'color : ' . $cCrlText . ' !important;';
		}

		if ( !empty($cClrBack) ) {
			$this->cBtnFocus .= 'background : ' . $cClrBack . ' !important;';
		}

	}

	public function SetColorhover( $cCrlText = '', $cClrBack = '' ) {
		
		$this->cBtnHover = '';

		if ( !empty($cCrlText) ) {
			$this->cBtnHover .= 'color : ' . $cCrlText . ' !important;';
		}

		if ( !empty($cClrBack) ) {
			$this->cBtnHover .= 'background : ' . $cClrBack . ' !important;';
		}

	}

	
	public function Activate() {

		$this->CoorsUpdate();
	
		$cHtml	= '<div id="' . $this->cId . '" name="' .  $this->cId . '" ';		
		
		$cHtml .= $this->DefClass();								
		$cHtml .= $this->Datas();

		$cHtml .= 'style="';			
		$cHtml .= $this->StyleDim();			
		$cHtml .= $this->StyleCss();			
		$cHtml .= '"';			
		
		$cHtml .= '>';		
		
		$cHtml .= '<img class="tweb_bar_separator_init" src="' . TWEB_PATH_IMAGES . '/separator.gif"';
		$cHtml .= ' height="' . $this->nHeight . '"' ;
		$cHtml .= ' style="position: absolute; ';
		$cHtml .= ' left: 0px ; width: 7px ;height: ' . $this->nHeight . 'px;';		
		$cHtml .= ' overflow: hidden;">' ;		
		
		echo $cHtml;

		for ( $i = 0; $i < $this->nControls; $i++ ) {		

			$o = $this->aControls[$i] ;
			
			$o->Activate();			
		}
		
		$cHtml = '</div>';
			
		echo $cHtml;			

		// background color
		if ( !empty($this->cColorBg) ) {
			$cStyle  = "width : 100% !important;";
			$cStyle .= "padding-left: 3px !important;";
      // $cStyle .= "border-top-left-radius: 10px;";
    	// $cStyle .= "border-top-right-radius: 10px;";
    	$cStyle .= "background: " . $this->cColorBg . " !important;";
			$cStyle .= "border: 1px solid " . $this->cColorBg . " !important";

			$cJS  = '<script>' . PHP_EOL;
			$cJS .= '	 $(".tweb_bar").attr("style", "' . $cStyle . '");' . PHP_EOL; 
			$cJS .= '</script>';
			
			echo $cJS;
		}

	}	
	
	public function Separator(){	
		
		$cHtml  = '<div class="tweb_bar_separator" >' ;		
		$cHtml .= '<img src="' . TWEB_PATH_IMAGES . '/separator_simple.gif" height="' . $this->nHeight . '" ' ;		
		$cHtml .= 'style="width: 3px;height: ' . $this->nHeight . 'px; ';		
		$cHtml .= 'overflow: hidden;"/>' ;
		$cHtml .= '</div>' ;

		$o = new TCode( $this, $cHtml );			
	}	
	
	public function NewItem( $cId, $cCaption = '', $nWidth = 150, $cImage = '' ) {
	
		$oItem = [ 'id' => $cId, 'caption' => $cCaption, 'width' => $nWidth, 'image' => $cImage ];
		
		$this->aItems[] = $oItem;	
	}		

}

//************************************************************************//

class TButton extends TControl {

	public $uValue		= '';
	public $cCssImg		= 'padding-top: 3px;display: block;margin: auto;';
	public $cCssText	= 'margin: auto;display: block;text-align: center;';
	public $lFiles		= false;
	public $cIdFiles	= 'files';	// nombre del contenedor de los ficheros (name del input)
	public $cAccept  	= ''; 		  // 'image/*'; // ".gif,.png",  http://www.w3schools.com/tags/att_input_accept.asp  	
	public $lMultiple	= false;
	public $cIcon 		= '';

	public $cStyleColor   = '';
	public $cStyleFocus   = '';
	public $cStyleHover   = '';

	private $aItems 	  = [];
	private $lOnlyImage = false;
	private $marginTop  = '';

	public function __construct( $oWnd , $cId = null, $nTop = 0, $nLeft = 0, $cCaption = 'Button',  
	                             $bClick = '' , $nWidth  = null, $nHeight = null, $cImage = '', $cClass = '' ) {  
	
		$nWidth  = TDefault( $nWidth , TWEB_CONTROL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );			

		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cControl 	= 'tbutton';	
		$this->cClass			= TDefault($cClass, 'tweb_button');													
		$this->cCaption		= $cCaption;													
		$this->bClick			= $bClick;													
		$this->nHeight		= $nHeight;																							
		$this->nWidth			= $nWidth;	
		$this->cImage			= $cImage;	

		//if ( gettype( $oWnd ) == 'object' ) 
		//	$oWnd->InitDefault( $this );		
	}	

	public function OnlyImage() {
		$this->cAccept 		= "image/*";
		$this->lOnlyImage	= true;
  }

	public function SetCssText( $cCss = '' ) { 
		$this->cCssText = $cCss; 
	} 
	
	public function SetCssImg( $cCss = '' ) { 
		$this->cCssImg = $cCss; 
	} 
	
	public function AddItem( $cPrompt, $cFunc, $cImg = '' ) {
		$this->aItems[] = array( 'prompt' => $cPrompt, 'func' => $cFunc, 'image' => $cImg );
	}

	// Albeiro Valencia
	public function SetColor( $cCrlText = '', $cClrBack = '' ) {
		
		$this->cStyleColor = '';

		if ( !empty($cCrlText) ) {
			$this->cStyleColor .= 'color : ' . $cCrlText . ' !important;';
		}

		if ( !empty($cClrBack) ) {
			$this->cStyleColor .= 'background : ' . $cClrBack . ' !important;';
		}

	}

	public function SetColorfocus( $cCrlText = '', $cClrBack = '' ) {
		
		$this->cStyleFocus = '';

		if ( !empty($cCrlText) ) {
			$this->cStyleFocus .= 'color : ' . $cCrlText . ' !important;';
		}

		if ( !empty($cClrBack) ) {
			$this->cStyleFocus .= 'background : ' . $cClrBack . ' !important;';
		}

	}

	public function SetColorhover( $cCrlText = '', $cClrBack = '' ) {
		
		$this->cStyleHover = '';

		if ( !empty($cCrlText) ) {
			$this->cStyleHover .= 'color : ' . $cCrlText . ' !important;';
		}

		if ( !empty($cClrBack) ) {
			$this->cStyleHover .= 'background : ' . $cClrBack . ' !important;';
		}

	}
	
	public function Activate() {

		// CODE HTML -----------------
	
		$cHtml = '';
	
		//	Check si es tipo files...
		
		if ( $this->lFiles ) {
		
			$this->cControl 	= 'tbuttonfiles';
		
			$cId_BtnFiles = '_buttonfiles_' . $this->cId;

			$cHtml  .= '<input type="file" name="' . $this->cIdFiles . '" id="' .  $cId_BtnFiles . '" ';	
			
			if ( !empty( $this->cAccept ) )
				$cHtml  .= ' accept="' . $this->cAccept . '" ';
			
			if ( $this->lMultiple )
				$cHtml  .= ' multiple ';
				
			$cHtml .= 'style="display: none;" '; 
			
			//if ( !empty( $this->bAction ) )
			//	$cHtml .= 'onChange="javascript: return ' . $this->bAction . ';" ';
			
			
			if ( !empty( $this->bClick ) ) {
				// $cHtml .= 'onChange="javascript: return ' . $this->bClick . ';" ';
				$cHtml .= 'onChange="' . $this->bClick . ';" ';
			}			
				
			$cHtml .= '/>'; 	
			
			$this->bClick = "$( '#" . $cId_BtnFiles . "').trigger('click') ";

		
		}
		
		//	Creamos Button...
		
		$this->CoorsUpdate();

		$cHtml  .= '<button type="button" id="' . $this->cId . '" ' ;

		switch ( $this->cControl ) {
		
			case 'tbtnbar':
			
				if ( $this->cControl == 'tbtnbar' ) {
				
					if ( $this->cAlign == 'right' ) 
						$this->cClass = 'tweb_btnbar tweb_btnbar_right';
					else
						$this->cClass = 'tweb_btnbar';
				} 

				$this->marginTop = 'margin-top: 2px;';
				
				break;													
		
		}
		
		if ( $this->lFiles ) {		
			$this->cControl = 'tbuttonfiles';
		}
				
		$cHtml .= $this->DefClass();
		$cHtml .= $this->Datas();

		//	STYLE -----------------------------------------			
		
		if ($this->cPosition == '') {
			$this->cPosition = 'relative';
		}  

		$cHtml .= 'style="position: ' . $this->cPosition . '; overflow: hidden; ';

		$cHtml .= $this->marginTop;

		if ( $this->cControl === 'tbutton' || $this->cControl == 'tbuttonfiles' ) {
			$cHtml .= 'left : ' . $this->nLeft . ';';
			$cHtml .= 'top : ' . $this->nTop . ';';
		}
			
		if ($this->nWidth > 0 ) {			
			$cHtml .= ' width: ' . $this->nWidth  . '; ';
		}	
			
		if ($this->nHeight > 0 ) {			
			$cHtml .= ' height:' . $this->nHeight . '; ';
		}	
			
		// $cHtml .= $this->StyleDim();
		$cHtml .= $this->StyleBorder();
		$cHtml .= $this->StyleColor();			
		$cHtml .= $this->StyleCss();		
				
		$cHtml .= '" '; 
		
		//	ATRIBUTTES ------------------------------------
		
		if ( $this->lDisabled ) {
			$cHtml .= 'disabled ' ;
		}	
				
		if ( !empty( $this->cTooltip ) ) {
			$cHtml .= 'title="' . $this->cTooltip . '" ';						
		}		
			
		//	EVENTS ----------------------------------------

		$cHtml .= $this->OnClick();									
		$cHtml .= $this->OnMessage();	
		
		//	-----------------------------------------------		
		
		$cHtml .= '>';

		if (!empty( $this->cImage ) ){		

			//$cHtml .= '<img class="btn_img" ' ;
			$cHtml .= '<img  ' ;
			$cHtml .= 'style="';
			$cHtml .= $this->cCssImg;
			$cHtml .= '" ';
			$cHtml .= 'src="' . $this->cImage . '" />';

		}		
		
		$cHtml .= '<p ';
		$cHtml .= 'style="margin:0px; ';
		$cHtml .= $this->cCssText;
		$cHtml .= '" ';				
		$cHtml .= '>' ;
		
		//	Fonts Awesome...
		
		if ( $this->cIcon ) {
			$nPos = strpos( $this->cIcon, '<i' );
				
			if ( $nPos === false ) 
				$cHtml .= '<i class="' . $this->cIcon . '"></i>&nbsp';
			else
				$cHtml .= $this->cIcon . '&nbsp';			
		}
			
		//	------------------------------		
		
		$cHtml .= $this->cCaption ;
		$cHtml .= '</p>' ;
		
		$cHtml .= '</button>' ;			
		
		echo $cHtml;

		// CODE STYLE  -----------------
		
		$cStyle  = '<style>' . PHP_EOL;

		if ( !empty($this->cStyleColor ) ) {
			$cStyle .= '#' . $this->cId . '{' . PHP_EOL;
			$cStyle .= $this->cStyleColor 		. PHP_EOL;
			$cStyle .= '}' 										. PHP_EOL;
		}

		if ( !empty($this->cStyleFocus) ) {
			// $cStyle .= '#' . $this->cId . ', #' . $this->cId . ':focus {' . PHP_EOL;
			$cStyle .= '#' . $this->cId . ':focus {' . PHP_EOL;
			$cStyle .= $this->cStyleFocus 		. PHP_EOL;
			$cStyle .= '}' 										. PHP_EOL;
		}

		if ( !empty($this->cStyleHover) ) {
			$cStyle .= '#' . $this->cId . ':hover {' . PHP_EOL;
			$cStyle .= $this->cStyleHover 		. PHP_EOL;
			$cStyle .= '}' 										. PHP_EOL;
		}

		$cStyle .= '</style>' . PHP_EOL;

		echo $cStyle;

		// CODE JAVASCRIPT -----------------

		$this->DefMessage();
		$this->DefTooltip();

		$nItems = count( $this->aItems );
		
		if ( $nItems > 0 ) {
		
			$cJS  = 'var menu = new []; ';
		
			for ($i = 0; $i < $nItems; $i++) {
			
				$aItem = $this->aItems[$i];
			
				$cJS .= 'menu[' . $i . '] = { ' ;
				$cJS .= 'name: "' . $aItem[ 'prompt' ] . '" ,';
				$cJS .= 'img: "'  . $aItem[ 'image'  ] . '" ,';
				$cJS .= 'fun: '  . $aItem[ 'func'  ] . ' ';
				$cJS .= '}; ' . PHP_EOL ;			
			}		
			
			$cJS .= "var options = { triggerOn: '" . TWeb_SetupBtnItems::$triggerOn 		. "', ";
			$cJS .= "		displayAround: '" 	. TWeb_SetupBtnItems::$displayAround 	. "', ";
			$cJS .= "		position: '" 		. TWeb_SetupBtnItems::$position 		. "', ";
			$cJS .= "		verAdjust: " 		. TWeb_SetupBtnItems::$verAdjust 		. ", ";
			$cJS .= "		mouseClick: '" 	. TWeb_SetupBtnItems::$mouseClick 		. "' ";
			$cJS .= "}; " . PHP_EOL ;
										 
			$cJS .= "$('#" . $this->cId . "' ).contextMenu( menu, options );" . PHP_EOL;
			
			ExeJSReady( $cJS );

		}	
		
		if ( $this->lFiles && $this->lOnlyImage ) {
				
			$cJS  = '<script>';
			$cJS .= 'function file_is_image() { ' . PHP_EOL;
			$cJS .= '	 var fileInput = document.getElementById("' . $cId_BtnFiles . '");' . PHP_EOL; 
			$cJS .= '  var filePath  = fileInput.value;' . PHP_EOL; 
			$cJS .= '  var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;' . PHP_EOL; 
			$cJS .= '  if (!allowedExtensions.exec(filePath)) {' . PHP_EOL; 
			$cJS .= '     fileInput.value = ""' . PHP_EOL; 
			$cJS .= '  		return false;' . PHP_EOL; 
			$cJS .= '  } else { ' . PHP_EOL;
			$cJS .= '	    return true;' . PHP_EOL;  
			$cJS .= '  }' . PHP_EOL;
			$cJS .= '}' . PHP_EOL;
			$cJS .= '</script>';  

			echo $cJS;
		}
		
	}
}

//************************************************************************//
// https://stackoverflow.com/questions/34135249/html-convert-string-to-multiple-lines
class TSay extends TControl {
  public $lMemo 					= false;
	public $lVerticalAlign 	= true;
	public $lScrollbar 			= false;
	public $cIcon 					= '';
	public $cClass          = 'tweb_say';      
	public $cAlign          = 'left';
	
	public function __construct( $oWnd , $cId = '', $nTop = 0, $nLeft = 0, $cCaption = '', 
															 $nWidth = null, $nHeight = null, $oFont = null, $cClass = '' ) {  	
	
		$nWidth  = TDefault( $nWidth , TWEB_CONTROL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass			= $cClass;  
		$this->cControl 	= 'tsay';					
		$this->cCaption		= $cCaption;		
		$this->oFont			= $oFont;					
		
		if ( gettype( $oWnd ) == 'object' ) {
			$oWnd->InitDefault( $this );
		}	

	}	
	
	public function Activate() {

		$cHtml = '<div class="'.$this->cClass.'" style="position: absolute; display: table-cell;';

		if ( empty( $this->cAlign ) ) {
			$cHtml .= 'text-align:' . $this->cAlign . ';';
		}

		$cHtml .= $this->StyleDim();	
		$cHtml .= $this->StyleFont();

		if ( $this->lMemo ) {
			$cHtml .= 'word-wrap: break-word;';
		}

		$cHtml .= '">';

		$cHtml .= '<p id="' . $this->cId . '" class="' . $this->cClass . '" ';
		
		$cHtml .= ' style="margin: 0;"';
		$cHtml .= ' data-control="' . $this->cControl . '" ';		
		$cHtml .= ' data-message="' . $this->cMessage . '" ';

		$cHtml .= $this->OnClick();	

		$cHtml .= '>' ; 
		
		//	Fonts Awesome...
		if ( $this->cIcon ) {
			$nPos = strpos( $this->cIcon, '<i' );
			if ( $nPos === false ) {
				 $cHtml .= '<i class="' . $this->cIcon . '"></i>&nbsp';
			} else {
			 	$cHtml .= $this->cIcon . '&nbsp';			
			}	
		}
				
		$cHtml .= $this->cCaption  ; 
		
		$cHtml .= '</p>' ; 

		$cHtml .= '</div>';

		// console_php( $cHtml );
		
		echo $cHtml ;	

		$cJS  = '<script>'  . PHP_EOL;
		$cJS .= '$(function() {' . PHP_EOL;
    $cJS .= "  var o = new TControl();"  . PHP_EOL;			
    $cJS .= "  o.InitCtrlMessage( '" . $this->cId . "' );"  . PHP_EOL;
		$cJS .= "});"  . PHP_EOL;
		$cJS .= '</script>';
		
		echo $cJS;

	}

}

//************************************************************************//

class TGet extends TControl {

	public  $lMemo				= false;
	public  $lEditor			= false;
	public  $uValue				= '';
	public  $lPassword		= false;
	public  $nMaxLength		= 0;
	public  $lDate				= false;
	public  $lTime				= false;

	public  $lIconAction	= false;
	public  $cIconImage   = 'fa-search';     
	public  $cIconColor   = 'black';     
	public  $cIconAction	= '';	

	public  $lBtnAction		= false;	
	public  $cBtnImage		= 'fa-search';	
	public  $cBtnAction		= '';	
	public  $cBtnColor		= '#f9f7fb';
	public  $cBtnBack			= '#089fef';

	public  $cPicture			= false;
	public  $cPlaceHolder	= '';
	public  $cType				= 'text';

	private $cColorFocus   = '';
	private $bKeyEnter     = '';
	
	public function __construct( 	$oWnd , $cId = null, $nTop = 0, $nLeft = 0, $cCaption = '', 
																$nWidth = null, $nHeight = null, $cPicture = '', $bChange = '',
																$cClass = '' ) {  
		
		$nWidth  = TDefault( $nWidth , TWEB_CONTROL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );	

		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass			= $cClass;													
		$this->cControl		= 'tget';													
		$this->cCaption		= $cCaption;																							
		$this->cAlign			= 'left';
		$this->cPicture		= $cPicture;
		$this->bChange		= $bChange;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}

	public function SetPlaceHolder( $cPlaceHolder = '' ) { $this->cPlaceHolder = $cPlaceHolder; }

	public function SetDate( $lDate = true, $cPicture = '99/99/9999' ) { 
		$this->lDate 	= $lDate ;	
		$this->cPicture = $cPicture ;			
	}

	public function SetTime( $lTime = true ) { $this->lTime = $lTime; }
	public function SetPicture( $cPicture = '' ) { $this->cPicture = $cPicture ;	}	
	public function SetChange( $bChange = '' ) { $this->bChange = $bChange ;	}
	public function SetColorfocus( $cColor = '' ) { $this->cColorFocus = $cColor; }
	
	public function BtnAction( $cAction = '', $cIcon = '', $cColor = '', $cBack = '' ) {	
		$this->lBtnAction = true;
		$this->cBtnAction = $cAction;
		if ( !empty($cIcon) && substr($cIcon, 0, 2) == "fa" ) {
			$this->cBtnImage  = $cIcon;
		}
		if ( !empty($cColor) ) {
			$this->cBtnColor  = $cColor;
		}	
		if ( !empty($cBack) ) {
			$this->cBtnBack  = $cBack;
		}		
	}
	
	public function IconAction( $cAction = '', $cIcon  = '', $cColor = '' ) {	
		$this->lIconAction = true;
		$this->cIconAction = $cAction;
		if ( !empty($cIcon) && substr($cIcon, 0, 2) == "fa" ) {
			$this->cIconImage  = $cIcon;
		}	
    if ( !empty($cColor) ) {
			$this->cIconColor  = $cColor;
		}	
	}

	public function SetKeyEnter( $cAction = '' ) {
		$this->bKeyEnter = $cAction;
	}

	private function EventKeyEnter() {

		if ( !empty($this->bKeyEnter) ) {
			$cJS  = PHP_EOL;
			$cJS .= '$("#' . $this->cId .'").keypress( function(e) {'     . PHP_EOL;
			$cJS .= '	  var keycode = (e.keyCode ? e.keyCode : e.which);' . PHP_EOL;
			$cJS .= '	  if (keycode == 13) {'                             . PHP_EOL;
			$cJS .= '	  	 e.preventDefault();'                           . PHP_EOL;
			$cJS .= '      ' . $this->bKeyEnter . ';'                     . PHP_EOL;    
			$cJS .= '	  }'                                                . PHP_EOL;
			$cJS .= '});'                                                 . PHP_EOL;	
			ExeJs($cJS);
		} 
		
	}

	public function Activate() {
	
		$this->createHTML();
		
		$this->DefMessage();		
		$this->DefTooltip();		

		
		//	Javascript PICTURE -----------------------------------------------------------
		$cFunction = '';
			
		if ( !empty( $this->cPicture ) ){		
			$cFunction .= '$("#' . $this->cId . '" ).mask("' . $this->cPicture . '"); ';
		}		
			
		if ( $this->lDate ) {
			//$cFunction  .= '$( "#' . $this->cId . '" ).datepicker({ firstDay: 1  });' ;
			$cFunction  .= '$( "#' . $this->cId . '" ).datepicker();' ;
		} else if ( $this->lTime ) {
			$cFunction  .= '$( "#' . $this->cId . '" ).timepicker();' ;
		}

		if ( !empty( $cFunction ) ) {
			ExeJSReady( $cFunction );
		}				
		
		//************************************************************************//----------------------
		
		$this->DefKeys();

		$this->EventKeyEnter();

		if ( $this->lEditor ) {
		
			$cFunction = 'TWeb_InitEditor( "' . $this->cId . '"); ';			
			
			ExeJS( $cFunction );
			/*
			if ( $this->lReadOnly || $this->lDisabled ) {			
				$cFunction = '$( "#' . $this->cId . '" ).sceditor("' . 'instance' . '").readOnly( true );' ;
				ExeJSReady( $cFunction );
			}
			*/			
		}

		// https://makitweb.com/how-to-add-important-to-css-property-with-jquery/
		if ( $this->cColorFocus ) {
			
			 $cStyle  = 'width: 100%; height: 20px; text-align: left;';
			 $cStyle .= 'background-color:' . $this->cColorFocus . ' !important';		
	
			 $cJS  = '$(function() {' . PHP_EOL;
			 $cJS .= '  $("input").on("focusin", function() {' . PHP_EOL;
			 $cJS .= '		$(this).attr("style", "' . $cStyle . '");' . PHP_EOL; 
			 $cJS .= '  });' . PHP_EOL;
			 $cJS .= '  $("input").focusout(function(){' . PHP_EOL;
			 $cJS .= '	  $(this).css("background-color", "blue");' . PHP_EOL;
			 $cJS .= '  });' . PHP_EOL;
			 $cJS .= '});' . PHP_EOL;
			 ExeJS( $cJS );

		}

	}	

	private function createHTML() {

		if ( $this->lEditor )
			$this->lMemo = true;

		if ( empty( $this->cClass ) ) 
			$this->cClass = ( $this->lMemo ) ? 'tweb_getmemo' : 'tweb_get';
			
		if ( $this->lBtnAction ){		
			$nBtnLeft 	= $this->nLeft;
			$nBtnWidth 	= $this->nWidth;			
		}
				
		$this->cControl = ( $this->lMemo ) ? 'tgetmemo' : 'tget';
		
		$this->CoorsUpdate();
	
		if ( $this->lPassword )
			$cType  = 'password' ;
		else 
			$cType = $this->cType;	

		// inicio contenedor del input   
		$cHtml  = '<div '; 		

		$cHtml .= 'class="tweb_get_container" ';
		$cHtml .= 'style="position: absolute;display:flex;';
		$cHtml .= 'overflow: hidden; background:transparent; ';		
		$cHtml .= $this->StyleDim();				
		
		if ( $this->nDeepShadow > 0 )
			$cHtml .= 'border-radius: ' . $this->nDeepShadow . 'px; ';
		
		$cHtml .= '" ';	
		$cHtml .= '>';	

		// begin div para el input y el icon Search
		// $cHtml .= '<div style="float:left;width: ' . $this->nWidth . '">';
		$cHtml .= '<div style="float:left;width: 100%">';  // correccion 09/09/2021
		
		if ( $this->lMemo || $this->lEditor ) {
			$cHtml  .= '<textarea id="' . $this->cId    . '" ';
		} else {
			$cHtml  .= '<input type="' . $cType . '" id="' . $this->cId . '" ' ;	
		}		

		$cHtml .= $this->DefClass();		
		$cHtml .= $this->Datas();			
		
		//	STYLE -----------------------------------------
		$cHtml .= ' style="width: 100%;height: ' . $this->nHeight . ';';

		$cHtml .= $this->StyleColor();		
		$cHtml .= $this->StyleFont();

		if ( $this->lHide ) 
			$cHtml .= 'display:none; ';		
					
		if ( ! $this->lMemo )	
			$cHtml .= 'text-align: ' . $this->cAlign . '; ';

		$cHtml .= '" '; 
		
		//	ATRIBUTTES ------------------------------------		
		
		if ( $this->lDisabled )
			$cHtml .= ' disabled ' ;
				
		if ( $this->lReadOnly )
			$cHtml .= ' readonly ' ;			

		if ( $this->nMaxLength > 0 ) 
			$cHtml .= 'maxlength="' . $this->nMaxLength . '" ';

		if ( !empty( $this->cPlaceHolder ) )
			$cHtml .= 'placeholder="' . $this->cPlaceHolder . '" ';
			
		//	EVENTS ----------------------------------------
		
		$cHtml .= $this->OnClick();	
		$cHtml .= $this->OnChange();	

		//	-----------------------------------------------							
		
		if ( $this->lMemo )	{	
			$cHtml .= '>';
			$cHtml .= $this->cCaption;
			$cHtml .= '</textarea>';
		} else {
			$cHtml .= 'value ="' . $this->cCaption . '" ';
			$cHtml .= '>';
		}
		
		//	ICON ------------------------
		if ( $this->lIconAction ) {		
			$cHtml .= '<a class="tweb_get_search"'; 
			$cHtml .= ' style="position: absolute;margin-left: -17px;margin-top: 8px;">';
			$cHtml .= '<i class="fa ' . $this->cIconImage . '"'; 

			if ( !empty( $this->cIconAction ) ) {
				$cHtml .= ' onClick="javascript: ' . $this->cIconAction . '"';				
			}	

			if ( !empty( $this->cIconColor ) ) {
				$cHtml .= ' style="color: ' . $this->cIconColor . '"' ;
			}

			$cHtml .= '"></i>';
			$cHtml .= '</a>';
		}

		// end div input and icon search 
		$cHtml .= '</div>';

		//	BUTTON --------------------
		if ( $this->lBtnAction ) {	

			$cHtml .= '<a class="tweb_get_button"'; 
			$cHtml .= 'style="float: right;margin-left: 2px;border-radius : 4px;' ;
			$cHtml .= ' background-color: ' . $this->cBtnBack . ';'; 
			// $cHtml .= ' width: 25px; height: auto;padding: 3px 2px 2px 3px ">';
			$cHtml .= ' width: 25px; height: auto;padding: 6px ">';
			$cHtml .= '<i class="fa ' . $this->cBtnImage . '"';
			
			if ( !empty( $this->cBtnAction ) ) {
				$cHtml .= 'onClick="javascript: ' . $this->cBtnAction . ';" ';				
			}	

			if ( !empty( $this->cBtnColor ) ) {
				$cHtml .= ' style="color: ' . $this->cBtnColor . '"' ;
			}

			$cHtml .= '"></i>';
			$cHtml .= '</a>';
				
		}		

		// final contenedor del input   
		$cHtml .= '</div>';
		
		echo $cHtml;

	}
		
}

//************************************************************************//--

class TDatePicker extends TControl {

  public function __construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight ) {
    
    parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
    
    $this->cId     = TDefault($cId, 'tdatepicker' . time());
    $this->nTop    = TDefault($nTop, '0px');
    $this->nLeft   = TDefault($nLeft,'0px');
    $this->nWidth  = TDefault($nWidth, '200px');
    $this->nHeight = TDefault($nHeight, '30px');

    $this->cControl = 'tdatepicker';

  }

  public function Activate() {
    
    $cHtml  = $this->createHTML();
    $cHtml .= $this->createJS();

    echo $cHtml;
  }
  
  private function createHTML() {

    // Creando Contendeor
    $cHtml  = '<div id="tweb-datepicker" ';
    $cHtml .='   style="position : absolute';
    $cHtml .= '  ;top : ' . $this->nTop;
    $cHtml .= '  ;left : ' . $this->nLeft;
    $cHtml .= '  ;width : auto';
    $cHtml .= '  ;height : auto';
    $cHtml .= '">';
    $cHtml .= '<input type="text" id="' . $this->cId . '" class="date-input" readonly="readonly">';
    $cHtml .= '</div>';

    // Creando Dialog Modal
    $cHtml .= '<!-- Dialog Modal UI-Materials-->';
    $cHtml .= '<div class="m-datepicker-overlay">';
    $cHtml .= ' <div class="m-datepicker">';
    $cHtml .= '   <div class="m-datepicker-header">';
    $cHtml .= '     <small>SELECCIONE FECHA</small>';
    $cHtml .= '     <div>';
    $cHtml .= '       <p class="m-datepicker-status">';
    $cHtml .= '         <span>Mon</span>,';
    $cHtml .= '         <span>Nov 17</span>';
    $cHtml .= '       </p>';
    // $cHtml .= '       <button>';
    // $cHtml .= '         <img src="' . TWEB_PATH_LIBS  . '/datepicker/icons/edit.png" alt="pencil">';
    // $cHtml .= '       </button>';
    $cHtml .= '     </div>';
    $cHtml .= '   </div>';
    $cHtml .= '   <div class="m-datepicker-body">';
    $cHtml .= '   </div>';
    $cHtml .= '   <div class="m-datepicker-footer">';
    $cHtml .= '     <button class="m-datepicker-cancel">Cancel</button>';
    $cHtml .= '     <button class="m-datepicker-ok">Ok</button>';
    $cHtml .= '   </div>';
    $cHtml .= ' </div>';
    $cHtml .= '</div>';

    return $cHtml;

  }

  private function createJS() {

    $cJS  = '<script>';
    $cJS .= PHP_EOL;
    $cJS .= 'var oDate = new TDatePicker("' . $this->cId . '");' . PHP_EOL;
    $cJS .= 'var oCtrl = new TControl();' . PHP_EOL;
    $cJS .= 'oCtrl.ControlInit("' . $this->cId . '", oDate, "tdatepicker");' . PHP_EOL;
    $cJS .= PHP_EOL;
    $cJS .= '</script>';
    return $cJS;
  }

}

//************************************************************************//--

Class THidden extends TControl {

	public  $uValue		= '';
	
	public function __construct( $oWnd , $cId = null, $uValue = ''  ) {  

		parent::__construct( $oWnd, $cId );
	
		$this->cClass		= '';													
		$this->cControl		= 'thidden';																	
		$this->uValue		= $uValue;																	
	} 
	
	public function Activate() {			

		$cHtml  = '<input type="hidden"  ';
		$cHtml .= $this->Datas();		
		$cHtml .= ' id="' . $this->cId . '" name="' . $this->cId  . '" value="' . $this->uValue . '" >';
		
		echo $cHtml ;				
	}	
}

//************************************************************************//

class TRadio extends TControl {

	private $aItems		= [];
	private $nOption 	= 0;	

	public function __construct( $oWnd , $cId = null, $nOption = 0, $bChange = '' ) {  
	
		parent::__construct( $oWnd, $cId );
	
		$this->cClass		= 'tweb_radio';
		$this->cControl		= 'tradio';
		$this->nOption		= $nOption  ;		
		$this->bChange		= $bChange  ;		
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function AddItem( $cCaption = '', $nTop = 0, $nLeft = 0, $nWidth = null, $nHeight = null ) {
	
		$nWidth  = TDefault( $nWidth , TWEB_CONTROL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );		
	
		$this->aItems[] = array( 'cId' => $cCaption, 
								'nTop' => $nTop,
								'nLeft' => $nLeft, 
								'nWidth' => $nWidth,
								'nHeight' => $nHeight
								);		
	}
	
	public function Activate() {

		$cHtml 	= '';
		
		$nItems = count( $this->aItems );
		//$aMessage = array( $nItems );
		
		for ( $i = 0; $i < $nItems; $i++) {
		
			$aItem   = $this->aItems[$i];
			$cIndex  = $this->cId . '_' . ( $i + 1 );
			
			//$aMessage[ $i ] = $cIndex;

			$nTop 		= $this->Unit( $aItem[ 'nTop' ] );
			$nLeft		= $this->Unit( $aItem[ 'nLeft' ] );
			$nWidth		= $this->Unit( $aItem[ 'nWidth' ] );
			$nHeight	= $this->Unit( $aItem[ 'nHeight' ] );					
			
			$cHtml  .= '<div id="' . $this->cId . '" ';

			$cHtml .= $this->Datas();	
			
			$cHtml  .= 'style="position: absolute; box-sizing: border-box; overflow:hidden; ';
			
				$cHtml .= 'top: ' . $nTop . '; ';
				$cHtml .= 'left: ' . $nLeft . '; ';
				$cHtml .= 'width: ' . $nWidth . '; ';
				$cHtml .= 'height: ' . $nHeight . '; ';
				
				$cHtml .= $this->StyleBorder();			
				$cHtml .= $this->StyleColor();		
				$cHtml .= $this->StyleCss();				
			
			$cHtml .= '"> ' ;					

			$cHtml .= '<input type="radio" name="' . $this->cId . '" id="' . $cIndex . '" value="' . ( $i + 1 ) . '" ';				
			
			$cHtml .= $this->DefClass();
			
			if ( ( $i + 1 ) == $this->nOption ) {
				$cHtml .= ' checked ';
			}
			
			if ( $this->lDisabled )
				$cHtml .= ' disabled ' ;
			
			if ( $this->lReadOnly )
				$cHtml .= ' readonly ' ;
				
			$cHtml .= $this->OnChange();				
			$cHtml .= $this->OnClick();				
			
			$cHtml .= '>' ;
			
			$cHtml .= '<label class="' . $this->cClass . '" for="' . $cIndex . '" name="'  . $this->cId . '" ';
			
			$cHtml  .= 'style="';	

				$cHtml .= $this->StyleFont();
				$cHtml .= $this->StyleCss();				

			$cHtml .= '" >' . $aItem[ 'cId' ] . '</label>';
			
			$cHtml .= '</div>';						
			
		}										
		
		echo $cHtml ;
		
		$this->DefMessage( $this->cId );		
		
		/*for ( $i = 0; $i < $nItems; $i++) {
		
			$this->DefMessage( $aMessage[ $i ] );		
		}*/

	}		
	
}

//************************************************************************//

class TCheckbox extends TControl {

	public $lChecked	= false;

	public function __construct( $oWnd , $cId = null, $nTop = 0, $nLeft = 0, $lChecked = false, 
	                             $cCaption = '', $nWidth  = null, $nHeight = null  ) {  
	
		$nWidth  = TDefault( $nWidth , TWEB_CONTROL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );	
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass		= 'tweb_checkbox';
		$this->cControl		= 'tcheckbox';
		
		$this->lChecked		= $lChecked  ;
		$this->cCaption		= $cCaption  ;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		//	Este control necesita estar dentro de un div para que funcione Ok		
		
		$cHtml  = '<div ';
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
		$cHtml .= $this->StyleDim();			
		$cHtml .= $this->StyleBorder();
		$cHtml .= $this->StyleColor();				
		$cHtml .= '" ';	
		
		//	EVENTS ----------------------------------------

			if ( ! empty( $this->bClick ) ) {
				$cHtml .= $this->OnClick();	
			} elseif ( ! empty( $this->bChange ) ) {
				$cHtml .= $this->OnChange();
			}

		//	-----------------------------------------------
		
		$cHtml .= '> ';	
		
		
		$cHtml .= '<input type="checkbox" id="' . $this->cId . '" name="' . $this->cId . '" ' ;
			
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();		
 		
			if ( $this->lChecked )
				$cHtml .= ' checked ';
			
			if ( $this->lDisabled )
				$cHtml .= ' disabled ';					
			
		
		$cHtml .= ' >';
		
		if ( !empty( $this->cCaption ) ){ 
		
			$cIdLabel = '__tweblabel__' . $this->cId ;
			
			$cHtml .= '<label class="' . $this->cClass . '" for="' . $this->cId. '" ';			
			$cHtml .= 'id="' . $cIdLabel . '" ';			
			
			$cHtml .= 'style="';			
			$cHtml .= $this->StyleFont();
			$cHtml .= $this->StyleCss();				
			$cHtml .= '" >';									
			
			$cHtml .= '&nbsp;'; 
			$cHtml .= $this->cCaption; 

			$cHtml .= '</label>';
		}
		
		
		$cHtml .= '</div>';				

		echo $cHtml ;		
	}				
	
}

//************************************************************************//

class TCombobox extends TControl {

	public $aItems 		= [];
	public $aItemsText	= [];
	public $cSelected	= '';
	public $cPosition	= '';
	public $lFilter 	= true;
	public $lSingle 	= true;
	public $lTop 		= false;
	public $nMaxHeight	= 250;
	public $lExtended	= true;
	
	public function __construct( $oWnd , $cId = null, $nTop = 0, $nLeft = 0, $aItems = [], 
	                             $aItemsText = [], $cSelected = '', $nWidth  = null, $nHeight = null  ) {  

		$nWidth  = TDefault( $nWidth , TWEB_CONTROL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );		
		
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass			= '';																									
		$this->cControl		= '';																									
		$this->aItems 		= $aItems  ;
		$this->aItemsText = $aItemsText  ;
		$this->cSelected 	= $cSelected  ;																						
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {
	
		if ( $this->lExtended ) {
			$this->cControl		= 'tcombobox_ex';
			$this->cClass		= 'tweb_combobox_ex';					
		} else {
			$this->cClass		= 'tweb_combobox';		
			$this->cControl		= 'tcombobox';	
		}
		
		$this->CoorsUpdate();
		
		$cHtml = '';
		
		//	Este control necesita estar dentro de un div para que funcione Ok
		
		if ( $this->lExtended ) {
		
			$cHtml  .= '<div ';

			//	STYLE -----------------------------------------	
			
				$cHtml .= 'style="position: absolute;';
				$cHtml .= 'box-sizing: border-box;';
				$cHtml .= 'display: flex;';				
			
				$cHtml .= $this->StyleDim();	
				
				//$cHtml .= 'z-index:1; ' ;			
				$cHtml .= 'overflow: visible;" >' ;
			
			//	------------------------------------------------

				$cHtml .= '<select class="' . $this->cClass . '" name="' . $this->cId . '" id="' . $this->cId . '" ';
				
				$cHtml .= $this->Datas();				
				
				if ( ! $this->lSingle )
					$cHtml .= 'multiple="multiple" ';			
				
				$cHtml .= 'style="position: absolute; top: 0px; left: 0px; width: 100%; ';
				
				$cHtml .= '" ';	
			
			//	ATRIBUTTES ------------------------------------				
			
				if ( $this->lReadOnly || $this->lDisabled )
					$cHtml .= ' disabled ';			
				
				
			$cHtml .= '>';	
		
		} else {
		
				$cHtml .= '<select class="' . $this->cClass . '" name="' . $this->cId . '" id="' . $this->cId . '" ';				
				
				$cHtml .= $this->Datas();						
				
				$cHtml .= 'style="position: absolute; ';			
				
				$cHtml .= $this->StyleDim();	
				
				$cHtml .= $this->StyleBorder();	
				$cHtml .= $this->StyleFont();	
				$cHtml .= $this->StyleColor();	
				$cHtml .= $this->StyleCss();	
				
				$cHtml .= '" ';	
			
			//	ATRIBUTTES ------------------------------------				
			
				$cHtml .= $this->OnChange();
				
				if ( $this->lReadOnly || $this->lDisabled )
					$cHtml .= ' disabled ';			
				
				
			$cHtml .= '>';						
		}
			
		//	ITEMS...	-----------------------------------
		
			$nItems	= count( $this->aItems );
			$lText	= count( $this->aItemsText ) == $nItems ? true : false ;		
			$aSelected = ( gettype( $this->cSelected ) == 'array' ) ? $this->cSelected : array( $this->cSelected );
			
			
			for ( $i = 0; $i < $nItems; $i++) {		
			
				$cHtml .= '<option value="' . $this->aItems[ $i ] . '"';
				if ( in_array( $this->aItems[ $i ], $aSelected ) )
					$cHtml .= ' selected' ;																	

				$cHtml .= '>';				
					
				$cHtml .= ( $lText ) ? $this->aItemsText[ $i ] : $this->aItems[ $i ];
				
					
				$cHtml .= '</option>' ;		
			}	

			$cHtml .= '</select>';	
			
			
		if ( $this->lExtended ) {
		
			$cHtml .= '</div>';								
		}
		
		echo $cHtml ;
		
		if ( $this->lExtended ) {		
		
			$cFilter  = '{ ';
			$cFilter .= 'filter: ' . (( $this->lFilter ) ? 'true' : 'false' ) . ', ';
			$cFilter .= 'single: ' . (( $this->lSingle ) ? 'true' : 'false' ) . ', ';
			$cFilter .= 'position: "' .(( $this->lTop ) ? 'top' : '' ) . '", ';		
			
			if ( !empty( $this->bClick ) )
				$cFilter .= 'onClick: function(view) { ' . $this->bClick . ' },';		
				
			$cFilter .= 'maxHeight: ' . $this->nMaxHeight . ' ';		
			$cFilter .= '}';

			$cFunction  = '$( "#' .  $this->cId . '" ).multipleSelect( ' . $cFilter . ' );';
			
			//ExeJS( $cFunction, 'Activando combobox multiple: ' . $this->cId );					
			ExeJS( $cFunction );					
		}
	}
	
}

//************************************************************************//

class TImage extends TControl {

	public  $lDisable   = true;
	public  $cImageZoom	= '';
	public  $lZoom		  = false;
	public  $cGallery	  = '';	
	
	public function __construct( $oWnd , $cId = null, $nTop = 0, $nLeft = 0, $cImage = '', 
	                             $nWidth  = null, $nHeight = null, $cImageZoom = '', 
															 $cCaption = '', $cClass = '' ) {  
	
		$nWidth  = TDefault( $nWidth , TWEB_IMAGE_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_IMAGE_HEIGHT_DEFAULT );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass		  = TDefault($cClass, 'tweb_image');	
		$this->cControl 	= 'timage';	
		$this->cCaption		= $cCaption;		
		$this->cImage 		= $cImage;
		$this->cImageZoom = $cImageZoom;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {

		$this->CoorsUpdate();
		
		$cHtml  = '<div id="' . $this->cId . '" ';
		
		$cHtml .= $this->DefClass();			
		$cHtml .= $this->Datas();

		//	STYLE -----------------------------------------		
		
		$cHtml .= 'style="position: absolute; ';
			
		$cHtml .= $this->StyleDim();		
		$cHtml .= $this->StyleBorder();
		$cHtml .= $this->StyleColor();			
		$cHtml .= $this->StyleCss();
		$cHtml .= $this->StyleShadow();			
		
		$cHtml .= '" ' ;
			
		//	EVENTS ----------------------------------------
		
		$cHtml .= $this->OnClick();	

		//	-----------------------------------------------				
			
			
		$cHtml .= '> ' ;
		
		//	ZOOM o BIG IMAGE -----------------------------
		
		
			if (!empty( $this->cImageZoom ) or ( $this->lZoom ) ){  
			
				if ( empty( $this->cImageZoom ) )
					$this->cImageZoom = $this->cImage;
				
				$cHtml .= '<a href="' . $this->cImageZoom . '" '; 
				
				if ( empty( $this->cGallery ) ){				
					$cHtml .= 'data-lightbox="fwimg_' . $this->cId . '" '; 
				} else {
					$cHtml .= 'data-lightbox="' . $this->cGallery . '" '; 
				}
				
				if ( !empty( $this->cCaption ) ){
					$cHtml .= 'data-title="' . $this->cCaption . '" ';
				}
				
				$cHtml .= ' >';
			}						
		
			//	IMAGEN
			
			//$cHtml .= '<span style="display:block;text-align:center;" >';

			$cHtml .= '<img src="' . $this->cImage . '" ';			
			$cHtml .= 'id="img_' . $this->cId .'" ';
			$cHtml .= 'style="position: absolute; ';
			$cHtml .= 'top: 0px; left: 0px; width: 100%; height:100%; ' ;
			
			if ( empty($this->cImage) && $this->lDisable ) {
				$cHtml .= 'display:none;';
			}
				
			$cHtml .= '">' ;
			//$cHtml .= '</span>' ;
			
			//	ZOOM o BIG IMAGE	

			if (!empty( $this->cImageZoom ) or ( $this->lZoom ) ) {			
				$cHtml .= '</a>';			
			}						
			
		$cHtml .= '</div>';			
	
		echo $cHtml;

		$this->DefMessage();				
	}
}

//************************************************************************//

class TLogo extends TControl {
	public $cClass_Image = 'tweb_logo_image';
	
	public function __construct( $oWnd , $cId = '1', $cImage = '' ) {  
	
		parent::__construct( $oWnd, $cId, 0, 0, '100%', '100%'  );
	
		$this->cClass		= 'tweb_logo';	
		$this->cControl 	= 'tlogo';	
		$this->cImage 		= $cImage;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {

		$this->CoorsUpdate();
		
		$cHtml  = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();			
			$cHtml .= $this->Datas();

		//	STYLE -----------------------------------------		
		
			$cHtml .= 'style="position: relative; ';
			
			//$cHtml .= $this->StyleDim();		
			//$cHtml .= $this->StyleBorder();
			//$cHtml .= $this->StyleColor();			
			//$cHtml .= $this->StyleCss();
			
			$cHtml .= '" ' ;
			
		//	EVENTS ----------------------------------------
		
			$cHtml .= $this->OnClick();	

		//	-----------------------------------------------				
			
			
		$cHtml .= '> ' ;

			$cHtml .= '<img src="' . $this->cImage . '" ' ;			

			
			$cHtml .= 'class="' . $this->cClass_Image . '" >';
			
		$cHtml .= '</div>';			
	
		echo $cHtml;				
	}
}

//************************************************************************//

class TFolder extends TControl {

	private	$aTabs		= [];
	private	$aControls	= [];
	private $nControls	= 0;
	public  $cIcon  	= '';


    public function __construct( $oWnd, $cId = null, $nTop = 10, $nLeft = 10, $nWidth = null, $nHeight = null, $cColor = CLR_HGRAY ) {     

		$nWidth  = TDefault( $nWidth , TWEB_FOLDER_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_FOLDER_HEIGHT_DEFAULT );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass		= 'tweb_folder';													
		$this->cControl		= 'tfolder';				
		$this->cColor	= $cColor ;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}    
	
    public function __destruct() {}
	
	public function AddTab( $cId = '', $cLabel = 'Label', $cFunction = '', $cIcon = '' ){
		$this->aTabs[] = array( $cId, $cLabel, $cFunction, $cIcon ) ;	
	}	
	
	public function AddControl( $oControl ){
		$this->aControls[] = $oControl ;
		$this->nControls++ ;
	}
	
	public function Activate() {
	
		$this->CoorsUpdate();		

		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
	
		
		$cHtml .= 'style="position: ' . $this->cPosition . '; ';
		$cHtml .= 'box-sizing: border-box; ';
		
			$cHtml .= $this->StyleDim();
			$cHtml .= $this->StyleBorder();			
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleFont();
			$cHtml .= $this->StyleCss();		
			
		//$cHtml .= 'overflow: hidden;"> ' ;			
		$cHtml .= 'overflow: ' . $this->cOverflow . ';"> ' ;			
		
		echo $cHtml; 		
		
//		Pintem 	les pestanyes...

		$nLen = count( $this->aTabs );
		
		$cHtml = '<ul>';
		
		$aType = [];
		
		for ( $i = 0; $i < $nLen; $i++) {		
		
			$cId 		= $this->aTabs[$i][0];
			$cPrompt 	= $this->aTabs[$i][1];
			$cFunction	= $this->aTabs[$i][2];
			$cIcon		= $this->aTabs[$i][3];
			
			//	Check if cFunction is a php file 

				$nPos = strrpos( $cFunction,'.',-1) ;

				if ( $nPos ) {				
					$cExt = strtolower( substr( $cFunction, $nPos + 1, strlen( $cFunction ) ) ); 				
				} else {
					$cExt = '';
				}							
			
				
				if ( $cExt == 'php' ) {
				
					$cType = 'php' ;
					
				} else if ( ((int) function_exists( $cFunction ) > 0) ) {
				
					$cType = 'function';
					
				} else { 
				
					$cType = 'code';
				}
				
			$aType[] = array( $cType, $cFunction );
			

			if ( $cType !== 'php' ) {

				$cHtml .= '<li><a href="#' . $cId . '" style=\'line-height: 16px;\'>'; 
				
				if ( !empty( $cIcon ) ){
				
					if ( substr( $cIcon, 0, 7 ) == 'ui-icon' ) {                    
						$cHtml .= '<span class="ui-icon ' . $cIcon . '"></span>';
					} else {
						$cHtml .= '<img style=\'vertical-align: middle;\' src=\'' . $cIcon . '\' />';                                                    
					}				
				}
				
				if ( !empty( $cPrompt ) ){
					$cHtml .= '&nbsp' . $cPrompt . '&nbsp&nbsp' ;	
				}
				
				$cHtml .= '</a></li>';				

			}  else {
			
				$cPhp 	= $cFunction;
				$cHtml .= '<li><a href="' . $cPhp . '">' . $cPrompt . '</a></li>';				
			}
		}	
		
		$cHtml .= '</ul>';		
		
		echo $cHtml; 
		
		$cHtml = '';

		$nLen = count( $aType );

		for ( $i = 0; $i < $nLen; $i++) {				

			$cType	= $aType[$i][0];
			
			switch ( $cType ) {
			
				case 'function':
					$cFunction = $aType[$i][1];
					
					call_user_func( $cFunction, $this );					
					
					break;
					
				case 'code':
					$cCode 	 = $aType[$i][1];
					$cHtml  .= $cCode;
					break;
			
				case 'php':
			
					// Pendiente...;
					break;
			}		
		
		}							

		$cHtml .= '</div>';			

		echo $cHtml;
		
//		--------------------------------------------------------------------------------------------		
	
		for ( $i = 0; $i < $this->nControls; $i++) {
		
			$o = $this->aControls[$i] ;
			
			$o->Activate();		
		}					
			
		
		echo '</div>';
		
		$cHtml  = '<script>';
		$cHtml .= '$(function() {';
		$cHtml .= '   $( "#' . $this->cId . '" ).tabs(); ' ;
		$cHtml .= ' });';
		$cHtml .= '</script>';
		
		$cHtml .= '<script>';
		$cHtml .= '$(function() {';
		$cHtml .= '   $( "#' . $this->cId . '" ).tabs({';
		$cHtml .= '		beforeLoad: function( event, ui ) {';
		$cHtml .= '            ui.jqXHR.error(function(oError) {';
		$cHtml .= '               console.log( oError );';
		$cHtml .= '               ui.panel.html( _( "_error_tab_load_php" )  );';
		$cHtml .= '               MsgError( oError.responseText );';
		$cHtml .= '				});';
		$cHtml .= ' 		},';
		$cHtml .= ' 	activate: function( event, ui ) {';
		$cHtml .= ' 	      var oPanel = ui.newPanel[0] ;';
		$cHtml .= ' 	      var cId = $(oPanel).attr("id");';
		$cHtml .= ' 	      var fn = window["' . $this->bChange . '"];';
		$cHtml .= ' 	      if (typeof fn === "function") {';
		$cHtml .= ' 	      	var fnparams = [ cId ];	';
		$cHtml .= ' 	      	fn.apply(null, fnparams );	';
		$cHtml .= ' 	      }';
		$cHtml .= ' 	      		';
		$cHtml .= ' 		}';
		$cHtml .= ' 	});';
//		$cHtml .= ' 	$( "#' . $this->cId . '" ).draggable({ disabled: true });';		
		$cHtml .= '});';
		$cHtml .= '</script>';
	
		echo $cHtml;					
		
	}

}

//************************************************************************//

class TProgressBar extends TControl {

	public $nValue	= 0;

	public function __construct( $oWnd , $cId = null, $nTop = 0, $nLeft = 0, 
	                             $nValue = 0, $nWidth  = null, $nHeight = null  ) {  
	
		$nWidth  = TDefault( $nWidth , TWEB_CONTROL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass		= 'tweb_progressbar';
		$this->cControl		= 'tprogressbar';
		$this->nValue		= $nValue;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {
		
		$this->CoorsUpdate();

		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
			$cHtml .= $this->StyleDim();			
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleCss();
			$cHtml .= $this->StyleShadow();					
		
		$cHtml .= '"> ';			
		
		$cHtml .= '</div>';				

		echo $cHtml ;		
		
//		Inicalitzem widget...
		
		$cFunction  = '$(document).ready(function() {' ;
		$cFunction .= "     var o = $( '#" . $this->cId  . "' ); ";		
		$cFunction .= "     o.progressbar( { value: 0, max: 100 } ); ";		
		$cFunction .= "     o.find( 'div.ui-progressbar-value' ).css( 'background', '" . $this->cColor . "'); ";			
		$cFunction .= '}); ' ;
		
		ExeJS( $cFunction );		
	}				
	
}

//************************************************************************//

class TGroup extends TPanel {

	private	$aControls	= [];
	private $nControls	= 0;
	public  $lChecked	= false;
	
	public function __construct( $oWnd , $cId = null, $nTop = 0, $nLeft = 0, 
	                             $cCaption = 'Group', $nWidth  = null, $nHeight = null  ) { 
	
		$nWidth  = TDefault( $nWidth , TWEB_GROUP_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_GROUP_HEIGHT_DEFAULT );	
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass			= 'tweb_group';
		$this->cControl		= 'tgroup';
		$this->cCaption		= $cCaption;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}
	
	public function AddControl( $oControl ){
		$this->aControls[] = $oControl ;
		$this->nControls++ ;
	}		
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
		$cHtml .= $this->DefClass();		
		$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box;';
		$cHtml .= 'margin: 0px;';
		$cHtml .= 'overflow: hidden;';
		
		$cHtml .= $this->StyleDim();			
		$cHtml .= $this->StyleBorder();
		$cHtml .= $this->StyleColor();
		$cHtml .= $this->StyleCss();		
		$cHtml .= $this->StyleShadow();				
			
		$cHtml .= '"> ';																		

		$cHtml .= '<fieldset style="top:0px;left:0px;width:100%;height:100%;box-sizing: border-box;border: 1px solid #434141;border-radius: 4px;">';
		
		if ( !empty( $this->cCaption ) ) {
			$cHtml  .= '<legend style="';
			$cHtml .= $this->StyleFont();			
			$cHtml  .= '">';
			$cHtml  .= $this->cCaption;						

			$cHtml  .= '</legend>';			
		}
			
		$cHtml  .= '</fieldset>';
		
		echo $cHtml;

		for ( $i = 0; $i < $this->nControls; $i++) {		

			$o = $this->aControls[$i] ;
			
			$o->Activate();				
		}
		
		$cHtml = '</div>';				

		echo $cHtml ;			
	}				
	
}

//************************************************************************//

class TCalendar extends TControl {
	public $nValue	= 0;
	public $bClick 	= '';
	
	public function __construct( $oWnd , $cId = null, $nTop = 0, $nLeft = 0, 
	                             $nValue = 0, $nWidth  = null, $nHeight = null  ) {  

		$nWidth  = TDefault( $nWidth , TWEB_CONTROL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );

		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass		= 'tweb_calendar';
		$this->cControl		= 'tcalendar';
		$this->nValue		= $nValue;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
	
			$cHtml .= $this->StyleDim();			
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleCss();
			$cHtml .= $this->StyleShadow();					
		
		$cHtml .= '"> ';			
		
		$cHtml .= '</div>';				

		echo $cHtml ;		
		
//		Inicalitzem widget...
		
		$cFunction  = '$(document).ready(function() {' ;
		$cFunction .= "     var o = $( '#" . $this->cId  . "' ); ";		
		$cFunction .= "     var aOptions = { ";		
			
		if ( !empty( $this->bClick ) )
			$cFunction .= "onSelect: " . $this->bClick . " ";		
		
		$cFunction .= "}; ";		
		
		$cFunction .= "     o.datepicker( aOptions ); ";				
		$cFunction .= '}); ' ;

		ExeJSReady( $cFunction );		

	}				
}

//************************************************************************//

class TBlock extends TPanel {

	private	$aControls	= [];
	private $nControls	= 0;
	
	public function __construct( $oWnd , $cId = null, $nHeight = null, $cColor = CLR_HGRAY) { 	
	
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );	

		parent::__construct( $oWnd, $cId, null, null, null, $nHeight, $cColor );
	
		$this->cClass	= 'tweb_block';
		$this->cControl	= 'tblock';
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}
	
	public function AddControl( $oControl ){
		$this->aControls[] = $oControl ;
		$this->nControls++ ;
	}		
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
		$cHtml .= $this->DefClass();		
		$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: relative; ';
		$cHtml .= 'box-sizing: border-box;';
		$cHtml .= 'margin: 0px;';
		
		if ( $this->lHide ) {
			$cHtml .= 'display:none;';
		}	
			
		$cHtml .= 'height: ' . $this->nHeight . '; ';
		$cHtml .= 'overflow: auto;';
							
		$cHtml .= $this->StyleBorder();
		$cHtml .= $this->StyleColor();
		$cHtml .= $this->StyleCss();							
		
		$cHtml .= '">';																		
		
		echo $cHtml;

		for ( $i = 0; $i < $this->nControls; $i++) {		

			$o = $this->aControls[$i] ;
			
			$o->Activate();				
		}
		
		$cHtml = '</div>';				

		echo $cHtml ;			
	}					
}

//************************************************************************//

class TBox extends TControl {

	public function __construct( $oWnd , $cId = null, $nTop = 0, $nLeft = 0, 
	                            $nWidth = null, $nHeight = null, $cColor = CLR_GRAY ) {

		$nWidth  = TDefault( $nWidth , TWEB_BOX_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_BOX_HEIGHT_DEFAULT );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  );
	
		$this->cClass		= 'tweb_box';
		$this->cControl		= 'tbox';
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
			$cHtml .= $this->StyleDim();			
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleCss();
			$cHtml .= $this->StyleShadow();		
		
		$cHtml .= '"> ';			
		
		$cHtml .= '</div>';				

		echo $cHtml ;			
	}					
}

// renderizar
// http://mozilla.github.io/pdf.js/
// https://stackoverflow.com/questions/19654577/html-embedded-pdf-iframe
class TFrame extends TControl {

	public $cSrc = '';

	public function __construct( $oWnd , $cId = null, $nTop = 0, $nLeft = 0, 
	                             $cSrc = '', $nWidth = null, $nHeight = null, $cColor = CLR_GRAY ) {

		$nWidth  = TDefault( $nWidth , TWEB_FRAME_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_FRAME_HEIGHT_DEFAULT );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  );
	
		$this->cClass		= 'tweb_frame';
		$this->cControl		= 'tframe';
		$this->cSrc			= $cSrc;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
			$cHtml .= $this->StyleDim();			
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleCss();
			$cHtml .= $this->StyleShadow();		
		
		$cHtml .= '"> ';			
		
		$cHtml .= '<iframe src="' . $this->cSrc . '" width="100%" height="100%"  frameborder="0" marginheight="0" >';	// scrolling=no
		$cHtml .= '</iframe>';								
		
		$cHtml .= '</div>';				

		echo $cHtml ;			
	}					
}


//	TObject
// 	Difference between iframe, embed and object elements --> https://stackoverflow.com/questions/16660559/difference-between-iframe-embed-and-object-elements

/*
http://www.ohlone.edu/org/webteam/proceduresnotes/objecttag.html
MIME Type 	File Type / Extension
text/html 	html, htm
application/msword 	doc (docx?)
application/pdf 	pdf
application/rtf 	rtf
application/vnd.ms-excel 	xls
application/vnd.ms-powerpoint 	ppt
application/x-director 	dcr
application/x-mspublisher 	pub
application/x-shockwave-flash 	swf
audio/mpeg 	mp3
text/plain 	txt
video/mpeg 	mpg, mpeg
video/quicktime 	mov, qt
video/x-msvideo 	avi
video/x-ms-wmv 	wmv
*/

//************************************************************************//

class TObject extends TControl {

	public $cSrc  = '';
	public $cType = '';

	public function __construct( $oWnd , $cId = null, $nTop = 0, $nLeft = 0, $cSrc = '', 
	                             $cType = '',  $nWidth = null, $nHeight = null, $cColor = CLR_GRAY ) {

		$nWidth  = TDefault( $nWidth , TWEB_OBJECT_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_OBJECT_HEIGHT_DEFAULT );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  );
	
		$this->cClass		= 'tweb_object';
		$this->cControl	= 'tobject';
		$this->cSrc			= $cSrc;
		$this->cType		= $cType;
		
		if ( gettype( $oWnd ) == 'object' ) {
			$oWnd->InitDefault( $this );		
		}

	}	
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
		$cHtml .= $this->DefClass();		
		$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
		$cHtml .= $this->StyleDim();			
		$cHtml .= $this->StyleBorder();
		$cHtml .= $this->StyleColor();		
		$cHtml .= $this->StyleCss();
		$cHtml .= $this->StyleShadow();				
		
		$cHtml .= '"> ';			
		
		$cHtml .= '<object data="' . $this->cSrc . '" type="' .  $this->cType .'" width="100%" height="100%" frameborder="0" marginheight="0" scrolling=no>';
		$cHtml .= '    <embed src="' . $this->cSrc . '" type="' .  $this->cType .'" width="100%" height="100%" frameborder="0" marginheight="0" scrolling=no/>';
		$cHtml .= '</object>';								
		
		$cHtml .= '</div>';				

		echo $cHtml ;			
	}					
}

//************************************************************************//

class TMaps extends TControl {

	public function __construct( $oWnd , $cId = null, $nTop = 0, $nLeft = 0, 
	                             $nWidth = null, $nHeight = null, $cColor = CLR_GRAY ) {

		$nWidth  = TDefault( $nWidth , TWEB_MAPS_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_MAPS_HEIGHT_DEFAULT );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  );
	
		$this->cClass		= 'tweb_maps';
		$this->cControl	= 'tmaps';
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	

	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
		$cHtml .= $this->DefClass();		
		$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
		$cHtml .= $this->StyleDim();			
		$cHtml .= $this->StyleBorder();
		$cHtml .= $this->StyleColor();		
		$cHtml .= $this->StyleCss();
		$cHtml .= $this->StyleShadow();			
		
		$cHtml .= '"> ';			
		
		$cHtml .= '</div>';				

		echo $cHtml ;	


	}					
}

//************************************************************************//

class TYoutube extends TControl {

	private $cSource  		= '';

	public function __construct( $oWnd , $cId = null, $nTop = 0, $nLeft = 0, 
	                             $cSource = null, $nWidth = null, $nHeight = null, $cColor = CLR_GRAY) {

		$nWidth  = TDefault( $nWidth , TWEB_YOUTUBE_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_YOUTUBE_HEIGHT_DEFAULT );		
		$cSource = TDefault( $cSource, '' );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  );
	
		$this->cClass		= 'tyoutube_maps';
		$this->cControl		= 'tyoutube';
		$this->cSource		= $cSource;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}

  public function SetVideo( $cSource = '') { $this->cSource = $cSource ; }		
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
		$cHtml .= $this->DefClass();		
		$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
		$cHtml .= $this->StyleDim();			
		$cHtml .= $this->StyleBorder();
		$cHtml .= $this->StyleColor();		
		$cHtml .= $this->StyleCss();
		$cHtml .= $this->StyleShadow();				
		
		$cHtml .= '"> ';

		$cHtml .= '<iframe width="100%" height="100%" ';
		$cHtml .= 'src="https://www.youtube.com/embed/' . $this->cSource . '" frameborder="0" allowfullscreen>';
		$cHtml .= '</iframe>';		
		
		$cHtml .= '</div>';				

		echo $cHtml ;			
	}					
}

//************************************************************************//

class TTree extends TControl {

	public  $aData  		= [];
	public  $bClick  		= '';
	public  $bDblclick  = '';
	public  $lCheckbox 	= false;
	public  $nAnimation	= 0;	

	public function __construct( $oWnd , $cId = null, $nTop = 0, $nLeft = 0, 
	                             $nWidth = null, $nHeight = null, $cColor = CLR_HGRAY, $aData = null ) {

		$nWidth  = TDefault( $nWidth , TWEB_TREE_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_TREE_HEIGHT_DEFAULT );						
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  );
	
		$this->cClass		= '';
		$this->cControl		= 'ttree';
		
		if ( gettype( $aData ) == 'array' ) {
			$this->aData = $aData;
		}
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
		
	}

	public function SetTree( $aData = [] ) { $this->aData = $aData; }	
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
		$cHtml .= $this->DefClass();		
		$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: auto;';
		
		$cHtml .= $this->StyleDim();			
		$cHtml .= $this->StyleBorder();
		$cHtml .= $this->StyleColor();		
		$cHtml .= $this->StyleCss();		
		$cHtml .= $this->StyleShadow();
			
		$cHtml .= '"> ';
		
		$cHtml .= '</div>';				

		echo $cHtml ;	
		
		
		//	Inicialitzem la Clase de Javascript --------------------------------------------------------
		$cScript  = 'var aDat = ' . json_encode( $this->aData ) . ';' ; 		
		$cScript .= 'var oT = new TTree( "' . $this->cId . '", aDat );';
		
		$cScript .= 'oT.nAnimation = ' . $this->nAnimation . ';' ;				
		
		if ( $this->bClick !== '' ) {
			$cScript .= 'oT.bClick = ' . $this->bClick . ';' ;
		}	
			
		if ( $this->bDblclick !== '' ) {
			$cScript .= 'oT.bDblclick = ' . $this->bDblclick . ';' ;	
		}	
			
		if ( $this->lCheckbox )	{		
			$cScript .= 'oT.lCheckbox = true;';			
		}	
		
		$cScript .= 'oT.Init();';
		
		ExeJS( $cScript );
	}					
}

//************************************************************************//

class TAccordion extends TControl {

	public  $lDefaultIcons  = true;
	public  $aIcons  		= [];
	private	$aTabs			= [];
	public  $nOption    	= null;

	public function __construct( $oWnd , $cId = '', $nTop = 0, $nLeft = 0, 
	                             $nWidth = null, $nHeight = null, $cColor = CLR_GRAY ) {

		$nWidth  = TDefault( $nWidth , TWEB_ACCORDION_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_ACCORDION_HEIGHT_DEFAULT );						
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  );
	
		$this->cClass		= 'tweb_accordion';
		$this->cControl	= 'taccordion';		
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}
	
	public function AddTab( $cId = '', $cLabel = 'Label', $cFunction = '', $cCode = '' ){
		$this->aTabs[] = array( $cId, $cLabel, $cFunction, $cCode ) ;	
	}	

	public function SetIcons( $cIconClosed, $cIconOpen = null ) {

		if ( $cIconClosed ) {
			$this->aIcons[] = $cIconClosed;
		}	
		
		if ( $cIconOpen ) {
			$this->aIcons[] = $cIconOpen;
		}	

		$this->lDefaultIcons = false;

	}
	
	public function Activate() {

		$this->CoorsUpdate();
		
		//	Accordion Caja		
	
		$cHtml = '<div id="' . $this->cId . '" ';
		
		$cHtml .= $this->DefClass();		
		$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
		$cHtml .= $this->StyleDim();			
		$cHtml .= $this->StyleBorder();
		$cHtml .= $this->StyleColor();		
		$cHtml .= $this->StyleCss();
		$cHtml .= $this->StyleShadow();			
		$cHtml .= '"';		
		
		$cHtml .= '>';	
		
		//	Accordion Contenido
		
		$cHtml .= '<div id="' . $this->cId . '_content" >';		
		
		echo $cHtml;
		
		//	Tabs... --------------------------------------------------------------------------------		
		
		$nLen = count( $this->aTabs );		
		
		for ( $i = 0; $i < $nLen; $i++) {		
		
			$cId_Tab	= $this->aTabs[$i][0];
			$cPrompt 	= $this->aTabs[$i][1];
			$cFunction 	= $this->aTabs[$i][2];
			$cCode 		= $this->aTabs[$i][3];
			
			//	Inicio Tab con el titulo y un div contenedor del diálogo...
			
				echo '<h3>' . $cPrompt . '</h3>';
				echo '<div style="padding:0px;">';				
				if ( !empty( $cFunction ) ) {	
				
					if ((int) function_exists( $cFunction ) > 0) {		
				
						call_user_func( $cFunction, $this );					
					} 
				}														
				//	Cierro div contenedor
				echo '</div>';
		}		
		
		//	----------------------------------------------------------------------------------------
			
		echo '</div>';	//	Cierra Content	
		echo '</div>';	//	Cierra Caja
		
		$cFunction = '';
	
		//	Icones...
		
		$cType = gettype(  $this->aIcons );
		
		if ( $cType == 'array' && ( count( $this->aIcons ) > 0 )) {
		
			if ( count( $this->aIcons ) == 2 )
				$cFunction .= 'var icons = { header:"' . $this->aIcons[0] . '",activeHeader:"' . $this->aIcons[1] . '"}; ';					
			else 
				$cFunction .= 'var icons = { header:"' . $this->aIcons[0] . '",activeHeader:"' . $this->aIcons[0] . '"}; ';					
				//$cFunction .= 'var icons = {}; ';	
		} else {
		
			if ( $this->lDefaultIcons )
				$cFunction .= 'var icons = { header: "iconClosed", activeHeader: "iconOpen" }; ';		
			else
				$cFunction .= 'var icons = null; ';		
		}
		if ( $this->nOption ) 
			$cFunction .= 'var nOption = ' . ( $this->nOption - 1 ) . ';';
		else
			$cFunction .= 'var nOption = false;';			
		
		$cFunction .= '   $( "#' . $this->cId . '_content" ).accordion( { icons: icons, collapsible: true, active: nOption, heightStyle: "fill" }); ' ;
		$cFunction .= '   $( "#' . $this->cId . '").resize( function() {';
		$cFunction .= '       $( "#' . $this->cId . '_content" ).accordion( "refresh" );';
		$cFunction .= '   });';				

		ExeJS( $cFunction );

	}					
}

//************************************************************************//

class TMsgItem extends TControl {

	public $cImage 	= '';

	public function __construct( $oWnd , $cId = '1', $nWidth = null, $cCaption = '',  $cImage = '' , $bClick = '' ) {  	
	
		$nWidth  = TDefault( $nWidth , TWEB_PANEL_WIDTH_DEFAULT );
		
		parent::__construct( $oWnd, $cId, 3, 0, $nWidth, 21, CLR_HGRAY );
												
		$this->cClass		= 'tweb_msgitem' ;													
		$this->cControl 	= 'tmsgitem';													
		$this->cCaption		= $cCaption;													
		$this->bClick		= $bClick;			
		$this->cImage		= $cImage;			
	}	
	
	public function Activate() {		
		
		$this->CoorsUpdate();
		
		$cHtml  = '<div id="' . $this->cId . '" ';
		$cHtml .= 'style="position: absolute; display: table-cell; ';
			
				$cHtml .= $this->StyleDim();	
				$cHtml .= $this->StyleBorder();		
				$cHtml .= $this->StyleColor();			
				$cHtml .= $this->StyleFont();
				$cHtml .= $this->StyleCss();	
				
		$cHtml .= '" ';
		
		$cHtml .= $this->DefClass();		
		$cHtml .= $this->Datas();	
		$cHtml .= $this->OnClick();	
		$cHtml .= '> ' ;	
		
		$cHtml .= '<img id="msgitem_img_' . $this->cId . '" src="' . $this->cImage . '" ';
		
		if ( $this->cImage == '' )
			$cHtml .= 'style="display:none;" ';
		
		$cHtml .= '>';
				
		$cHtml .= '<p id="msgitem_txt_' . $this->cId . '" align="' . $this->cAlign . '" >';
		
		$cHtml .= $this->cCaption  ; 
		
		$cHtml .= '</p>' ; 
		$cHtml .= '</div>' ; 
		
		echo $cHtml ;	
	}
}

//************************************************************************//

class TMenu extends TControl {

	private	$aMenu		= [];
	private	$cTarget	= '';

    public function __construct( $oWnd, $cId = '', $nTop = null, $nHeight = 24, $cColor = '#F2F2F2' ) {     
	
		parent::__construct( $oWnd, $cId, $nTop, 0, '100%', $nHeight, $cColor  );
	
		$this->cClass		= 'tweb_menu';
		$this->cControl	= 'tmenu';
	}    
	
  public function __destruct() {}
	
	public function AddMenu( $cId = '', $cPrompt = '' ){
	
		$oSubmenu		= new TSubmenu();
		$oMenu 			= array( 'cId' => $cId, 'cPrompt' => $cPrompt, 'oSubmenu' => $oSubmenu );
		
		$this->aMenu[] 	= $oMenu;
		
		return $oSubmenu;		
	}	
	
	public function AddSubmenu( $cId = '', $cPrompt = '' ){	
	
	
	}
	
	public function Activate() {
	
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();

		$cHtml .= 'style="position: ' . $this->cPosition . '; ';
		$cHtml .= 'box-sizing: border-box; ';
		
			$cHtml .= $this->StyleDim();
			$cHtml .= $this->StyleBorder();			
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleFont();
			$cHtml .= $this->StyleCss();		
			
		$cHtml .= 'overflow: hidden;" ' ;									
		$cHtml  .= '> '; 		
		
		$nMenu = count( $this->aMenu );		
	
		$cHtml .= '<ul>';
		
		for ( $i = 0; $i < $nMenu; $i++) {
		
			$cId 		= $this->aMenu[$i][ 'cId' ];
			$cPrompt 	= $this->aMenu[$i][ 'cPrompt' ];
			
			$cHtml .= '	<li id="' . $cId . '">' . $cPrompt ;					
			$cHtml .= '	</li>';				
		}	

		$cHtml .= '</ul>';			
		
		$cHtml .= '</div>';						
		
		echo $cHtml;
		
//		Inyectamos JAVASCRIPT MENU ---------------------------------------------------------

		$aoMenu = [];
		$nMenu = count( $this->aMenu );		
	
		$cFunction = '';
		
		for ( $i = 0; $i < $nMenu; $i++) {
		
			$cId 		= $this->aMenu[$i][ 'cId' ];
			$cPrompt 	= $this->aMenu[$i][ 'cPrompt' ];			
			$oSubmenu 	= $this->aMenu[$i][ 'oSubmenu' ];			
			
			$aItems = $oSubmenu->GetItems();
			$nItems = count( $aItems );	
			
			if ( $nItems > 0 ) {
			
				$cVar = "oMenu" . $i;
				$aoMenu[] = $cVar;
				
				$cFunction  .= "var " . $cVar ." = new TSubMenu( '" . $cId . "' );";						
				
				//$cFunction  .= FWeb_Submenu( $cVar, null, $aItems );
				$cFunction  .= $this->Build_SubMenu( $cVar, null, $aItems );			
			}
		}
		
		ExeJS( $cFunction );	

	//	Activo MENU	

		$cFunction  = '$(document).ready(function() {' ;
		
			$nMenu = count( $aoMenu );
			
			for ( $i = 0; $i < $nMenu; $i++) {
			
				$oMenu = $aoMenu[$i];
	
				$cFunction .= "     " . $oMenu . ".Init(); ";								
			}
		
		$cFunction .= '}); ' ;

		ExeJS( $cFunction );		
		
	}
	
	private function Build_SubMenu( $oMenu, $cId, $aItems ) {
	
		$cParent = is_null( $cId ) ? 'null' :  $cId ;
		
		$nItems = count( $aItems );	
		
		$cFunction 	= '';
		$cLog 		= '';
		
		for ( $j = 0; $j < $nItems; $j++) {			
		
			$cItem_Id  = $aItems[$j][ 'cId'		];
			$cPrompt   = $aItems[$j][ 'cPrompt'	];
			$bAction   = $aItems[$j][ 'bAction'	] == null ? 'null' : "'" . $aItems[$j][ 'bAction'	] . "'";
	//		$bAction   = 'null';
			$cImage    = $aItems[$j][ 'cImage'	];
			$cTooltip  = $aItems[$j][ 'cTooltip' ] == null ? 'null' : "'" . $aItems[$j][ 'cTooltip'	] . "'";
			$lDisabled = $aItems[$j][ 'lDisabled'	] == null ? 'false' : "'" . $aItems[$j][ 'lDisabled' ] . "'";
			$oSubmenu  = $aItems[$j][ 'oSubmenu'];
			

			$aItemsSub = $oSubmenu->GetItems();
			$nItemsSub = count( $aItemsSub );	
			
	// 		El addslashes serveisx per si hi ha texte p.e. amb apostrofes   l'email...		
			
			$cCmd 		= $cItem_Id . " = " . $oMenu . ".Item( " . $cParent . ", '" . addslashes($cPrompt) . "' , " . $bAction . ", '" . $cImage . "', " . $lDisabled . ", " .  $cTooltip . " );"  ;
		
			$cFunction .= $cCmd ;
				
			if ( $nItemsSub > 0 ) {
			
				$cFunction .= $this->Build_SubMenu( $oMenu, $cItem_Id, $aItemsSub );
			}				
		}
		
		return $cFunction;				
	}
	
}

//************************************************************************//

class TSubmenu {

	private	$aItems		= [];
	
    public function __construct() { 			
	
	}	
	
	public function AddItem( $cId = '', $cPrompt = '', $bAction = '', $cImage = '', $lDisabled = false, $cTooltip = '' ){	
	
		$oSubmenu		= new TSubmenu();
	
		$this->aItems[] = array( 'cId' => $cId, 'cPrompt' => $cPrompt, 'bAction' => $bAction, 'cImage' => $cImage, 'lDisabled' => $lDisabled ,'cTooltip' => $cTooltip, 'oSubmenu' => $oSubmenu );
		
		return $oSubmenu;
	}
	
	public function GetItems(){ return $this->aItems; }		
}

//************************************************************************//--

function NewId() {

	static $count = 0;

	$count++;

return 'tweb' . $count;
}

//************************************************************************//--

function ExeJS( $cFunction, $cLog = null ) {

	$cEcho   =  "<script type='text/javascript'>";
	$cEcho  .= 	       $cFunction ;
	
	if ( $cLog )
		$cEcho  .=  "console.info( 'info', '" . $cLog . "' );";
	
	
	$cEcho  .= "</script>";
	
	echo $cEcho;
}

//************************************************************************//--

function ExeJSReady( $cFunction = '', $cLog = null ) {

	$cEcho  = "<script type='text/javascript'>";
	$cEcho .= "  $( document ).ready(function() {";
	$cEcho .= 		$cFunction ;
	
	if ( $cLog )
		$cEcho .= "console.info( 'info', '" . $cLog . "' );";
	
	$cEcho .= "  })";
	$cEcho .= "</script>";	
	
	echo $cEcho;
}

//************************************************************************//--

function TDefault( $uValue, $uDefault ) {
  return ( $uValue == null || empty($uValue) ) ? $uDefault : $uValue;
}

//************************************************************************//--

function _Msg( $cMsgType = 'info', $uValue = null ) {

	$uVal = is_null( $uValue) ? '' : $uValue;

	if ( gettype( $uVal ) === 'array' ){
	
		$cString = '';
		
		foreach ($uVal as $key => $value) {
			$cType =  gettype( $value );
			
			if ( $cType !== 'array' ) 		
				$cString .= "[$key] => (" . $cType . ") $value \\n";
			  else
				$cString .= "[$key] => (" . $cType . ")";
		}									
	} else {
		$cString = $uVal ;
	}

	$cFunction  = '$(document).ready(function() {' ;
	
	switch ( $cMsgType ) {
	
		case 'info' 	: $cFunction .= '  MsgInfo ( "' . $cString . '" );';  break;
		case 'error' 	: $cFunction .= '  MsgError( "' . $cString . '" );';  break;

	}

	$cFunction .= '});';						
	
	ExeJS( $cFunction );	
}

//************************************************************************//--

function MsgInfo ( $uValue = '' ) { _Msg( 'info' , $uValue ); }
function MsgError( $uValue = '' ) { _Msg( 'error', $uValue ); }

if ( isset( $_GET[ 'tweb_version' ] ) ) ExeJS( "console.log( '" . TWEB_VERSION . "' )" );

//	Setups Menu Button ----------------------------------------------------------------------

	//	See demo and documentation on http://ignitersworld.com/lab/contextMenu.html

	function TWeb_SetupBtnItems( $aSetup ) {

		if ( array_key_exists( 'triggerOn', $aSetup ) )	
			TWeb_SetupBtnItems::$triggerOn 		= $aSetup[ 'triggerOn' ];
			
		if ( array_key_exists( 'displayAround', $aSetup ) )	
			TWeb_SetupBtnItems::$displayAround	= $aSetup[ 'displayAround' ];	
			
		if ( array_key_exists( 'position', $aSetup ) )
			TWeb_SetupBtnItems::$position			= $aSetup[ 'position' ];
			
		if ( array_key_exists( 'verAdjust', $aSetup ) )
			TWeb_SetupBtnItems::$verAdjust		= $aSetup[ 'verAdjust' ];
			
		if ( array_key_exists( 'mouseClick', $aSetup ) )
			TWeb_SetupBtnItems::$mouseClick		= $aSetup[ 'mouseClick' ];
	}

	class TWeb_SetupBtnItems {

		public static $triggerOn 		= 'click';
		public static $displayAround 	= 'trigger';
		public static $position			= 'bottom';
		public static $verAdjust		= -2;
		public static $mouseClick		= 'right';
	}
	
//	Setup Tooltips ------------------------------------------------

	function TWeb_SetupTooltips( $aSetup ) {

		if ( array_key_exists( 'image', $aSetup ) ) TWeb_SetupTooltips::$cImage = $aSetup[ 'image' ];			
		if ( array_key_exists( 'show', $aSetup ) ) 	TWeb_SetupTooltips::$cShow  = $aSetup[ 'show' ];				
		if ( array_key_exists( 'hide', $aSetup ) ) 	TWeb_SetupTooltips::$cHide  = $aSetup[ 'hide' ];							
	}

	class TWeb_SetupTooltips {		
		public static $cImage 	= TWEB_PATH . '/images/info2.png';
		public static $cShow 	= '{ duration: "fast"  }';
		public static $cHide 	= '{ effect: "hide"  }';
	}

//	---------------------------------------------------------------

?>