<script>

</script>
<?php 

class TEditor extends TControl {

	public function __construct( $oWnd , $cId = '', $nTop = 0, $nLeft = 0, $cCaption = '', $nWidth  = TWEB_GRID_WIDTH_DEFAULT, $nHeight = TWEB_GRID_HEIGHT_DEFAULT  ) {  
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass		= 'tweb_editor';
		$this->cControl		= 'teditor';
		$this->cCaption		= $cCaption;
		
		$this->SetBorderInset();						
	}

	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
			$cHtml .= $this->StyleDim();			
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleCss();		
		
		$cHtml .= '"> ';			
		
			$cId_Editor = $this->cId . '_editor';		
		
			$cHtml .= '<textarea id="' . $cId_Editor . '" style="height:100%;width:100%;">';
			//$cHtml .= '<div id="' . $cId_Editor . '" style="height:100%;width:100%;">';
			$cHtml .= $this->cCaption ; 
			$cHtml .= '</textarea>';
			//$cHtml .= '</div>';
		
		$cHtml .= '</div>';				

		echo $cHtml ;
		
		$cFunction  = 'var _oEdit = new TEditor( "' . $cId_Editor . '" );';
		$cFunction .= '_oEdit.Init();';

		ExeJS( $cFunction );
	}	
	
}
?>
<script>

	$( document ).ready(function() {
	
		console.log( 'load...')


	
	});	

</script>