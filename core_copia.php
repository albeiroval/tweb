<?php 
/*
Libreria			: TWEB (FrameWork for Web)
Autor				: Carles Aubia
Versio				: 1.4
Data Inici 			: 24/12/2014
Ult. Modificacio    : 11/10/2018
Descripcio			: Framework para ayudar a contruir de manera rapida, facil y productiva una
					  web de mantenimiento, diseñando incluso pantallas con workshop acelerando
					  asi su tiempo de diseño.
*/
if ( ! defined( 'TWEB_PATH' ) ) {
	define( 'TWEB_PATH'		, dirname(__FILE__) . '/' );
} 

//	Includes TWeb ...
require_once ( 'core.config.php' );
require_once ( 'core.constant.php' );
require_once ( 'core.session.php' );
require_once ( 'core.tools.php' );
require_once ( 'core.rc.php' );

//  Includes Nuevas ...
require_once ( 'core.datatable.php' );
require_once ( 'core.selectpicker.php' );
require_once ( 'core.sidebar.php' );
require_once ( 'core.login.php' );

Class TWeb {

	public  $cTitle 		= '';
	public  $cIcon 			= '';	
	public  $cCss			= '';
	public  $cJQueryCss		= '';
	public  $cBackground 	= '';
	public  $cBrush 		= '';
	public  $cColor		 	= '';	
	public  $cLang		 	= TWEB_HTML_LANG;	
	public  $lLibs		 	= true;
	public  $nBlur		 	= 0;
	private $aCss 			= array();
	private $aJs 			  = array();
	private $aMeta			= array();	
	public  $lMaps		 	= false;				//	Carga plugin Maps
	public  $lUpload	 	= false;				//	Carga plugin Upload
	public  $lEditor	 	= false;				//	Carga plugin Editor
	public  $lPreloader	 	= true;				
	public  $lAwesome 		= false;				//	Carga fonts Awesome
	
    public function __construct( $cTitle = 'TWeb' ) {     

		$this->cTitle		= $cTitle;	
		$this->cIcon 		= TWEB_PATH_IMAGES 	. '/tweb.png';
		$this->cCss 		= TWEB_PATH 		. '/core.css';
		
		//	Estilos Css jQuery 
		// 	'/jquery/css/cupertino/jquery-ui-1.10.4.custom.css';
		// 	'/jquery/css/smoothness/jquery-ui-1.10.4.custom.css';
		// 	'/jquery/css/sunny/jquery-ui-1.10.4.custom.css';
		
		$this->cJQueryCss	= TWEB_PATH_LIBS	. '/jquery/css/tweb/jquery-ui-1.10.4.custom.css';
	}
	
	public function Activate() {

		$cHtml  = '<!DOCTYPE html>';
		$cHtml .= '<html lang="' . $this->cLang . '">';
		$cHtml .= '<head>';		
		
		$cHtml .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';	
		$cHtml .= '<meta name="viewport" content="width=device-width , initial-scale=1, maximum-scale=1" />';	
		$cHtml .= '<title>' . $this->cTitle . '</title>';

		$cHtml .= '<link rel="icon" href="' . $this->cIcon . '" type="image/x-icon" >';
		$cHtml .= '<link rel="shortcut icon" href="'  . $this->cIcon .  '" type="image/x-icon">';
		
		//	JQuery ---------------------------------------------------------------------------------

		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/jquery/jquery-1.10.2.js"></script>';
		
		if ( $this->lLibs ) {
		
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/jquery/jquery-ui-1.10.4.custom.js"></script>';	
			$cHtml .= '<link rel="stylesheet" href="' . $this->cJQueryCss . '">';			
			
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/jquery/jquery.ui.progressbar.js"></script>';		// ProgressBar		
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/jquery/jquery.ui.datepicker-es.js"></script>';	// Calendario
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/jquery/jquery.event.drag-2.2.js"></script>';		// Calendario		
			
			//	Dialog Extend ---------------------------------------------------------------------- 				
			
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/timepicker/jquery-ui-timepicker-addon.min.js"></script>';	// Timepicker
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/timepicker/jquery-ui-sliderAccess.js"></script>';			// Timepicker
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/timepicker/jquery-ui-timepicker-addon.min.css">';			
			
			//	Dialog Extend ---------------------------------------------------------------------- 				

			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/dialogextend/jquery.dialogextend.js"></script>';   // Original 
						
			//	Multiselect ------------------------------------------------------------------------
			
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/multiple-select/multiple-select.js"></script>'; 	
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/multiple-select/multiple-select.css" type="text/css" media="screen" />';			
			
			//	Notificacions ----------------------------------------------------------------------
			
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/notify/notify.js"></script>'; 
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/notify/styles/bootstrap/notify-bootstrap.js"></script>'; 		

			//	Cookies ----------------------------------------------------------------------------
			
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/cookies/jquery.cookie.js"></script>'; 
			
			//	Trees ------------------------------------------------------------------------------
			
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/jstree/jstree.js"></script>'; 
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/jstree/themes/default/style.css" type="text/css" media="screen" />';			
			
			
			//	Mask Input -------------------------------------------------------------------------
			
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/masked/jquery.maskedinput.min.js"></script>';   // Original 		
		
			//	Image ------------------------------------------------------------------------------

			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/lightbox/lightbox.js"></script>' ;
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/lightbox/css/lightbox.css">';			
			
			//	Menus ------------------------------------------------------------------------------	
			
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/menu/contextMenu.js"></script>'; 	
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/menu/contextMenu.css" type="text/css" media="screen" />';	
						
			//	Resize -----------------------------------------------------------------------------
			
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/resize/jquery.ba-resize.js"></script>' ;
			
			//	Upload -----------------------------------------------------------------------------
			
			if ( $this->lUpload ) {
				$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/fileupload/jquery.ui.widget.js"></script>' ;
				$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/fileupload/jquery.iframe-transport.js"></script>' ;
				$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/fileupload/jquery.fileupload.js"></script>' ;
			}
			
			//	Grid -------------------------------------------------------------------------------
			
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/slick.core.js"></script>' ;
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/slick.formatters.js"></script>' ;
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/slick.editors.js"></script>' ;
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/slick.grid.js"></script>' ;
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/slick.dataview.js""></script>' ;
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/controls/slick.pager.js"></script>' ;
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/plugins/slick.rowselectionmodel.js"></script>' ;
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/plugins/slick.autotooltips.js"></script>' ;
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/plugins/slick.headerbuttons.js"></script>' ;
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/plugins/slick.cellexternalcopymanager.js"></script>' ;			
			
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/plugins/footer/TotalsDataView.js"></script>' ;			
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/plugins/footer/TotalsPlugin.js"></script>' ;			
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/slickgrid/plugins/footer/jquery.getscrollbarwidth.js""></script>' ;			
			
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/slickgrid/plugins/footer/TotalsPlugin.css" type="text/css" media="screen" />';	
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/slickgrid/plugins/footer/styles.css" type="text/css" media="screen" />';	
						
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/slickgrid/slick.grid.css" type="text/css" media="screen" />';	
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/slickgrid/examples.css" type="text/css" media="screen" />';	
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/slickgrid/controls/slick.pager.css" type="text/css" media="screen" />';				
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/slickgrid/plugins/slick.headerbuttons.css" type="text/css" media="screen" />';				
			
			//	Touch ------------------------------------------------------------------------------
			
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/touch/jquery.touch.min.js"></script>' ;
			
			//	Maps -------------------------------------------------------------------------------
			
			if ( $this->lMaps )	 {
				$cHtml .= '<script src="https://maps.googleapis.com/maps/api/js?key=' . TWEB_KEY_MAP . '"></script>';
			}			
			
			//	Editor -----------------------------------------------------------------------------
			
//			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/tinymce/tinymce.min.js"></script>' ;
//			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/tinymce/plugins/autoresize/plugin.min.js"></script>' ;
			
			//	Editor -----------------------------------------------------------------------------
			
			if ( $this->lEditor ) {			
				//$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/sceditor/minified/jquery.sceditor.xhtml.min.js"></script>' ;
				//$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/sceditor/minified/jquery.sceditor.bbcode.min.js"></script>' ;
				//$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/sceditor/minified/formats/xhtml.js"></script>' ;
				//$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/sceditor/minified/formats/bbcode.js"></script>' ;
				//$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/sceditor/languages/es.js"></script>' ;
				//$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/sceditor/minified/themes/square.min.css" type="text/css" media="screen" />';
				
				//$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/sceditor/minified/themes/default.min.css" type="text/css" media="screen" />';
				$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/sceditor/minified/themes/default.min.css" id="theme-style" />';
				$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/sceditor/minified/sceditor.min.js"></script>' ;
				$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/sceditor/minified/icons/monocons.js"></script>' ;
				$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/sceditor/minified/formats/bbcode.js"></script>' ;
			}
			
			if ( $this->lAwesome ) {			
				// $cHtml .= '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">';	
				// $cHtml .= '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">';	
				$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/font-awesome/css/font-awesome.min.css">';	
			}

			// Sweetalert2  -----------------------------------------------------------------------------
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/sweetalert2/promise.min.js"></script>' ;   
  		$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/sweetalert2/sweetalert2.min.js"></script>' ; 
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/sweetalert2/sweetalert2.min.css" type="text/css" media="screen" />';
						
			// Funciones Propias  -----------------------------------------------------------------------------
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/mylibs/jquery.maskMoney.min.js"></script>' ;   
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/mylibs/jquery.messages.js"></script>' ;   
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/mylibs/funciones.js"></script>' ;   

			// Datatables  -----------------------------------------------------------------------------
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/datatable/datatables.min.js"></script>' ;   	
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/datatable/datatables.min.css" type="text/css" media="screen" />';	

			// Bootsatrp v3.3.6
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/bootstrap-3.3.6/js/bootstrap.min.js"></script>' ;   	
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/bootstrap-3.3.6/css/bootstrap.min.css" type="text/css" media="screen" />';	

			// BootStrap-Selet 13.18
			$cHtml .= '<script src="' . TWEB_PATH_LIBS . '/bootstrap-select/js/bootstrap-select.min.js"></script>' ;   	
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/bootstrap-select/css/bootstrap-select.min.css" type="text/css" media="screen" />';	

			// Custom extend  
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/custom/css/login.css"  type="text/css" media="screen">';
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/custom/css/sweetalert.css" type="text/css" media="screen" />';
			$cHtml .= '<link rel="stylesheet" href="' . TWEB_PATH_LIBS . '/custom/css/tweb.css" type="text/css" media="screen" />';
		}
		
		//	META's ------------------------------------------------------------------------------ //
		
		$nMeta = count( $this->aMeta );

		for ( $i = 0; $i < $nMeta; $i++) {
			$cName 		= $this->aMeta[$i][0];		
			$cContent 	= $this->aMeta[$i][1];		
			$cHtml .= '<meta name="' . $cName . '" content="' . $cContent . '" >';					
		}

		//	JS Public --------------------------------------------------------------------------- //

		$nJs = count( $this->aJs );

		for ( $i = 0; $i < $nJs; $i++) {
			$cJs = $this->aJs[$i];		
			$cHtml .= '<script src="' . $cJs . '"></script>';					
		}

		//	Css TWeb -------------------------------------------------------------------------------
			$cHtml .= '<link type="text/css" rel="stylesheet" href="' . $this->cCss . '" media="screen">';		
		//	$cHtml .= '<link type="text/css" rel="stylesheet" href="' . TWEB_PATH . '/reset.css' . '" media="screen">';		
		
		//	----------------------------------------------------------------------------------------

		//	CSS Public ------------------------------------------------------------------------- //
		$nCss = count( $this->aCss );

		for ( $i = 0; $i < $nCss; $i++) {
			$cCss = $this->aCss[$i];			
			$cHtml .= '<link type="text/css" rel="stylesheet" href="' . $cCss . '" media="screen">';					
		}		
		
	
		if ( $this->lPreloader ){
		
			$cHtml .= "<script type='text/javascript'>";
			$cHtml .= "$(window).load(function() {";
			$cHtml .= "		$('#preloader').fadeOut('slow');";
			$cHtml .= "		$('body').css({'overflow':'visible'});";
			$cHtml .= "});";
			$cHtml .= "</script>";
		}		
		$cHtml .= '</head>';	
		
		//$cHtml .= '<body oncontextmenu="return false" onkeydown="return false">';	// OK. Evitar click derecho raton y activar inspector...
		$cHtml .= '<body>';
		
		if ( $this->lPreloader ){
		
			$cHtml .= '<div id="preloader">';
			$cHtml .= '    <div id="loader"></div>';
			$cHtml .= '</div>';					
		}		
		
		
		//	Pantalla browser...

		$cHtml .= '<div class="tweb_screen" id="tweb_screen" ';	
			
			if ( !empty( $this->cColor )){
				$cHtml .= ' style="background-color: ' . $this->cColor . ';" ';
			} else if ( !empty( $this->cBrush ) ){
				$cHtml .= ' style="background: url( ' . $this->cBrush . ')" ';		
			} else if ( !empty( $this->cBackground )){
				$cHtml .= ' style="background: url( ' . $this->cBackground . ') center center no-repeat; background-size: cover; ';
				
				if ( $this->nBlur > 0 )
					$cHtml .= ' -webkit-filter: blur(' . $this->nBlur . 'px); -moz-filter: blur(' . $this->nBlur . 'px); filter: blur(' . $this->nBlur . 'px);';
					
				$cHtml .= ' "' ;
			}
		
		$cHtml .= '>';					
	
		//	Inicializamos variables globales para JS...
		
		$cFunction  = 'var TWEB_PATH = "' . TWEB_PATH . '";' ;
		$cFunction .= 'var TWEB_PATH_LIBS = "' . TWEB_PATH_LIBS . '";' ;
		$cFunction .= 'var TWEB_PATH_IMAGES = "' . TWEB_PATH_IMAGES . '";' ;
		$cFunction .= 'var TWEB_PATH_SOUND = "' . TWEB_PATH_SOUND . '";' ;
		
		ExeJS( $cFunction );			
		
		//	Core.js ----------------------------------------------------------------------------

		$cHtml .= '<script src="' . TWEB_PATH . '/core.js"></script>' ;
		$cHtml .= '<script src="' . TWEB_PATH . '/core.msg.js"></script>' ;
		$cHtml .= '<script src="' . TWEB_PATH . '/core.dialog.js"></script>' ;
		$cHtml .= '<script src="' . TWEB_PATH . '/core.selectpicker.js"></script>' ;
		$cHtml .= '<script src="' . TWEB_PATH . '/core.datatable.js"></script>' ;
		
		echo $cHtml ;
		
	}
	
	public function SetBackground( $cBackground = '', $nBlur = 0 ) { $this->cBackground = $cBackground; $this->nBlur = $nBlur;	}
	public function SetIcon( $cIcon = '' ){ $this->cIcon = $cIcon ;	}
	public function SetBrush( $cBrush = '' ){ $this->cBrush = $cBrush ;	}
	public function SetColor( $cColor = '' ) { $this->cColor = $cColor ;	}
	public function AddJs( $cJs = '' ) { $this->aJs[] = $cJs ;	}
	public function AddCss( $cCss = '' ) { $this->aCss[] = $cCss ;	}
	public function AddMeta( $cName = '', $cContent = '' ) { $this->aMeta[] = array( $cName, $cContent );	}	
	public function InitMaps() { $this->lMaps = true ;	}	
	public function InitUpload() { $this->lUpload = true ;	}	
	public function InitPreloader() { $this->lPreloader = true ;	}	

	public function End() {	
	
		$cHtml	 = '</div>';
		$cHtml  .= '<script src="' . TWEB_PATH . '/core.init.js"></script>' ;
		$cHtml	.= '</body>';
		$cHtml	.= '</html>';		

		echo $cHtml ;
	}
	
	public function Device() {		
	
//		Fuente: http://7sabores.com/blog/detectar-tipo-dispositivo-mobile-tablet-desktop-php 	
	
		$tablet_browser = 0;
		$mobile_browser = 0;
		$body_class 	= 'desktop';
		 
		if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
			$tablet_browser++;
			$body_class = "tablet";
		}
		 
		if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
			$mobile_browser++;
			$body_class = "mobile";
		}
		 
		if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
			$mobile_browser++;
			$body_class = "mobile";
		}
		 
		$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
		$mobile_agents = array(
			'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
			'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
			'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
			'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
			'newt','noki','palm','pana','pant','phil','play','port','prox',
			'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
			'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
			'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
			'wapr','webc','winw','winw','xda ','xda-');
		 
		if (in_array($mobile_ua,$mobile_agents)) {
			$mobile_browser++;
		}
		 
		if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
			$mobile_browser++;
			//Check for tablets on opera mini alternative headers
			$stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
			if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
			  $tablet_browser++;
			}
		}
		
		if ($tablet_browser > 0) {			//	Tablet
		   return 1;
		} else if ($mobile_browser > 0) {	//	Browser
		   return 2;
		} else {							// 	Desktop
		   return 0;
		}		
	}

	public function ViewLog( $cPhp_View_Log = '' ) {

		$cPhp_View_Log = empty( $cPhp_View_Log ) ? ( TWEB_PATH . 'dlg_log.php' ) : $cPhp_View_Log ;
		
		echo "<div id='_log'><span class='_log'>Log</span></div>";
		
		ExeJsReady( "InitViewLog( '" . $cPhp_View_Log . "' )" );		
	}

	
}

function NewId() {

    static $count = 0;

    $count++;
	
	return 'tweb' . $count;
}

class TCode {

	private $cCode	= '';
	
    public function __construct( $oWnd, $cCode = ''  ) {  	
	
		$this->cCode = $cCode;
		
		if ( gettype( $oWnd ) == 'object' )
			$oWnd->AddControl( $this );			
	}
	
	public function Activate() {	
	
		echo $this->cCode; 	
	}	
}

class TFont {

	public $cFaceName 	= '';
	public $nSize 		= 0;
	public $lBold 		= false;
	public $lItalic		= false;
	public $lUnderline	= false;
	public $nClrFore	= null;
	public $nClrBack	= null;


    public function __construct( $cName = 'Verdana' , $nSize = 12, $lBold = false, $lItalic = false, $lUnderline = false, $nClrFore = null, $nClrBack = null ) {  
	
		$this->cFaceName	= $cName;
		$this->nSize		= $nSize;
		$this->lBold		= ( gettype( $lBold ) == 'boolean' ) ? $lBold : false;
		$this->lItalic		= ( gettype( $lItalic ) == 'boolean' ) ? $lItalic : false;
		$this->lUnderline	= ( gettype( $lUnderline ) == 'boolean' ) ? $lUnderline : false;
		$this->nClrFore		= $nClrFore ;	
		$this->nClrBack		= $nClrBack ;	
	}
}


class TControl { 

    public  $oWndParent		= null;
    public  $cId			= '';
    public  $nTop			= 0;
    public  $nLeft			= 0;
    public  $nWidth			= 100;
    public  $nHeight		= 100;
    public  $nRight			= -1;
    public  $nBottom		= -1;
    public  $lBorder		= false;
    public  $lHide			= false;
    public  $lDisabled		= false;	
    public  $lReadOnly		= false;	
	public  $cCss			= '';	
	public  $cClass			= '';	
	public  $cControl		= '';	
	public  $cColor			= '';	
	public  $cBackground	= '';	
	public  $cBrush			= '';	
	public  $cCaption		= '';	
	public  $cTooltip		= '';	
	public  $bAction		= '';	
	public  $bClick			= '';	
	public  $bChange		= '';	
	public  $bWhen			= '';	
	public  $hRef			= '';	
	public  $cAlign			= '';	
	public  $cOverflow		= 'hidden';	
	public  $cPosition		= 'absolute';
	public  $cBorderType	= '';	// 'inset white'; //'solid black'; // 'groove'; // 'ridge' ; // 'outset'; //'inset';
	public  $oFont 			= '';
	public  $twebclass		= '';
	public  $cMessage		= '';
	public  $cData  		= '';
	public  $aKeys			= array();	
	public  $nDeepShadow	= 0;	
	public  $cColorShadow	= 'grey';	
//	public  $lColorFocus 	= false;	
//	public  $lStandard 		= false;	//	CAF NOU
 
    function __construct( $oWnd, $cId, $nTop = 0, $nLeft = 0, $nWidth = 0, $nHeight = 0, $cColor = '' ) {
	
		if ( $cId == null )
			$cId = NewId();
	
		$this->oWndParent 	= $oWnd;
		$this->cId			= $cId;
		$this->nTop			= $nTop;
		$this->nLeft		= $nLeft;
		$this->nWidth		= $nWidth;
		$this->nHeight		= $nHeight;					
		$this->cColor		= $cColor;	
			
		
		if ( gettype( $oWnd ) == 'object' ) {

			$oWndParent = $oWnd;
			$oWnd->AddControl( $this );			// $this->oWndParent
		}
    }
 
    function __destruct() {  }	
/*	
	public function GetDefaultClass( $cControl = '') {
	
		$cClass = '';
	
		if ( gettype( $this->oWndParent ) == 'object' ) {	
		
			if ( array_key_exists( $cControl, $this->oWndParent->aClass ) ) {
			
				$cClass = $this->oWndParent->aClass[ $cControl ];
				
			} 
		}
		
		return $cClass;	
	}
*/	

	public function DefClass() {

		$cHtml = '';
	
		if ( !empty( $this->cClass ) ) {
			$cHtml .= 'class="' . $this->cClass . '" ';	
		} 						
			
		return $cHtml;	
	}	
	
	public function SetData( $cKey = '', $cValue = '' ) {
	
		$this->cData = $this->cData . ' ' . $cKey . '="' . $cValue . '" ';
	}

	public function Datas() {	
		
		$cHtml  = 'data-control="' . $this->cControl . '" ';		
		$cHtml .= 'data-message="' . $this->cMessage . '" ';

		if ( !empty( $this->bWhen ) )
			$cHtml .= 'data-when="' . $this->bWhen . '" ';		
		
		if ( !empty( $this->cTooltip ) )
			$cHtml .= 'title="' . $this->cTooltip . '" ';		
			
		$cHtml .= $this->cData ;		
	
		return $cHtml;	
	}
	
	public function DefMessage( $cId = null ) {
	
		$cHtml = '';
	
		if ( !empty( $this->cMessage ) ) { 
		
			$cId = ( gettype( $cId ) == 'NULL' ) ? $this->cId : $cId ;
		
			$cFunction  = "var o = new TControl();" ;			

			$cFunction .= "o.InitCtrlMessage( '" . $cId . "' );" ;
			
			ExeJSReady( $cFunction, 'Activando ControlMessage -> ' . $cId );				
		}
			
		return $cHtml;	
	}
	
	public function DefTooltip() {
  
        if ( empty( $this->cTooltip ) ) 
            return null;
            
        //$cImage = 'images/info.png';
        $cImage = TWeb_SetupTooltips::$cImage;
            
        $cFunction  = '$( "#' . $this->cId . '" ).tooltip({ ';
        $cFunction .= '                position: { ';
        $cFunction .= '                    my: "center top",';
        $cFunction .= '                    at: "center bottom+5"';
        $cFunction .= '                },';
        //$cFunction .= '                show: { duration: "fast"  },';
        $cFunction .= '                show: ' . TWeb_SetupTooltips::$cShow . ',';
        //$cFunction .= '                hide: { effect: "hide"  },';
        $cFunction .= '                hide: ' . TWeb_SetupTooltips::$cHide . ',';

        if ( ! empty ( $cImage ) )
            $cFunction .= 'content: "<img style=\'vertical-align: middle;\' src=\'' . $cImage . '\' /><span style=\'vertical-align: middle;\'>&nbsp' . $this->cTooltip  . '</span> "';
        else
            $cFunction .= 'content: "' . $this->cTooltip . '</span> "';
       
        $cFunction .= ' });';             
		
       // $cFunction .= ' console.log( $( "#' . $this->cId . '" ).attr( \'title\' ));';             
        
        ExeJSReady( $cFunction );        
    }
	


	public function DefKeys() {

		$cFunction  = '$( "#' . $this->cId . '" ).bind("keydown", "", function (evt) { ' ;
		$cFunction .= '  switch( evt.keyCode ) {';
		
		$nTotalKey = count( $this->aKeys );
		
		for ( $i = 0; $i < $nTotalKey; $i++) {
		
			$nKey 		= $this->aKeys[$i][0];
			$cAction 	= $this->aKeys[$i][1];
			$cId_Jump 	= $this->aKeys[$i][2];
			
			$cFunction .= '  case ' . $nKey . ':';	
			$cFunction .= '    ' . $cAction . '; ' ;	
			$cFunction .= '    evt.preventDefault();' ;	
			
			if ( !empty( $cId_Jump ) ) {
				$cFunction .= '      $( "#' . $cId_Jump . '" ).focus(); ';
			}

			$cFunction .= '    break;' ;				
		}
		
		$cFunction .= '  }';
		$cFunction .= '});' ;

		ExeJS( $cFunction );		
	}
	
	public function StyleBorder() {	

		$cHtml = '';
			
		if ( $this->lBorder ) {
		
			$cHtml .= 'border: 1px ';
			
			if ( $this->cBorderType == '' ) {
				$cHtml .= 'solid; border-color: black; ';
			} else {
				$cHtml .= $this->cBorderType . '; '  ;				
				$cHtml .= 'border-style: ' . $this->cBorderType. '; '  ;
			}
		}
		
		return $cHtml;
	}
	
	public function StyleDim() {
	
		$cHtml  = 'margin:0px; ';
	
		if ($this->nTop >= 0 ) 			
			$cHtml .= 'top:' . $this->nTop .'; ';
			
		if ($this->nLeft >= 0 ) 			
			$cHtml .= 'left:' . $this->nLeft . '; ';		
		
		if ($this->nWidth > 0 ) 			
			$cHtml .= ' width: ' . $this->nWidth  . '; ';
			
		if ($this->nHeight > 0 ) 			
			$cHtml .= ' height:' . $this->nHeight . '; ';

		if ($this->nBottom >= 0 ) 			
			$cHtml .= ' bottom:' . $this->nBottom . '; ';				
			
		if ($this->nRight >= 0 ) 			
			$cHtml .= ' right:' . $this->nRight . '; ';				
			
		return $cHtml;	
	}
	
	public function StyleCss() {
	
		$cHtml = '';
	
		if ( $this->lHide ) 
			$cHtml .= 'display:none; ';		
	
		if ( ! empty( $this->cCss ) )
			$cHtml .= $this->cCss;
			
		return $cHtml;
	
	}
	
	public function StyleColor() {	
		
		$cHtml = '';
		
		if ( !empty( $this->cBrush ) )
			$cHtml .= 'background: url( ' . $this->cBrush . ')" ';	
		else if ( !empty( $this->cBackground ) )
			$cHtml .= 'background: url( ' . $this->cBackground . ') center center no-repeat; background-size: cover; '; 	
		else if ( !empty( $this->cColor ) )
			$cHtml .= 'background-color:' . $this->cColor . '; '; 

		return $cHtml;
	}	
	
	//	Styles ...
	
	public function SetBorderInset() {
		
		$this->lBorder		= true;	
		$this->cBorderType 	= 'inset white';	
	}
	
	public function SetBorderOutset() {
		
		$this->lBorder		= true;	
		$this->cBorderType 	= 'outset white';	
	}
	

	
	public function SetShadow( $nDeep = 3, $cColor = 'grey' ) {
		$this->nDeepShadow = $nDeep; 
		$this->cColorShadow = $cColor; 
	}
	
	//	----------------------------------------------------------------------------
	
	public function StyleShadow() {
	
		$cHtml = '';
		
		if ( $this->nDeepShadow > 0 ) {
			//$cHtml = 'box-shadow: 3px 3px 3px gray; ';
			$cHtml = 'box-shadow: ' . $this->nDeepShadow . 'px ' .  $this->nDeepShadow . 'px '.  $this->nDeepShadow . 'px  ' . $this->cColorShadow . '; ';
		}
		
		return $cHtml;
	}
	
	
	public function StyleFont() {
	
		$oFont = null;
		$cHtml = '';
		
		if ( gettype( $this->oFont ) == 'object' )	{
			$oFont = $this->oFont ;
		} else if ( gettype( $this->oWndParent ) == 'object' && gettype( $this->oWndParent->oFont ) == 'object') {
			$oFont = $this->oWndParent->oFont ;			
		}
		
		if ( $oFont ) {
		
			if ( !empty( $oFont->cFaceName ) )
				$cHtml .= "font-family:" . $oFont->cFaceName . "; ";	//	$cHtml .= "font-family:'" . $oFont->cFaceName . "'; ";
				

			if ( $oFont->lItalic ) 	
				$cHtml .= 'font-style: italic; '; 
			  else
				$cHtml .= 'font-style: normal; ';  		
				
			if ( $oFont->nSize > 0 ) 				
				$cHtml .= 'font-size: ' . $oFont->nSize . 'px; ';  	
				
			if ( $oFont->lBold ) 						
				$cHtml .= 'font-weight: bold; ';  		
			  else 	
				$cHtml .= 'font-weight: normal; ';  		
				
			if ( !empty( $oFont->nClrFore ) ) 				
				$cHtml .= 'color: ' . $oFont->nClrFore . '; ';  

			if ( $oFont->lUnderline ) 				
				$cHtml .= 'text-decoration: underline; ';  				
				
			if ( !empty( $oFont->nClrBack ) ) 									
				$cHtml .= 'background-color: ' . $oFont->nClrBack  . '; '; 		
		}			
		
		return $cHtml;
	}		
	
	public function OnClick() {
	
		$cHtml = '';
	
		if ( !empty( $this->bClick ) ) {
			$cHtml = 'onClick="' . $this->bClick . '" ';
		} else if ( !empty( $this->hRef ) ) {
			$cHtml = 'onClick="window.location.href=\'' . $this->hRef . '\'" ';		
		}
		
		return $cHtml;	
	}
	
	public function OnChange() {
	
		$cHtml = '';
	
		if ( !empty( $this->bChange ) ) {
			$cHtml = 'onchange="' . $this->bChange . '" ';			
		}		
		
		return $cHtml;	
	}	
	
	
	public function OnMessage() {
		
	}
	
	public function SetClass( $cClass = '' ) { $this->cClass = $cClass ;	}	
	public function SetCss( $cCss = '' ) { $this->cCss .= $cCss ;	}	
	public function SetAction( $bAction = '' ) { $this->bAction = $bAction; }
	public function SetClick( $bClick = '' ) { $this->bClick = $bClick; }
	public function SetFont( $oFont ) { $this->oFont = $oFont;	}
	public function SetBrush( $cBrush = '' ){ $this->cBrush = $cBrush ;	}
	public function SetBackground( $cBackground = '' ){ $this->cBackground = $cBackground ;	}
	public function SetColor( $cColor = null, $cBackground = null ) {
	
		if ( $cColor )		
			$this->cColor = $cColor;
			
		if ( $cBackground )
			$this->cBackground = $cBackground;			

	}
	
	public function SetKey( $nKey = 0, $cAction = '' , $cId_Jump = '') {
		$this->aKeys[] = array( $nKey, $cAction, $cId_Jump );
	}	
	
	
	//	O bien especificamos el ancho de control 'width' con un valor en positivo 
	//	o la distancia hasta el ancho total del panel 'right' en negativo.	
	
	public function SetWidth( $nWidth = 0 ) {
	
		if ( gettype( $nWidth ) == 'integer' ) {
		
			if ( $nWidth < 0 ) {
				$this->nWidth = 0;
				$this->nRight = Abs( $nWidth );			
			}						
		}			
	}
	
	//	O bien especificamos la altura de control 'height' con un valor en positivo 
	//	o la distancia hasta la altura total del panel 'bottom' en negativo.
	
	public function SetHeight( $nHeight = 0 ) {
	
		if ( gettype( $nHeight ) == 'integer' ) {
		
			if ( $nHeight < 0 ) {
				$this->nHeight = 0;
				$this->nBottom = Abs( $nHeight );			
			}						
		}			
	}
	
	/*
		+------+-------+-------+
		| nTop |nHeight|nBottom| 
		+------+-------+-------+
		|      |   X   |   X   |
		+------+-------+-------+
		|  X   |   X   |       |
		+------+-------+-------+
		|  X   |       |   X   |
		+------+-------+-------+			
	*/
	public function SetBottom( $nBottom = null, $nHeight = null ) {	
	
		if ( gettype( $nBottom ) == 'integer' ) {		
		
			$this->nBottom = $nBottom;	
			
			if ( gettype( $nHeight ) == 'integer' ) {
		
				$this->nTop 	= null;
				$this->nHeight 	= $nHeight;			
			} else {
			
				$this->nHeight 	= null;			
			}
		} else if ( $nBottom == '100%' ) {

			$this->nTop 	= ( gettype( $this->nTop ) !== 'NULL' ) ? $this->nTop : 0;
			$this->nHeight 	= null;		
			$this->nBottom 	= 0;		
		}		
	}
	
	public function SetRight( $nRight = null, $nWidth = null ) {	
	
		if ( gettype( $nRight ) == 'integer' ) {		
		
			$this->nRight = $nRight;	
			
			if ( gettype( $nWidth ) == 'integer' ) {
		
				$this->nLeft 	= null;
				$this->nWidth 	= $nWidth;			
			} else {
			
				$this->nWidth 	= null;			
			}
		} else if ( $nRight == '100%' ) {

			$this->nLeft 	= ( gettype( $this->nLeft ) !== 'NULL' ) ? $this->nLeft : 0;
			$this->nWidth 	= null;		
			$this->nRight 	= 0;		
		}		
	}	
		
	public function CoorsUpdate() {			

		$this->nTop 	= $this->Unit( $this->nTop );
		$this->nLeft	= $this->Unit( $this->nLeft );
		$this->nWidth	= $this->Unit( $this->nWidth );
		$this->nHeight	= $this->Unit( $this->nHeight );		
		$this->nRight	= $this->Unit( $this->nRight );		
		$this->nBottom	= $this->Unit( $this->nBottom );		
	}
	
	// 	Se trata	de cuando pase una coordenada, saber si se especifica unidades que podran ser
	// 	% o px. Si en la variable hay unidad (p.e. 100px o 100%), la funcion devuelve la misma var. Si en la variable 
	// 	no existe unidades devolveremos la variable + px , que es la unidad que tratamos por defecto	
	
	public function Unit( $uVar ) {
	
		//if ( gettype( $uVar ) == 'NULL'  || $uVar == 'auto' )
		//	return 'auto';
	
		if ( strpos( $uVar, '%' ) !== false ) 
			return $uVar;
		else if ( strpos( $uVar, 'px' ) !== false ) 
			return $uVar;
		else 
			return $uVar . 'px';				
	}
	
	//	CAF NOU PER TESTEJAR
	public function SetColorFocus() {
	
		/*
			.tweb_get:focus {
				background-color: beige; 
				border-color: #66afe9;
				border-image: none;
				border-style: solid;
				border-width: 1px;	
				cursor: text;
			}	
		*/	
		
		//$this->lColorFocus = true;	
	}
		

}

class TWindow extends TPanel {

    function __construct( $cId = null, $nTop = 0, $nLeft = 0, $nWidth = '100%', $nHeight = '100%', $cColor = CLR_HGRAY ) {
	
		parent::__construct( null, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor );

		$this->cClass			= 'tweb_window';	
		$this->cControl 		= 'twindow';							
	}		
}

class TDialog extends TPanel {

    function __construct( $cId = null, $nWidth = 300, $nHeight = 200, $cColor = CLR_HGRAY ) {
		
		if ( isset( $_POST[ '_id' ] ) )
			$cId = 'tdialog_' . $_POST[ '_id' ];
		else 
			$cId = ( empty( $cId ) ) ? time() : $cId ;
			
		parent::__construct( null, $cId, 0, 0, $nWidth, $nHeight, $cColor );

		$this->cClass			= 'tweb_dialog';	
		$this->cControl 		= 'tdialog_container';			
		
		//	El dialogo (panel) NO estará visible. Cuando se active el diálogo lo cambia 
		
		$this->lHide  			= true;			
	}
}

class TTabs extends TPanel {

    function __construct( $cId = null, $cType = '', $cColor = CLR_HGRAY ) {
	
		parent::__construct( null, $cId, 0, 0, '100%', '100%', $cColor );

		$this->cClass			= 'tweb_tabs';	
		$this->cControl 		= 'ttabs';												
		
		$cType = strtolower( $cType );
		
		switch ( $cType ) {
		
			case 'folder':
				$this->cPosition = 'initial';		//	Folders...											
				break;
				
			case 'accordion':
				$this->cPosition = 'relative';		//	Accordions...
				break;	

			default:
		
				$this->cPosition = 'initial';				
								
		}				
	}		
}

class TPanel extends TControl {

    private $oWnd			= null;
	private	$aControls		= array();
	private $nControls		= 0;
	public  $aDefaults		= array();
	public  $aClass 		= array();
	public  $aDlgs      	= array();	
	public  $cId_Init  		= '';	
	

    function __construct( $oWnd, $cId, $nTop = 0, $nLeft = 0, $nWidth = null, $nHeight = null, $cColor = '' ) {
	
		$nWidth  = TDefault( $nWidth , TWEB_PANEL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_PANEL_HEIGHT_DEFAULT );		

		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  );
		
		$this->cClass		= 'tweb_panel';	
		$this->cControl 	= 'tpanel';			
	}
	
	public function AddDialog( $cId = '', $cFunction = '' ){
		$this->aDlgs[] = array( $cId, $cFunction ) ;	
	}	
	
	public function AddControl( $oControl ){
		$this->aControls[] = $oControl ;
		$this->nControls++ ;
	}	

	public function SetDefault( $cControl = '', $cProperty = '' , $uValue = null ) {	
	
		if ( array_key_exists( $cControl,  $this->aDefaults ) ) 
			$aProp = $this->aDefaults[ $cControl ];	
		else
			$aProp = array();
			
		$aProp[ $cProperty ] = $uValue;
		
		$this->aDefaults[ $cControl ] = $aProp;				
	}	
	
	public function InitDefault( $o ) {
		if ( $this->cId !== -1 ) {	//	Si NO es un control estático
		
			$cControl = $o->cControl;		
		
			if ( array_key_exists( $cControl,  $this->aDefaults ) ) {
			
				$aProp 	= $this->aDefaults[ $cControl ];		

				foreach ($aProp as $key => $value) {		

					$uValue 	= $aProp[ $key ];
			
					switch ( $key ) {

						case 'SetClass()':  		$o->cClass = $uValue; break;
						case 'SetBorderInset()':  	$o->SetBorderInset(); break;
						case 'SetShadow()':  		$o->SetShadow(); break;
						case 'SetFont()':  			$o->SetFont( $uValue ); break;
					}
				}			
			}		
		}
	}

	
	public function Activate() {
	
		$this->CoorsUpdate();	
		
		$cHtml	= '<div id="' . $this->cId . '" name="' .  $this->cId . '" ';
			
			$cHtml .= $this->DefClass();	
			$cHtml .= $this->Datas();	
		
		//	STYLE -----------------------------------------
		
			$cHtml .= 'style="position: ' . $this->cPosition . '; ';
		
				$cHtml .= $this->StyleDim();
				$cHtml .= $this->StyleBorder();			
				$cHtml .= $this->StyleColor();		
				$cHtml .= $this->StyleFont();
				$cHtml .= $this->StyleCss();		
				$cHtml .= $this->StyleShadow();		
		
			$cHtml .= 'overflow: ' . $this->cOverflow . '; ' ;	
		
			//	Això pot ser important. Quan hi han estils a vegades tardan milésimes en 
			//	refrescarse. En canvi si ocultem la finestra, en el moment q estigui
			//	la finestra preparada la posem visible i desapareix l'efecte...
		
				//$cHtml .= 'display:none; ' ;	
			
		//	-------------------------------------------------------------------------

		$cHtml .= $this->StyleBorder();

		//if ( !empty( $this->cColor ) and  empty( $this->cClass ) )
//		if ( !empty( $this->cBackground ) )
//			$cHtml .= 'background:' . $this->cBackground . '; '; 		

		if ( $this->lHide ) 
			$cHtml .= 'display:none; ';		
		
		$cHtml .= '"> ' ;			

		echo $cHtml;	

		//	Pintado de Diálogos definidos en el TPanel ---------------------------------------------
		
		$nLen = count( $this->aDlgs );

		for ( $i = 0; $i < $nLen; $i++) {				

			$cId 	= $this->aDlgs[$i][0];
			$cFunc 	= $this->aDlgs[$i][1];
	
			if ( !empty( $cFunc ) ) {
			
				if ((int) function_exists( $cFunc ) > 0) {	
		
					call_user_func( $cFunc, $this ) ;					


					if ( $this->cId_Init !== $cId ) {
						$cJS 	 = '$( "#' . $cId . '" ).css( "display", "none" );';
						ExeJS( $cJS );	
					}				
					
				} else { 					
//					$cHtml .= $cCode ;	
				}
			}
		}			
		
		
		//	Pintado de Controles definidos en el TPanel --------------------------------------------
		
		for ( $i = 0; $i < $this->nControls; $i++) {
		
			$o = $this->aControls[$i] ;	
			
			$o->Activate();		
		}			
		
		echo '</div>';
		
		$this->DefKeys();
		
		//	Activar EvalWhen...
		
			//$cFunction = 'TWeb_EvalWhen();';		//Pendent de revisar...

			//ExeJSReady( $cFunction );

	}
	
	public function InitMessage( $bFunction = '' ) {
	
		$cFunction  = "var o = new TControl();" ;
		//$cFunction .= "o.InitMessage( '" . $bFunction . "' );" ;

		$cFunction .= "o.InitMessage( " . $bFunction . " );" ;
		
		ExeJSReady( $cFunction, 'Activando Message' );		
	}
	
	public function InitSignal( $bFunction = '' ) {	
	
		$cFunction  = "var o = new TControl();" ;
		$cFunction .= "o.InitSignal( '" . $bFunction . "' );" ;
		
		ExeJSReady( $cFunction, 'Activando Signal' );		
	}	
}
/*	---------------------------------------------------------------------------
	La clase TDiv pretende ser un TPanel pero sin ninguna clase ni css, pero
	con la capacidad de crear objetos dentro del div usando la misma manera 
	que se usa en toda la libreria. El control del div será a cargo del usuario
	via aplicación de su propio css... 
	--------------------------------------------------------------------------- */	

class TDiv extends TControl {

    private $oWnd			= null;
	private	$aControls		= array();
	private $nControls		= 0;
	public  $aDlgs      	= array();	
	public  $cId_Init  		= '';	
	

    function __construct( $oWnd, $cId, $cClass = '' ) {		

		parent::__construct( $oWnd, $cId  );
		
		$this->cClass		= $cClass;	
		$this->cControl 	= 'tdiv';			
	}
	
	public function AddDialog( $cId = '', $cFunction = '' ){
		$this->aDlgs[] = array( $cId, $cFunction ) ;	
	}	
	
	public function AddControl( $oControl ){
		$this->aControls[] = $oControl ;
		$this->nControls++ ;
	}

	public function InitDefault() {}
	
	public function Activate() {
		
		$cHtml	= '<div id="' . $this->cId . '" name="' .  $this->cId . '" ';	
		$cHtml .= 'data-control="tdiv" ';	
		
			if ( !empty( $this->cClass ) )
				$cHtml .= 'class="' . $this->cClass . '" ';
				
			//	Si hay controles en el interior del DIV marcaremos la position: relativa para
			//	que se situen correctament. Si el usuario no lo quiere ya lo podrá cambiar
			//	posteriormente via css...
				
				if ( $this->nControls > 0 ) 
					$cHtml .= 'style="position:relative; ';
		
		$cHtml .= '"> ' ;			

		echo $cHtml;	

		//	Pintado de Diálogos definidos en el TPanel ---------------------------------------------
		
		$nLen = count( $this->aDlgs );

		for ( $i = 0; $i < $nLen; $i++) {				

			$cId 	= $this->aDlgs[$i][0];
			$cFunc 	= $this->aDlgs[$i][1];
	
			if ( !empty( $cFunc ) ) {
			
				if ((int) function_exists( $cFunc ) > 0) {	
		
					call_user_func( $cFunc, $this ) ;					

					if ( $this->cId_Init !== $cId ) {
						$cJS 	 = '$( "#' . $cId . '" ).css( "display", "none" );';
						ExeJS( $cJS );	
					}				
					
				} else { 					
//					$cHtml .= $cCode ;	
				}
			}
		}					
		
		//	Pintado de Controles definidos en el TDiv --------------------------------------------
		
		for ( $i = 0; $i < $this->nControls; $i++) {
		
			$o = $this->aControls[$i] ;	
			
			$o->Activate();		
		}			
		
		echo '</div>';
	}
}

class TBar extends TControl {

	public  $oWnd		= false;
	private	$aControls	= array();
	private $nControls	= 0;
	public  $nBtnWidth  = 0;


	public function __construct( $oWnd , $cId = null, $nTop = 0, $nLeft = 0, $nWidth = null, $nHeight = null ) {  
	
		$nWidth  = TDefault( $nWidth , '100%' );
		$nHeight = TDefault( $nHeight, TWEB_BAR_HEIGHT_DEFAULT );	
		
		$this->cClass  		= 'tweb_bar';
		$this->cControl  	= 'tbar';
		$this->oWnd 		= $oWnd;
		$this->cId  		= $cId;
		$this->nTop 		= $nTop;
		$this->nLeft 		= $nLeft;
		$this->nWidth 		= $nWidth;
		$this->nHeight		= $nHeight;
		$this->nBtnWidth	= $nHeight;
		
		if ( gettype( $oWnd ) == 'object' ) {
			$oWnd->AddControl( $this );			// $this->oWndParent
		}		
	}

	public function AddControl( $oControl ){
		$this->aControls[] = $oControl ;
		$this->nControls++ ;
	}	
	
	public function AddButton( $cId, $cPrompt, $bClick = '', $cImage = '',  $cTooltip = '' ){
		
		$oBtn = new TButton( $this, $cId, 0, 0 , $cPrompt, $bClick );
		$oBtn->cImage  		= $cImage; // 'images.app/go.png';
		$oBtn->cClass 		= 'tweb_btnbar';		
		$oBtn->cControl		= 'tbtnbar';		
		$oBtn->cPosition	= '';		
		$oBtn->nHeight		= null;		
		$oBtn->nWidth		= $this->nBtnWidth;		
		
		return $oBtn;				
	}

	public function AddButtonFiles( $cId, $cPrompt, $bClick = '', $cImage = '', $lMultiple = true  ){
		
		$oBtn = new TButton( $this, $cId, 0, 0 , $cPrompt, $bClick );
		$oBtn->cImage  		= $cImage; // 'images.app/go.png';
		$oBtn->cClass 		= 'tweb_btnbar';		
		$oBtn->cControl		= 'tbtnbar';		
		$oBtn->cPosition	= '';		
		$oBtn->nHeight		= null;		
		$oBtn->nWidth		= $this->nBtnWidth;		
		$oBtn->lFiles 		= true;		
		$oBtn->lMultiple	= $lMultiple;		
		
		return $oBtn;				
	}

	
	public function Activate() {

		$this->CoorsUpdate();
	
		$cHtml	= '<div id="' . $this->cId . '" name="' .  $this->cId . '" ';		
		
			$cHtml .= $this->DefClass();								
			$cHtml .= $this->Datas();

			$cHtml .= 'style="';			
			$cHtml .= $this->StyleDim();			
			$cHtml .= $this->StyleCss();			
			$cHtml .= '"';			
			

		$cHtml .= '>';		
		
		$cHtml .= '<img  class="tweb_bar_separator_init" src="' . TWEB_PATH_IMAGES . '/separator.gif" height="' . $this->nHeight . '" ' ;
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'left: 0px ; width: 7px ;height: ' . $this->nHeight . 'px; ';		
		$cHtml .= 'overflow: hidden;">' ;		
		
		echo $cHtml;

		for ( $i = 0; $i < $this->nControls; $i++) {		

			$o = $this->aControls[$i] ;
			
			$o->Activate();			
		}
		
		$cHtml = '</div>';
			
		echo $cHtml;			
	}
	
	public function Separator(){	
		
		$cHtml  = '<div class="tweb_bar_separator" >' ;		
		$cHtml .= '<img src="' . TWEB_PATH_IMAGES . '/separator_simple.gif" height="' . $this->nHeight . '" ' ;		
		$cHtml .= 'style="width: 3px;height: ' . $this->nHeight . 'px; ';		
		$cHtml .= 'overflow: hidden;"/>' ;
		$cHtml .= '</div>' ;

		$o = new TCode( $this, $cHtml );			
	}	
	
	public function NewItem( $cId, $cCaption = '', $nWidth = 150, $cImage = '' ) {
	
		$oItem = array ( 'id' => $cId, 'caption' => $cCaption, 'width' => $nWidth, 'image' => $cImage );
		
		$this->aItems[] = $oItem;	
	}		
}


class TButton extends TControl {

	public $uValue		= '';
	public $cCssImg		= '';
	public $cCssText	= '';
	public $lFiles		= false;
	public $cIdFiles	= 'files';	// nombre del contenedor de los ficheros (name del input)
	public $cAccept  	= ''; 		// 'image/*'; // ".gif,.png",  http://www.w3schools.com/tags/att_input_accept.asp  	
	public $lMultiple	= false;
	private $aItems 	= array();
	public $cIcon 		= '';

	public function __construct( $oWnd , $cId = null, $nTop, $nLeft, $cCaption = 'Button',  $bClick = '' , $nWidth  = null, $nHeight = null, $cImage = '' ) {  
	
		$nWidth  = TDefault( $nWidth , TWEB_CONTROL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );			

		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cControl 	= 'tbutton';	
		$this->cClass			= 'tweb_button';													
		$this->cCaption		= $cCaption;													
		$this->bClick			= $bClick;													
		$this->nHeight		= $nHeight;																							
		$this->nWidth			= $nWidth;	
		$this->cImage			= $cImage;	
		
		//if ( gettype( $oWnd ) == 'object' ) 
		//	$oWnd->InitDefault( $this );		
	}	
	
	public function SetCssText( $cCss = '' ) { $this->cCssText = $cCss; } 
	public function SetCssImg( $cCss = '' ) { $this->cCssImg = $cCss; } 

	public function AddItem( $cPrompt, $cFunc, $cImg = '' ) {
		$this->aItems[] = array( 'prompt' => $cPrompt, 'func' => $cFunc, 'image' => $cImg );
	}
	
	public function Activate() {
	
		$cHtml = '';
	
		//	Check si es tipo files...
		
		if ( $this->lFiles ) {
		
			$this->cControl 	= 'tbuttonfiles';
		
			$cId_BtnFiles = '_buttonfiles_' . $this->cId;

			$cHtml  .= '<input type="file" name="' . $this->cIdFiles . '" id="' .  $cId_BtnFiles . '" ';	
			
			if ( !empty( $this->cAccept ) )
				$cHtml  .= ' accept="' . $this->cAccept . '" ';
			
			if ( $this->lMultiple )
				$cHtml  .= ' multiple ';
				
			$cHtml .= 'style=" display: none" '; 
			
			//if ( !empty( $this->bAction ) )
			//	$cHtml .= 'onChange="javascript: return ' . $this->bAction . ';" ';
			
			
			if ( !empty( $this->bClick ) )
				$cHtml .= 'onChange="javascript: return ' . $this->bClick . ';" ';
						
				
			$cHtml .= '/>'; 	
			
			$this->bClick = "$( '#" . $cId_BtnFiles . "').trigger('click') ";

		
		}
		
		//	Creamos Button...
		
		$this->CoorsUpdate();

		$cHtml  .= '<button type="button" id="' . $this->cId . '" ' ;

		switch ( $this->cControl ) {
		
			case 'tbtnbar':
			
				if ( $this->cControl == 'tbtnbar' ) {
				
					if ( $this->cAlign == 'right' ) 
						$this->cClass = 'tweb_btnbar tweb_btnbar_right';
					else
						$this->cClass = 'tweb_btnbar';
				} 
				
				break;													
		
		}
		
		if ( $this->lFiles ) {		
			$this->cControl = 'tbuttonfiles';
		}
		
		
			$cHtml .= $this->DefClass();
			$cHtml .= $this->Datas();

		//	STYLE -----------------------------------------			
		
			if ($this->cPosition == ''){
				$this->cPosition = 'relative';
			}  

			$cHtml .= 'style="position: ' . $this->cPosition . '; overflow: hidden; ';
			
			if ($this->nWidth > 0 ) {			
				$cHtml .= ' width: ' . $this->nWidth  . '; ';
			}	
			
			if ($this->nHeight > 0 ) {			
				$cHtml .= ' height:' . $this->nHeight . '; ';
			}	
			
			// $cHtml .= $this->StyleDim();
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();			
			$cHtml .= $this->StyleCss();		
				
			$cHtml .= '" '; 
		
		//	ATRIBUTTES ------------------------------------
		
			if ( $this->lDisabled )
				$cHtml .= 'disabled ' ;
				
			if ( !empty( $this->cTooltip ) )
				$cHtml .= 'title="' . $this->cTooltip . '" ';						
			
		//	EVENTS ----------------------------------------

			$cHtml .= $this->OnClick();									
			$cHtml .= $this->OnMessage();	

		//	-----------------------------------------------		
		
			
		$cHtml .= '>';

		if (!empty( $this->cImage ) ){		

			//$cHtml .= '<img class="btn_img" ' ;
			$cHtml .= '<img  ' ;
			$cHtml .= 'style="';
			$cHtml .= $this->cCssImg;
			$cHtml .= '" ';
			$cHtml .= 'src="' . $this->cImage . '" />';

		}		
		
		$cHtml .= '<p ';
		$cHtml .= 'style="margin:0px; ';
		$cHtml .= $this->cCssText;
		$cHtml .= '" ';				
		$cHtml .= '>' ;
		
		//	Fonts Awesome...
		
			if ( $this->cIcon ) {
				$nPos = strpos( $this->cIcon, '<i' );
				
				if ( $nPos === false ) 
					$cHtml .= '<i class="' . $this->cIcon . '"></i>&nbsp';
				else
					$cHtml .= $this->cIcon . '&nbsp';			
			}
			
		//	------------------------------		
		
		$cHtml .= $this->cCaption ;
		$cHtml .= '</p>' ;
		
		$cHtml .= '</button>' ;						
		
		echo $cHtml ;

		$this->DefMessage();
		$this->DefTooltip();


		$nItems = count( $this->aItems );
		
		if ( $nItems > 0 ) {
		
			$cScript  = 'var menu = new Array(); ';
		
			for ($i = 0; $i < $nItems; $i++) {
			
				$aItem = $this->aItems[$i];
			
				$cScript .= 'menu[' . $i . '] = { ' ;
				$cScript .= 'name: "' . $aItem[ 'prompt' ] . '" ,';
				$cScript .= 'img: "'  . $aItem[ 'image'  ] . '" ,';
				$cScript .= 'fun: '  . $aItem[ 'func'  ] . ' ';
				$cScript .= '}; ' . PHP_EOL ;			
			}		
			
			$cScript .= "var options = { triggerOn: '" 		. TWeb_SetupBtnItems::$triggerOn 		. "', ";
			$cScript .= "				 displayAround: '" 	. TWeb_SetupBtnItems::$displayAround 	. "', ";
			$cScript .= "				 position: '" 		. TWeb_SetupBtnItems::$position 		. "', ";
			$cScript .= "				 verAdjust: " 		. TWeb_SetupBtnItems::$verAdjust 		. ", ";
			$cScript .= "				 mouseClick: '" 	. TWeb_SetupBtnItems::$mouseClick 		. "' ";
			$cScript .= "}; " . PHP_EOL ;
										 
			$cScript .= "$('#" . $this->cId . "' ).contextMenu( menu, options );";	
			
			ExeJSReady( $cScript );
		}				

	}
}

class TSay extends TControl {

	public $lMemo 					= false;
	public $lVerticalAlign 	= true;
	public $lScrollbar 			= false;
	public $cIcon 					= '';

	public function __construct( $oWnd , $cId = null, $nTop, $nLeft, $cCaption = '', 
															 $nWidth  = null, $nHeight = null, $oFont = null, $cClass = '' ) {  	
	
		$nWidth  = TDefault( $nWidth , TWEB_CONTROL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass			= $cClass;  // Cambio AV 25-05-2020													
		$this->cControl 	= 'tsay';																									
		$this->cCaption		= $cCaption;													
		$this->oFont			= $oFont;	

		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );
		
	}	
	
	public function Activate() {
	
		if ( empty( $this->cClass ) )
			$this->cClass = ( $this->lMemo ) ? 'tweb_say_memo' : 'tweb_say';
			
		$nHeight_Initial = $this->nHeight;	//	CoorsUpdate transforma el numero a numero + px 
		
		$this->CoorsUpdate();
		
		$cHtml  = '<p id="' . $this->cId . '" align="' . $this->cAlign . '" ';

				
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();		
		
		//	STYLE -----------------------------------------		
		
			$cHtml .= 'style="position: absolute; display: table-cell; ';
			
				$cHtml .= $this->StyleDim();	
				$cHtml .= $this->StyleBorder();		
				$cHtml .= $this->StyleColor();			
				$cHtml .= $this->StyleFont();
				$cHtml .= $this->StyleCss();
				$cHtml .= $this->StyleShadow();
				
				if( $this->lMemo ) {		
				
					$cHtml .= 'white-space: pre-wrap; ' ;
					if ( $this->lScrollbar ) {
						$cHtml .= 'overflow: auto; ' ;
					}				
				}
				
				if( $this->lVerticalAlign &&  !$this->lMemo ) {		

					$nHeight = ( $this->lBorder ) ? ( $nHeight_Initial - 2 ) : $nHeight_Initial;
					$nHeight = $this->Unit( $nHeight );
					
					$cHtml .= 'line-height: ' . $nHeight . '; ';
				}
				
				if ( !empty( $this->bClick ) )
					$cHtml .= 'cursor: pointer; '; 

			$cHtml .= '" ';

		//	EVENTS ----------------------------------------

			$cHtml .= $this->OnClick();	

		//	-----------------------------------------------
		
		$cHtml .= '>' ; 
		
		//	Fonts Awesome...
		
			if ( $this->cIcon ) {
				$nPos = strpos( $this->cIcon, '<i' );
				
				if ( $nPos === false ) 
					$cHtml .= '<i class="' . $this->cIcon . '"></i>&nbsp';
				else
					$cHtml .= $this->cIcon . '&nbsp';			
			}
			
		//	------------------------------
		
		$cHtml .= $this->cCaption  ; 
		
		$cHtml .= '</p>' ; 
		
		echo $cHtml ;	
		
		$this->DefTooltip();	

	
	}
}

class TGet extends TControl {

	public  $lMemo			= false;
	public  $lEditor		= false;
	public  $uValue			= '';
	public  $lPassword		= false;
	public  $nMaxLength		= 0;
	public  $lDate			= false;
	public  $lTime			= false;
	public  $lBtnAction		= false;
	public  $cBtnImage		= 'tweb/images/go.png';	
	public  $cBtnAction		= '';	
	public  $cPicture		= false;
	public  $cPlaceHolder	= '';
	public  $cType			= 'text';

	public function __construct( $oWnd , $cId = null, $nTop = 0, $nLeft = 0, $cCaption = '', $nWidth = null, $nHeight = null, $cPicture = '', $bChange = ''  ) {  
		
		$nWidth  = TDefault( $nWidth , TWEB_CONTROL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );	

		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass		= '';													
		$this->cControl		= 'tget';													
		$this->cCaption		= $cCaption;																							
		$this->cAlign		= 'left';
		$this->cPicture		= $cPicture;
		$this->bChange		= $bChange;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}

	public function SetDate( $lDate = true, $cPicture = '99/99/9999' ) { 
		$this->lDate 	= $lDate ;	
		$this->cPicture = $cPicture ;			
	}

	public function SetTime( $lTime = true ) { $this->lTime = $lTime; }
	
	public function SetPicture( $cPicture = '' ) { $this->cPicture = $cPicture ;	}	
	public function SetChange( $bChange = '' ) { $this->bChange = $bChange ;	}
	
	public function BtnAction( $cAction = '', $cImage = '' ) {	
	
		$this->lBtnAction = true;
		$this->cBtnAction = $cAction;
		$this->cBtnImage  = $cImage;
	}	

	public function Activate() {
	
		if ( $this->lEditor )
			$this->lMemo = true;

		if ( empty( $this->cClass ) ) 
			$this->cClass = ( $this->lMemo ) ? 'tweb_getmemo' : 'tweb_get';
			
		if ( $this->lBtnAction ){		
			$nBtnLeft 	= $this->nLeft;
			$nBtnWidth 	= $this->nWidth;			
		}
		
		
		$this->cControl = ( $this->lMemo ) ? 'tgetmemo' : 'tget';
		
		$this->CoorsUpdate();
	
		if ( $this->lPassword )
			$cType  = 'password' ;
		else 
			$cType = $this->cType;	
		
		
		

		$cHtml  = '<div ';
		$cHtml .= 'class="tweb_get_container" ';
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; margin: 0px; padding:0px; overflow: hidden; background:transparent; ';		
		$cHtml .= $this->StyleDim();				
		$cHtml .= $this->StyleShadow();							
		
		if ( $this->nDeepShadow > 0 )
			$cHtml .= 'border-radius: ' . $this->nDeepShadow . 'px; ';
			
		// CAF NEW
		//if ( $this->lColorFocus ) {	
			//$cHtml .= 'background-color:focus: blue; color:white; ';		
		//}
		
		$cHtml .= '" ';	
		$cHtml .= '>';	
		
			if ( $this->lMemo || $this->lEditor ) {
				$cHtml  .= '<textarea id="' . $this->cId    . '" ';
			} else {
				$cHtml  .= '<input type="' . $cType . '" id="' . $this->cId . '" ' ;	
			}		

//$cHtml .= 'title=\'' . $this->cTooltip . '\' ';			
			
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
		
		//	STYLE -----------------------------------------
		
			$cHtml .= 'style="position: relative; width: 100%; height: 100%;';

				//$cHtml .= $this->StyleDim();
				$cHtml .= $this->StyleBorder();			
				$cHtml .= $this->StyleColor();		
				$cHtml .= $this->StyleFont();

				if ( $this->lHide ) 
					$cHtml .= 'display:none; ';		
					
				if ( ! $this->lMemo )	
					$cHtml .= 'text-align: ' . $this->cAlign . '; ';



			$cHtml .= '" '; 
		
		//	ATRIBUTTES ------------------------------------		
		
			if ( $this->lDisabled )
				$cHtml .= ' disabled ' ;
				
			if ( $this->lReadOnly )
				$cHtml .= ' readonly ' ;			

			if ( $this->nMaxLength > 0 ) 
				$cHtml .= 'maxlength="' . $this->nMaxLength . '" ';

			if ( !empty( $this->cPlaceHolder ) )
				$cHtml .= 'placeholder="' . $this->cPlaceHolder . '" ';
			
		//	EVENTS ----------------------------------------
		
			$cHtml .= $this->OnClick();	
			$cHtml .= $this->OnChange();	


		//	-----------------------------------------------							
		
		if ( $this->lMemo )	{	
			$cHtml .= '>';
			$cHtml .= $this->cCaption;
			$cHtml .= '</textarea>';
		} else {
			$cHtml .= 'value ="' . $this->cCaption . '" ';
			$cHtml .= '>';
		}
		
		
		$cHtml .= '</div>';
		
		//	BtnAction...
		
		if ( $this->lBtnAction ){		
		
			$cHtml  .= '<button class="tweb_get_button" type="button" id="tweb_btn_' . $this->cId . '" 
			name="' . $this->cId . '" ' ;
			$cHtml .= 'style="position: absolute; top:' . $this->nTop . '; left:' . $this->Unit( $nBtnLeft + $nBtnWidth + 3 ) . '; ';
			$cHtml .= '"'; 
				
			if ( !empty( $this->cBtnAction ) )
				$cHtml .= 'onClick="javascript: ' . $this->cBtnAction . ';" ';				
				
			$cHtml .= '>';
			
			if (!empty( $this->cBtnImage ) ){
				$cHtml .= '<img src="' . $this->cBtnImage . '" />';
			}

			$cHtml .= '</button>' ;

		}		
		
	
		echo $cHtml;
		
		$this->DefMessage();		
		$this->DefTooltip();		

		
		//	Javascript -----------------------------------------------------------
		
			$cFunction = '';
			
			//	Picture
		
			if ( !empty( $this->cPicture ) ){		
				$cFunction .= '$("#' . $this->cId . '" ).mask("' . $this->cPicture . '"); ';
			}		
			
			if ( $this->lDate ) {

				//$cFunction  .= '$( "#' . $this->cId . '" ).datepicker({ firstDay: 1  });' ;
				$cFunction  .= '$( "#' . $this->cId . '" ).datepicker();' ;
				
			} else if ( $this->lTime ){
			
				$cFunction  .= '$( "#' . $this->cId . '" ).timepicker();' ;
			}

			if ( !empty( $cFunction ) ) {
				ExeJSReady( $cFunction );
			}				
		
		//	----------------------------------------------------------------------
		
		$this->DefKeys();

		if ( $this->lEditor ) {
		
			$cFunction = 'TWeb_InitEditor( "' . $this->cId . '"); ';			
			
			ExeJS( $cFunction );
			/*
			if ( $this->lReadOnly || $this->lDisabled ) {			
				$cFunction = '$( "#' . $this->cId . '" ).sceditor("' . 'instance' . '").readOnly( true );' ;
				ExeJSReady( $cFunction );
			}
*/			
		}

		
	}	
}

Class THidden extends TControl {

	public  $uValue		= '';
	
	public function __construct( $oWnd , $cId = null, $uValue = ''  ) {  

		parent::__construct( $oWnd, $cId );
	
		$this->cClass		= '';													
		$this->cControl		= 'thidden';																	
		$this->uValue		= $uValue;																	
	} 
	
	public function Activate() {			

		$cHtml  = '<input type="hidden"  ';
		$cHtml .= $this->Datas();		
		$cHtml .= ' id="' . $this->cId . '" name="' . $this->cId  . '" value="' . $this->uValue . '" >';
		
		echo $cHtml ;				
	}	
}


class TRadio extends TControl {

	private $aItems		= array();
	private $nOption 	= 0;	

	public function __construct( $oWnd , $cId = null, $nOption = 0, $bChange = '' ) {  
	
		parent::__construct( $oWnd, $cId );
	
		$this->cClass		= 'tweb_radio';
		$this->cControl		= 'tradio';
		$this->nOption		= $nOption  ;		
		$this->bChange		= $bChange  ;		
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function AddItem( $cCaption = '', $nTop = 0, $nLeft = 0, $nWidth = null, $nHeight = null ) {
	
		$nWidth  = TDefault( $nWidth , TWEB_CONTROL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );		
	
		$this->aItems[] = array( 'cId' => $cCaption, 
								'nTop' => $nTop,
								'nLeft' => $nLeft, 
								'nWidth' => $nWidth,
								'nHeight' => $nHeight
								);		
	}
	
	public function Activate() {

		$cHtml 	= '';
		
		$nItems = count( $this->aItems );
		//$aMessage = array( $nItems );
		
		for ( $i = 0; $i < $nItems; $i++) {
		
			$aItem   = $this->aItems[$i];
			$cIndex  = $this->cId . '_' . ( $i + 1 );
			
			//$aMessage[ $i ] = $cIndex;

			$nTop 		= $this->Unit( $aItem[ 'nTop' ] );
			$nLeft		= $this->Unit( $aItem[ 'nLeft' ] );
			$nWidth		= $this->Unit( $aItem[ 'nWidth' ] );
			$nHeight	= $this->Unit( $aItem[ 'nHeight' ] );					
			
			$cHtml  .= '<div id="' . $this->cId . '" ';

			$cHtml .= $this->Datas();	
			
			$cHtml  .= 'style="position: absolute; box-sizing: border-box; overflow:hidden; ';
			
				$cHtml .= 'top: ' . $nTop . '; ';
				$cHtml .= 'left: ' . $nLeft . '; ';
				$cHtml .= 'width: ' . $nWidth . '; ';
				$cHtml .= 'height: ' . $nHeight . '; ';
				
				$cHtml .= $this->StyleBorder();			
				$cHtml .= $this->StyleColor();		
				$cHtml .= $this->StyleCss();				
			
			$cHtml .= '"> ' ;					

			$cHtml .= '<input type="radio" name="' . $this->cId . '" id="' . $cIndex . '" value="' . ( $i + 1 ) . '" ';				
			
			$cHtml .= $this->DefClass();
			
			if ( ( $i + 1 ) == $this->nOption ) {
				$cHtml .= ' checked ';
			}
			
			if ( $this->lDisabled )
				$cHtml .= ' disabled ' ;
			
			if ( $this->lReadOnly )
				$cHtml .= ' readonly ' ;
				
			$cHtml .= $this->OnChange();				
			$cHtml .= $this->OnClick();				
			
			$cHtml .= '>' ;
			
			$cHtml .= '<label class="' . $this->cClass . '" for="' . $cIndex . '" name="'  . $this->cId . '" ';
			
			$cHtml  .= 'style="';	

				$cHtml .= $this->StyleFont();
				$cHtml .= $this->StyleCss();				

			$cHtml .= '" >' . $aItem[ 'cId' ] . '</label>';
			
			$cHtml .= '</div>';						
			
		}										
		
		echo $cHtml ;
		
		$this->DefMessage( $this->cId );		
		
		/*for ( $i = 0; $i < $nItems; $i++) {
		
			$this->DefMessage( $aMessage[ $i ] );		
		}*/

	}		
	
}

class TCheckbox extends TControl {

	public $lChecked	= false;

	public function __construct( $oWnd , $cId = null, $nTop, $nLeft, $lChecked = false, $cCaption = '', $nWidth  = null, $nHeight = null  ) {  
	
		$nWidth  = TDefault( $nWidth , TWEB_CONTROL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );	
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass		= 'tweb_checkbox';
		$this->cControl		= 'tcheckbox';
		
		$this->lChecked		= $lChecked  ;
		$this->cCaption		= $cCaption  ;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		//	Este control necesita estar dentro de un div para que funcione Ok		
		
		$cHtml  = '<div ';
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
		$cHtml .= $this->StyleDim();			
		$cHtml .= $this->StyleBorder();
		$cHtml .= $this->StyleColor();				
		$cHtml .= '" ';	
		
		//	EVENTS ----------------------------------------

			if ( ! empty( $this->bClick ) ) {
				$cHtml .= $this->OnClick();	
			} elseif ( ! empty( $this->bChange ) ) {
				$cHtml .= $this->OnChange();
			}

		//	-----------------------------------------------
		
		$cHtml .= '> ';	
		
		
		$cHtml .= '<input type="checkbox" id="' . $this->cId . '" name="' . $this->cId . '" ' ;
			
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();		
 		
			if ( $this->lChecked )
				$cHtml .= ' checked ';
			
			if ( $this->lDisabled )
				$cHtml .= ' disabled ';					
			
		
		$cHtml .= ' >';
		
		if ( !empty( $this->cCaption ) ){ 
		
			$cIdLabel = '__tweblabel__' . $this->cId ;
			
			$cHtml .= '<label class="' . $this->cClass . '" for="' . $this->cId. '" ';			
			$cHtml .= 'id="' . $cIdLabel . '" ';			
			
			$cHtml .= 'style="';			
			$cHtml .= $this->StyleFont();
			$cHtml .= $this->StyleCss();				
			$cHtml .= '" >';									
			
			$cHtml .= '&nbsp;'; 
			$cHtml .= $this->cCaption; 

			$cHtml .= '</label>';
		}
		
		
		$cHtml .= '</div>';				

		echo $cHtml ;		
	}				
	
}

class TCombobox extends TControl {

	public $aItems 		= array();
	public $aItemsText	= array();
	public $cSelected	= '';
	public $cPosition	= '';
	public $lFilter 	= true;
	public $lSingle 	= true;
	public $lTop 		= false;
	public $nMaxHeight	= 250;
	public $lExtended	= true;
	

	public function __construct( $oWnd , $cId = null, $nTop, $nLeft, $aItems = array(), $aItemsText = array(), $cSelected = '', $nWidth  = null, $nHeight = null  ) {  

		$nWidth  = TDefault( $nWidth , TWEB_CONTROL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );		
		
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass		= '';																									
		$this->cControl		= '';																									
		$this->aItems 		= $aItems  ;
		$this->aItemsText 	= $aItemsText  ;
		$this->cSelected 	= $cSelected  ;																						
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {
	
		if ( $this->lExtended ) {
			$this->cControl		= 'tcombobox_ex';
			$this->cClass		= 'tweb_combobox_ex';					
		} else {
			$this->cClass		= 'tweb_combobox';		
			$this->cControl		= 'tcombobox';	
		}
		
		$this->CoorsUpdate();
		
		$cHtml = '';
		
		//	Este control necesita estar dentro de un div para que funcione Ok
		
		if ( $this->lExtended ) {
		
			$cHtml  .= '<div ';

			//	STYLE -----------------------------------------	
			
				$cHtml .= 'style="position: absolute;';
				$cHtml .= 'box-sizing: border-box;';
				$cHtml .= 'display: flex;';				
			
				$cHtml .= $this->StyleDim();	
				
				//$cHtml .= 'z-index:1; ' ;			
				$cHtml .= 'overflow: visible;" >' ;
			
			//	------------------------------------------------

				$cHtml .= '<select class="' . $this->cClass . '" name="' . $this->cId . '" id="' . $this->cId . '" ';
				
				$cHtml .= $this->Datas();				
				
				if ( ! $this->lSingle )
					$cHtml .= 'multiple="multiple" ';			
				
				$cHtml .= 'style="position: absolute; top: 0px; left: 0px; width: 100%; ';
				
				$cHtml .= '" ';	
			
			//	ATRIBUTTES ------------------------------------				
			
				if ( $this->lReadOnly || $this->lDisabled )
					$cHtml .= ' disabled ';			
				
				
			$cHtml .= '>';	
		
		} else {
		
				$cHtml .= '<select class="' . $this->cClass . '" name="' . $this->cId . '" id="' . $this->cId . '" ';				
				
				$cHtml .= $this->Datas();						
				
				$cHtml .= 'style="position: absolute; ';			
				
				$cHtml .= $this->StyleDim();	
				
				$cHtml .= $this->StyleBorder();	
				$cHtml .= $this->StyleFont();	
				$cHtml .= $this->StyleColor();	
				$cHtml .= $this->StyleCss();	
				
				$cHtml .= '" ';	
			
			//	ATRIBUTTES ------------------------------------				
			
				$cHtml .= $this->OnChange();
				
				if ( $this->lReadOnly || $this->lDisabled )
					$cHtml .= ' disabled ';			
				
				
			$cHtml .= '>';						
		}
			
		//	ITEMS...	-----------------------------------
		
			$nItems	= count( $this->aItems );
			$lText	= count( $this->aItemsText ) == $nItems ? true : false ;		
			$aSelected = ( gettype( $this->cSelected ) == 'array' ) ? $this->cSelected : array( $this->cSelected );
			
			
			for ( $i = 0; $i < $nItems; $i++) {		
			
				$cHtml .= '<option value="' . $this->aItems[ $i ] . '"';
				if ( in_array( $this->aItems[ $i ], $aSelected ) )
					$cHtml .= ' selected' ;																	

				$cHtml .= '>';				
					
				$cHtml .= ( $lText ) ? $this->aItemsText[ $i ] : $this->aItems[ $i ];
				
					
				$cHtml .= '</option>' ;		
			}	

			$cHtml .= '</select>';	
			
			
		if ( $this->lExtended ) {
		
			$cHtml .= '</div>';								
		}
		
		echo $cHtml ;
		
		if ( $this->lExtended ) {		
		
			$cFilter  = '{ ';
			$cFilter .= 'filter: ' . (( $this->lFilter ) ? 'true' : 'false' ) . ', ';
			$cFilter .= 'single: ' . (( $this->lSingle ) ? 'true' : 'false' ) . ', ';
			$cFilter .= 'position: "' .(( $this->lTop ) ? 'top' : '' ) . '", ';		
			
			if ( !empty( $this->bClick ) )
				$cFilter .= 'onClick: function(view) { ' . $this->bClick . ' },';		
				
			$cFilter .= 'maxHeight: ' . $this->nMaxHeight . ' ';		
			$cFilter .= '}';

			$cFunction  = '$( "#' .  $this->cId . '" ).multipleSelect( ' . $cFilter . ' );';
			
			//ExeJS( $cFunction, 'Activando combobox multiple: ' . $this->cId );					
			ExeJS( $cFunction );					
		}
	}
	
}

class TImage extends TControl {

	public  $cImageZoom	= '';
	public  $lZoom		= false;
	public  $cGallery	= '';	
	
	public function __construct( $oWnd , $cId = null, $nTop, $nLeft, $cImage = '', $nWidth  = null, $nHeight = null, $cImageZoom = '', $cCaption = '' ) {  
	
		$nWidth  = TDefault( $nWidth , TWEB_IMAGE_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_IMAGE_HEIGHT_DEFAULT );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass		= 'tweb_image';	
		$this->cControl 	= 'timage';	
		$this->cCaption		= $cCaption;		
		$this->cImage 		= $cImage;
		$this->cImageZoom 	= $cImageZoom;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {

		$this->CoorsUpdate();
		
		$cHtml  = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();			
			$cHtml .= $this->Datas();

		//	STYLE -----------------------------------------		
		
			$cHtml .= 'style="position: absolute; ';
			
			$cHtml .= $this->StyleDim();		
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();			
			$cHtml .= $this->StyleCss();
			$cHtml .= $this->StyleShadow();			
			
			$cHtml .= '" ' ;
			
		//	EVENTS ----------------------------------------
		
			$cHtml .= $this->OnClick();	

		//	-----------------------------------------------				
			
			
		$cHtml .= '> ' ;
		
		//	ZOOM o BIG IMAGE -----------------------------
		
		
			if (!empty( $this->cImageZoom ) or ( $this->lZoom ) ){  
			
				if ( empty( $this->cImageZoom ) )
					$this->cImageZoom = $this->cImage;
				
				$cHtml .= '<a href="' . $this->cImageZoom . '" '; 
				
				if ( empty( $this->cGallery ) ){				
					$cHtml .= 'data-lightbox="fwimg_' . $this->cId . '" '; 
				} else {
					$cHtml .= 'data-lightbox="' . $this->cGallery . '" '; 
				}
				
				if ( !empty( $this->cCaption ) ){
					$cHtml .= 'data-title="' . $this->cCaption . '" ';
				}
				
				$cHtml .= ' >';
			}						
		
			//	IMAGEN
			
			//$cHtml .= '<span style="display:block;text-align:center;" >';

			$cHtml .= '<img src="' . $this->cImage . '" ' ;			
				$cHtml .= 'style="position: absolute; ';
				$cHtml .= 'top: 0px; left: 0px; width: 100%; height:100%; ' ;
				
				if ( empty( $this->cImage ) ) {
					$cHtml .= 'display:none;';
				}
				
			$cHtml .= '">' ;
			//$cHtml .= '</span>' ;
			
			//	ZOOM o BIG IMAGE	

			if (!empty( $this->cImageZoom ) or ( $this->lZoom ) ) {			
				$cHtml .= '</a>';			
			}						
			
		$cHtml .= '</div>';			
	
		echo $cHtml;

		$this->DefMessage();				
	}
}


class TLogo extends TControl {
	public $cClass_Image = 'tweb_logo_image';
	
	public function __construct( $oWnd , $cId = '1', $cImage = '' ) {  
	
		parent::__construct( $oWnd, $cId, 0, 0, '100%', '100%'  );
	
		$this->cClass		= 'tweb_logo';	
		$this->cControl 	= 'tlogo';	
		$this->cImage 		= $cImage;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {

		$this->CoorsUpdate();
		
		$cHtml  = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();			
			$cHtml .= $this->Datas();

		//	STYLE -----------------------------------------		
		
			$cHtml .= 'style="position: relative; ';
			
			//$cHtml .= $this->StyleDim();		
			//$cHtml .= $this->StyleBorder();
			//$cHtml .= $this->StyleColor();			
			//$cHtml .= $this->StyleCss();
			
			$cHtml .= '" ' ;
			
		//	EVENTS ----------------------------------------
		
			$cHtml .= $this->OnClick();	

		//	-----------------------------------------------				
			
			
		$cHtml .= '> ' ;

			$cHtml .= '<img src="' . $this->cImage . '" ' ;			

			
			$cHtml .= 'class="' . $this->cClass_Image . '" >';
			
		$cHtml .= '</div>';			
	
		echo $cHtml;				
	}
}

class TFolder extends TControl {

	private	$aTabs		= array();
	private	$aControls	= array();
	private $nControls	= 0;
	public  $cIcon  	= '';


    public function __construct( $oWnd, $cId = null, $nTop = 10, $nLeft = 10, $nWidth = null, $nHeight = null, $cColor = CLR_HGRAY ) {     

		$nWidth  = TDefault( $nWidth , TWEB_FOLDER_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_FOLDER_HEIGHT_DEFAULT );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass		= 'tweb_folder';													
		$this->cControl		= 'tfolder';				
		$this->cColor	= $cColor ;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}    
	
    public function __destruct() {}
	
	public function AddTab( $cId = '', $cLabel = 'Label', $cFunction = '', $cIcon = '' ){
		$this->aTabs[] = array( $cId, $cLabel, $cFunction, $cIcon ) ;	
	}	
	
	public function AddControl( $oControl ){
		$this->aControls[] = $oControl ;
		$this->nControls++ ;
	}
	
	public function Activate() {
	
		$this->CoorsUpdate();		

		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
	
		
		$cHtml .= 'style="position: ' . $this->cPosition . '; ';
		$cHtml .= 'box-sizing: border-box; ';
		
			$cHtml .= $this->StyleDim();
			$cHtml .= $this->StyleBorder();			
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleFont();
			$cHtml .= $this->StyleCss();		
			
		//$cHtml .= 'overflow: hidden;"> ' ;			
		$cHtml .= 'overflow: ' . $this->cOverflow . ';"> ' ;			
		
		echo $cHtml; 		
		
//		Pintem 	les pestanyes...

		$nLen = count( $this->aTabs );
		
		$cHtml = '<ul>';
		
		$aType = array();
		
		for ( $i = 0; $i < $nLen; $i++) {		
		
			$cId 		= $this->aTabs[$i][0];
			$cPrompt 	= $this->aTabs[$i][1];
			$cFunction	= $this->aTabs[$i][2];
			$cIcon		= $this->aTabs[$i][3];
			
			//	Check if cFunction is a php file 

				$nPos = strrpos( $cFunction,'.',-1) ;

				if ( $nPos ) {				
					$cExt = strtolower( substr( $cFunction, $nPos + 1, strlen( $cFunction ) ) ); 				
				} else {
					$cExt = '';
				}							
			
				
				if ( $cExt == 'php' ) {
				
					$cType = 'php' ;
					
				} else if ( ((int) function_exists( $cFunction ) > 0) ) {
				
					$cType = 'function';
					
				} else { 
				
					$cType = 'code';
				}
				
			$aType[] = array( $cType, $cFunction );
			

			if ( $cType !== 'php' ) {

				$cHtml .= '<li><a href="#' . $cId . '" style=\'line-height: 16px;\'>'; 
				
				if ( !empty( $cIcon ) ){
				
					if ( substr( $cIcon, 0, 7 ) == 'ui-icon' ) {                    
						$cHtml .= '<span class="ui-icon ' . $cIcon . '"></span>';
					} else {
						$cHtml .= '<img style=\'vertical-align: middle;\' src=\'' . $cIcon . '\' />';                                                    
					}				
				}
				
				if ( !empty( $cPrompt ) ){
					$cHtml .= '&nbsp' . $cPrompt . '&nbsp&nbsp' ;	
				}
				
				$cHtml .= '</a></li>';				

			}  else {
			
				$cPhp 	= $cFunction;
				$cHtml .= '<li><a href="' . $cPhp . '">' . $cPrompt . '</a></li>';				
			}
		}	
		
		$cHtml .= '</ul>';		
		
		echo $cHtml; 
		
		$cHtml = '';

		$nLen = count( $aType );

		for ( $i = 0; $i < $nLen; $i++) {				

			$cType	= $aType[$i][0];
			
			switch ( $cType ) {
			
				case 'function':
					$cFunction = $aType[$i][1];
					
					call_user_func( $cFunction, $this );					
					
					break;
					
				case 'code':
					$cCode 	 = $aType[$i][1];
					$cHtml  .= $cCode;
					break;
			
				case 'php':
			
					// Pendiente...;
					break;
			}		
		
		}							

		$cHtml .= '</div>';			

		echo $cHtml;
		
//		--------------------------------------------------------------------------------------------		
	
		for ( $i = 0; $i < $this->nControls; $i++) {
		
			$o = $this->aControls[$i] ;
			
			$o->Activate();		
		}					
			
		
		echo '</div>';
		
		$cHtml  = '<script>';
		$cHtml .= '$(function() {';
		$cHtml .= '   $( "#' . $this->cId . '" ).tabs(); ' ;
		$cHtml .= ' });';
		$cHtml .= '</script>';
		
		$cHtml .= '<script>';
		$cHtml .= '$(function() {';
		$cHtml .= '   $( "#' . $this->cId . '" ).tabs({';
		$cHtml .= '		beforeLoad: function( event, ui ) {';
		$cHtml .= '            ui.jqXHR.error(function(oError) {';
		$cHtml .= '               console.log( oError );';
		$cHtml .= '               ui.panel.html( _( "_error_tab_load_php" )  );';
		$cHtml .= '               MsgError( oError.responseText );';
		$cHtml .= '				});';
		$cHtml .= ' 		},';
		$cHtml .= ' 	activate: function( event, ui ) {';
		$cHtml .= ' 	      var oPanel = ui.newPanel[0] ;';
		$cHtml .= ' 	      var cId = $(oPanel).attr("id");';
		$cHtml .= ' 	      var fn = window["' . $this->bChange . '"];';
		$cHtml .= ' 	      if (typeof fn === "function") {';
		$cHtml .= ' 	      	var fnparams = [ cId ];	';
		$cHtml .= ' 	      	fn.apply(null, fnparams );	';
		$cHtml .= ' 	      }';
		$cHtml .= ' 	      		';
		$cHtml .= ' 		}';
		$cHtml .= ' 	});';
//		$cHtml .= ' 	$( "#' . $this->cId . '" ).draggable({ disabled: true });';		
		$cHtml .= '});';
		$cHtml .= '</script>';
	
		echo $cHtml;					
		
	}

}


class TProgressBar extends TControl {

	public $nValue	= 0;

	public function __construct( $oWnd , $cId = null, $nTop, $nLeft, $nValue = 0, $nWidth  = null, $nHeight = null  ) {  
	
		$nWidth  = TDefault( $nWidth , TWEB_CONTROL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass		= 'tweb_progressbar';
		$this->cControl		= 'tprogressbar';
		$this->nValue		= $nValue;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {
		
		$this->CoorsUpdate();

		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
			$cHtml .= $this->StyleDim();			
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleCss();
			$cHtml .= $this->StyleShadow();					
		
		$cHtml .= '"> ';			
		
		$cHtml .= '</div>';				

		echo $cHtml ;		
		
//		Inicalitzem widget...
		
		$cFunction  = '$(document).ready(function() {' ;
		$cFunction .= "     var o = $( '#" . $this->cId  . "' ); ";		
		$cFunction .= "     o.progressbar( { value: 0, max: 100 } ); ";		
		$cFunction .= "     o.find( 'div.ui-progressbar-value' ).css( 'background', '" . $this->cColor . "'); ";			
		$cFunction .= '}); ' ;
		
		ExeJS( $cFunction );		
	}				
	
}

class TGroup extends TPanel {

	private	$aControls	= array();
	private $nControls	= 0;
	public  $lChecked	= false;
	
	public function __construct( $oWnd , $cId = null, $nTop, $nLeft, $cCaption = 'Group', $nWidth  = null, $nHeight = null  ) { 
	
		$nWidth  = TDefault( $nWidth , TWEB_GROUP_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_GROUP_HEIGHT_DEFAULT );	
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass		= 'tweb_group';
		$this->cControl		= 'tgroup';
		$this->cCaption		= $cCaption;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}
	
	public function AddControl( $oControl ){
		$this->aControls[] = $oControl ;
		$this->nControls++ ;
	}		
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box;';
		$cHtml .= 'margin: 0px;';
		$cHtml .= 'overflow: hidden;';
		
			$cHtml .= $this->StyleDim();			
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();
			$cHtml .= $this->StyleCss();		
			$cHtml .= $this->StyleShadow();				
			
		$cHtml .= '"> ';																		

		$cHtml .= '<fieldset style="top:0px;left:0px;width:100%;height:100%;box-sizing: border-box;">';
		
		if ( !empty( $this->cCaption ) ) {
			$cHtml  .= '<legend style="';
				$cHtml .= $this->StyleFont();			
			$cHtml  .= '">';
			$cHtml  .= $this->cCaption;						

			$cHtml  .= '</legend>';			
		}
			
		$cHtml  .= '</fieldset>';
		
		echo $cHtml;

		for ( $i = 0; $i < $this->nControls; $i++) {		

			$o = $this->aControls[$i] ;
			
			$o->Activate();				
		}
		
		$cHtml = '</div>';				

		echo $cHtml ;			
	}				
	
}


class TCalendar extends TControl {

	public $nValue	= 0;
	public $bClick 	= '';

	public function __construct( $oWnd , $cId = null, $nTop, $nLeft, $nValue = 0, $nWidth  = null, $nHeight = null  ) {  

		$nWidth  = TDefault( $nWidth , TWEB_CONTROL_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );

		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass		= 'tweb_calendar';
		$this->cControl		= 'tcalendar';
		$this->nValue		= $nValue;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
	
			$cHtml .= $this->StyleDim();			
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleCss();
			$cHtml .= $this->StyleShadow();					
		
		$cHtml .= '"> ';			
		
		$cHtml .= '</div>';				

		echo $cHtml ;		
		
//		Inicalitzem widget...
		
		$cFunction  = '$(document).ready(function() {' ;
		$cFunction .= "     var o = $( '#" . $this->cId  . "' ); ";		
		$cFunction .= "     var aOptions = { ";		
			
		if ( !empty( $this->bClick ) )
			$cFunction .= "onSelect: " . $this->bClick . " ";		
		
		$cFunction .= "}; ";		
		
		$cFunction .= "     o.datepicker( aOptions ); ";				
		$cFunction .= '}); ' ;

		ExeJSReady( $cFunction );		

	}				
}

class TBlock extends TPanel {

	private	$aControls	= array();
	private $nControls	= 0;
	
	public function __construct( $oWnd , $cId = null, $nHeight = null, $cColor = CLR_HGRAY) { 	
	
		$nHeight = TDefault( $nHeight, TWEB_CONTROL_HEIGHT_DEFAULT );	

		parent::__construct( $oWnd, $cId, null, null, null, $nHeight, $cColor );
	
		$this->cClass	= 'tweb_block';
		$this->cControl	= 'tblock';
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}
	
	public function AddControl( $oControl ){
		$this->aControls[] = $oControl ;
		$this->nControls++ ;
	}		
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: relative; ';
		$cHtml .= 'box-sizing: border-box;';
		$cHtml .= 'margin: 0px;';
		
		if ( $this->lHide )
			$cHtml .= 'display:none;';
			
		$cHtml .= 'height: ' . $this->nHeight . '; ';
		$cHtml .= 'overflow: auto;';
							
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();
			$cHtml .= $this->StyleCss();							
		
		$cHtml .= '">';																		
		
		echo $cHtml;

		for ( $i = 0; $i < $this->nControls; $i++) {		

			$o = $this->aControls[$i] ;
			
			$o->Activate();				
		}
		
		$cHtml = '</div>';				

		echo $cHtml ;			
	}					
}




class TBox extends TControl {

	public function __construct( $oWnd , $cId = null, $nTop, $nLeft, $nWidth = null, $nHeight = null, $cColor = CLR_GRAY ) {

		$nWidth  = TDefault( $nWidth , TWEB_BOX_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_BOX_HEIGHT_DEFAULT );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  );
	
		$this->cClass		= 'tweb_box';
		$this->cControl		= 'tbox';
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
			$cHtml .= $this->StyleDim();			
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleCss();
			$cHtml .= $this->StyleShadow();		
		
		$cHtml .= '"> ';			
		
		$cHtml .= '</div>';				

		echo $cHtml ;			
	}					
}

class TFrame extends TControl {

	public $cSrc = '';

	public function __construct( $oWnd , $cId = null, $nTop, $nLeft, $cSrc = '', $nWidth = null, $nHeight = null, $cColor = CLR_GRAY ) {

		$nWidth  = TDefault( $nWidth , TWEB_FRAME_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_FRAME_HEIGHT_DEFAULT );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  );
	
		$this->cClass		= 'tweb_frame';
		$this->cControl		= 'tframe';
		$this->cSrc			= $cSrc;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
			$cHtml .= $this->StyleDim();			
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleCss();
			$cHtml .= $this->StyleShadow();		
		
		$cHtml .= '"> ';			
		
		$cHtml .= '<iframe src="' . $this->cSrc . '" width="100%" height="100%"  frameborder="0" marginheight="0" >';	// scrolling=no
		$cHtml .= '</iframe>';								
		
		$cHtml .= '</div>';				

		echo $cHtml ;			
	}					
}


//	TObject
// 	Difference between iframe, embed and object elements --> https://stackoverflow.com/questions/16660559/difference-between-iframe-embed-and-object-elements

/*
http://www.ohlone.edu/org/webteam/proceduresnotes/objecttag.html
MIME Type 	File Type / Extension
text/html 	html, htm
application/msword 	doc (docx?)
application/pdf 	pdf
application/rtf 	rtf
application/vnd.ms-excel 	xls
application/vnd.ms-powerpoint 	ppt
application/x-director 	dcr
application/x-mspublisher 	pub
application/x-shockwave-flash 	swf
audio/mpeg 	mp3
text/plain 	txt
video/mpeg 	mpg, mpeg
video/quicktime 	mov, qt
video/x-msvideo 	avi
video/x-ms-wmv 	wmv
*/

class TObject extends TControl {

	public $cSrc  = '';
	public $cType = '';

	public function __construct( $oWnd , $cId = null, $nTop, $nLeft, $cSrc = '', $cType = '',  $nWidth = null, $nHeight = null, $cColor = CLR_GRAY ) {

		$nWidth  = TDefault( $nWidth , TWEB_OBJECT_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_OBJECT_HEIGHT_DEFAULT );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  );
	
		$this->cClass		= 'tweb_object';
		$this->cControl		= 'tobject';
		$this->cSrc			= $cSrc;
		$this->cType		= $cType;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
			$cHtml .= $this->StyleDim();			
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleCss();
			$cHtml .= $this->StyleShadow();				
		
		$cHtml .= '"> ';			
		
		$cHtml .= '<object data="' . $this->cSrc . '" type="' .  $this->cType .'" width="100%" height="100%" frameborder="0" marginheight="0" scrolling=no>';
		$cHtml .= '    <embed src="' . $this->cSrc . '" type="' .  $this->cType .'" width="100%" height="100%" frameborder="0" marginheight="0" scrolling=no/>';
		$cHtml .= '</object>';								
		
		$cHtml .= '</div>';				

		echo $cHtml ;			
	}					
}


class TMaps extends TControl {

	public function __construct( $oWnd , $cId = null, $nTop, $nLeft, $nWidth = null, $nHeight = null, $cColor = CLR_GRAY ) {

		$nWidth  = TDefault( $nWidth , TWEB_MAPS_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_MAPS_HEIGHT_DEFAULT );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  );
	
		$this->cClass		= 'tweb_maps';
		$this->cControl		= 'tmaps';
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}	

	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
			$cHtml .= $this->StyleDim();			
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleCss();
			$cHtml .= $this->StyleShadow();			
		
		$cHtml .= '"> ';			
		
		$cHtml .= '</div>';				

		echo $cHtml ;	


	}					
}


class TYoutube extends TControl {

	private $cSource  		= '';

	public function __construct( $oWnd , $cId = null, $nTop, $nLeft, $cSource = null, $nWidth = null, $nHeight = null, $cColor = CLR_GRAY) {

		$nWidth  = TDefault( $nWidth , TWEB_YOUTUBE_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_YOUTUBE_HEIGHT_DEFAULT );		
		$cSource = TDefault( $cSource, '' );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  );
	
		$this->cClass		= 'tyoutube_maps';
		$this->cControl		= 'tyoutube';
		$this->cSource		= $cSource;
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}

    public function SetVideo( $cSource = '') { $this->cSource = $cSource ; }		
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
			$cHtml .= $this->StyleDim();			
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleCss();
			$cHtml .= $this->StyleShadow();				
		
		$cHtml .= '"> ';

		$cHtml .= '<iframe width="100%" height="100%" ';
		$cHtml .= 'src="https://www.youtube.com/embed/' . $this->cSource . '" frameborder="0" allowfullscreen>';
		$cHtml .= '</iframe>';		
		
		$cHtml .= '</div>';				

		echo $cHtml ;			
	}					
}

class TTree extends TControl {

	public  $aData  	= array();
	public  $bClick  	= '';
	public  $bDblclick  = '';
	public  $lCheckbox 	= false;
	public  $nAnimation	= 0;	

	public function __construct( $oWnd , $cId = null, $nTop, $nLeft, $nWidth = null, $nHeight = null, $cColor = CLR_HGRAY, $aData = null ) {

		$nWidth  = TDefault( $nWidth , TWEB_TREE_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_TREE_HEIGHT_DEFAULT );						
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  );
	
		$this->cClass		= '';
		$this->cControl		= 'ttree';
		
		if ( gettype( $aData ) == 'array' ) {
			$this->aData = $aData;
		}
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
		
	}

	public function SetTree( $aData = array() ) { $this->aData = $aData; }	
	
	public function Activate() {
		
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
				
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: auto;';
		
			$cHtml .= $this->StyleDim();			
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleCss();		
			$cHtml .= $this->StyleShadow();
			
		$cHtml .= '"> ';
		
		$cHtml .= '</div>';				

		echo $cHtml ;	
		
		
		//	Inicialitzem la Clase de Javascript --------------------------------------------------------
		
			$cScript  = 'var aDat = ' . json_encode( $this->aData ) . ';' ; 		

			$cScript .= 'var oT = new TTree( "' . $this->cId . '", aDat );';
			
			$cScript .= 'oT.nAnimation = ' . $this->nAnimation . ';' ;				
			
			if ( $this->bClick !== '' )
				$cScript .= 'oT.bClick = ' . $this->bClick . ';' ;
				
			if ( $this->bDblclick !== '' )
				$cScript .= 'oT.bDblclick = ' . $this->bDblclick . ';' ;	
				
			if ( $this->lCheckbox )			
				$cScript .= 'oT.lCheckbox = true;';			
		
			$cScript .= 'oT.Init();';
			
			ExeJS( $cScript );
	}					
}

class TAccordion extends TControl {

	public  $lDefaultIcons  = true;
	public  $aIcons  		= array();
	private	$aTabs			= array();
	public  $nOption    	= null;

	public function __construct( $oWnd , $cId = '', $nTop, $nLeft, $nWidth = null, $nHeight = null, $cColor = CLR_GRAY ) {

		$nWidth  = TDefault( $nWidth , TWEB_ACCORDION_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_ACCORDION_HEIGHT_DEFAULT );						
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight, $cColor  );
	
		$this->cClass		= 'tweb_accordion';
		$this->cControl		= 'taccordion';		
		
		if ( gettype( $oWnd ) == 'object' ) 
			$oWnd->InitDefault( $this );		
	}
	
	public function AddTab( $cId = '', $cLabel = 'Label', $cFunction = '', $cCode = '' ){
		$this->aTabs[] = array( $cId, $cLabel, $cFunction, $cCode ) ;	
	}	

	public function SetIcons( $cIconClosed, $cIconOpen = null ) {

		if ( $cIconClosed )
			$this->aIcons[] = $cIconClosed;
		
		if ( $cIconOpen )
			$this->aIcons[] = $cIconOpen;

		$this->lDefaultIcons = false;
	}
	
	public function Activate() {

		$this->CoorsUpdate();
		
		//	Accordion Caja		
	
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();			
				
			$cHtml .= 'style="position: absolute; ';
			$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
			$cHtml .= $this->StyleDim();			
			$cHtml .= $this->StyleBorder();
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleCss();
			$cHtml .= $this->StyleShadow();			
			$cHtml .= '"';		
		
		$cHtml .= '>';	
		
		//	Accordion Contenido
		
		$cHtml .= '<div id="' . $this->cId . '_content" >';		
		
		echo $cHtml;
		
		//	Tabs... --------------------------------------------------------------------------------		
		
			$nLen = count( $this->aTabs );		
			
			for ( $i = 0; $i < $nLen; $i++) {		
			
				$cId_Tab	= $this->aTabs[$i][0];
				$cPrompt 	= $this->aTabs[$i][1];
				$cFunction 	= $this->aTabs[$i][2];
				$cCode 		= $this->aTabs[$i][3];
				
				//	Inicio Tab con el titulo y un div contenedor del diálogo...
				
					echo '<h3>' . $cPrompt . '</h3>';
					echo '<div style="padding:0px;">';				

					if ( !empty( $cFunction ) ) {	
					
						if ((int) function_exists( $cFunction ) > 0) {		
					
							call_user_func( $cFunction, $this );					
						} 
					}														
	
				//	Cierro div contenedor
					echo '</div>';
			}		
		
		//	----------------------------------------------------------------------------------------
			
			echo '</div>';	//	Cierra Content	
		echo '</div>';		//	Cierra Caja
		
		$cFunction = '';
	
		//	Icones...
		
			$cType = gettype(  $this->aIcons );
			
			if ( $cType == 'array' && ( count( $this->aIcons ) > 0 )) {
			
				if ( count( $this->aIcons ) == 2 )
					$cFunction .= 'var icons = { header:"' . $this->aIcons[0] . '",activeHeader:"' . $this->aIcons[1] . '"}; ';					
				else 
					$cFunction .= 'var icons = { header:"' . $this->aIcons[0] . '",activeHeader:"' . $this->aIcons[0] . '"}; ';					
					//$cFunction .= 'var icons = {}; ';	

			} else {
			
				if ( $this->lDefaultIcons )
					$cFunction .= 'var icons = { header: "iconClosed", activeHeader: "iconOpen" }; ';		
				else
					$cFunction .= 'var icons = null; ';		
			}

			if ( $this->nOption ) 
				$cFunction .= 'var nOption = ' . ( $this->nOption - 1 ) . ';';
			else
				$cFunction .= 'var nOption = false;';			
		
		$cFunction .= '   $( "#' . $this->cId . '_content" ).accordion( { icons: icons, collapsible: true, active: nOption, heightStyle: "fill" }); ' ;
		$cFunction .= '   $( "#' . $this->cId . '").resize( function() {';
		$cFunction .= '       $( "#' . $this->cId . '_content" ).accordion( "refresh" );';
		$cFunction .= '   });';				

		ExeJS( $cFunction );
	}					
}

class TMsgItem extends TControl {

	public $cImage 	= '';

	public function __construct( $oWnd , $cId = '1', $nWidth = null, $cCaption = '',  $cImage = '' , $bClick = '' ) {  	
	
		$nWidth  = TDefault( $nWidth , TWEB_PANEL_WIDTH_DEFAULT );
		
		parent::__construct( $oWnd, $cId, 3, 0, $nWidth, 21, CLR_HGRAY );
												
		$this->cClass		= 'tweb_msgitem' ;													
		$this->cControl 	= 'tmsgitem';													
		$this->cCaption		= $cCaption;													
		$this->bClick		= $bClick;			
		$this->cImage		= $cImage;			
	}	
	
	public function Activate() {		
		
		$this->CoorsUpdate();
		
		$cHtml  = '<div id="' . $this->cId . '" ';
		$cHtml .= 'style="position: absolute; display: table-cell; ';
			
				$cHtml .= $this->StyleDim();	
				$cHtml .= $this->StyleBorder();		
				$cHtml .= $this->StyleColor();			
				$cHtml .= $this->StyleFont();
				$cHtml .= $this->StyleCss();	
				
		$cHtml .= '" ';
		
		$cHtml .= $this->DefClass();		
		$cHtml .= $this->Datas();	
		$cHtml .= $this->OnClick();	
		$cHtml .= '> ' ;	
		
		$cHtml .= '<img id="msgitem_img_' . $this->cId . '" src="' . $this->cImage . '" ';
		
		if ( $this->cImage == '' )
			$cHtml .= 'style="display:none;" ';
		
		$cHtml .= '>';
				
		$cHtml .= '<p id="msgitem_txt_' . $this->cId . '" align="' . $this->cAlign . '" >';
		
		$cHtml .= $this->cCaption  ; 
		
		$cHtml .= '</p>' ; 
		$cHtml .= '</div>' ; 
		
		echo $cHtml ;	
	}
}

class TMenu extends TControl {

	private	$aMenu		= array();
	private	$cTarget	= '';

    public function __construct( $oWnd, $cId = '', $nTop = null, $nHeight = 24, $cColor = '#F2F2F2' ) {     
	
		parent::__construct( $oWnd, $cId, $nTop, 0, '100%', $nHeight, $cColor  );
	
		$this->cClass		= 'tweb_menu';
		$this->cControl		= 'tmenu';
	}    
	
    public function __destruct() {}
	
	public function AddMenu( $cId = '', $cPrompt = '' ){
	
		$oSubmenu		= new TSubmenu();
		$oMenu 			= array( 'cId' => $cId, 'cPrompt' => $cPrompt, 'oSubmenu' => $oSubmenu );
		
		$this->aMenu[] 	= $oMenu;
		
		return $oSubmenu;		
	}	
	
	public function AddSubmenu( $cId = '', $cPrompt = '' ){	
	
	
	}
	
	public function Activate() {
	
		$this->CoorsUpdate();
		
		$cHtml = '<div id="' . $this->cId . '" ';
		
			$cHtml .= $this->DefClass();		
			$cHtml .= $this->Datas();

		$cHtml .= 'style="position: ' . $this->cPosition . '; ';
		$cHtml .= 'box-sizing: border-box; ';
		
			$cHtml .= $this->StyleDim();
			$cHtml .= $this->StyleBorder();			
			$cHtml .= $this->StyleColor();		
			$cHtml .= $this->StyleFont();
			$cHtml .= $this->StyleCss();		
			
		$cHtml .= 'overflow: hidden;" ' ;									
		$cHtml  .= '> '; 		
		
		$nMenu = count( $this->aMenu );		
	
		$cHtml .= '<ul>';
		
		for ( $i = 0; $i < $nMenu; $i++) {
		
			$cId 		= $this->aMenu[$i][ 'cId' ];
			$cPrompt 	= $this->aMenu[$i][ 'cPrompt' ];
			
			$cHtml .= '	<li id="' . $cId . '">' . $cPrompt ;					
			$cHtml .= '	</li>';				
		}	

		$cHtml .= '</ul>';			
		
		$cHtml .= '</div>';						
		
		echo $cHtml;
		
//		Inyectamos JAVASCRIPT MENU ---------------------------------------------------------

		$aoMenu = array();
		$nMenu = count( $this->aMenu );		
	
		$cFunction = '';
		
		for ( $i = 0; $i < $nMenu; $i++) {
		
			$cId 		= $this->aMenu[$i][ 'cId' ];
			$cPrompt 	= $this->aMenu[$i][ 'cPrompt' ];			
			$oSubmenu 	= $this->aMenu[$i][ 'oSubmenu' ];			
			
			$aItems = $oSubmenu->GetItems();
			$nItems = count( $aItems );	
			
			if ( $nItems > 0 ) {
			
				$cVar = "oMenu" . $i;
				$aoMenu[] = $cVar;
				
				$cFunction  .= "var " . $cVar ." = new TSubMenu( '" . $cId . "' );";						
				
				//$cFunction  .= FWeb_Submenu( $cVar, null, $aItems );
				$cFunction  .= $this->Build_SubMenu( $cVar, null, $aItems );			
			}
		}
		
		ExeJS( $cFunction );	

	//	Activo MENU	

		$cFunction  = '$(document).ready(function() {' ;
		
			$nMenu = count( $aoMenu );
			
			for ( $i = 0; $i < $nMenu; $i++) {
			
				$oMenu = $aoMenu[$i];
	
				$cFunction .= "     " . $oMenu . ".Init(); ";								
			}
		
		$cFunction .= '}); ' ;

		ExeJS( $cFunction );		
		
	}
	
	private function Build_SubMenu( $oMenu, $cId, $aItems ) {
	
		$cParent = is_null( $cId ) ? 'null' :  $cId ;
		
		$nItems = count( $aItems );	
		
		$cFunction 	= '';
		$cLog 		= '';
		
		for ( $j = 0; $j < $nItems; $j++) {			
		
			$cItem_Id  = $aItems[$j][ 'cId'		];
			$cPrompt   = $aItems[$j][ 'cPrompt'	];
			$bAction   = $aItems[$j][ 'bAction'	] == null ? 'null' : "'" . $aItems[$j][ 'bAction'	] . "'";
	//		$bAction   = 'null';
			$cImage    = $aItems[$j][ 'cImage'	];
			$cTooltip  = $aItems[$j][ 'cTooltip' ] == null ? 'null' : "'" . $aItems[$j][ 'cTooltip'	] . "'";
			$lDisabled = $aItems[$j][ 'lDisabled'	] == null ? 'false' : "'" . $aItems[$j][ 'lDisabled' ] . "'";
			$oSubmenu  = $aItems[$j][ 'oSubmenu'];
			

			$aItemsSub = $oSubmenu->GetItems();
			$nItemsSub = count( $aItemsSub );	
			
	// 		El addslashes serveisx per si hi ha texte p.e. amb apostrofes   l'email...		
			
			$cCmd 		= $cItem_Id . " = " . $oMenu . ".Item( " . $cParent . ", '" . addslashes($cPrompt) . "' , " . $bAction . ", '" . $cImage . "', " . $lDisabled . ", " .  $cTooltip . " );"  ;
		
			$cFunction .= $cCmd ;
				
			if ( $nItemsSub > 0 ) {
			
				$cFunction .= $this->Build_SubMenu( $oMenu, $cItem_Id, $aItemsSub );
			}				
		}
		
		return $cFunction;				
	}
	
}

class TSubmenu {

	private	$aItems		= array();
	
    public function __construct() { 			
	
	}	
	
	public function AddItem( $cId = '', $cPrompt = '', $bAction = '', $cImage = '', $lDisabled = false, $cTooltip = '' ){	
	
		$oSubmenu		= new TSubmenu();
	
		$this->aItems[] = array( 'cId' => $cId, 'cPrompt' => $cPrompt, 'bAction' => $bAction, 'cImage' => $cImage, 'lDisabled' => $lDisabled ,'cTooltip' => $cTooltip, 'oSubmenu' => $oSubmenu );
		
		return $oSubmenu;
	}
	
	public function GetItems(){ return $this->aItems; }		
}

function TDefault( $uValue, $uDefault ){

	return ( gettype( $uValue ) == 'NULL' ) ? $uDefault : $uValue;
}

function ExeJS( $cFunction, $cLog = null ) {

	$cEcho   =  "<script type='text/javascript'>";
	$cEcho  .= 	       $cFunction ;
	
	if ( $cLog )
		$cEcho  .=  "console.info( 'info', '" . $cLog . "' );";
	
	
	$cEcho  .= "</script>";
	
	echo $cEcho;
}

function ExeJSReady( $cFunction = '', $cLog = null ) {

	$cEcho  = "<script type='text/javascript'>";
	$cEcho .= "  $( document ).ready(function() {";
	$cEcho .= 		$cFunction ;
	
	if ( $cLog )
		$cEcho .= "console.info( 'info', '" . $cLog . "' );";
	
	$cEcho .= "  })";
	$cEcho .= "</script>";	
	
	echo $cEcho;
}

function _Msg( $cMsgType = 'info', $uValue = null ) {

	$uVal = is_null( $uValue) ? '' : $uValue;

	if ( gettype( $uVal ) === 'array' ){
	
		$cString = '';
		
		foreach ($uVal as $key => $value) {
			$cType =  gettype( $value );
			
			if ( $cType !== 'array' ) 		
				$cString .= "[$key] => (" . $cType . ") $value \\n";
			  else
				$cString .= "[$key] => (" . $cType . ")";
		}									
	} else {
		$cString = $uVal ;
	}

	$cFunction  = '$(document).ready(function() {' ;
	
	switch ( $cMsgType ) {
	
		case 'info' 	: $cFunction .= '  MsgInfo ( "' . $cString . '" );';  break;
		case 'error' 	: $cFunction .= '  MsgError( "' . $cString . '" );';  break;

	}

	$cFunction .= '});';						
	
	ExeJS( $cFunction );	
}

function MsgInfo ( $uValue = '' ) { _Msg( 'info' , $uValue ); }
function MsgError( $uValue = '' ) { _Msg( 'error', $uValue ); }

if ( isset( $_GET[ 'tweb_version' ] ) ) ExeJS( "console.log( '" . TWEB_VERSION . "' )" );

//	Setups Menu Button ----------------------------------------------------------------------

	//	See demo and documentation on http://ignitersworld.com/lab/contextMenu.html

	function TWeb_SetupBtnItems( $aSetup ) {

		if ( array_key_exists( 'triggerOn', $aSetup ) )	
			TWeb_SetupBtnItems::$triggerOn 		= $aSetup[ 'triggerOn' ];
			
		if ( array_key_exists( 'displayAround', $aSetup ) )	
			TWeb_SetupBtnItems::$displayAround	= $aSetup[ 'displayAround' ];	
			
		if ( array_key_exists( 'position', $aSetup ) )
			TWeb_SetupBtnItems::$position			= $aSetup[ 'position' ];
			
		if ( array_key_exists( 'verAdjust', $aSetup ) )
			TWeb_SetupBtnItems::$verAdjust		= $aSetup[ 'verAdjust' ];
			
		if ( array_key_exists( 'mouseClick', $aSetup ) )
			TWeb_SetupBtnItems::$mouseClick		= $aSetup[ 'mouseClick' ];
	}

	class TWeb_SetupBtnItems {

		public static $triggerOn 		= 'click';
		public static $displayAround 	= 'trigger';
		public static $position			= 'bottom';
		public static $verAdjust		= -2;
		public static $mouseClick		= 'right';
	}
	
//	Setup Tooltips ------------------------------------------------

	function TWeb_SetupTooltips( $aSetup ) {

		if ( array_key_exists( 'image', $aSetup ) ) TWeb_SetupTooltips::$cImage = $aSetup[ 'image' ];			
		if ( array_key_exists( 'show', $aSetup ) ) 	TWeb_SetupTooltips::$cShow  = $aSetup[ 'show' ];				
		if ( array_key_exists( 'hide', $aSetup ) ) 	TWeb_SetupTooltips::$cHide  = $aSetup[ 'hide' ];							
	}

	class TWeb_SetupTooltips {		
		public static $cImage 	= TWEB_PATH . '/images/info2.png';
		public static $cShow 	= '{ duration: "fast"  }';
		public static $cHide 	= '{ effect: "hide"  }';
	}

//	---------------------------------------------------------------
?>