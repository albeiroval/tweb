<?php
/**
 * CLASS TMysql 
 * Autor : Albeiro Valencia
 * Fecha : 08/02/2022
 * Clase para trabajar bases de datos MySQL
 * Documentacion : https://codeshack.io/super-fast-php-mysql-database-class/
 *                 https://www.w3schools.com/php/func_mysqli_query.asp
 * 
 * Test de Rendimiento de la clase :
 * http://www.spearheadsoftwares.com/tutorials/php-performance-benchmarking/50-mysql-fetch-assoc-vs-mysql-fetch-array-vs-mysql-fetch-object
 * 
 * https://www.ionos.es/digitalguide/hosting/cuestiones-tecnicas/introduccion-a-mysqli/
 * https://stackoverflow.com/questions/38857768/multiple-calls-to-stmt-bind-param
 * https://www.w3schools.com/php/php_mysql_prepared_statements.asp
 * 
 */

class TMysql {

  protected $host   = "";
  protected $user   = "";
  protected $pass   = "";
  protected $dbname = "";
  protected $port   = 3306;
  protected $socket = "";
  protected $mysql  = null;
  protected $resul  = null;
  protected $query_closed = true;
  protected $show_errors  = true;
  protected $isObject     = false; 
  protected $arrayResul   = [];

  function __construct( $host = "", $user = "", $pass = "", $dbname = "", $port = null, $socket = "" ) 
  {
    $this->host    = $host;
    $this->user    = $user;
    $this->pass    = $pass;
    $this->dbname  = $dbname;
    $this->port    = $port;
    $this->socket  = $socket;                      
    $this->connect();
  }

  //---------------------------------------------------------

  public function connect() {

    mysqli_report(MYSQLI_REPORT_STRICT);
    
    try {
      $this->mysql = new mysqli( $this->host, $this->user, $this->pass, $this->dbname, $this->port ); 
    } catch( Exception $e ) {
      printf("No ha sido posible conectar con la base de datos,<br>por favor espere o revise la configuración");
    }

    // https://www.php.net/manual/es/mysqli.set-charset.php
    if (!$this->mysql->set_charset("utf8")) {
      printf("Error cargando el conjunto de caracteres utf8: %s\n", $this->mysql->error);
    } 
    
    // Check connection
    if ( $this->mysql->connect_errno) {
      $this->error("Failed to connect to MySQL: " . $this->mysql->connect_error);
    }

    // Check if server is alive
    if ( !$this->mysql->ping() ) {
      $this->error("Failed server off line : ". $this->mysql->error);
    }

    $this->mysql->autocommit(false);

  }

  //---------------------------------------------------------

  public function query( $sql = "" ) : bool {

    $this->isObject = true;

    // Free query if exist
    if ( !$this->query_closed ) {
      $this->resul->close();
    }

    try {

      $this->resul = $this->mysql->query($sql);
      $this->query_closed = false;
      $success = true;

      if( !$this->resul ) {
        throw new Exception('Error en TMySQL : method query. ' . $sql);
      }

    } catch( Exception $e ) {

      $this->error( $e->getMessage() );
      $this->query_closed = true;
      $success = false;
    }

    return $success;

  }

  //---------------------------------------------------------
  
  public function getrow() { 

    if ( $this->isObject ) {
       $row = $this->resul->fetch_assoc(); 
    } else {
       $row = array_shift( $this->arrayResul ); 
    }  

    if ( $row ) {
      $row = $this->utf8_converter($row);
    }
  
    return $row;
  
  }

  //---------------------------------------------------------

  public function execute( $sql ) : bool {

    // Free query if exist
    if ( !$this->query_closed ) {
      $this->resul->close();
    }

    // https://www.php.net/manual/es/exception.getmessage.php
    try {

      $this->resul = $this->mysql->query($sql);
      $this->query_closed = true; 
      $success = true;

      if( !$this->resul ) {
        throw new Exception('Error TMySQL : method execute ' . $sql);
      }

      $this->mysql->commit();

    } catch( Exception $e ) {

      $this->mysql->rollback();

      $this->error( $e->getMessage() );
      $this->query_closed = true;
      $success = false;

    }

    return $success;

  }

  //---------------------------------------------------------

  public function transaction( $sql ) : bool {

    // Free query if exist
    if ( !$this->query_closed ) {
      $this->resul->close();
    }

    try {

      $this->mysql->begin_transaction();
      
      $this->resul = $this->mysql->query($sql);
      
      if( !$this->resul ) {
        throw new Exception('Error en TMySQL : method transaction');
      }

      $this->mysql->commit();

      $this->query_closed = true;
      $success = true;

    } catch ( Exception $e ) {

      $this->mysql->rollback();

      $this->error( $e->getMessage() );
      $this->query_closed = true;
      $success = false;

    }

    return $success;
  
  }

  //---------------------------------------------------------

  public function IncrementValue( $Table = '', $Field = '' )  {

    if ( empty($Table) || empty($Field) ) {
      throw new Exception('Error en TMySQL : method IncrementValue, Table or Field Empty');
    }

    $this->isObject = true;

    $number = -1;

    try {

      $this->mysql->begin_transaction();

      $cType = $this->FieldType( $Table, $Field );

      if ( $cType == "int" || $cType == "decimal" ) {
        
        $row = $this->GetLastValue( $Table, $Field );

        $number = $row[ $Field ] + 1;

        /* Actualiza el contador agregando 1 */
        $sql = "UPDATE $Table SET $Field = $number LIMIT 1";
        
        $this->resul = $this->mysql->query($sql);
        if( !$this->resul ) {
          throw new Exception('Error en string SQL : UPDATE IncrementValue');
        }

        $this->mysql->commit();

        $this->query_closed = false;
      
      }

    } catch ( Exception $e ) {

      $this->mysql->rollback();

      $this->error( $e->getMessage() );
      $this->query_closed = true;
      
    }

    return $number;
  
  }

  //---------------------------------------------------

  private function GetLastValue( $Table, $Field )  {

    $sql = "SELECT $Field FROM $Table ORDER BY $Field DESC LIMIT 1";

    $this->resul = $this->mysql->query($sql);

    if( !$this->resul ) {
      throw new Exception('Error en string SQL : method GetLastValue');
    }

    return $this->getrow();  
    
  }  

  //---------------------------------------------------

  private function FieldType( $Table, $Field )  {

    $type = "NULL";

    $sql = "SELECT DATA_TYPE 
            FROM information_schema.COLUMNS 
            WHERE TABLE_NAME = '$Table' AND COLUMN_NAME = '$Field' LIMIT 1";
    
    $this->resul = $this->mysql->query($sql);

    if( !$this->resul ) {
      throw new Exception('Error en string SQL : method FieldType');
    }

    if ( $row = $this->getrow() ) {
      $type = $row["DATA_TYPE"];
    } 

    return $type;
  
  }  

  //---------------------------------------------------
  
  /* 
  * $sql = "INSERT INTO table1 (name, age) VALUES (?,?);"
  * $bindParams = ["hello", 15] 
  * $bindParams = [$param1, $param2, ... ]
  */

  public function bind_params( $sql, $bindParams, $insertUpdate = false ) : bool {
    
    // Free query if exist
    if ( !$this->query_closed ) {
      $this->resul->close();
    }

    $stmt = $this->mysql->prepare($sql);

    if ( $stmt ) {   
      
      $this->query_closed = true; 

      $sqltype = '';  // types of params
      $sqldata = [];  // parameters

      foreach($bindParams as $data) {  
        $sqltype  .= $this->_gettype($data);
        $sqldata[] = $data; 
      }

      array_unshift( $sqldata, $sqltype ); // agrega los types al inicio del array $sqldata

      call_user_func_array([$stmt, 'bind_param'], $this->refValues($sqldata));  // execute $smtm->bind_params

      try {

        if ( $insertUpdate ) {
          $this->mysql->begin_transaction();
        }  
        
        $stmt->execute();
        if ($stmt->errno) {
          throw new Exception('Error TMySQL : Unable to process MySQL query (check your params) - ' . $stmt->error);
        }
        
        if ( $insertUpdate ) {
          $this->mysql->commit();
        }  

        $success = true;

      } catch ( Exception $e ) {

        if ( $insertUpdate ) {
          $this->mysql->rollback();
        }  

        $this->error( $e->getMessage() );
        
        $success = false;

      }

      $this->isObject = false;
      
      if ( $success ) {
         $this->arrayResul = $this->get_result( $stmt );
      }

    } else {

      $this->error('Error TMySQL : method bind_params ' . $sql);
      
      $success = false;
    
    }

    $stmt->close();

    return $success;
    
  }

  //---------------------------------------------------

  public function insert( $table, $aColumns, $aValues ) : bool {
    
    $this->query_closed = true; 

    // Asigna Columnas
    $columns = "(";
    foreach ($aColumns as $item) {
      $columns .= $item . ",";
    }
    $columns = substr($columns, 0, -1);
    $columns .= ") ";

    // Asigna Valores
    $values = " VALUES ";
    if ( is_array($aValues[0]) ) {
      foreach ($aValues as $row) {
        $values .= "(";
        foreach ($row as $item) {
          $values .= $this->getValueEscape($item) . ",";
        }
        $values = substr($values, 0, -1);
        $values .= "),";
      }
      $values = substr($values, 0, -1);
    } else {
      $values .= "(";
      foreach ($aValues as $item) {
          $values .= $this->getValueEscape($item) . ",";
        }
        $values = substr($values, 0, -1);
        $values .= ")";
    }  
    
    // Crea Sentencia
    $sql = "INSERT INTO $table " . $columns . $values;

    try {

      $this->mysql->begin_transaction();

      $this->resul = $this->mysql->query($sql);
      
      $success = true;

      if( !$this->resul ) {
        throw new Exception('Error TMySQL : method insert ' . $sql);
      }

      $this->mysql->commit();

    } catch( Exception $e ) {

      $this->mysql->rollback();

      $this->error( $e->getMessage() );
      $success = false;

    }

    return $success;

  }

  //---------------------------------------------------------

  private function getValueEscape( $value ) {

    $resul = " ";

    switch ( gettype($value) ) {
      case 'string':
        $resul = "'" . $this->escape_string($value) . "'";
        break;

      case 'integer':
        $resul = $value;
        break;
        
      case 'double':
        $resul = $value;
        break;  
      
      default:
        $resul = " ";
        break;
    }

    return $resul;
  }

  //---------------------------------------------------------

  private function _gettype($var) {
    if (is_string($var)) return 's';
    if (is_float($var)) return 'd';
    if (is_int($var)) return 'i';
    return 'b';
  }

  //---------------------------------------------------------

  private function refValues($arr) {
    if (strnatcmp(phpversion(),'5.3') >= 0) //Reference is required for PHP 5.3+
    {
        $refs = array();
        foreach($arr as $key => $value)
            $refs[$key] = &$arr[$key];
        return $refs;
    }
    return $arr;  
  }

  //---------------------------------------------------------

  private function utf8_converter($array) {
    array_walk_recursive($array, function(&$item, $key){
      if(!mb_detect_encoding($item, 'utf-8', true)){
          $item = utf8_encode($item);
      }
    }); 
    return $array;
  }

  //---------------------------------------------------------
  // https://stackoverflow.com/questions/10752815/mysqli-get-result-alternative

  private function get_result( $stmt ) {
    $RESULT = [];
    $stmt->store_result();
    for ( $i = 0; $i < $stmt->num_rows; $i++ ) {
        $Metadata = $stmt->result_metadata();
        $PARAMS = [];
        while ( $Field = $Metadata->fetch_field() ) {
            $PARAMS[] = &$RESULT[ $i ][ $Field->name ];
        }
        call_user_func_array( array( $stmt, 'bind_result' ), $PARAMS );
        $stmt->fetch();
    }
    return $RESULT;
  }

  //---------------------------------------------------------

  public function escape_string( $value ) {
		return $this->mysql->real_escape_string( $value );
  }

  //---------------------------------------------------------
  
  public function num_rows() {
    return $this->resul->num_rows;
  }

  //---------------------------------------------------------

  public function affectedRows() {
		return $this->mysql->affected_rows;
  }
  
  //---------------------------------------------------------

  public function lastInsertID() {
    return $this->mysql->insert_id;
  }

  //---------------------------------------------------------

  public function error($error) {
    if ($this->show_errors) {
       exit( $error );
    }
  }

  //---------------------------------------------------------

  // Free connection 
  public function close() {
    $this->mysql->close();
  }

}

//-----------------------------------------------------------------------

function filter_post( $varname, $defvalue = '' ) {

  $input = filter_input(INPUT_POST, $varname);
  
  if ( !isset($input) ) {
    if ( $defvalue ) {
      $input = $defvalue;
    } else  {
      $input = '';
    }   
  }
  
  return $input;
}  

//-----------------------------------------------------------------------

function filter_get( $varname, $defvalue ) {
  
  $input = filter_input(INPUT_GET, $varname);
  
  if ( !isset($input) ) {
    if ( $defvalue ) {
      $input = $defvalue;
    } else  {
      $input = '';
    }   
  }
  
  return $input;
} 


/*
*  if (!filter_input(INPUT_GET, "email", FILTER_VALIDATE_EMAIL)) {
*    echo("Email is not valid");
*  } else {
*    echo("Email is valid");
*  }
*/

?>