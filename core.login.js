/*
*  CLASS TLogin
*  SOLO PRA USO DE LA LIBRERIA TVALWEB
*	 AUTOR : ALBEIRO VALENCIA
*  FECHA : 06/10/2021
*/

class TLogin {
  
  constructor(cId, cMask = '\u25CF') {

    var cId = '#' + cId;
    var idUser = 'username';
    var idPassword = 'password';
    var cMaskPass = cMask;

    this.show = function () {

      $(cId).modal({ show: true, backdrop: "static" });

      setTimeout(function () {
        document.getElementById(idUser).value = "";
        document.getElementById(idPassword).value = "";
      }, 500);

      document.getElementById(idUser).focus();

    };

    this.hide = function () {
      $(cId).modal('hide');
    };

    this.getuser = function () {
      let elem = document.getElementById(idUser);
      return elem.value;
    };

    this.getpassword = function () {
      let elem = document.getElementById("pass-hide");
      return elem.value;
    };

    this.init = function () {

      new MaskedPassword(document.getElementById(idPassword), cMaskPass);

      var user = document.getElementById(idUser);
      var clave = document.getElementById(idPassword);

      // Bloque control input usuario
      user.onpaste = function (e) {
        e.preventDefault();
        MsgNotify("esta acción está prohibida", "error");
      };

      user.oncopy = function (e) {
        e.preventDefault();
        MsgNotify("esta acción está prohibida", "error");
      };

      user.addEventListener("keydown", function (event) {
        if (event.key === "Enter") {
          event.preventDefault();
          clave.focus();
        }
      });

      // Bloque control input clave
      clave.onpaste = function (e) {
        e.preventDefault();
        MsgNotify("esta acción está prohibida", "error");
      };

      clave.oncopy = function (e) {
        e.preventDefault();
        MsgNotify("esta acción está prohibida", "error");
      };

      clave.addEventListener("keydown", function (event) {
        if (event.keycode == 13) {
          event.preventDefault();
          alert("Enter!");
        }
      });

      $(".toggle-password").click(function () {
        let elem = document.getElementById("pass-hide");
        MsgNotify(elem.value, "error");
      });

      /*
      $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
         } else {
            input.attr("type", "password");
         }
      });
      */
    };

    this.init();

  }
}

// Funcion submit para usar con Recaptcha

function onSubmit(token) {
  document.getElementById("form-login").submit();
}

// FINAL