<?php

class TNavBar { 
  private $aTitle1    = [ "text" => 'MAIN MENU', "color" => 'black', "size" => '14' ];
  private $aTitle2    = null;
  private $cId        = '';
	private $cColor     = '#e3f2fd';
  private $aOptions   = [];
  private $aButtons   = [];
  
  //-----------------------------------------------------------
	
	public function __construct( $cId = '', $cColor = '#e3f2fd' ) {
    $this->cId		= $cId;
		$this->cColor = $cColor;
  }
  
  //-----------------------------------------------------------

	public function AddOption( $cId, $cText, $cFunction ) {
		$oOption = new NavbarItem( $cId, $cText, $cFunction );
		$this->aOptions[] = $oOption;
		return $oOption;
  }

  //-----------------------------------------------------------

	public function AddButton( $cId, $cText, $cFunction, $cIcon = '', $colorTXT = 'black' ) {
		$oButton = new NavbarButton( $cId, $cText, $cFunction, $cIcon, $colorTXT );
		$this->aButtons[] = $oButton;
		return $oButton;
  }

  //-----------------------------------------------------------

  public function Title( $cText = 'MAIN MENU', $color = 'black', $size = '14' ) {
    $this->aTitle1 = [ "text" => $cText, "color" => $color, "size" => $size ];    
  }

  //-----------------------------------------------------------

  public function SubTitle( $cText = 'SUB TITLE', $color = 'black', $size = '14' ) {
    $this->aTitle2 = [ "text" => $cText, "color" => $color, "size" => $size ];    
  }
  
  //-----------------------------------------------------------

	private function CreateItems() {

    $cHtml  = '<div class="navbar-collapse collapse" id="navbarColor03" style="">' . PHP_EOL;
    $cHtml .= '<ul class="navbar-nav mr-auto">' . PHP_EOL;

		$nCount = count( $this->aOptions );
		for ( $i = 0; $i < $nCount; $i++ ) {
			$option = $this->aOptions[ $i ];
			if ( ! $option->dropDown ) {
				$cHtml .= $this->item( [	'id'   => $option->cId,
																	'text' => $option->cText,
																	'func' => $option->cFunction ], $i );		
			}	else {
				$cHtml .= $this->dropDown( $option );
			}
		}	

    $cHtml .= '</ul>' . PHP_EOL;

    $cHtml .= $this->buttons();
    
    $cHtml .= '</div>' . PHP_EOL;

		return $cHtml;

  }
  
  //-----------------------------------------------------------

	private function item( $aitem, $i ) {

    if ( $i  == 0 ) {
      $cHtml  = '<li class="nav-item active">' . PHP_EOL;
      $cHtml .= '<a class="nav-link" href="#" onclick="' . $aitem['func'] . '">' . $aitem['text'] . '<span class="sr-only">(current)</span></a>' . PHP_EOL;
      $cHtml .= '</li>' . PHP_EOL;
    } else {  
      $cHtml  = '<li class="nav-item">' . PHP_EOL;
      $cHtml .= '<a class="nav-link" href="#" onclick="' . $aitem['func'] . '">' . $aitem['text'] . '</a>' . PHP_EOL;
      $cHtml .= '</li>' . PHP_EOL;
    }  

		return $cHtml;
  }
  
	//-----------------------------------------------------------

	private function dropDown( $option ) {

    $cHtml  = '<ul class="nav nav-pills">' . PHP_EOL;
    $cHtml .= ' <li class="dropdown" style="margin-top: 7px;padding-right: .5rem;padding-left: .5rem;">' . PHP_EOL;
    $cHtml .= '   <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: rgba(0,0,0,.5);">' . $option->cText . '<b class="caret"></b></a>' . PHP_EOL;
    $cHtml .= '   <ul class="dropdown-menu">' . PHP_EOL;

    $nCount = count( $option->aItems );
		for ( $i = 0; $i < $nCount; $i++ ) {
      $item = $option->aItems[$i];
      $cHtml .= '<li class="dropdown-item" onclick="return ' . $item[1] . '">' . $item[0] . '</li>' . PHP_EOL;
    }
    $cHtml .= '   </ul>' . PHP_EOL;
    $cHtml .= ' </li>' . PHP_EOL;
    $cHtml .= '</ul>' . PHP_EOL; 

		return $cHtml;
  } 

  //-----------------------------------------------------------
  
  // https://dev.to/codeply/how-to-make-a-icon-text-navbar-in-bootstrap-4-5gbc
  // https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_icon_buttons
  private function buttons() {

    $cHtml = " ";

    $nCount = count( $this->aButtons );

    if ( $nCount > 0) {
      $cHtml .= '<ul class="navbar-nav nav-justified text-left">' . PHP_EOL;
    }

		for ( $i = 0; $i < $nCount; $i++ ) {
      $aItem = $this->aButtons[$i];
      $cHtml .= '<li id="navbar-btn" class="btn btn-link">';
      $cHtml .= '<a class="navbar-label" style="color: ' . $aItem->colorTXT . '"'; 
      $cHtml .= ' onclick="' . $aItem->cFunction . '">'. PHP_EOL;
      $cHtml .= '  <i class="navbar-icon fa ' . $aItem->cIcon . '" aria-hidden="true"></i> ' . $aItem->cText . PHP_EOL;
      $cHtml .= '</a>' . PHP_EOL;
      $cHtml .= '</li>' . PHP_EOL;
    }

    if ( $nCount > 0) {
       $cHtml .= '</ul>' . PHP_EOL;
    }
    
    return $cHtml;
      
  }
	
  //-----------------------------------------------------------

	public function Activate() {

    // load CSS
    $cHtml = '<link href="' . TWEB_PATH . 'core.navbar.css" rel="stylesheet">';

    // create HTML
    $cHtml .= $this->createHTML();

    echo $cHtml;
    
  } 

  //-----------------------------------------------------------

  private function createHTML() {

    $cHtml  = '<div id="' . $this->cId . '">' . PHP_EOL;
    $cHtml .= '<nav class="navbar navbar-expand-lg navbar-light" style="background-color:'.$this->cColor.';">' . PHP_EOL;

    $cHtml .= '<div class="navbar-brand">' . PHP_EOL;
    $cHtml .= ' <span class="navbar-title">' . PHP_EOL;
    $cHtml .= '   <p style="color:' . $this->aTitle1["color"] . ';font-size:' . $this->aTitle1["size"] . ';">'; 
    $cHtml .=      $this->aTitle1["text"]; 
    $cHtml .= '   </p>' . PHP_EOL;
    if ( $this->aTitle2 ) {
       $cHtml .= '<p style="color:' . $this->aTitle2["color"] . ';font-size:' . $this->aTitle2["size"] . ';">'; 
       $cHtml .=   $this->aTitle2["text"]; 
       $cHtml .= '</p>' . PHP_EOL;
    }
    $cHtml .= ' </span>' . PHP_EOL;
    $cHtml .= '</div>' . PHP_EOL;
    
    $cHtml .= '<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">' . PHP_EOL;
    $cHtml .= '<span class="navbar-toggler-icon"></span>' . PHP_EOL;
    $cHtml .= '</button>' . PHP_EOL;

    $cHtml .= $this->CreateItems();

    $cHtml .= '</nav>' . PHP_EOL;
    $cHtml .= '</div>' . PHP_EOL;

    return $cHtml;
    
  }
  
}

//------------------------------------------------------------------------------------------

class NavbarItem {
  public $dropDown  = false;
	public $cId     	= '';
	public $cText 		= '';
	public $cFunction = '';
	public $aItems    = [];

	//-----------------------------------------------------------

	public function __construct( $cId, $cText, $cFunction ) {
		$this->cId  			= $cId;
		$this->cText 			= $cText;
		$this->cFunction 	= $cFunction;
		
		if ( is_array($cFunction) ) {
			$this->dropDown = true;
			$this->aItems   = $cFunction;
		} else {
      $this->cFunction = str_replace( '"', "'", $cFunction);
    }
  }  

}

//------------------------------------------------------------------------------------------

class NavbarButton {
  public $cId     	= '';
	public $cText 		= '';
	public $cFunction = '';
	public $cIcon			= '';
  public $colorTXT  = 'black';
	
	//-----------------------------------------------------------

	public function __construct( $cId, $cText, $cFunction, $cIcon, $colorTXT = 'black' ) {
		$this->cId  			= $cId;
    $this->cText 			= $cText;
    $this->cFunction  = str_replace( '"', "'", $cFunction);
		$this->cIcon 			= $cIcon;
    $this->colorTXT   = $colorTXT;
  }  

}

?>


