<?php
/*
*  CLASS TModal : para crea dialogos modales en forma dinamica con Bootstrap
*  Autor : Albeiro Valencia 
*  Fecha : 11/09/2020 Derechos Reservados
*  https://github.com/shaack/bootstrap-show-modal
*  https://www.jqueryscript.net/demo/Dynamic-Bootstrap-4-Modals/
*  https://www.codehim.com/download/dynamic-bootstrap-modals.zip
*/

// Width modal
define("MODAL_MEDIUM", '');
define("MODAL_LARGE",  'modal-lg');
define("MODAL_SMALL",  'modal-sm');

// Type control
define('IS_INPUT'   , '_INPUT'); 
define('IS_ARRAY'   , '_ARRAY'); 
define('IS_MEMO'    , '_MEMO'); 
define('IS_SELECT'  , '_SELECT'); 
define('IS_RADIO'   , '_RADIO'); 
define('IS_CHECKBOX', '_CHECKBOX'); 
define('IS_LABEL'   , '_LABEL'); 
define('IS_SPINNER' , '_SPINNER');
define('IS_UPLOAD'  , '_UPLOAD');
define('IS_MAP'     , '_MAP');
define('IS_IMAGE'   , '_IMAGE');
define('IS_TABLE'   , "_TABLE");
define('IS_HTML'    , "_HTML");

// Type input
define('TYPE_NUMBER', 'number');
define('TYPE_TEXT', 'text');
define('TYPE_EMAIL', 'email');
define('TYPE_PASSWORD', 'password');
define('TYPE_DATE', 'date');

define('ES_LANG', 'es_lang');
define('EN_LANG', 'en_lang');

// Align label
define('LBL_LEFT', 'text-align: left');
define('LBL_RIGHT', 'text-align: right');
define('LBL_CENTER', 'text-align: center');

// type button
define('BTN_DEFAULT', 0);
define('BTN_CANCEL', 1);

// Column Align label
if ( ! defined('AL_LEFT') ) { define('AL_LEFT', 'left'); }
if ( ! defined('AL_RIGHT') ) { define('AL_RIGHT', 'right'); }
if ( ! defined('AL_CENTER') ) { define('AL_CENTER', 'center'); }

//----------------------------------------------

class TModal {   

  private $width      = MODAL_MEDIUM;
  private $titleHtml  = '';
  private $bodyHtml   = '';
  private $footerHtml = '';
  
  private $cId          = '';
  private $cTitleText   = '';
  private $cTitleClr    = '';
  private $cTitleBg     = '';
  private $aButtons     = []; 
  private $aControls    = [];
  private $isTable      = false;
  private $dataTable    = [];
  private $isUpload     = false;
  private $idUpload     = 'null';
  
  //----------------------------------------------
  
  function __construct( $cId = '', $cTitle = '', $cClrTxt = '', $cClrBg = '', $width = MODAL_LARGE ) {

    $this->cId        = TDefault( $cId, 'modal_' . time() );
    $this->cTitleText = TDefault( $cTitle, 'Test Modal' );
    $this->cTitleClr  = TDefault( $cClrTxt, 'black' );
    $this->cTitleBg   = TDefault( $cClrBg, 'aliceblue' );
    
    $this->checkWidth( $width );
    
  }  

  //----------------------------------------------

  private function checkWidth( $width ) {

    $aType = [ MODAL_LARGE, MODAL_MEDIUM, MODAL_SMALL ];
    if ( in_array( $this->width, $aType) ) {
      $this->width = $width;
    } else {
      $this->width = MODAL_LARGE;
    }

  }

  //----------------------------------------------

  public function AddInput( $cLabel, $cId, $cType = TYPE_TEXT, $cHolder = '', $cIcon = null, $cAction = '' ) 
  {
    $this->aControls[] = [ IS_INPUT, 
                           new TModalInput( $cLabel, $cId, $cType, LBL_LEFT, $cHolder, $cIcon, $cAction ) ];
  }

  //----------------------------------------------

  public function AddTextArea( $cLabel, $cId ) {
    $this->aControls[] = [ IS_MEMO, 
                           new TModalMemo( $cLabel, $cId, LBL_LEFT ) ];
  }

  //----------------------------------------------

  public function AddSelect( $cLabel, $cId, $aItems = [], $cIcon = null, $cAction = '' ) {
    $this->aControls[] = [ IS_SELECT, 
                          new TModalSelect( $cLabel, $cId, LBL_LEFT, $aItems, $cIcon, $cAction ) ];     
  }

  //----------------------------------------------

  public function AddRadio( $cLabel = '', $cId = 'radio', $aItems = [] ) {
    $this->aControls[] = [ IS_RADIO, 
                          new TModalRadio( $cLabel, $cId, $aItems, LBL_LEFT ) ];     
  }

  //----------------------------------------------

  public function AddCheckBox( $cLabel = '', $cId = 'chkbox' ) {
    $this->aControls[] = [ IS_CHECKBOX, 
                          new TModalCheckBox( $cLabel, $cId, LBL_LEFT ) ];     
  }

  //----------------------------------------------

  public function AddLabel( $cLabel = '', $cId = '', $size = 16 ) {
    $this->aControls[] = [ IS_LABEL, 
                           new TModalLabel( $cLabel, $cId, $size ) ];     
  }

  //----------------------------------------------

  public function AddHtml( $cHtml = '' ) {
    if ( $cHtml ) {
      $this->aControls[] = [ IS_HTML, $cHtml ];     
    }
  }

  // https://embed.plnkr.co/plunk/ZDkUYz
  // https://stackoverflow.com/questions/54657785/how-to-display-a-google-map-inside-modal-bootstrap-4/54664163

  //----------------------------------------------
 
  public function AddButton( $cId = '', $cLabel = '', $cAction = '', 
                             $cClrTxt = '', $cClrBg = '', $cType = BTN_DEFAULT ) 
  {

    if ( empty($cAction) ) {
      $cAction = ( $cType <> BTN_CANCEL ) ? "oModal.Self()" : "";
    }

    $this->aButtons[] = [ "id"     => ( empty($cId) ) ? 'btn_' . time() : $cId, 
                          "texto"  => ( empty($cLabel) ) ? 'Button' : $cLabel, 
                          "action" => $cAction, 
                          "color"  => $cClrTxt,
                          "fondo"  => $cClrBg ];
  }

  //----------------------------------------------

  public function AddTable( $aData = [] ) {

    $oTable = new TModalTable( $aData );
    
    $this->isTable      = true;
    $this->dataTable    = $aData;
    $this->aControls[]  = [ IS_TABLE, $oTable ];

    return $oTable;     

  }

  //----------------------------------------------

  public function AddSpinner( $cId = 'idSpinner', $label = 'CANTIDAD :' ) {
    $this->aControls[] = [ IS_SPINNER, [ "id" => $cId, "label" => $label ] ];     
  }

  //----------------------------------------------

  public function AddUploadBase64( $cId, $cLang = ES_LANG ) {
    // https://github.com/kartik-v/bootstrap-fileinput
    if ( !$this->isUpload ) {
      $this->isUpload = true;
      $this->idUpload = $cId;
      $this->aControls[] = [ IS_UPLOAD, [ "id" => $cId, "lang" => $cLang ] ];
    }
  }

  //----------------------------------------------

  public function AddMapWeb() {
    $this->aControls[] = [ IS_MAP ];     
  }

  //----------------------------------------------

  public function AddImage( $cId = 'idImage', $image = '', $cAlign = AL_CENTER, $width = '200px' ) {
    $this->aControls[] = [ IS_IMAGE, [ "id" => $cId, "image" => $image, "align" => $cAlign, "width" => $width ] ];     
  }

  //----------------------------------------------

  public function InLineControls() 
  {
    $oInline = new TModalInline();
    $this->aControls[] = [ IS_ARRAY, $oInline ];
    return $oInline;
  }

  //----------------------------------------------
  
  private function CreateControlsBody() {

    $cHtml  = "'";
    $cHtml .= '<form>';

    $nCount = count( $this->aControls );
		for ( $i = 0; $i < $nCount; $i++ ) { 
      $oCtrl = $this->aControls[$i];

      switch ( $oCtrl[0] ) {
        case IS_INPUT:
          $cHtml .= '<div class="item-modal">';
          $cHtml .= $this->CreateInput( $oCtrl[1] );
          $cHtml .= '</div>';
          break;

        case IS_MEMO:
          $cHtml .= $this->createTextArea( $oCtrl[1] );
          break;  

        case IS_SELECT:
          $cHtml .= '<div class="item-modal">';
          $cHtml .= $this->CreateSelect( $oCtrl[1] );
          $cHtml .= '</div>';
          break;  

        case IS_LABEL:
          $cHtml .= '<div class="item-modal">';
          $cHtml .= $this->CreateLabel( $oCtrl[1] );
          $cHtml .= '</div>';
          break;    
        
        case IS_RADIO:
          $cHtml .= '<div class="item-modal">';
          $cHtml .= $this->CreateRadio( $oCtrl[1] );
          $cHtml .= '</div>';
          break;  

        case IS_CHECKBOX:
          $cHtml .= '<div class="item-modal">';
          $cHtml .= $this->CreateCheckBox( $oCtrl[1] );
          $cHtml .= '</div>';
          break;  

        case IS_SPINNER:
          $cHtml .= '<div class="item-modal">';
          $cHtml .= $this->CreateSpinner( $oCtrl[1]["id"], $oCtrl[1]["label"] );  
          $cHtml .= '</div>';
          break;  
        
        case IS_ARRAY:  
          $cHtml .= $this->CreateControlsInline( $oCtrl[1] );
          break;

        case IS_UPLOAD:
          $cHtml .= $this->CreateBtnUpload( $oCtrl[1]["id"], $oCtrl[1]["lang"] );
          break;      

        case IS_MAP:
          $cHtml .= $this->CreateMap();    
          break;

        case IS_IMAGE:
          $cHtml .= $this->CreateImage( $oCtrl[1]["id"], $oCtrl[1]["image"], $oCtrl[1]["align"], $oCtrl[1]["width"] );
          break;

        case IS_TABLE:
          $cHtml .= $this->CreateTable( $oCtrl[1] );
          break;  

        case IS_HTML:
          $cHtml .= $oCtrl[1];
          break;    

        default:
          # code...
          break;
      }

    }  

    $cHtml .= '</form>';
    $cHtml .= "'";

    return $cHtml;
  }

  //----------------------------------------------

  private function CreateInput( $input ) {

    $cHtml  = '<div class="left">';
    $cHtml .= ' <label style="padding-right: 10px;'.$input->cAlign.';">'; 
    $cHtml .= $input->cLabel.'</label>';
    $cHtml .= '</div>';
    $cHtml .= '<div class="rigth">';
    $cHtml .= ' <input type="'.$input->cType.'" id="'.$input->cId .'"';
    if ( $input->cPlaceholder ) {
       $cHtml .= ' placeholder="'.$input->cPlaceholder.'" ';
    }
    if ( $input->cIcon ) {
      $cHtml .= ' class="mobil-input"';
      if ( $input->required ) {
        $cHtml .= ' required minlength="2"';
      } 
      $cHtml .= '/>';
      $cHtml .= '<span class="btn-input" onclick="'.$input->cAction.'">'; 
      $cHtml .= '  <i class="'.$input->cIcon .'" style="color:white"></i>'; 
      $cHtml .= '</span>'; 
    } else {
      $cHtml .= ' style="width: 100%"';
      if ( $input->required ) {
        $cHtml .= ' required minlength="2"';
      }   
      $cHtml .= '/>';
    }  
    $cHtml .= '<div class="invalid-feedback">';
    $cHtml .= 'Please provide a valid city.';
    $cHtml .= '</div>';
    $cHtml .= '</div>';
    
    return $cHtml;
  
  }

  //----------------------------------------------

  public function createTextArea( $input ) {
    
    $cHtml  = '<div class="item-modal" style="margin-bottom:5px;">';
    $cHtml .= ' <div class="left">';
    $cHtml .= '   <label style="width: 120px;padding-right: 10px;'.$input->cAlign.';">'; 
    $cHtml .= $input->cLabel.'</label>';
    $cHtml .= ' </div>';
    $cHtml .= ' <div class="rigth">';
    $cHtml .= '   <textarea id="'.$input->cId.'" type="text" style="width: 100%;padding: 5px;" rows="3"></textarea>';
    $cHtml .= ' </div>';
    $cHtml .= '</div>';
    
    return $cHtml;

  }

  //----------------------------------------------

  private function CreateRadio( $radio ) {

    // https://stackoverflow.com/questions/9618504/how-to-get-the-selected-radio-button-s-value

    $cHtml  = '<div class="left">';
    $cHtml .= '   <label style="width:111px;padding-right:10px;'.$radio->cAlign .';">'.$radio->cLabel.'</label>';
    $cHtml .= ' </div>';
    $cHtml .= ' <div class="rigth">';

    $count = count($radio->aItems);
    for ($i=0; $i < $count; $i++) { 
      $value  = $radio->aItems[$i];
      $cId    = "radio".$i;
      $cHtml .= '<div class="form-check form-check-inline">';
      if ( $i == 0 ) {
        $cHtml .= '<input class="form-check-input" type="radio" id="'.$cId.'" name="radio-'.$radio->cId.'" value="'.$value.'" checked>';
      } else {
        $cHtml .= '<input class="form-check-input" type="radio" id="'.$cId.'" name="radio-'.$radio->cId.'" value="'.$value.'">';
      }
      
      $cHtml .= ' <label class="form-check-label" for="'.$cId.'">'.$value.'</label>';
      $cHtml .= '</div>';
    }

    $cHtml .= "</div>";
    
    return $cHtml;
  }

  //----------------------------------------------

  private function CreateCheckBox( $checkBox ) {

    $cHtml  = '<div class="left">';
    $cHtml .= '  <label style="width: 120px;padding-right: 10px;'.$checkBox->cAlign.';white-space: nowrap;">'; 
    $cHtml .= $checkBox->cLabel.'</label>';
    $cHtml .= '</div>';
    $cHtml .= '<div class="rigth">';
    $cHtml .= '  <input type="checkbox" class="form-check-input modal-checkbox" id="'.$checkBox->cId.'">';
    $cHtml .= '</div>';

    return $cHtml;

  }

  //----------------------------------------------
  
  private function CreateLabel( $label ) {

    $cHtml  = '<label id="'.$label->cId.'" style="font-size: '.$label->size.' px;">'.$label->cLabel;
    $cHtml .= "</label>";
    
    return $cHtml;
  }

  //----------------------------------------------
  // http://paulrose.com/bootstrap-select-sass/
   
  private function CreateSelect( $select ) {

    if ( $select->cIcon ) {
      $cStyle = 'style="width: 85%;display: inline-flex;"';
    } else {
      $cStyle = "";
    }  

    $cHtml  = '<div class="left">';
    $cHtml .= '  <label style="width: 150px;padding-right: 10px;'.$select->cAlign.';">'.$select->cLabel.'</label>';
    $cHtml .= '</div>';
    $cHtml .= '<div class="rigth" '.$cStyle.'>';
    $cHtml .= ' <select id="'.$select->cId.'" class="form-control selectpicker select-modal"';
    $cHtml .= '  data-live-search="true" data-name="select">';
    
    $nItems = (is_array($select->aItems)) ? count( $select->aItems ) : 0;
    if ($nItems > 0) {
      if (is_array($select->aItems[0])) {
        for ($item = 0; $item < $nItems; $item++) {
          $cHtml .= '<option data-tokens="' .$select->aItems[$item][0]. '"'; 
          $cHtml .= ' value="'.$select->aItems[$item][0].'">'.$select->aItems[$item][1];
          $cHtml .= '</option>';
        }
      } else {
        for ($item = 0; $item < $nItems; $item++) {
          $cHtml .= '<option data-tokens="' .$select->aItems[$item]. '"'; 
          $cHtml .= ' value="'.$select->aItems[$item].'">'.$select->aItems[$item];
          $cHtml .= '</option>';
        }
      }
    } else {
      $cHtml .= '<option>No hay items</option>';
    }   
    $cHtml .= '  </select>';
    if ( $select->cIcon ) {
      $cHtml .= '<span class="btn-select" onclick="'.$select->cAction.'">'; 
      $cHtml .= '  <i class="'.$select->cIcon .'" style="color:white"></i>'; 
      $cHtml .= '</span>'; 
    }
    $cHtml .= '</div>'; 

    // console_php($cHtml);
    
    return $cHtml;

  }

  //----------------------------------------------
  // https://stackoverflow.com/questions/9643311/pass-a-string-parameter-in-an-onclick-function

  private function CreateSpinner( $cId, $label ) {

    $cHtml  = '<div class="left">';
    $cHtml .= ' <label style="width: 120px;padding-right: 10px;text-align: left;">'.$label .'</label>';
    $cHtml .= '</div>';
    $cHtml .= '<div class="right" style="width:100%;">';
    $cHtml .= ' <div class="minus-spinner">';
    $cHtml .= '   <button type="button" class="btn btn-primary btn-spinner" onclick="clickMinus();">';
    $cHtml .= '     <span class="fa fa-minus"></span>';
    $cHtml .= '   </button>';
    $cHtml .= ' </div>';
    $cHtml .= ' <div class="div-input-spinner">';
    $cHtml .= '   <input type="text" id="'. $cId .'" class="input-spinner" value="1" ';
    $cHtml .= '    onclick="SetSpinner(this);" onkeypress="SetSpinner(this);">';
    $cHtml .= ' </div>';
    $cHtml .= ' <div class="plus-spinner">';
    $cHtml .= '   <button type="button" class="btn btn-primary btn-spinner" onclick="clickPlus();">';
    $cHtml .= '     <span class="fa fa-plus"></span>';
    $cHtml .= '   </button>';
    $cHtml .= ' </div>';
    $cHtml .= '</div>';
    
    return $cHtml;

  }
  
  //----------------------------------------------

  private function CreateBtnUpload( $cId, $cLang ) {

    $cUpload = ($cLang == ES_LANG) ? "Subir" : "Upload";
    $cChoose = ($cLang == ES_LANG) ? "Seleccione Archivo" : "Choose file";
    
    $cHtml  = '<div class="input-group">';
    $cHtml .= '  <div class="input-group-prepend">';
    $cHtml .= '    <span class="input-group-text" id="inputGroupFileAddon01">'.$cUpload .'</span>';
    $cHtml .= '  </div>';
    $cHtml .= '  <div class="custom-file">';
    $cHtml .= '    <input type="file" class="custom-file-input" id="'.$cId.'"'; 
    $cHtml .= '           onchange="oModal.uploadFile()"';
    $cHtml .= '           aria-describedby="inputGroupFileAddon01">';
    $cHtml .= '    <label class="custom-file-label" for="'.$cId.'">'.$cChoose.'</label>';
    $cHtml .= '  </div>';
    $cHtml .= '</div>';
    
    return $cHtml;

  }

  //----------------------------------------------

  private function CreateImage( $cId, $image, $cAlign, $width ) {

    $image = ($image) ? $image : TWEB_PATH."/images/no-image.png";

    $size  = "width :".$width .";height: ".$width.";";
    
    $cHtml  = '<div class="item-modal" style="margin-bottom:10px;">';
    $cHtml .= '<img class="img-responsive" id="'.$cId.'" src="'.$image .'" ';
    
    switch ( $cAlign ) {
      case AL_CENTER:
        $cHtml .= ' style="margin-left: auto;margin-right: auto;'.$size .'">';
        break;

      case AL_LEFT:
        $cHtml .= ' style="margin-left: 0;margin-right: auto;'.$size .'">';
        break;
          
      case AL_RIGHT:
        $cHtml .= ' style="margin-left: auto;margin-right: 0;'.$size .'">';
        break;    
    }    

    $cHtml .= '</div>';
    
    return $cHtml;

  }

  //----------------------------------------------

  // https://mappinggis.com/2013/06/como-crear-un-mapa-con-leaflet/
  private function CreateMap() {
    
    $cHtml  = '<div class="item-modal">';
    $cHtml .= '<div id="map_canvas" style="width: 100%;height: 480px;box-shadow: 5px 5px 5px #888;"></div>';
    $cHtml .= '</div>';
    
    return $cHtml;
  }
  
  //----------------------------------------------

  private function CreateButtons() {

    $cHtml  = "'";

    $nCount = count( $this->aButtons );

    if ( $nCount > 0 ) {
      foreach($this->aButtons as $index => $value) {
        $cHtml .= '<button type="submit" class="btn btn-default" id="'.$value["id"].'" ';
        if ( $value["action"] ) {
          $cHtml .= 'onclick="'.$value["action"].'" ';  
        } else {
          $cHtml .= 'onclick="oModal.clearUpload()" ';
          $cHtml .= 'data-dismiss="modal" ';
        }
        if ( !empty($value["color"]) && !empty($value["fondo"]) ) {
          $cHtml .= 'style="color:'.$value["color"].';';
          $cHtml .= 'background-color:'.$value["fondo"].';'; 
          $cHtml .= 'border-color:'.$value["fondo"].'"';
        }  
        $cHtml .= '>';
        $cHtml .= $value["texto"].'</button>' ;
      }
    } else {
      $cHtml .= '<button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>' ;
    }

    $cHtml .= "'";

    return $cHtml;
    
  }

  //----------------------------------------------

  private function CreateTable( $table ) {

    $cHtml  = $table->CreateButtons();
    $cHtml .= $table->CreateBSTable();
    
    return $cHtml;

  }

  //----------------------------------------------
  
  private function CreateControlsInline( $oItem ) {

    $cHtml = '<div class="item-modal">';

    $nCount = count( $oItem->aObjects );
    for ( $i = 0; $i < $nCount; $i++ ) { 
      $oCtrl = $oItem->aObjects[$i];

      switch ( $oCtrl[0] ) {
        case IS_INPUT:
          $cHtml .= $this->CreateInput( $oCtrl[1] );
          break;

        case IS_MEMO:
          $cHtml .= $this->createTextArea( $oCtrl[1] );
          break;  

        case IS_SELECT:
          $cHtml .= $this->CreateSelect( $oCtrl[1] );
          
          break;  

        case IS_LABEL:
          $cHtml .= $this->CreateLabel( $oCtrl[1] );
          break;    
        
        case IS_RADIO:
          $cHtml .= $this->CreateRadio( $oCtrl[1] );
          break;  

        case IS_CHECKBOX:
          $cHtml .= $this->CreateCheckBox( $oCtrl[1] );
          break;  

        case IS_HTML:
          $cHtml .= $oCtrl[1];
          break;    

        default:
          $cHtml .= "";
          break;
      }

    }  

    $cHtml .= '</div>';

    return $cHtml;
  }
  
  //----------------------------------------------

  private function CreateCodeJs() {

    $cId      = '"' . $this->cId . '"';
    // $width    = $this->width;
    $titleclr = '{ clrtext : "' . $this->cTitleClr . '", bgtext : "' . $this->cTitleBg .'"}';

    $cJS  = '<script>';
    $cJS .= PHP_EOL; 
    $cJS .= 'var oModal;' . PHP_EOL;  
    $cJS .= PHP_EOL; 
    $cJS .= '$(function() {'                                . PHP_EOL;
    $cJS .= '  oModal = new TModal();'                      . PHP_EOL;  
    $cJS .= '  var props = {};'                             . PHP_EOL;
    $cJS .= '  props.id       = ' . $cId                    . PHP_EOL; 
    $cJS .= '  props.title    = ' . $this->titleHtml  . ';' . PHP_EOL; 
    $cJS .= '  props.titleclr = ' . $titleclr  . ';'        . PHP_EOL; 
    $cJS .= '  props.body     = ' . $this->bodyHtml   . ';' . PHP_EOL; 
    $cJS .= '  props.footer   = ' . $this->footerHtml . ';' . PHP_EOL;
    $cJS .= '  props.width    = "' . $this->width . '";'    . PHP_EOL;
    $cJS .= '  props.options  = {backdrop : false, keyboard : false};'  . PHP_EOL;
    $cJS .= '  props.data     = ' . json_encode($this->dataTable) . ';' . PHP_EOL;
    $cJS .= '  props.istable  = ' . (($this->isTable) ? 'true' : 'false') . ';' . PHP_EOL;
    $cJS .= '  props.idupload = "' . $this->idUpload . '";' . PHP_EOL;
    $cJS .= '  props.modal    = oModal;'     . PHP_EOL;
    $cJS .= '  oModal.setmodal( props );'    . PHP_EOL;
    $cJS .= '});'                            . PHP_EOL; 
    $cJS .= PHP_EOL;
    $cJS .= '</script>';

    echo $cJS;
    
	}

  //----------------------------------------------

  private function LibsBootstrapTable() {

    static $nLoads = 0;
    $nLoads++;

    $cHtml = '';

    if ( $nLoads == 1) {

      $cHtml .= '<!-- PLUGIN CORE.MODAL.JS -->';  // Load Css y JavaScript para TModal
      $cHtml .= '<link type="text/css" rel="stylesheet" href="' . TWEB_PATH . '/core.modal.css" media="screen">';					
      $cHtml .= '<script src="' . TWEB_PATH . '/core.modal.js"></script>';
      $cHtml .= '<link type="text/css" rel="stylesheet" href="' . TWEB_PATH . '/modal.spinner.css">';
      $cHtml .= '<script src="' . TWEB_PATH . '/modal.spinner.js"></script>';
    }

    echo $cHtml ;
          
  }
  
  //----------------------------------------------

  public function Activate() {

    // Cargamos Las librerias de tables bootstrap
    $this->LibsBootstrapTable();
    
    // Agregamos el codigo a la clase
    $this->titleHtml   = "'" . '<h5 class="modal-title">' .$this->cTitleText . '</h5>' . "'";
    $this->bodyHtml    = $this->CreateControlsBody();
    $this->footerHtml  = $this->CreateButtons();
    
    // codigo javascript
    $this->CreateCodeJs();
    
  }

} 

//----------------------------------------------
/*
*  CLASS INPUT   
*/

class TModalInput {
  public $cLabel        = '';
  public $cId           = '';
  public $cType         = TYPE_TEXT;
  public $cAlign        = LBL_LEFT;
  public $cPlaceholder  = '';
  public $cIcon         = null;
  public $cAction       = '';
  public $required      = false;
  
  function __construct( $cLabel = '', $cId = '', $cType = TYPE_TEXT, $cAlign = LBL_LEFT, 
                        $cHolder = '', $cIcon = null, $cAction = '', $required = false ) 
  {
    $this->cLabel       = $cLabel;
    $this->cId          = $cId;
    $this->cType        = $cType;
    $this->cAlign       = $cAlign;
    $this->cPlaceholder = $cHolder;
    $this->cIcon        = $cIcon;
    $this->cAction      = $cAction;
    $this->required     = $required;
  }
  
}

//----------------------------------------------
/*
*  CLASS SELECT   
*/

class TModalSelect {
  public $cLabel  = '';
  public $cId     = '';
  public $cAlign  = LBL_LEFT;
  public $aItems  = [];
  public $cIcon   = null;
  public $cAction = '';
  
  function __construct( $cLabel = '', $cId = '', $cAlign = LBL_LEFT, $aItems = [], $cIcon = null, $cAction = '' ) {
    $this->cLabel   = $cLabel;
    $this->cId      = $cId;
    $this->cAlign   = $cAlign;
    $this->aItems   = $aItems;
    $this->cIcon    = $cIcon;
    $this->cAction  = $cAction;
  }
  
}

//----------------------------------------------
/*
*  CLASS RADIO   
*/

class TModalRadio {
  public $cId     = '';
  public $cLabel  = '';
  public $aItems  = [];
  public $cAlign  = '';
  
  function __construct( $cLabel = '', $cId = '', $aItems = [], $cAlign = LBL_LEFT) {
    $this->cId      = $cId;    
    $this->cLabel   = $cLabel;
    $this->aItems   = $aItems;
    $this->cAlign   = $cAlign;
  }
  
}

//----------------------------------------------
/*
*  CLASS CHECKBOX   
*/

class TModalCheckBox {
  public $cId     = '';
  public $cLabel  = '';
  public $cAlign  = '';
  
  function __construct( $cLabel = '', $cId = '', $cAlign = LBL_LEFT) {
    $this->cId      = $cId;    
    $this->cLabel   = $cLabel;
    $this->cAlign   = $cAlign;
  }
  
}

//----------------------------------------------
/*
*  CLASS LABEL   
*/

class TModalLabel {
  public $cLabel   = '';
  public $cId      = '';
  public $size     = 18;
  
  function __construct( $cLabel = '', $cId = '', $size = 18 ) {
    $this->cLabel = $cLabel;
    $this->cId    = $cId;
    $this->size   = $size;
  }
  
}

//----------------------------------------------
/*
*  CLASS MEMO   
*/

class TModalMemo {
  public $cLabel   = '';
  public $cId      = '';
  public $cAlign   = AL_LEFT;
  
  function __construct( $cLabel = '', $cId = '', $cAlign = AL_LEFT  ) {
    $this->cLabel = $cLabel;
    $this->cId    = $cId;
    $this->cAlign = $cAlign;
  }
  
}

//----------------------------------------------
/*
*  CLASS INLINE CONTROLS   
*/

class TModalInline extends TModal {

  public $aObjects = [];

  function __construct() {
    
  }

  //----------------------------------------------

  public function Input(  $cLabel, $cId, $cType = TYPE_TEXT, $cHolder = '', $cIcon = null, $cAction = '', $required = false ) 
  {
    $this->aObjects[] = [ IS_INPUT, 
                          new TModalInput( $cLabel, $cId, $cType, LBL_LEFT, $cHolder, $cIcon, $cAction, $required ) ];
  }

  //----------------------------------------------

  public function Select( $cLabel, $cId, $aItems = [] ) {
    $this->aObjects[] = [ IS_SELECT, 
                          new TModalSelect( $cLabel, $cId, LBL_LEFT, $aItems ) ];     
  }

  //----------------------------------------------

  public function Radio( $cLabel = '', $cId = 'radio', $aItems = [],  ) {
    $this->aObjects[] = [ IS_RADIO, 
                          new TModalRadio( $cLabel, $cId, $aItems, LBL_LEFT ) ];     
  }

  //----------------------------------------------

  public function CheckBox( $cLabel = '', $cId = 'chkbox' ) {
    $this->aObjects[] = [ IS_CHECKBOX, 
                          new TModalCheckBox( $cLabel, $cId, LBL_LEFT ) ];     
  }

  //----------------------------------------------

  public function Label( $cLabel = '', $cId = '', $size = 16 ) {
    $this->aObjects[] = [ IS_LABEL, 
                          new TModalLabel( $cLabel, $cId, $size ) ];     
  }

  //----------------------------------------------

  public function Html( $cHtml = '' ) {
    $this->aObjects[] = [ IS_HTML, $cHtml ];     
  }

}

//----------------------------------------------
/*
*  CLASS TABLE   
*/

class TModalTable {

  private $heightTable   = 300;
  private $aColumns      = [];
  private $dataTable     = [];
  private $aOptionsTable = [];
  private $aButtons      = []; 
  private $buttonsCss    = "btn-primary"; 

  function __construct( $aData = [] ) {

    $this->dataTable = $aData;

    $this->aOptionsTable = [ "filter"   => true,
                             "pagin"    => true,
                             "cardview" => true,
                             "footer"   => false,
                             "check"    => true ];

  }

  //----------------------------------------------

  public function AddColumn( $cId, $cTitle, $cPicture = '', $cAlign = AL_LEFT ) {
    
    $this->aColumns[] = [ "id"      => $cId, 
                          "title"   => $cTitle,
                          "format"  => $cPicture,
                          "align"   => $cAlign ];
  }
  
  //----------------------------------------------

  public function ColorHeaderTable( $clrBg = 'yellow', $clrTexto = 'black' ) {
    $this->clrHeaderBg  = $clrBg;
    $this->clrHeaderTxt = $clrTexto;
  }

  //----------------------------------------------

  public function AddButton( $cId = '', $cText = '', $cAction = '', $cIcon = '' ) {
    $this->aButtons[] = [ "id" => $cId, "label" => $cText, "action" => $cAction, "icon" => $cIcon ];
  }

  //----------------------------------------------

  public function ButtonsCSS( $cClass = "btn-primary" ) {
    $this->buttonsCss = $cClass;
  } 

  //----------------------------------------------

  public function Paging( $option = true) {
    $this->aOptionsTable["pagin"] = (gettype($option) == "boolean") ? $option : true;
  }

  //----------------------------------------------

  public function Cardview( $option = true) {
    $this->aOptionsTable["cardview"] = (gettype($option) == "boolean") ? $option : true;
  }

  //----------------------------------------------

  public function Search( $option = true ) {
    $this->aOptionsTable["filter"] = (gettype($option) == "boolean") ? $option : true;
  }

  //----------------------------------------------

  public function Footer( $option = true) {
    $this->aOptionsTable["footer"] = (gettype($option) == "boolean") ? $option : true;
  }

  //----------------------------------------------

  public function CheckHeader( $option = true ) {
    $this->aOptionsTable["check"] = (gettype($option) == "boolean") ? $option : true;
  }  
  
  //----------------------------------------------

  public function Height( $height ) {
    $this->heightTable = (integer) $height;
  }

  //----------------------------------------------

  public function CreateButtons() {

    $buttons = count($this->aButtons);
    if ( $buttons > 0 ) {
      $cHtml  = '<div class="item-modal">';
      $cHtml .= '<div class="btn-group" role="group">';
      for ($i=0; $i < $buttons; $i++) { 
        $item = $this->aButtons[$i];
        $cHtml .= '<button type="button" class="btn '.$this->buttonsCss.'"';
        $cHtml .= ' id="'.$item["id"].'" onclick="'.$item["action"].'">';
        if ( !empty($item["icon"]) ) {
          $cHtml .= ' <i class="fa '.$item["icon"].'"></i> ';
        }
        $cHtml .= $item["label"];
        $cHtml .= '</button>';  
      }
      $cHtml .= '</div>';
      $cHtml .= '</div>';
    } else {
      $cHtml = "";
    }

    return $cHtml;

  }

  //----------------------------------------------

  // https://examples.bootstrap-table.com/#options/trim-on-search.html#view-source
  // https://examples.bootstrap-table.com/#column-options/detail-formatter.html#view-source
  // https://bootstrap-table.com/docs/api/table-options/
      
  public function CreateBSTable() {

    $cHtml  = '<div class="item-modal">';

    $columns = count($this->aColumns);
    if ( $columns > 0 ) {
      
      if ( count($this->dataTable) == 0 ) {
        $aRow = "[{";
        for ($i=0; $i < $columns; $i++) { 
          $row = $this->aColumns[ $i ];  
          $aRow .= '"' . $row["id"] . '" : " ",';
        }
        $aRow = substr($aRow, 0, -1) . "}]";
        $this->dataTable = json_decode($aRow);
      }  

      $cHtml .= '<table id="table"';

      if ( $this->aOptionsTable["filter"] ) {
        $cHtml .= ' data-search="true"';
        $cHtml .= ' data-trim-on-search="false"';   // Evita eliminar el key space
        $cHtml .= ' data-search-highlight="true"';
      }

      if ( $this->aOptionsTable["check"] ) {
        $cHtml .= 'data-checkbox-header="true"';
      } else {  
        $cHtml .= 'data-checkbox-header="false"';
      }  

      $cHtml .= ' data-click-to-select="true"';
      $cHtml .= ' data-id-field="id"';
      $cHtml .= ' data-toggle="table"';
      $cHtml .= ' data-height="'.$this->heightTable .'"';
      $cHtml .= ' data-header-style="headerStyle"';

      if ( $this->aOptionsTable["pagin"] ) {
        $cHtml .= ' data-pagination="true"';
      }  

      if ( $this->aOptionsTable["cardview"] == true ) {
        $cHtml .= ' data-mobile-responsive="true"';
        $cHtml .= ' data-card-view="true"';
      } else {
        $cHtml .= ' data-card-view="false"';
      }

      if ( $this->aOptionsTable["footer"] ) {
        $cHtml .= ' data-show-footer="true"';
      }
      
      $cHtml .= '>';

      $cHtml .= ' <thead>';
      $cHtml .= '   <tr>';
      $cHtml .= '     <th data-checkbox="true"></th>';
      
      for ($i=0; $i < $columns; $i++) { 
        $row = $this->aColumns[ $i ];  
        $cHtml .= '<th data-field="' . $row["id"]. '" ';
        if ( !empty($row["format"]) ) {
          $cHtml .= 'data-formatter="' . $row["format"]  .'"';
        }
        if ( !empty($row["align"]) ) {
          $cHtml .= 'data-align="' . $row["align"] . '"';
        }
        $cHtml .= '>' . $row["title"] . '</th>';
      }

      $cHtml .= '  </tr>';  
      $cHtml .= ' </thead>';
      $cHtml .= '</table>';

    }  

    $cHtml .= '</div>';

    return $cHtml;

  }

}

?>
 
