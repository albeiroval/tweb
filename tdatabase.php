<?php
include_once( 'core.tools.php' );

class TDatabase {
 
  private $host;
  private $user;
  private $pass;
  private $name;
  private $port;
  private $link;
  private $error 		= '';
  private $errno 		= 0;
	private $reg;
	private $cSql			= '';
	private $fields;
	public  $query;
	private $excepcio;
	public  $affected_rows 	= 0; 
	private $aReg			= array();
	private $lEof   	= true;
	public  $lLog			= false;
	public  $lUtf8		= true;

  function __construct( $host, $user, $pass, $name = "", $nPort = null, $lConnect = 1) {
      $this -> host = $host;
      $this -> user = $user;
      $this -> pass = $pass;
      $this -> port = $nPort;
      if (!empty($name)) $this -> name = $name; 
      if ($lConnect == 1) $this -> connect();
	}
	
	public function connect() {
		$this->link = mysqli_connect( $this->host, $this->user, $this->pass, $this->name, $this->port );
		if ( !$this->link ) {
			if ( mysqli_connect_errno() ) {
			  $cError = '<b>TDatabase Error: </b>' . mysqli_connect_error();
			  $this->ShowError( $cError );
			  die();				
			}
		}
  }
	
	public function End() {
    if ($this->link) $this->Close();
  }	
	
	public function Close() {
    @mysqli_close( $this->link );
  }
	
	public function Utf8() {	
		mysqli_set_charset( $this->link, 'utf8' );
	}
	
	public function IsError() {
		if ( !empty($this->error))
			return true;
		else
		  return false;			
	}
	
	public function Error() {
		return  $this->excepcio . ' -> ' . $this->errno . ' ' . $this->error;	
	}			
	
  private function exception($message) {
	  $this->excepcio = $message; 
	  if ($this->link) {
    		$this->error = mysqli_error($this->link);
        $this->errno = mysqli_errno($this->link);
    } else {
    		$this->error = mysqli_error($this->link);
        $this->errno = mysqli_errno($this->link);
    }
	
		$cError = '<b>TDatabase Error: </b>' . $this->errno . ' - ' . $this->error;
	
		if ( !empty( $this->cSql ) ){
			$cError .= '<br>Sql: ' . $this->cSql;
		}
	
		$this->ShowError( $cError );

		die();
	}
	
	public function lastInsertId(){
		return mysqli_insert_id( $this->link );	
	}
	
  public function Open( $cSql) {	
		if ( $this->Query( $cSql ) ) {
			$this->Skip();
			return true;
		} else {
			return false;				
		}
	}
	
  public function Query($cSql) {
		if ( $this->lLog ) 
			$this->Log( 'Query: ' . $cSql );		

		if ( $this->lUtf8 )
			$cSql = utf8_decode( $cSql );
			
		$this->cSql = $cSql;

    if ($this->query = @mysqli_query( $this->link, $cSql )) {
			$this->cSql = '';
			return $this->query;
    } else {
    	$this->exception("Could not query database!");
      return false;
		}
				
	}
	
	public function Execute($cSql) {
		if ( $this->lLog ) 
			$this->Log( 'Execute: ' . $cSql  );	

		if ( $this->lUtf8 )
			$cSql = utf8_decode( $cSql );			
	
		$this->affected_rows = 0; 		

		if ( !mysqli_begin_transaction($this->link, MYSQLI_TRANS_START_READ_WRITE) ) {
			$this->exception("begin_transaction error!");
      return false;
		}

    if ($this->query = @mysqli_query( $this->link, $cSql )) {
			mysqli_commit($this->link);
			$this->affected_rows = mysqli_affected_rows( $this->link );
			return true;
		} else {	
			$this->exception( 'Error SQL' );
			return false;
		}
	}
	
	public function Eof() 			{ return $this->lEof ; }
	public function RecCount() 	{ return mysqli_num_rows($this->query);  }	
	public function GetRecord() { return $this->aReg ; }	
	public function SetRecord( $aReg = array() ) { $this->aReg = $aReg; }	
	
	public function Fetch() { 
		$this->aReg =  mysqli_fetch_array( $this->query, MYSQLI_ASSOC );		
		if ( $this->aReg && $this->lUtf8 )
			$this->aReg =  array_map('utf8_encode', $this->aReg );	
		return $this->aReg ; 
	}	
	
	public function Skip() {
		$this->Fetch();
		if ( $this->aReg ) 
			$this->lEof = false ;
		else {
			$this->lEof = true ;
			$this->aReg = array();
		}
		return $this->lEof;			
	}			
	
	public function Get( $cId = '', $utf8 = false ) {
		if ( $this->lEof == false ) {
			if ( array_key_exists( $cId, $this->aReg ) ){
				if ( $utf8 ) {
					$uVal = utf8_encode( $this->aReg[ $cId ] );
				} else {
					$uVal = $this->aReg[ $cId ];
				}	
			} else  {
				$uVal = 'No existe campo: ' . $cId;
			}						
			return $uVal;
		} else 
			return null;	
	}
	
	public function GetRaw( $cId = '' ) {
		if ( $this->lEof == false ) {
			if ( array_key_exists( $cId, $this->aReg ) ){
				$uVal = $this->aReg[ $cId ] ;
			} else  {
				$uVal = null;
			}						
			return $uVal;
			
		} else 
			return null;	
	}

	public function escape_string( $value ) {
		return mysqli_real_escape_string($this->link, $value);
	}

	public function GetAll() {
		$aData = array();
		while ( $this->Fetch() ){	
			$aData[] = $this->aReg;		
		}
		return $aData;			
	}

	public function Log( $uValue, $cTitle = '' ) {	
		_log( $uValue, $cTitle, 'log_db.txt' );
	}
	
	private function ShowError( $cError ) {
		$arrTrace 	= debug_backtrace();		
		$cTrace 	= '';
		$nTrace     = count( $arrTrace );

		for ( $i = 2; $i < $nTrace; $i++) {
		
			if (!isset ($arrTrace[ $i ]['file']))			
				$arrTrace[ $i ]['file'] = '[PHP Kernel]';
			
			if (!isset ($arrTrace[ $i ]['line']))			
				$arrTrace[ $i ]['line'] = '';	
			/*
				if ( empty( $arrTrace[ $i ][ 'class' ] ) ){
					$cClass = '';
				} else {
					$cClass = $arrTrace[ $i ][ 'class' ] . '->' .
					$arrTrace[ $i ][ 'function' ] . '()' ;
				}
			*/			
			$cTrace .=  '<br>Cridat desde: ' . $arrTrace[ $i ]['file'] . 
									' (' .  $arrTrace[ $i ]['line'] . ')' . 
									PHP_EOL ;		
		}
	
		echo '<br><b>Error:</b> ' . $cError . '';
		echo '<br><u>Trace</u>';
		echo $cTrace;
	}	

}

//--------------------------------

/* 	Pendent de implementar TRANSACCIONS -----------------------------------------------------------

	try {
		// First of all, let's begin a transaction
		$db->beginTransaction();

		// A set of queries; if one fails, an exception should be thrown
		$db->query('first query');
		$db->query('second query');
		$db->query('third query');

		// If we arrive here, it means that no exception was thrown
		// i.e. no query has failed, and we can commit the transaction
		$db->commit();
	} catch (Exception $e) {
		// An exception has been thrown
		// We must rollback the transaction
		$db->rollback();
	}

---------------------------------------------------------------------------------------------- */	
?>