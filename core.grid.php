<?php 
class TGrid extends TControl {

	private $aCols   					= [] ;
	public  $aKey  						= [] ;
	public  $aProperty				= [] ;
	public  $cTitle						= '' ;
	public  $bChange					= '' ;
	public  $bSelect					= '' ;	//	Lo mismo que DblClick
	public  $bDblClick				= '' ;
	public  $bPostEdit				= '' ;
	public  $bBeforeEdit			= '' ;
	public  $nRowHeight				= 0;
	public  $lEdit 						= true;
	public  $aData 						= null;
	public  $lForceFitColumns	= false;	
	public  $cStyleTotals     = 'background:#eee;text-align:left;';


	public function __construct( $oWnd , $cId = '', $nTop = 0, $nLeft = 0, 
															 $nWidth  = null, $nHeight = null, $aData = array() ) { 

		$nWidth  = TDefault( $nWidth , TWEB_GRID_WIDTH_DEFAULT );
		$nHeight = TDefault( $nHeight, TWEB_GRID_HEIGHT_DEFAULT  );		
	
		parent::__construct( $oWnd, $cId, $nTop, $nLeft, $nWidth, $nHeight  );
	
		$this->cClass		= 'tweb_grid';
		$this->cControl	= 'tgrid';
		$this->aData		= $aData;
		
		$this->SetBorderInset();		
		
		//	Define parámetros por defecto ----------------

		$this->aProperty = array( 'editable' 				=> true ,
									  					'pagination' 			=> false,
															'group' 					=> false,
															'headerRowHeight' => 30,
    													'createFooterRow' => true,
    													'showFooterRow'		=> true,
    													'footerRowHeight'	=> 21							  
														);				
	
	}

	public function SetEdit( $lOnOff = true ) { 
		$this->aProperty[ 'editable' ] = $lOnOff; 
	}

	public function SetHeaderHeight( $nHeigth = 30 ) {
		$this->aProperty[ 'headerRowHeight' ] = $nHeigth;
	}

	public function SetFooterHeight( $nHeigth = 21 ) {
		$this->aProperty[ 'createFooterRow' ] = true;
    $this->aProperty[ 'showFooterRow'		] = true;
    $this->aProperty[ 'footerRowHeight'	] = $nHeigth;
	}

	/*	uDummy es el 2 parametro para ser compatible con el SetData() de TControl 
	*   que tiene 2 parametros...
	*/
	public function SetData( $aData = array(), $uDummy = null ) { 
		$this->aData = $aData; 
	}
	
	public function AddCol( $cId, $cHeader, $nWidth = 100, $cType = '', $lEdit = true, 
												  $cValid = '', $cCss = '', $lHasTotal = false, $cHeaderCssClass = '' ){
	
		$oCol = new TGridCol( $cId, $cHeader, $nWidth, $cType, 
													$lEdit, $cValid, $cCss, $lHasTotal, $cHeaderCssClass );
		
		$this->aCols[] = $oCol ;

		return $oCol;
		
	}
	/* TOTI Pendent de mirar x q te conflicte amb la SetKey de TControl()

		public function SetKey( $nKey = 0, $bAction = '', $uDummy = null ) {
			$this->aKey[] = array( $nKey, $bAction );
		}
	*/	

	public function SetStyleTotals( $cStyle ) {
		$this->cStyleTotals = $cStyle;
	} 
	
	public function Activate() {

		$cHtml = '<script src="' . TWEB_PATH . 'core.grid.js"></script>';
		echo $cHtml ;

		$nNewTop 	= $this->nTop + 24;
		$nNewHeight	= $this->nHeight - 24;
		
		$this->CoorsUpdate();
		
		//	Title
		
		$cHtml 		= '';
		$nOffSet	= 0;
	
		if ( !empty( $this->cTitle ) ) {		
	
			$cHtml .=  '<div id="' . $this->cId . '_title" class="grid-header"  ';
			$cHtml .= 'style="position: absolute; ';		
			$cHtml .= $this->StyleDim();
			$cHtml .= '">';

			//  FILTER !!!
			$cHtml .= '<label>' . $this->cTitle . '</label>';					
			$cHtml .= '</div>';	
			
			echo $cHtml;
			
			$this->nTop 	= $this->Unit( $nNewTop );
			$this->nHeight 	= $this->Unit( $nNewHeight );		
		}

		//	-----------------------------------------
		
		$cHtml  = '<div id="' . $this->cId . '" ';
		
		$cHtml .= $this->DefClass();		
		$cHtml .= $this->Datas();		
		
		$cHtml .= 'style="position: absolute; ';
		$cHtml .= 'box-sizing: border-box; overflow: hidden;';
		
		$cHtml .= $this->StyleDim();			
		$cHtml .= $this->StyleBorder();
		$cHtml .= $this->StyleColor();
		$cHtml .= $this->StyleCss();		
		$cHtml .= $this->StyleShadow();			
		
		$cHtml .= '"> ';	

		
		$cHtml .= '</div>';				

		echo $cHtml ;

		// STYLES ---------------------------------
		$cStyle  = '<style>' 														. PHP_EOL;

		$cStyle .= '	.slick-row.totals .slick-cell {'  . PHP_EOL;
		$cStyle .=  $this->cStyleTotals                 . PHP_EOL;
		$cStyle .= '}'                                  . PHP_EOL;

		$cStyle .= '</style>'                           . PHP_EOL;

		echo $cStyle;

		//---------------------------------------
		
		$this->DefKeys();		
		
		//	Define Columnas ------------------------------------------
		
			$lHasTotals		= false;
		
			$nCols 			= count( $this->aCols );

			$cId_Columns 	= 'columns_' . $this->cId ;
			$cId_Options 	= 'options_' . $this->cId ;
			
			$cJS  = '' ;
			$cJS .= 'var ' . $cId_Columns . ' = [';
			
			// for ( $i = 0; $i < $nCols - 1 ; $i++) {
			for ( $i = 0; $i < $nCols; $i++) {	
			
				$oCol 		= $this->aCols[$i];	
				
				$cJS .= $this->DefineCol( $oCol );															
				$cJS .= ', ';	

				if ( $oCol->lHasTotal )
					$lHasTotals = true;
				
			}
			
			/*
			if ( $nCols > 0 ) {
				$oCol 		= $this->aCols[$nCols-1];					
				$cJS .= $this->DefineCol( $oCol );
				
				if ( $oCol->lHasTotal )
					$lHasTotals = true;				
			}
			*/
			
			$cJS .= '];';			
		
		//	Config Grid -----------------------------------------------
		
			$lEditable  = $this->aProperty[ 'editable'   ] ? 'true' : 'false';
			$lPag 			= $this->aProperty[ 'pagination' ] ? 'true' : 'false' ;
			$lGroup			= $this->aProperty[ 'group' 	 ] ? 'true' : 'false';		
			
			$lHasTotals	= (( $lHasTotals == true ) ? 'true' : 'false' );			

			$cJS .= '	' ;
			$cJS .= '	  var ' . $cId_Options . ' = {' . PHP_EOL;		
		
			if ( $this->nRowHeight > 0 ) { 
				$cJS .= '	rowHeight: ' . $this->nRowHeight . ',' . PHP_EOL;  
			}

			$cJS .= ' editable: ' . $lEditable . ', ' . PHP_EOL ;   	// true/false
			$cJS .= '	enableAddRow: false,' . PHP_EOL ;
			$cJS .= '	enableCellNavigation: true,' . PHP_EOL ;
			$cJS .= '	asyncEditorLoading: false,' . PHP_EOL ;
			//		$cJS .= '	multiColumnSort: true,' . PHP_EOL ;
			$cJS .= '	autoEdit: false, ' . PHP_EOL ;
			$cJS .= ' forceFitColumns: ' . ( ($this->lForceFitColumns) ? 'true,' : 'false' ) . ',' . PHP_EOL;
			//		$cJS .= '	autoHeight: true ,' . PHP_EOL ;
			//		$cJS .= '	editCommandHandler: queueAndExecuteCommand,' . PHP_EOL ;

			// Albeiro Valencia
			$cJS .= ' headerRowHeight : ' . strval($this->aProperty['headerRowHeight']) . ',' . PHP_EOL;
    	$cJS .= ' createFooterRow : ' . ($this->aProperty['createFooterRow'] ? 'true' : 'false') . ',' . PHP_EOL;
    	$cJS .= ' showFooterRow   : ' . ($this->aProperty['showFooterRow'] ? 'true' : 'false') . ',' . PHP_EOL;
    	$cJS .= ' footerRowHeight : ' . strval($this->aProperty['footerRowHeight']) . PHP_EOL;
			
			$cJS .= '	};' . PHP_EOL;					
			
			//	Inicialiazamos Grid en Javascript
		
			$cJS .= "  var oGrid = new TGrid( '"; 
			$cJS .= $this->cId . "' , "; 
			$cJS .= $cId_Columns . ", "; 
			$cJS .= $cId_Options . ", " ;
			$cJS .= $lPag . ", "; 
			$cJS .= $lGroup . ", ";  
			$cJS .= $lHasTotals . " );" ;
			
			$cJS .= "  var o	= new TControl();";							

			if ( !empty( $this->bChange ) ) 	{ $cJS .= " oGrid.bChange = '" . $this->bChange . "'; ";  }
			if ( !empty( $this->bSelect ) ) 	{ $cJS .= " oGrid.bSelect = '" . $this->bSelect . "'; ";  }
			if ( !empty( $this->bDblClick ) ) 	{ $cJS .= " oGrid.bDblClick = '" . $this->bDblClick . "'; ";  }
			if ( !empty( $this->bPostEdit ) ) 	{ $cJS .= " oGrid.bPostEdit = '" . $this->bPostEdit . "'; ";  }
			if ( !empty( $this->bBeforeEdit ) )	{ $cJS .= " oGrid.bBeforeEdit = '" . $this->bBeforeEdit . "'; ";  }
	
			if ( gettype( $this->aData ) == 'array' ) 
				$cJS .= " oGrid.SetData( " . json_encode(  $this->aData ) . ");"; 
			
			//	TOTI !!! Pendent de pasar l'array de Keys... (aKey )			
			
			$cJS .= " o.ControlInit( '" . $this->cId . "' , oGrid, 'grid' ) ;";			
			
			//	Control Resize...
			
			$cJS .= "$('#" . $this->cId . "').resize(); ";
			
			ExeJSReady( $cJS );						
	}

	private function DefineCol( $oCol ) {	
	
		if ( gettype( $oCol ) == 'NULL' )	
			return null;
	
		$cType = substr( $oCol->cType, 0, 1 );	
	
		$nDec  = 0;

		$cReg  = '{';
		$cReg .= 'id: "' 	. $oCol->cId 	. '", ' ;
		$cReg .= 'name: "' 	. $oCol->cName 	. '", ' ;
		$cReg .= 'width: ' 	. $oCol->nWidth . ',  '  ;	
		$cReg .= 'sort: "' 	. $oCol->cSort 	. '", ' ;		
	
		
		if ( $oCol->lEdit ) {					

			switch ( $cType ) {
			
				case 'C':
					$cReg .= 'editor: Slick.Editors.Text, '  ;											
					break;
					
				case 'N':

					$cReg .= 'editor: Slick.Editors.Integer, '  ;																					
					break;						
					
				case 'M':
					$cReg .= 'editor: Slick.Editors.LongText, '  ;	
					break;		

				case 'L':
					$cReg .= 'editor: Slick.Editors.Checkbox, '  ;	
	//				$cReg .= 'editor: Slick.Editors.YesNoSelect, '  ;	
					break;						
			
				case 'D':
					$cReg .= 'editor: Slick.Editors.Date, '  ;	
					break;
						
				case '%':
	//				$cReg .= 'editor: Slick.Editors.PercentComplete, '  ;	
					$cReg .= 'editor: Slick.Editors.Integer, '  ;	
					break;		
					
				case 'F':
					$cReg .= 'editor: Slick.Editors.Float, '  ; 
					break;	
					
				case '$':	
					$cReg .= 'editor: Slick.Editors.Float, '  ; 
					break;	
					
				case 'S':		
					
//					$cReg .= 'editorOptions:{options:"val1,val2,val3", values:"valor 1,valor 2,valor 3" }, ';				
					
					if ( gettype( $oCol->cKey ) == 'string' ){
					
						$cReg .= 'editor: Slick.Editors.Select, '  ; 
						
						$cReg .= 'editorOptions:{options:"' . $oCol->cKey . '",  ';
						
						if ( gettype( $oCol->cValues ) == 'string' ){
							$cReg .= 'values:"' . $oCol->cValues . '" ';
						}						
						
						$cReg .= ' }, ';						
					
					}										
					
					break;						
			}		
		}		

		if ( !empty( $oCol->cFormatter )){
		
			$cReg .= 'formatter: ' . $oCol->cFormatter . ', ';
			
		} else {
		
			switch ( $cType ) {			

				case 'L':
					$cReg .= 'formatter: Slick.Formatters.Checkmark, ';
					break;						
					
				case '%':
					$cReg .= 'formatter: Slick.Formatters.PercentCompleteBar, ';
					break;	
					
				case 'F':
					$cReg .= 'formatter: Slick.Formatters.Float, ';
					$nDec  = substr( $oCol->cType, 1, 1 );	  
					if ( empty( $nDec ) ) {
						$nDec = '2';
					}
					$cReg .= 'editorFixedDecimalPlaces: ' . $nDec . ', ';
					break;
					
				case '$':
					$cReg .= 'formatter: Slick.Formatters.Price, ';
					$nDec  = substr( $oCol->cType, 1, 1 );	  
					if ( empty( $nDec ) ) {
						$nDec = '2';
					}
					$cReg .= 'editorFixedDecimalPlaces: ' . $nDec . ', ';
					break;		

				case 'S':
					$cReg .= 'formatter: Slick.Formatters.Select, ';					
					break;
					
				default:
					break;
				
			}								
		}

		if ( !empty( $oCol->cValid ) )
			$cReg .= 'validator: ' . $oCol->cValid . ', '  ;	
			
		if ( !empty( $oCol->cCssClass ) )
			$cReg .= 'cssClass: "' . $oCol->cCssClass . '", '  ;	
			
		if ( !empty( $oCol->cHeaderCssClass ) )
			$cReg .= 'headerCssClass: "' . $oCol->cHeaderCssClass . '", '  ;				
		
		if ( ( $oCol->lSort ) || ( $oCol->cSort !== '' ) ) 		
			$cReg .= 'sortable: true, ' ;
			
		if ( $oCol->lFocusable == false ) 		
			$cReg .= 'focusable: false, ' ;	
			
		if ( $oCol->lHasTotal == true ) 		
			$cReg .= 'hasTotal: true, ' ;				

		if ( !empty( $oCol->cGroup ) ) {
			$this->lGroup = true;
			$cReg .= 'groupTotalsFormatter: ' . $oCol->cGroup . ', '  ;	
		}
		
		$cReg .= 'field: "' . $oCol->cId 	. '" ' ;
		
		$cReg .= '} ';	
		
		return $cReg;		
	}	
	
}

Class TGridCol {

	public $cId 				= '';
	public $cName 			= '';
	public $cField 			= '';
	public $nWidth			= 100;
	public $cCssClass 	= '';
	public $cEditor	 		= '';
	public $cValidator 	= '';
	public $cFormatter 	= '';
	public $bPostEdit 	= '';	
	public $cGroup 			= '';	
	public $nMinWidth 	= 10;
	public $nMaxWidth 	= 1000;	
	public $lResizable 	= true;
	public $lHasTotal 	= false;
	public $lEdit      	= false;
	public $cKey    		= null;
	public $cValues    	= null;
	
	public $lSort      		= false;
	public $cSort      		= '';
	public $lFocusable 		= true;
	public $cHeaderCssClass = '';
	
	public $cType     	= '';
	public $cValid     	= '';
	
	public function __construct( $cId, $cHeader, $nWidth, $cType, 
															 $lEdit, $cValid, $cCss, $lHasTotal, $cHeadCssClass ){

		$this->cId				= $cId ;
		$this->cName  		= $cHeader ;
		$this->nWidth			= $nWidth ;
		$this->cField 		= $cId ;
		$this->cType   		= $cType ;
		$this->lEdit  		= ( (gettype($lEdit) == 'boolean') ? $lEdit : true ) ;
		$this->cValid  		= $cValid ;	
		$this->cCssClass 	= $cCss ;	
		$this->lHasTotal 	= ( (gettype($lHasTotal) == 'boolean') ? $lHasTotal : false ) ;	
		$this->cHeaderCssClass = $cHeadCssClass;
	
	}
	
	public function SetSelect( $aKey, $aValues ) {
	
		$this->aKey 	= $aKey;
		$this->aValues	= $aValues;
		
		if ( gettype( $aKey ) == 'array' ) {
		
			$this->cKey = implode( ",", $aKey );
			
			if ( gettype( $aValues ) == 'array' ) {
			
				$this->cValues = implode( ",", $aValues );
			
			}
		}	
	
	}			
}

?>
