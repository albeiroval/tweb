/*
*  Author and copyright: Albeiro Valencia (http://avcsistemas.com/foro)
*  CORE para manejo de la clase TModal.
*  Fecha : 11/09/2020 Derechos Reservados
*  https://github.com/shaack/bootstrap-show-modal
*  https://www.jqueryscript.net/demo/Dynamic-Bootstrap-4-Modals/
*  https://www.codehim.com/download/dynamic-bootstrap-modals.zip
*/

var _curElement = [];

class Modal {

  constructor(props) {

    this.props = {
      title: "",
      body: "",
      footer: "",
      modalClass: "fade",
      modalDialogClass: "",
      options: null,

      // Events:
      onCreate: null,
      onShown: null,
      onDispose: null,
      onSubmit: null // Callback of $.showConfirm(), called after yes or no was pressed
    };
    for (var prop in props) {
      // noinspection JSUnfilteredForInLoop
      this.props[prop] = props[prop];
    }
    this.id = this.props.idModal;
    this.show();
    if (this.props.onCreate) {
      this.props.onCreate(this);
    }
  }

  createContainerElement() {
    var self = this;
    this.element = document.createElement("div");
    this.element.id = this.id;
    this.element.setAttribute("class", "modal " + this.props.modalClass);
    this.element.setAttribute("style", "overflow-y: auto");
    this.element.setAttribute("tabindex", "-1");
    this.element.setAttribute("data-backdrop", "static");
    this.element.setAttribute("role", "dialog");
    this.element.setAttribute("aria-labelledby", this.id);
    this.element.innerHTML = '<div class="modal-dialog ' + this.props.modalDialogClass + '" role="document">' +
      '<div class="modal-content">' +
      '<div class="modal-header" id="modal-header">' +
      '<h5 class="modal-title"></h5>' +
      '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
      '<i class="far fa-times-circle"></i>' +
      '</button>' +
      '</div>' +
      '<div class="modal-body"></div>' +
      '<div class="modal-footer" id="modal-footer" style="background-color: white;"></div>' +
      '</div>' +
      '</div>';
    document.body.appendChild(this.element);
    this.titleElement = this.element.querySelector(".modal-title");
    this.bodyElement = this.element.querySelector(".modal-body");
    this.footerElement = this.element.querySelector(".modal-footer");
    
    $(this.element).on('hide.bs.modal', function () {
      self.dispose();
      self.getCurElement();
    });  
    
    $(this.element).on('shown.bs.modal', function () {
      if (self.props.onShown) {
        self.props.onShown(self);
      }
    });

    _curElement.push( this.element );

  }

  show() {
    if (!this.element) {
      this.createContainerElement();
      if (this.props.options) {
        $(this.element).modal(this.props.options);
      } else {
        $(this.element).modal();
      }
    } else {
      $(this.element).modal('show');
    }
    if (this.props.title) {
      $(this.titleElement).show();
      this.titleElement.innerHTML = this.props.title;
    } else {
      $(this.titleElement).hide();
    }
    if (this.props.body) {
      $(this.bodyElement).show();
      this.bodyElement.innerHTML = this.props.body;
    } else {
      $(this.bodyElement).hide();
    }
    if (this.props.footer) {
      $(this.footerElement).show();
      this.footerElement.innerHTML = this.props.footer;
    } else {
      $(this.footerElement).hide();
    }
  }

  hide() {
    if ( _curElement.length == 1 ) {
      this.element = _curElement[0];
    }
    $(this.element).modal('hide');
  }
  
  dispose() {
    $(this.element).modal('dispose');
    document.body.removeChild(this.element);
    if (this.props.onDispose) {
      this.props.onDispose(this);
    }
  }

  getCurElement() {
    _curElement.pop();
    let last = _curElement.length;
    if ( last > 0 ) {
      this.element = _curElement[last-1];
    }
  }
  
}

// FINAL
